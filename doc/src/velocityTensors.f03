!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module velocityTensors

    use globalMeshVar
    use fieldvar

    implicit none

contains

    subroutine strainTensor(S,Ssum)

    !==================================================================================================================
    ! Description:
    !! strainTensor computes the stranin tensor and its inner product.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements,6), Ssum(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            ! Diagonal
            S(iElement,1) = velocity%phiGrad(iElement,1,1) ! dudx
            S(iElement,2) = velocity%phiGrad(iElement,2,2) ! dvdy
            S(iElement,3) = velocity%phiGrad(iElement,3,3) ! dwdz

            ! Off-diagonal
            S(iElement,4) = 0.5*(velocity%phiGrad(iElement,1,2) + velocity%phiGrad(iElement,2,1)) ! 0.5*(dvdx+dudy)
            S(iElement,5) = 0.5*(velocity%phiGrad(iElement,1,3) + velocity%phiGrad(iElement,3,1)) ! 0.5*(dwdx+dudz)
            S(iElement,6) = 0.5*(velocity%phiGrad(iElement,2,3) + velocity%phiGrad(iElement,3,2)) ! 0.5*(dwdy+dvdz)

            ! SijSij
            Ssum(iElement) = (S(iElement,1)**2+S(iElement,2)**2+S(iElement,3)**2) &
                    + 2*(S(iElement,4)**2+S(iElement,5)**2+S(iElement,6)**2)

        enddo

    end subroutine strainTensor

! *********************************************************************************************************************

    function strainTensorInnerProduct() result(Ssum)

    !==================================================================================================================
    ! Description:
    !! strainTensor computes the stranin tensor and its inner product.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: S(6), Ssum(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            ! Diagonal
            S(1) = velocity%phiGrad(iElement,1,1) ! dudx
            S(2) = velocity%phiGrad(iElement,2,2) ! dvdy
            S(3) = velocity%phiGrad(iElement,3,3) ! dwdz

            ! Off-diagonal
            S(4) = 0.5*(velocity%phiGrad(iElement,1,2) + velocity%phiGrad(iElement,2,1)) ! 0.5*(dvdx+dudy)
            S(5) = 0.5*(velocity%phiGrad(iElement,1,3) + velocity%phiGrad(iElement,3,1)) ! 0.5*(dwdx+dudz)
            S(6) = 0.5*(velocity%phiGrad(iElement,2,3) + velocity%phiGrad(iElement,3,2)) ! 0.5*(dwdy+dvdz)

            ! Inner product
            Ssum(iElement) = (S(1)**2+S(2)**2+S(3)**2) + 2*(S(4)**2+S(5)**2+S(6)**2)

        enddo

    end function strainTensorInnerProduct

! *********************************************************************************************************************

    function strainTensorModule() result(modS)

    !==================================================================================================================
    ! Description:
    !! strainTensorModule computes the stranin tensor module.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real ::  modS(numberOfElements)
        !! Strain rate module

        real :: S(6), Ssum
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            ! Diagonal
            S(1) = velocity%phiGrad(iElement,1,1) ! dudx
            S(2) = velocity%phiGrad(iElement,2,2) ! dvdy
            S(3) = velocity%phiGrad(iElement,3,3) ! dwdz

            ! Off-diagonal
            S(4) = 0.5*(velocity%phiGrad(iElement,1,2) + velocity%phiGrad(iElement,2,1)) ! 0.5*(dvdx+dudy)
            S(5) = 0.5*(velocity%phiGrad(iElement,1,3) + velocity%phiGrad(iElement,3,1)) ! 0.5*(dwdx+dudz)
            S(6) = 0.5*(velocity%phiGrad(iElement,2,3) + velocity%phiGrad(iElement,3,2)) ! 0.5*(dwdy+dvdz)

            ! SijSij (inner product)
            Ssum = (S(1)**2+S(2)**2+S(3)**2) + 2*(S(4)**2+S(5)**2+S(6)**2)

            ! Module: modS=sqrt(2*Ssum)
            modS(iElement) = sqrt(2*Ssum)

        enddo

    end function strainTensorModule

!**********************************************************************************************************************

    function fullStrainTensor() result(S)

    !==================================================================================================================
    ! Description:
    !! fullStrainTensor computes the stranin tensor as a full (numberOfElements x 3 x 3) matrix.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements,3,3)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            ! Diagonal
            S(iElement,1,1) = velocity%phiGrad(iElement,1,1)
            S(iElement,2,2) = velocity%phiGrad(iElement,2,2)
            S(iElement,3,3) = velocity%phiGrad(iElement,3,3)

            ! Off-diagonal
            S(iElement,1,2) = 0.5*(velocity%phiGrad(iElement,1,2) + velocity%phiGrad(iElement,2,1))
            S(iElement,1,3) = 0.5*(velocity%phiGrad(iElement,1,3) + velocity%phiGrad(iElement,3,1))
            S(iElement,2,3) = 0.5*(velocity%phiGrad(iElement,2,3) + velocity%phiGrad(iElement,3,2))

            S(iElement,2,1) = S(iElement,1,2)
            S(iElement,3,1) = S(iElement,1,3)
            S(iElement,3,2) = S(iElement,2,3)

        enddo

    end function fullStrainTensor

! *********************************************************************************************************************

    subroutine strainTensor_d(Sd,SdSum)

    !==================================================================================================================
    ! Description:
    !! strainTensor_d computes the modified stranin tensor and its inner product needed by the WALE model.
    !==================================================================================================================

        integer:: i, j, iElement
    !------------------------------------------------------------------------------------------------------------------

        real ::  SdSum(numberOfElements)

        real :: Sd(numberOfElements,6), g(3,3), g2(3,3)

        real :: c, trace
    !------------------------------------------------------------------------------------------------------------------

        c = 1.0/3.0

        do iElement=1,numberOfElements

            ! Compute g2
            g(1:3,1:3) = velocity%phiGrad(iElement,1:3,1:3) ! (dudx dudy dudz; dvdx dvdy dvdz; dwdx dwdy dwdz)

            do j=1,3
                do i=1,3
                    g2(i,j) = (g(i,1)*g(1,j) + g(i,2)*g(2,j) + g(i,3)*g(3,j))
                enddo
            enddo

            ! Compute Sd tensor
            trace = g2(1,1) + g2(2,2) + g2(3,3)

            Sd(iElement,1) = g2(1,1)-c*trace
            Sd(iElement,2) = g2(2,2)-c*trace
            Sd(iElement,3) = g2(3,3)-c*trace
            Sd(iElement,4) = 0.5*(g2(1,2) + g2(2,1))
            Sd(iElement,5) = 0.5*(g2(1,3) + g2(3,1))
            Sd(iElement,6) = 0.5*(g2(2,3) + g2(3,2))

            ! Sd_ijSd_ij
            SdSum(iElement) = (Sd(iElement,1)**2 + Sd(iElement,2)**2 + Sd(iElement,3)**2) &
                    + 2*(Sd(iElement,4)**2+Sd(iElement,5)**2+Sd(iElement,6)**2)

        enddo

    end subroutine strainTensor_d

! *********************************************************************************************************************

    function strainTensorInnerProduct_d() result(SdSum)

    !==================================================================================================================
    ! Description:
    !! strainTensorInnerProduct_d computes the inner product of the modified strain tensor.
    !==================================================================================================================

        integer:: i, j, iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: Sd(6), g(3,3), g2(3,3)

        real :: SdSum(numberOfElements), c, trace
    !------------------------------------------------------------------------------------------------------------------

        c = 1.0/3.0

        do iElement=1,numberOfElements

            ! Compute g2
            g(1:3,1:3) = velocity%phiGrad(iElement,1:3,1:3) ! (dudx dudy dudz; dvdx dvdy dvdz; dwdx dwdy dwdz)

            do j=1,3
                do i=1,3
                    g2(i,j) = (g(i,1)*g(1,j)+g(i,2)*g(2,j)+g(i,3)*g(3,j))
                enddo
            enddo

            ! Compute Sd tensor
            trace = g2(1,1)+g2(2,2)+g2(3,3)

            Sd(1) = g2(1,1)-c*trace
            Sd(2) = g2(2,2)-c*trace
            Sd(3) = g2(3,3)-c*trace
            Sd(4) = 0.5*(g2(1,2)+g2(2,1))
            Sd(5) = 0.5*(g2(1,3)+g2(3,1))
            Sd(6) = 0.5*(g2(2,3)+g2(3,2))

            ! Sd_ijSd_ij
            SdSum(iElement) = (Sd(1)**2 + Sd(2)**2 + Sd(3)**2) + 2*(Sd(4)**2+Sd(5)**2+Sd(6)**2)

        enddo

    end function strainTensorInnerProduct_d

! *********************************************************************************************************************

    function strainTensorModule_d() result(modSd)

    !==================================================================================================================
    ! Description:
    !! strainTensorModule_d computes the modified stranin tensor module.
    !==================================================================================================================

        integer:: i,j, iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: modSd(numberOfElements)

        real :: Sd(6), g(3,3), g2(3,3)

        real :: SdSum, c, trace
    !------------------------------------------------------------------------------------------------------------------

        c=1.0/3.0

        do iElement=1,numberOfElements

            ! Compute g2
            g(1:3,1:3) = velocity%phiGrad(iElement,1:3,1:3) ! (dudx dudy dudz; dvdx dvdy dvdz; dwdx dwdy dwdz)

            do j=1,3
                do i=1,3
                    g2(i,j) = (g(i,1)*g(1,j)+g(i,2)*g(2,j)+g(i,3)*g(3,j))
                enddo
            enddo

            ! Compute Sd tensor
            trace = g2(1,1)+g2(2,2)+g2(3,3)

            Sd(1) = g2(1,1)-c*trace
            Sd(2) = g2(2,2)-c*trace
            Sd(3) = g2(3,3)-c*trace
            Sd(4) = 0.5*(g2(1,2)+g2(2,1))
            Sd(5) = 0.5*(g2(1,3)+g2(3,1))
            Sd(6) = 0.5*(g2(2,3)+g2(3,2))

            ! Sd_ijSd_ij
            SdSum = (Sd(1)**2 + Sd(2)**2 + Sd(3)**2) + 2*(Sd(4)**2+Sd(5)**2+Sd(6)**2)

            ! module
            modSd(iElement) = sqrt(2*SdSum)

        enddo

    end function strainTensorModule_d

! *********************************************************************************************************************

    subroutine rotationTensor(R, Rsum)

    !==================================================================================================================
    ! Description:
    !! rotationTensor computes the rotation tensor and its inner product.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: dudx, dudy, dudz, dvdx, dvdy, dvdz, dwdx, dwdy, dwdz

        real :: R(numberOfElements,3), Rsum(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            dudx = velocity%phiGrad(iElement,1,1)
            dudy = velocity%phiGrad(iElement,2,1)
            dudz = velocity%phiGrad(iElement,3,1)
            dvdx = velocity%phiGrad(iElement,1,2)
            dvdy = velocity%phiGrad(iElement,2,2)
            dvdz = velocity%phiGrad(iElement,3,2)
            dwdx = velocity%phiGrad(iElement,1,3)
            dwdy = velocity%phiGrad(iElement,2,3)
            dwdz = velocity%phiGrad(iElement,3,3)

            R(iElement,1) = 0.5*(dudy-dvdx)
            R(iElement,2) = 0.5*(dudz-dwdx)
            R(iElement,3) = 0.5*(dvdz-dwdy)

            ! R is anti-symmetryc (trace=0 and lower triangle is opposite to upper triangular)
            Rsum(iElement) = 2.0*(R(iElement,1)**2+R(iElement,2)**2+R(iElement,3)**2)

        enddo

    end subroutine rotationTensor

! *********************************************************************************************************************

    function rotationTensorModule() result(modR)

    !==================================================================================================================
    ! Description:
    !! rotationTensorModule computes the rotation tensor module.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: dudx, dudy, dudz, dvdx, dvdy, dvdz, dwdx, dwdy, dwdz

        real :: R(3), Rsum, modR(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            dudx = velocity%phiGrad(iElement,1,1)
            dudy = velocity%phiGrad(iElement,2,1)
            dudz = velocity%phiGrad(iElement,3,1)
            dvdx = velocity%phiGrad(iElement,1,2)
            dvdy = velocity%phiGrad(iElement,2,2)
            dvdz = velocity%phiGrad(iElement,3,2)
            dwdx = velocity%phiGrad(iElement,1,3)
            dwdy = velocity%phiGrad(iElement,2,3)
            dwdz = velocity%phiGrad(iElement,3,3)

            R(1) = 0.5*(dudy-dvdx)
            R(2) = 0.5*(dudz-dwdx)
            R(3) = 0.5*(dvdz-dwdy)

            ! R is anti-symmetryc (trace=0 and lower triangle is opposite to upper triangular)
            Rsum = 2.0*(R(1)**2+R(2)**2+R(3)**2)

            ! module
            modR(iElement) = sqrt(2.0*Rsum)

        enddo

    end function rotationTensorModule

! *********************************************************************************************************************

    function SikRkj() result(SR)

    !==================================================================================================================
    ! Description:
    !! SikRkj computes the inner product between the strain and the rotation tensors.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------k
        real :: dudx, dudy, dudz, dvdx, dvdy, dvdz, dwdx, dwdy, dwdz

        real :: S(3,3), R(3,3), EW(3,3), SR(numberOfElements,3,3)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            dudx = velocity%phiGrad(iElement,1,1)
            dudy = velocity%phiGrad(iElement,2,1)
            dudz = velocity%phiGrad(iElement,3,1)
            dvdx = velocity%phiGrad(iElement,1,2)
            dvdy = velocity%phiGrad(iElement,2,2)
            dvdz = velocity%phiGrad(iElement,3,2)
            dwdx = velocity%phiGrad(iElement,1,3)
            dwdy = velocity%phiGrad(iElement,2,3)
            dwdz = velocity%phiGrad(iElement,3,3)

            ! Strain
            S(1,1) = dudx
            S(1,2) = 0.5*(dudy + dvdx)
            S(1,3) = 0.5*(dudz + dwdx)

            S(2,1) = S(1,2)
            S(2,2) = dvdy
            S(2,3) = 0.5*(dvdz + dwdy)

            S(3,1) = S(1,3)
            S(3,2) = S(2,3)
            S(3,3) = dwdz

            ! Rotation
            R(1,1) = 0.0
            R(1,2) = 0.5*(dudy - dvdx)
            R(1,3) = 0.5*(dudz - dwdx)

            R(2,1) = -R(1,2)
            R(2,2) = 0.0
            R(2,3) = 0.5*(dvdz - dwdy)

            R(3,1) = -R(1,3)
            R(3,2) = -R(3,2)
            R(3,3) = 0.0

            ! Multiply S and R
            EW = matmul(S,R)

            ! SijRij
            SR(iElement, 1:3, 1:3) = EW

        enddo

    end function SikRkj

! *********************************************************************************************************************

    function RikSkj() result(RS)

    !==================================================================================================================
    ! Description:
    !! RikSkj computes the inner product between the rotation and the strain tensors.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: dudx, dudy, dudz, dvdx, dvdy, dvdz, dwdx, dwdy, dwdz

        real :: S(3,3), R(3,3), WE(3,3), RS(numberOfElements,3,3)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            dudx = velocity%phiGrad(iElement,1,1)
            dudy = velocity%phiGrad(iElement,2,1)
            dudz = velocity%phiGrad(iElement,3,1)
            dvdx = velocity%phiGrad(iElement,1,2)
            dvdy = velocity%phiGrad(iElement,2,2)
            dvdz = velocity%phiGrad(iElement,3,2)
            dwdx = velocity%phiGrad(iElement,1,3)
            dwdy = velocity%phiGrad(iElement,2,3)
            dwdz = velocity%phiGrad(iElement,3,3)

            ! Strain
            S(1,1) = dudx
            S(1,2) = 0.5*(dudy + dvdx)
            S(1,3) = 0.5*(dudz + dwdx)

            S(2,1) = S(1,2)
            S(2,2) = dvdy
            S(2,3) = 0.5*(dvdz + dwdy)

            S(3,1) = S(1,3)
            S(3,2) = S(2,3)
            S(3,3) = dwdz

            ! Rotation
            R(1,1) = 0.0
            R(1,2) = 0.5*(dudy - dvdx)
            R(1,3) = 0.5*(dudz - dwdx)

            R(2,1) = -R(1,2)
            R(2,2) = 0.0
            R(2,3) = 0.5*(dvdz - dwdy)

            R(3,1) = -R(1,3)
            R(3,2) = -R(3,2)
            R(3,3) = 0.0

            ! Multiply S and R
            WE = matmul(R,S)

            ! SijRij
            RS(iElement, 1:3, 1:3) = WE

        enddo

    end function RikSkj

! *********************************************************************************************************************

    function velocityGradientModule() result(modGradU)

    !==================================================================================================================
    ! Description:
    !! velocityGradientModule computes the module of the velocity gradient.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: dudx, dudy, dudz, dvdx, dvdy, dvdz, dwdx, dwdy, dwdz, gradUSum

        real :: modGradU(numberOfElements)
   !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            dudx = velocity%phiGrad(iElement,1,1)
            dudy = velocity%phiGrad(iElement,2,1)
            dudz = velocity%phiGrad(iElement,3,1)
            dvdx = velocity%phiGrad(iElement,1,2)
            dvdy = velocity%phiGrad(iElement,2,2)
            dvdz = velocity%phiGrad(iElement,3,2)
            dwdx = velocity%phiGrad(iElement,1,3)
            dwdy = velocity%phiGrad(iElement,2,3)
            dwdz = velocity%phiGrad(iElement,3,3)

            gradUSum = (dudx**2 + dvdy**2 + dwdz**2 + dudy**2 + dudz**2 + dvdy**2 + dvdz**2 + dwdx**2 + dwdy**2)

            ! Module
            modgradU(iElement) = sqrt(2.0*gradUSum)

        end do

    end function velocityGradientModule

! *********************************************************************************************************************

    function anisotropyTensor(beta2) result(aij)

    !==================================================================================================================
    ! Description:
    !! anisotropyTensor computes the anisotropy tensor. This tensor must be multiplied by -2.0*nut/k when used in
    !! turbulnece models.
    !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: beta2

        real :: modGradU(numberOfElements)

        real ::  aij(numberOfElements,3,3), SR(numberOfElements,3,3), RS(numberOfElements,3,3), S(numberOfElements,3,3)
    !------------------------------------------------------------------------------------------------------------------

        S = fullStrainTensor()
        modGradU = velocityGradientModule()
        SR = SikRkj()
        RS = RikSkj()

        do iElement=1,numberOfElements
            aij(iElement, 1:3, 1:3) = S(iElement, 1:3, 1:3) + &
                                        beta2*(SR(iElement,1:3, 1:3) - RS(iElement,1:3, 1:3))/modGradU(iElement)
        enddo

    end function anisotropyTensor

!**********************************************************************************************************************

    subroutine computeSbarEdwards(Sbar)

    !===================================================================================================================
    ! Description:
    !! computeSbarEdwards computes a working term for the SA-edwards equation source term.
    !===================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements,6), Ssum(numberOfElements),  Sbar(numberOfElements)

        real :: trace, S12, S13, S23
    !------------------------------------------------------------------------------------------------------------------

        call strainTensor(S,Ssum)

        do iElement=1,numberOfElements

            trace = S(iElement,1)**2 + S(iElement,2)**2 + S(iElement,3)**2
            S12 = 4*S(iElement,4)**2
            S13 = 4*S(iElement,5)**2
            S23 = 4*S(iElement,6)**2

            Sbar(iElement) = (2*trace + S12 + S13 + S23)

        enddo

    end subroutine computeSbarEdwards

! *********************************************************************************************************************

    function AikBkj(A,B) result(AB)

    !==================================================================================================================
    ! Description:
    !! AikBkj computes the product between tow tensors.
     !==================================================================================================================

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: A(numberOfElements,3,3), B(numberOfElements,3,3), AB(numberOfElements,3,3)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements
            AB(iElement, 1:3, 1:3) = matmul(A(iElement, 1:3, 1:3), B(iElement, 1:3, 1:3))
        enddo

    end function AikBkj

! *********************************************************************************************************************

end module velocityTensors