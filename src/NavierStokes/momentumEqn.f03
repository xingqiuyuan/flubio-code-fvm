!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine momentumEquation()

    !==================================================================================================================
    ! Description:
    !! momentumEq assembles and solves the momentum equation.
    !==================================================================================================================

        use momentum
        use globalMeshVar
        use gradient
        use fieldvar
        use physicalConstants

        implicit none
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------------!
        ! Allocate space for coefficiets !
        !--------------------------------!

        call Ueqn%createEqCoeffs()
        call Ueqn%resetCoeffs()

        !--------------------------!
        ! Update "Old" terms       !
        !--------------------------!

        call velocity%updateOldFieldValues()
        call pressure%updateOldFieldValues()

        !----------------------------!
        ! Assemble momentum equation !
        !----------------------------!

        ! Transient
        if(steadySim==0) call Ueqn%addTransientTerm(velocity, rho)

        ! Diffusion
        call Ueqn%addDiffusionTerm(velocity, nu, boundaryNonOrthCorr=.true.)

        ! save coeffs and rhs to be use in prime
        if(steadySim==0) call Ueqn%updateDiffusionCoeffs()

        ! Convection
        if(trim(Ueqn%convOptName) /= 'none') then
            if(Ueqn%convImp==0) then
                call Ueqn%addConvectiveTerm(velocity)
            else
                call Ueqn%addConvectiveTermImplicit(velocity)
            end if
        end if

        ! Body forces (gradP + custom volume forces)
        call Ueqn%addBodyForce(pressure%phiGrad(:,:,1), Ueqn%nComp)

        ! Additional source term
        if(flubioSources%customSourcesFound) call Ueqn%addCustomSourceTerm(velocity)
        if(flubioSources%sourcesFound) call Ueqn%addVolumeSourceTerms()

        ! Boundary conditions
        call Ueqn%applyBoundaryConditionsMomentum()

        ! Relax momentum equation, steady state only
        if(steadySim==1) call Ueqn%relaxEq(velocity)

        !-------------------------!
        ! Solve momentum equation !
        !-------------------------!

        call Ueqn%solve(velocity, setGuess=1, reusePC=0, reuseMat=0)

        ! Clean up memory
        call Ueqn%clearMatAndRhs()

        !---------------------------------!
        ! Update velocities at boundaries !
        !---------------------------------!

        call velocity%updateBoundaryVelocityField()

        call computeGradient(velocity)

    end subroutine momentumEquation

! *********************************************************************************************************************

    subroutine momentumEquationIBM(F)

    !==================================================================================================================
    ! Description:
    !! momentumEq assembles and solves the momentum equation with external forces coming from immersed boundary.
    !! Please not that this routines must be called after the first prediction step has been done and the old velocity
    !! does not have to be updated.
    !==================================================================================================================

        use momentum
        use globalMeshVar
        use gradient
        use fieldvar
        use physicalConstants

        implicit none

        real, dimension(numberOfElements + numberOfBFaces,3) :: F
    !------------------------------------------------------------------------------------------------------------------

        !--------------------------------!
        ! Allocate space for coefficiets !
        !--------------------------------!

        call Ueqn%createEqCoeffs()
        call Ueqn%resetCoeffs()

        !----------------------------!
        ! Assemble momentum equation !
        !----------------------------!

        ! Transient
        if(steadySim==0) call Ueqn%addTransientTerm(velocity, rho)

        ! Diffusion
        call Ueqn%addDiffusionTerm(velocity, nu)

        ! save coeffs and rhs to be use in prime
        if(steadySim==0) call Ueqn%updateDiffusionCoeffs()

        ! Convection
        if(trim(Ueqn%convOptName) /= 'none') then
            if(Ueqn%convImp==0) then
                call Ueqn%addConvectiveTerm(velocity)
            else
                call Ueqn%addConvectiveTermImplicit(velocity)
            end if
        end if

        ! Body forces (gradP + custom volume forces)
        call Ueqn%addBodyForce(pressure%phiGrad(:,:,1), Ueqn%nComp)
        call Ueqn%addBodyForce(-F, Ueqn%nComp)

        ! Additional source term
        if(flubioSources%customSourcesFound) call Ueqn%addCustomSourceTerm(velocity)
        if(flubioSources%sourcesFound) call Ueqn%addVolumeSourceTerms()

        ! Boundary conditions
        call Ueqn%applyBoundaryConditionsMomentum()

        ! Relax momentum equation, steady state only
        if(steadySim==1) call Ueqn%relaxEq(velocity)

        !-------------------------!
        ! Solve momentum equation !
        !-------------------------!

        call Ueqn%solve(velocity, setGuess=1, reusePC=0, reuseMat=0)

        ! Clean up memory
        call Ueqn%clearMatAndRhs()

        !---------------------------------!
        ! Update velocities at boundaries !
        !---------------------------------!

        call velocity%updateBoundaryVelocityField()

        call computeGradient(velocity)

    end subroutine momentumEquationIBM

! *********************************************************************************************************************