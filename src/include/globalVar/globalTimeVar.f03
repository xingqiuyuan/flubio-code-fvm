!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module globalTimeVar

    implicit none

    integer :: itime
    !! Time-step or iteration counter.

    real :: time
    !! Simulation physical time.

    real :: dtime
    !! Time-step for unsteady simulations.

    real :: dtime0
    !! Preivious time step.

    real :: dtime00
    !! two times old time-step.

    integer :: dtAdj
    !! If 1, recompute the time-step size at the beginning of each time-step.

    real :: cflmax
    !! Maxium CFL allowed. Formulation of CFL follows Blazek's book 3rd edition pp. 184-185.

    real :: tmax
    !! Time to reach.

    real :: telap(2)
    !! telap(2)-telap(1) is the time elapsed for one time-step/iteration.

    real :: totalTime
    !! total run time

    logical :: isCoupled
    !! flag for the coupled solver

end module globalTimeVar