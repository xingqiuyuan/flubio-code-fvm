    program foam2FlubioPar

        use converters

        implicit none

        character(len=10) :: procID

        character(len=10) :: timeElapsed

        character(len=10) :: fileFormat

        character(len=10), dimension(:), allocatable :: args

        character(len=:), allocatable :: filePath
    !------------------------------------------------------------------------------------------------------------------

        integer :: id, nid, nargs, serialRun, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: t1, t2, telap
    !------------------------------------------------------------------------------------------------------------------

        call MPI_INIT(ierr)

        ! Assing an identity to processors
        call MPI_COMM_RANK(MPI_COMM_WORLD,id,ierr)
        call MPI_Comm_size(MPI_COMM_WORLD,nid,ierr) 
        
        write(procID, '(i0)' ) id

        ! Check command line arguments
        nargs = command_argument_count()
        if(nargs>0) then
            allocate(args(nargs))
            call getArgs(args, nargs, serialRun, fileFormat)
        end if

        if(serialRun == 0) then
            filePath = 'grid/processor'//trim(procID)//'/'
        else    
            filePath ='grid/serial/grid/'
        endif    

        t1 = mpi_wtime()
       
        if(id == 0) write(*,*) 'converting points...'
        call convertPoints(filePath, 'points', trim(fileFormat))

        if(id == 0) write(*,*) 'converting owner...'
        call convertOwner(filePath, 'owner', trim(fileFormat))

        if(id == 0) write(*,*) 'converting neighbour...'
        call convertNeighbour(filePath, 'neighbour', trim(fileFormat))

        if(id == 0) write(*,*) 'converting faces...'
        call convertFaces(filePath, 'faces', trim(fileFormat))

        if(serialRun == 0) then

            if(id == 0) write(*,*) 'converting cellProcAddressing...'
            call convertCellAddressing(filePath, 'cellProcAddressing', trim(fileFormat))

            if(id == 0) write(*,*) 'converting faceProcAddressing...'
            call convertFaceAddressing(filePath, 'faceProcAddressing', trim(fileFormat))

            if(id == 0) write(*,*) 'converting boundaryProcAddressing...'
            call convertBoundaryProcAddressing(filePath, 'boundaryProcAddressing', trim(fileFormat))

        else
            write(*,*) "Copying serial mesh to processor0/"
            call execute_command_line ("cp -r serial/grid/ processor0")
        endif    

        t2 = mpi_wtime()
        telap = t2-t1
        write(timeElapsed,'(f10.2)') telap

        if(id==0) write(*,*) 'FLUBIO: time to convert the mesh: '//adjustl(trim(timeElapsed))//' seconds'

        call MPI_FINALIZE(ierr)

    end program foam2FlubioPar

! *********************************************************************************************************************

    subroutine getArgs(args, nargs, serialRun, fileFormat)

        implicit none

        integer :: p, nargs, dataForm, stat, serialRun

        character(*) :: args(nargs)

        character(len=10) :: fileFormat
    !------------------------------------------------------------------------------------------------------------------

        ! Get the arguments
        do p=1,nargs
            call get_command_argument(number=p, value=args(p), status=stat)
        enddo

        ! Default format
        serialRun = 0
        fileFormat = 'ascii' 

        ! Process the options
        do p=1,nargs

            if(args(p)=='-serialRun') then

                serialRun = 1

            elseif(args(p)=='-format') then

               fileFormat = trim(args(p+1))
              
            elseif(args(p)=='-help') then

                write(*,*) 'Command line options:'
                write(*,*) '-serialRun: copy serial mesh in processor0 folder'
                write(*,*) '-header: number of headers line to skip (usaully 9 or 16)'
                stop
            endif

        enddo

    end subroutine getArgs

!*********************************************************************************************************************    
