!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module tvdInterpolations

!==================================================================================================================
! Description:
!! tvdInterpolations contains the method to perform a TVD interpolation.
!==================================================================================================================

    ! Global variables
    use meshvar
    use flubioFields
    use fieldvar
    use tvdLimiters

    implicit none

contains

    subroutine TVDInterpolation(field, convOpt, nComp)
        
    !==================================================================================================================
    ! Description:
    !! TVDInterpolation interpolates a variable \f$ \phi \f$ from cell centers to mesh faces using a TVD scheme.
    !==================================================================================================================
        
        type(flubioField) :: field
        !! field to interpolate
    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp, iComp, iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary , is, ie, iProc, pNeigh, convOpt
    !------------------------------------------------------------------------------------------------------------------

        integer :: pos(numberOfFaces,1)

        integer :: patchFace

        real :: upwindGrad(3,nComp), rCf(3), CN(3), dUD(3), grad_f(3), aux(3), dot, corr, d1, d2

        real :: phiC(nComp), phiD(nComp), phiU(nComp), phi_f, L, switch, psi, theta, d, absGradPhi, small
    !------------------------------------------------------------------------------------------------------------------

        pos = 0
        small = 1.0e-12

        !--------------------------!
        ! Update convective fluxes !
        !--------------------------!

        do iFace=1,numberOfIntFaces

           iOwner = mesh%owner(iFace)
           iNeighbour = mesh%neighbour(iFace)
    
            !-------------------------------!
            ! Find index of the upwind cell !
            !-------------------------------!

           if (mf(iFace,1)>=0) pos(iFace,1)=1

           iUpwind   =  pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour
           iDownwind =  pos(iFace,1)*iNeighbour + (1-pos(iFace,1))*iOwner
           upwindGrad = field%phiGrad(iUpwind,:,:)

            !------------------------------------------------------------------!
            ! Compute correction for HO schemes (Deferred correction approach) !
            !------------------------------------------------------------------!

           rCf = mesh%fcentroid(iFace,:) - mesh%centroid(iUpwind,:)
           dUD= mesh%centroid(iUpwind,:) - mesh%centroid(iDownwind,:)

               ! Compute inverse distance factor L
               call mag(mesh%CN(iFace,:), d1)
               call mag(rCf,d2)
               L = d1/d2

           do iComp=1, nComp

              ! Compute phiC, phiD, phiU (upwind, downwind and far upwind values)
              phiC(iComp) = field%phi(iUpwind, iComp)
              phiD(iComp) = field%phi(iDownwind, iComp)
              dot = dot_product(field%phiGrad(iUpwind,:,iComp),dUD)
              phiU(iComp) = phiD(iComp) + 2*dot

              ! compute theta
              d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)
              absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
              dot = dot_product(upwindGrad(:,iComp), dUD)
              theta = acos(abs(dot/(absGradPhi*d+small)))

              call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)
              field%phif(iFace,iComp) = phi_f

	       enddo

	enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh=0

            do iBFace=is,ie

                pNeigh=pNeigh+1
                iOwner = mesh%owner(iBFace)

                !-----------------------------------!
                ! Compute correction for HO schemes !
                !-----------------------------------!

                 if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                 switch = pos(iBFace,1)
                 rCf = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)*switch - mesh%ghost(pNeigh,:,iBoundary)*(1-switch)
                 upwindGrad = field%phiGrad(iOwner,:,:)*switch + field%ghostsGrad(pNeigh,:,iBoundary,:)*(1-switch)
    
                ! Compute phiC, phiD, phiU (upwind, downwind and far upwind values)
                 phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)
                 phiD = field%ghosts(pNeigh,:,iBoundary)*switch + field%phi(iOwner,:)*(1-switch)
                 dUD = mesh%centroid(iOwner,:)*(2*switch-1) - mesh%ghost(pNeigh,:,iBoundary)*(2*switch-1)

                ! Compute inverse distance factor L
                 call mag(mesh%CN(iBFace,:), d1)
                 call mag(rCf,d2)
                 L = d1/d2

                 do iComp=1, nComp
                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    phiU(iComp) = phiD(iComp) + 2*dot

                    d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)
                    absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    theta = acos(abs(dot/(absGradPhi*d+small)))

                    call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)
                    field%phif(iBFace,iComp) = phi_f
                 enddo
            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        patchFace = 0
        do iBoundary=1,mesh%boundaries%numberOfRealBound

           is = mesh%boundaries%startFace(iBoundary)
           ie = is+mesh%boundaries%nFace(iBoundary)-1

           do iBFace=is, ie
              patchFace=patchFace+1
              field%phif(iBFace,:) = field%phi(numberOfElements+patchFace,:)
            enddo

        enddo

	end subroutine TVDInterpolation

! *********************************************************************************************************************
    
    subroutine TVD_HR_Schemes(phiC, phiD, phiU, L, phi_f, corr, psiL, theta, opt)
            
    !==================================================================================================================
    ! Description:
    !! TVD_HR_Schemes computes the face value of a field according to a specific TVD scheme.
    !==================================================================================================================
    
        integer iFace, iUpwind, iDownwind, iComp, pos, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: phiC
        !! upwind cell value

        real :: phiD
        !! downwind cell value

        real :: phiU
        !! far upwind cell value

        real :: rf
        !! gradient ratio

        real :: phi_f
        !! face value

        real :: psi
        !! limiter

        real :: psiL
        !! limiter devided by L

        real :: corr
        !! correction to be use in the deferred correction

        real :: N
        !!  numerator

        real :: D
        !! denominator

        real :: L
        !! inverse distance weighting factor

        real :: theta
        !! usefull to blend schemes like STACS

        real :: small
        !! small number
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-12

        ! Compute nomalized phiC
        N = phiC - phiU
        D = phiD - phiC
        rf = N/(D+small)

        !----------------------------------------------!
        ! Get face value from target scheme definition !
        !----------------------------------------------!

        if(opt==0) then

           call upwindLimiter(rf, psi)

        elseif(opt==1) then

           call CDLimiter(rf, psi)

    !	elseif(opt==2) then

    !	   call SOULimiter(rf, psi)

    !	elseif(opt==3) then

    !	   call QUCIKLimiter(rf, psi)

    !	elseif(opt==4) then

    !	   call FROMMLimiter(rf, psi)

        elseif(opt==5) then

           call downwindLimiter(L, psi)

        elseif(opt==6) then

           call vanLeerLimiter(rf, psi)

        elseif(opt==7) then

           call CHARMLimiter(rf, psi)

        elseif(opt==8) then

           call HCUSLimiter(rf, psi)

        elseif(opt==9) then

           call HQUICKLimiter(rf, psi)

        elseif(opt==10) then

           call korenLimiter(rf, psi)

        elseif(opt==11) then

           call minModLimiter(rf, psi)

        elseif(opt==12) then

           call MCLimiter(rf, psi)

        elseif(opt==13) then

           call osherLimiter(rf, psi)

        elseif(opt==14) then

           call ospreLimiter(rf, psi)

        elseif(opt==15) then

           call SMARTLimiter(rf, psi)

        elseif(opt==16) then

           call sweebyLimiter(rf, 0.5, psi)

        elseif(opt==17) then

           call superbeeLimiter(rf, psi)

        elseif(opt==18) then

           call UMISTLimiter(rf, psi)

        elseif(opt==19) then

           call vanAlbada1Limiter(rf, psi)

        elseif(opt==20) then

           call vanAlbada2Limiter(rf, psi)

        elseif(opt==21) then

           call osherLimiterImproved(rf, L, psi)

        elseif(opt==22) then

           call superbeeLimiterImproved(rf, L, psi)

        elseif(opt==23) then

           call vanLeerLimiterImproved(rf, L, psi)

        elseif(opt==24) then

            call stoicLimiter(rf, psi)

        elseif(opt==25) then

            call mstoicLimiter(rf, psi)

        elseif(opt==26) then

            call msuperbeeLimiter(rf, psi)

        elseif(opt==27) then

            call STACS(rf, theta, psi)

        elseif(opt==28) then

            call hypercLimiter(rf, psi)

        elseif(opt==29) then

            call MHRIC(rf, theta, psi)

        elseif(opt==30) then

            call FBICS(rf, theta, psi)

        elseif(opt==31) then

            call MUSCL(rf, psi)

        elseif(opt==32) then

            call boundedCD(rf, psi)

        elseif(opt==33) then 

            ! I need to find a way to avoid hardcoding of k
            call limitedLinear(rf, 1.0, psi)

        elseif(opt==34) then 

            call venkatakrishnan(rf, psi)

        else

           call flubioStopMsg('ERROR: unknown tvd limiter')

        endif

        ! Find out the face value, deferred correction and limiter
        ! "improved" schemes already takes into account the inverse distance
        if(opt==21 .or. opt==22 .or. opt==23) then
            phi_f = phiC + psi*(phiD-phiC)
            corr = phi_f - phiC
            psiL= psi
        else
            phi_f = phiC + (psi/L)*(phiD-phiC)
            corr = phi_f - phiC
            psiL= psi/L
        end if

    end subroutine TVD_HR_Schemes

! *********************************************************************************************************************

    function computeTVDlimiters(field, convOpt, nComp) result(limiters)

    !==================================================================================================================
    ! Description:
    !! TVDlimiters returns the value of the face limiters using a TVD scheme.
    !==================================================================================================================

        type(flubioField) :: field
        !! field to interpolate

    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp, iComp, iFace, iBFace, iOwner, iNeighbour, iUpwind, iDownwind, iBoundary , is, ie, iProc, pNeigh, convOpt
    !------------------------------------------------------------------------------------------------------------------

        integer :: pos(numberOfFaces,1)

        integer :: patchFace

        real :: upwindGrad(3,nComp), rCf(3), CN(3), dUD(3), dot, corr, d1, d2

        real :: phiC(nComp), phiD(nComp), phiU(nComp), phi_f, L, switch, psi, theta, d, absGradPhi, small

        real :: limiters(numberOfFaces,nComp)
    !------------------------------------------------------------------------------------------------------------------

        pos=0
        small = 0.0000000001

        !--------------------------!
        ! Update convective fluxes !
        !--------------------------!

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            !-------------------------------!
            ! Find index of the upwind cell !
            !-------------------------------!

            if (mf(iFace,1)>=0) pos(iFace,1)=1

            iUpwind   =  pos(iFace,1)*iOwner + (1-pos(iFace,1))*iNeighbour
            iDownwind =  pos(iFace,1)*iNeighbour + (1-pos(iFace,1))*iOwner
            upwindGrad = field%phiGrad(iUpwind,:,:)

            !-----------------------------------!
            ! Compute correction for HO schemes !
            !-----------------------------------!

            rCf = mesh%fcentroid(iFace,:) - mesh%centroid(iUpwind,:)
            dUD= mesh%centroid(iUpwind,:) - mesh%centroid(iDownwind,:)

            ! Compute inverse distance factor L
            call mag(mesh%CN(iFace,:), d1)
            call mag(rCf,d2)
            L = d1/d2

            do iComp=1, nComp

                ! Compute phiC, phiD, phiU (upwind, downwind and far upwind values)
                phiC(iComp) = field%phi(iUpwind, iComp)
                phiD(iComp) = field%phi(iDownwind, iComp)
                dot = dot_product(field%phiGrad(iUpwind,:,iComp),dUD)
                phiU(iComp) = phiD(iComp) + 2*dot

                ! compute theta
                d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)
                absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                dot = dot_product(upwindGrad(:,iComp), dUD)
                theta = acos(abs(dot/(absGradPhi*d+small)))

                call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)
                limiters(iFace,iComp) = psi

            enddo

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh=0

            do iBFace=is,ie

                pNeigh=pNeigh+1
                iOwner = mesh%owner(iBFace)

                !-------------------------------------------------------------------!
                ! Compute correction for HO schemes (Deferred correction approach)  !
                !-------------------------------------------------------------------!

                if(mf(iBFace,1)>=0) pos(iBFace,1)=1

                switch = pos(iBFace,1)
                rCf = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)*switch - mesh%ghost(pNeigh,:,iBoundary)*(1-switch)
                upwindGrad = field%phiGrad(iOwner,:,:)*switch + field%ghostsGrad(pNeigh,:,iBoundary,:)*(1-switch)

                ! Compute phiC, phiD, phiU (upwind, downwind and far upwind values)
                phiC = field%phi(iOwner,:)*switch + field%ghosts(pNeigh,:,iBoundary)*(1-switch)
                phiD = field%ghosts(pNeigh,:,iBoundary)*switch + field%phi(iOwner,:)*(1-switch)
                dUD = mesh%centroid(iOwner,:)*(2*switch-1) - mesh%ghost(pNeigh,:,iBoundary)*(2*switch-1)

                ! Compute inverse distance factor L
                call mag(mesh%CN(iBFace,:), d1)
                call mag(rCf,d2)
                L = d1/d2

                do iComp=1, nComp
                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    phiU(iComp) = phiD(iComp) + 2*dot

                    d = sqrt(dUD(1)**2+dUD(2)**2+dUD(3)**2)
                    absGradPhi = sqrt(upwindGrad(1,iComp)**2+upwindGrad(2,iComp)**2+upwindGrad(3,iComp)**2)
                    dot = dot_product(upwindGrad(:,iComp), dUD)
                    theta = acos(abs(dot/(absGradPhi*d+small)))

                    call TVD_HR_Schemes(phiC(iComp), phiD(iComp), phiU(iComp), L, phi_f, corr, psi, theta, convOpt)
                    limiters(iBFace,iComp) = psi
                enddo
            enddo

        enddo

    end function computeTVDlimiters

!**********************************************************************************************************************
    
    function getTVDLimiterValue(rf, opt) result(psi)
            
    !==================================================================================================================
    ! Description:
    !! getTVDLimiterValue computes the TVD limiter given the gradient ratio.
    !==================================================================================================================
    
        integer :: opt
        !! limiter option
    !------------------------------------------------------------------------------------------------------------------

        real :: rf
        !! gradient ratio

        real :: psi
        !! limiter
    !------------------------------------------------------------------------------------------------------------------

        !----------------------------------------------!
        ! Get face value from target scheme definition !
        !----------------------------------------------!

        if(opt==0) then

           call upwindLimiter(rf, psi)

        elseif(opt==6) then

           call vanLeerLimiter(rf, psi)

        elseif(opt==7) then

           call CHARMLimiter(rf, psi)

        elseif(opt==8) then

           call HCUSLimiter(rf, psi)

        elseif(opt==9) then

           call HQUICKLimiter(rf, psi)

        elseif(opt==10) then

           call korenLimiter(rf, psi)

        elseif(opt==11) then

           call minModLimiter(rf, psi)

        elseif(opt==12) then

           call MCLimiter(rf, psi)

        elseif(opt==13) then

           call osherLimiter(rf, psi)

        elseif(opt==14) then

           call ospreLimiter(rf, psi)

        elseif(opt==15) then

           call SMARTLimiter(rf, psi)

        elseif(opt==16) then

           call sweebyLimiter(rf, 0.5, psi)

        elseif(opt==17) then

           call superbeeLimiter(rf, psi)

        elseif(opt==18) then

           call UMISTLimiter(rf, psi)

        elseif(opt==19) then

           call vanAlbada1Limiter(rf, psi)

        elseif(opt==20) then

           call vanAlbada2Limiter(rf, psi)

        elseif(opt==24) then

            call stoicLimiter(rf, psi)

        elseif(opt==25) then

            call mstoicLimiter(rf, psi)

        elseif(opt==26) then

            call msuperbeeLimiter(rf, psi)

        elseif(opt==28) then

            call hypercLimiter(rf, psi)

        elseif(opt==31) then

            call MUSCL(rf, psi)

        elseif(opt==32) then

            call boundedCD(rf, psi)

        elseif(opt==33) then 

            call limitedLinear(rf, 1.0, psi)

        elseif(opt==34) then 

            call venkatakrishnan(rf, psi)

        else

           call flubioStopMsg('ERROR: unknown tvd limiter')

        endif

    end function getTVDLimiterValue

end module tvdInterpolations