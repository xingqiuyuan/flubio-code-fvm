!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine voidBoundary(aC, bC, fComp)

	!==================================================================================================================
	! Description:
	!! voidBoundary computes the diagonal coefficient and rhs correction when an void boundary condition is applied.
	!! It is equivalent to a zero gradient boundary condition and it is usually used in 2D simulations on side boundaries.
	!! the correction reads:
	!! \begin{eqnarray}
	!!    &a_C^b = 0 \\
	!!    &b_C^b = 0
	!! \end{eqnarray}
	!==================================================================================================================

		implicit none

		integer :: fComp
		!! number of components
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(fComp)
		!! diagonal coefficent correction

		real :: bC(fComp)
		!! rhs correction

	!------------------------------------------------------------------------------------------------------------------

	! DO NOTHING
		aC = 0.0
		bC = 0.0

	end subroutine voidBoundary

! *********************************************************************************************************************

	subroutine updateVoidBoundaryField(phi, iBoundary, fComp)

	!==================================================================================================================
	! Description:
	!! updateVoidBoundaryField loops over all the boundary faces of a target patch
	!!and updates the field face values with the cell center ones:
	!! \begin{equation*}
	!!    \phi_b = \phi_C \\
	!! \end{equation*}
	!==================================================================================================================

		use meshvar

		implicit none

		integer :: iBoundary, iBFace, is, ie, iOwner, fComp, patchFace
	!------------------------------------------------------------------------------------------------------------------

		real :: phi(numberOfElements+numberOfBFaces, fComp)
		!! field values
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace = numberOfElements + (is-numberOfIntFaces-1)

		do iBFace=is,ie
		   patchFace=patchFace+1
		   iOwner = mesh%owner(iBFace)
		   phi(patchFace,1:fComp)=phi(iOwner,1:fComp)
		enddo

	end subroutine updatevoidBoundaryField

! *********************************************************************************************************************

