!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module runTimeTasks

    use CoField
    use continuityErrors
    use derivedFields
    use forceMonitors
    use probes
    use lineProbes
    use sampledSurfaces
    use statistics

    implicit none

    type, public :: runTimeObjs
        class(runTimeObj), allocatable :: task
    end type runTimeObjs

    ! Global array for runtime objects
    type(runTimeObjs), dimension(:), allocatable :: runTimeObjects

contains

    subroutine setMonitorsList()

    !==================================================================================================================
    ! Description:
    !! setRunTimeProc fills the the run time processing dictionary.
    !==================================================================================================================

    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, typeValue, monitorName, path
        !! First array element lenght

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: iMonitor
    !------------------------------------------------------------------------------------------------------------------

        logical :: keyFound, typeFound, monitorFound, monitorsFound, nameFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Allocate the monitors array
        allocate(runTimeObjects(flubioMonitors%numberOfMonitors))

        do iMonitor=1,flubioMonitors%numberOfMonitors

            ! Get monitors pointer
            call jcore%get_child(monitorsPointer, iMonitor, monitorPointer, monitorFound)

            ! Get monitor name
            call jcore%get_path(monitorPointer, path, nameFound)
            if(nameFound) then
                call split_in_array(path, splitPath, '.')
                monitorName = splitPath(2)
            else
                call flubioStopMsg('ERROR: cannot find the monitor name, please check if the dictionary is correct.')
            end if

            ! Get monitor type
            call jcore%get(monitorPointer, 'type', typeValue, typeFound)

            if(typeFound)then

                if(lowercase(trim(typeValue))=='forcecoeffs') then
                    allocate(forceCoeffs::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='pressurecoeff') then
                    allocate(Cp::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='frictioncoeff') then
                    allocate(Cf::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='flowstatistics') then
                    allocate(flowStats::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName

                ! Derived Field
                elseif(lowercase(trim(typeValue))=='cp0') then
                    allocate(CP0::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='vorticity') then
                    allocate(vorticity::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='qcriterion') then
                    allocate(Q::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='stainrate') then
                    allocate(E::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='yplus') then
                    allocate(yplus::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='co') then
                    allocate(Co::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='continuityerrors') then
                    allocate(contErr::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName

                ! Sampling
                elseif(lowercase(trim(typeValue))=='probes') then
                    allocate(probeObj::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='lineprobes') then
                    allocate(lineProbeObj::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                elseif(lowercase(trim(typeValue))=='sampledsurfaces') then
                    allocate(sampledSurfaceObj::runTimeObjects(iMonitor)%task)
                    runTimeObjects(iMonitor)%task%objName = monitorName
                else
                    call flubioStopMsg('ERROR: unknown monitor type: '//trim(typeValue))
                end if
            else
                call flubioStopMsg("ERROR: cannot find the monitor type. Please either specify it or check the spelling.")
            end if
        enddo

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine setMonitorsList

!**********************************************************************************************************************

    subroutine initialiseTasks

    !==================================================================================================================
    ! Description:
    !! initialiseTasks initialises the tasks sequencially as they are stored in the polymorphic vector.
    !==================================================================================================================

        integer :: iTask
    !-----------------------------------------------------------------------------------------------------------------

        do iTask =1,flubioMonitors%numberOfMonitors
            call runTimeObjects(iTask)%task%initialise()
        enddo

    end subroutine initialiseTasks

!**********************************************************************************************************************

    subroutine runTasks

    !==================================================================================================================
    ! Description:
    !! runTasks runs the tasks sequencially as they are stored in the polymorphic vector.
    !==================================================================================================================

        integer :: iTask
    !-----------------------------------------------------------------------------------------------------------------

        do iTask =1,flubioMonitors%numberOfMonitors
            call runTimeObjects(iTask)%task%run()
        enddo

    end subroutine runTasks

!**********************************************************************************************************************

end module runTimeTasks
