!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module timeSchemes

!==================================================================================================================
! Description:
!! This module contains the methods to dicretise the first order time derivative appering in transiet transport equations.
!! It implements three schemes: Euler, second order upwind Euler, Crank-Nicholson.
!==================================================================================================================
    
    use globalTimeVar
    use flubioMpi
    use flubioDictionaries
    use meshvar
    
    implicit none

contains

! *********************************************************************************************************************

    subroutine Euler(phio, ddtCoef, fComp, aC, bC)

    !==================================================================================================================
    ! Description:
    !! Euler computes the contributions to the diagonal coefficient and rhs according to 1st order Euler scheme:
    !! \begin{eqnarray}
    !!     a_C = \frac{\rho_C V_C}{\Delta t} \\
    !!     b_C = \frac{\rho_C^{\circ} V_C}{\Delta t}\phi_C^{\circ}
    !! \end{eqnarray}
    !==================================================================================================================

        integer :: iElement, iComp, fComp
    !------------------------------------------------------------------------------------------------------------------

        real :: ddtCoef(numberOfElements)
        !! time-derivative coefficient

        real :: aC(numberOfElements,fComp)
        !! diagonal coefficient

        real :: bC(numberOfElements,fComp)
        !! righ hand side

        real :: corr
        !! diagonal coefficient correction
    !------------------------------------------------------------------------------------------------------------------

        real :: phio(numberOfElements+numberOfBFaces, fComp)
        !! old time level field values
    !------------------------------------------------------------------------------------------------------------------

        ! Modify coeffs according to implicit euler time discretization
        do iElement=1,numberOfElements

            corr = ddtCoef(iElement)*mesh%volume(iElement)/dtime

            aC(iElement,:) = aC(iElement,:) + corr
            bC(iElement,:) = bC(iElement,:) + corr*phio(iElement,:)

        enddo

    end subroutine Euler

! *********************************************************************************************************************

    subroutine AdamsMoulton(phio, phioo, ddtCoef, ddtCoef0, ddtCoef00, fComp, aC, bC)

    !==================================================================================================================
    ! Description:
    !! AdamsMoulton computes the contributions to the diagonal coefficient and rhs according to 2nd order Adams-Moulton scheme
    !! (known also as three level implicit):
    !! \begin{eqnarray}
    !!    a_C = (\frac{1}{\Delta t}+\frac{1}{\Delta t+\Delta t^{\circ}})\rho_C V_C  \\
    !!    a_C^{\circ} = -(\frac{1}{\Delta t}+\frac{1}{\Delta t^{\circ}})\rho_C^{\circ} V_C \\
    !!    a_C^{\circ \circ} = (\frac{\Delta t}{\Delta t + \Delta t^{\circ}})\rho_C^{\circ \circ} V_C \\
    !!    b_C = -a_C^{\circ}\phi^{\circ} -a_C^{\circ \circ}\phi^{\circ \circ}
    !! \end{eqnarray}
    !==================================================================================================================

        integer :: iElement, iComp, fComp
    !------------------------------------------------------------------------------------------------------------------

        real :: ddtCoef(numberOfElements)
        !! time-derivative coefficient

        real :: ddtCoef0(numberOfElements)
        !! time-derivative coefficient one time step old

        real :: ddtCoef00(numberOfElements)
        !! time-derivative coefficient two time steps old

        real :: aC(numberOfElements,fComp)
        !! diagonal coefficient

        real :: bC(numberOfElements,fComp)
        !! righ hand side
    !------------------------------------------------------------------------------------------------------------------

        real :: phio(numberOfElements+numberOfBFaces, fComp)
        !! old time level field values

        real :: phioo(numberOfElements+numberOfBFaces, fComp)
        !! two time level old field values
    !------------------------------------------------------------------------------------------------------------------

        real :: a, b, c, pvol, pvol0, pvol00, corr, corr0, corr00
    !------------------------------------------------------------------------------------------------------------------

        real :: dtdt0, dt0dt00, rdt, rdt0
    !------------------------------------------------------------------------------------------------------------------

        rdt = 1.0/dtime
        dtdt0 = 1.0/(dtime+dtime0)
        dt0dt00 = 1.0/(dtime0+dtime00)
        rdt0 = dtime0/dtime

        a = rdt+dtdt0
        b = -(rdt+dtdt0+rdt0*dt0dt00)
        c = rdt0*dt0dt00

        ! Modify coeffs according to implicit SOUE time discretization
        do iElement=1,numberOfElements

            pvol = ddtCoef(iElement)*mesh%volume(iElement)
            pvol0 = ddtCoef0(iElement)*mesh%volume(iElement)
            pvol00 = ddtCoef00(iElement)*mesh%volume(iElement)

            corr = a*pvol
            corr0 = b*pvol0
            corr00 = c*pvol00

            ! SOUE
            ! -b and -c, they go to rhs
            aC(iElement,:) = aC(iElement,:) + corr
            bC(iElement,:) = bC(iElement,:) - corr0*phio(iElement,:) - corr00*phioo(iElement,:)

        enddo

    end subroutine AdamsMoulton

! *********************************************************************************************************************

    subroutine crankNicholson(phio, phioo, ddtCoef, ddtCoef0, ddtCoef00, fComp, aC, bC)

    !==================================================================================================================
    ! Description:
    !! crankNicholson computes the contributions to the diagonal coefficient and rhs according to 2nd order Crank-Nicolson scheme:
    !! \begin{eqnarray}
    !!    a_C = (\frac{\Delta t^{\circ}}{\Delta t+\Delta t^{\circ}})\rho_C V_C  \\
    !!    a_C^{\circ} = \frac{\Delta t}{\Delta t \Delta t^{\circ}} - \frac{\Delta t^{\circ \circ}}{\Delta t^{\circ} \Delta t^{\circ \circ}}\rho_C^{\circ \circ} V_C \\
    !!    a_C^{\circ \circ} = (\frac{\Delta t^{\circ}}{\Delta t^{\circ} + \Delta t^{\circ \circ}})\rho_C^{\circ \circ} V_C \\
    !!    b_C = -a_C^{\circ}\phi^{\circ} -a_C^{\circ \circ}\phi^{\circ \circ}
    !! \end{eqnarray}
    !==================================================================================================================

        integer iElement, iComp, fComp
    !------------------------------------------------------------------------------------------------------------------

        real :: ddtCoef(numberOfElements)
        !! time-derivative coefficient

        real :: ddtCoef0(numberOfElements)
        !! time-derivative coefficient

        real :: ddtCoef00(numberOfElements)
        !! time-derivative coefficient

        real :: aC(numberOfElements,fComp)
        !! diagonal coefficient

        real :: bC(numberOfElements,fComp)
        !! righ hand side
    !------------------------------------------------------------------------------------------------------------------

        real :: phio(numberOfElements+numberOfBFaces, fComp)
        !! old time level field values

        real :: phioo(numberOfElements+numberOfBFaces, fComp)
        !! two time level old field values
    !------------------------------------------------------------------------------------------------------------------

        real :: a, b, c, pvol, pvol0, pvol00, corr, corr0, corr00
    !------------------------------------------------------------------------------------------------------------------

        real :: dtdt0, dt0dt00, rdt, rdt0
    !------------------------------------------------------------------------------------------------------------------

        !---------!
        ! Prepare !
        !---------!

        a = dtime0/(dtime+dtime0)
        b = dtime/(dtime+dtime0)-dtime00/(dtime0+dtime00)
        c = -dtime00/(dtime0+dtime00)

        ! Modify coeffs according to implicit SOUE time discretization
        do iElement=1,numberOfElements

            pvol = ddtCoef(iElement)*mesh%volume(iElement)/dtime
            pvol0 = ddtCoef0(iElement)*mesh%volume(iElement)/dtime
            pvol00 = ddtCoef00(iElement)*mesh%volume(iElement)/dtime

            corr = a*pvol
            corr0 = b*pvol0
            corr00 = c*pvol00

            ! CN
            aC(iElement,:) = aC(iElement,:) + corr
            bC(iElement,:) = bC(iElement,:) - corr0*phio(iElement,:) - corr00*phioo(iElement,:)

        enddo

    end subroutine crankNicholson

! *********************************************************************************************************************

    subroutine adjustTimeStep

    !==================================================================================================================
    ! Description:
    !! adjustTimeStep computes the local cell CFL number (according to Blazek's book) and
    !! it adjusts the global time step size accordingly.
    !==================================================================================================================

        use fieldvar
        use physicalConstants

        integer :: iElement, iFace, iBFace, iBoundary, iOwner, iNeighbour, iProc, is, ie, pNeigh
    !------------------------------------------------------------------------------------------------------------------

        real :: CFL(numberOfElements)
        !! total CFL

        real :: CFLc(numberOfElements)
        !! convective CFL

        real :: CFLv(numberOfElements)
        !! viscous CFL

        real :: Ueqv(numberOfElements)
        !! equivalnt velocity scale

        real :: dtLocal(numberOfElements), minCFL, maxCFL, dtMin, dtMax
    !------------------------------------------------------------------------------------------------------------------

        real :: LambdaC(3), LambdaV(3), u(3), dS(numberOfElements,3), coef, nuLocal, vol, Cvis
    !------------------------------------------------------------------------------------------------------------------

        Cvis = 2.0

        !-----------------------!
        ! Compute average areas !
        !-----------------------!

        dS = 0.0

        do iFace=1,numberOfIntFaces
            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)
            dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iFace,:))
            dS(iNeighbour,:) = dS(iNeighbour,:) + 0.5*abs(mesh%Sf(iFace,:))
        enddo

        ! Processor boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh=0

            do iBFace=is,ie
                pNeigh=pNeigh+1
                iOwner = mesh%owner(iBFace)
                dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iBFace,:))
            enddo
        enddo

        ! Real boundaries
        do iBoundary=1,mesh%boundaries%numberOfRealBound
            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            do iBFace=is,ie
                iOwner = mesh%owner(iBFace)
                dS(iOwner,:) = dS(iOwner,:) + 0.5*abs(mesh%Sf(iBFace,:))
            enddo
        enddo

        !-------------------!
        ! Compute CFL field !
        !-------------------!

        do iElement=1,numberOfElements

            vol = mesh%volume(iElement)

            !----------------!
            ! Convective CFL !
            !----------------!

            u=abs(velocity%phi(iElement,:))

            LambdaC(1) = u(1)*dS(iElement,1)
            LambdaC(2) = u(2)*dS(iElement,2)
            LambdaC(3) = u(3)*dS(iElement,3)

            CFLc(iElement) = dtime*(LambdaC(1)+LambdaC(2)+LambdaC(3))/vol

            !-------------!
            ! Viscous CFL !
            !-------------!

            coef = 4./(3*rho%phi(iElement,1))

            nuLocal = nu%phi(iElement,1)/rho%phi(iElement,1)

            LambdaV(1)=(coef*nuLocal*dS(iElement,1)**2)/vol
            LambdaV(2)=(coef*nuLocal*dS(iElement,2)**2)/vol
            LambdaV(3)=(1-bdim)*(coef*nuLocal*dS(iElement,3)**2)/vol

            CFLv(iElement) = Cvis*dtime*(LambdaV(1)+LambdaV(2)+LambdaV(3))/vol

            !---------------------!
            ! Equivalent velocity !
            !---------------------!

            Ueqv(iElement) = (LambdaC(1)+LambdaC(2)+LambdaC(3)) + Cvis*(LambdaV(1)+LambdaV(2)+LambdaV(3))

            !----------------------------!
            ! Find the maximum time-step !
            !----------------------------!

            dtLocal(iElement) = cflMax*vol/Ueqv(iElement)

        enddo

        !-----------!
        ! Total CFL !
        !-----------!

        CFL = CFLc + CFLv

        !-------------------------------------!
        ! Find the maximum time-step globally !
        !-------------------------------------!

        call minMaxScalarAll(dtLocal, dtMin, dtMax)
        call minMaxScalar(CFLc, minCFL, maxCFL)

        !-----------------------------------------!
        ! Finally, impose the next time-step size !
        !-----------------------------------------!

        dtime = dtMin

    end subroutine adjustTimeStep

! *********************************************************************************************************************

    subroutine minMaxScalar(T, minT, maxT)

    !==================================================================================================================
    ! Description:
    !! minMaxScalar computes the maximum and the minimum of an array.
    !==================================================================================================================

    ! Local variables

        integer :: iComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: sbuf(nid,1), rbuf(nid,1), minT, maxT, T(numberOfElements,1)
    !------------------------------------------------------------------------------------------------------------------

        ! max value
        sbuf = 0.0
        rbuf = 0.0

        ! Fill the buffer array
        sbuf(id+1,1) = maxval(T(1:numberOfElements,1))

        ! Send and Receive the buffer
        call mpi_Reduce(sbuf, rbuf ,nid, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
        maxT = maxval(rbuf(:,1))

        ! min value
        sbuf = 0.0
        rbuf = 0.0

        ! Fill the buffer array
        sbuf(id+1,1) = minval(T(1:numberOfElements,1))

        ! Send and Receive the buffer
        call mpi_Reduce(sbuf, rbuf ,nid, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
        minT = minval(rbuf(:,1))

    end subroutine minMaxScalar

! *********************************************************************************************************************

    subroutine minMaxScalarAll(T, minT,maxT)

    !==================================================================================================================
    ! Description:
    !! minMaxScalar computes the maximum and the minimum of an array and shares it with all the processors.
    !==================================================================================================================

        integer :: iComp, ierr
    !-------------------------------------------------------------------------------------------------------------------

        real :: sbuf(nid,1), rbuf(nid,1), minT, maxT, T(numberOfElements,1)
    !-------------------------------------------------------------------------------------------------------------------

        ! max value
        sbuf = 0.0
        rbuf = 0.0

        ! Fill the buffer array
        sbuf(id+1,1) = maxval(T(1:numberOfElements,1))

        ! Send and Receive the buffer
        call mpi_AllReduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
        maxT = maxval(rbuf(:,1))

        ! min value
        sbuf = 0.0
        rbuf = 0.0

        ! Fill the buffer array
        sbuf(id+1,1)=minval(T(1:numberOfElements,1))

        ! Send and Receive the buffer
        call mpi_AllReduce(sbuf, rbuf, nid, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
        minT = minval(rbuf(:,1))

    end subroutine minMaxScalarAll

!**********************************************************************************************************************

end module timeSchemes
