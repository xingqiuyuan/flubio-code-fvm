
install( DIRECTORY    ${home_dir_flubio}/0--build/lib
         DESTINATION  ${install_dir_flubio}
)

install( DIRECTORY    ${home_dir_flubio}/0--build/mod
         DESTINATION  ${install_dir_flubio}
)

#install( TARGETS  ${app_flubio}
#  RUNTIME DESTINATION  ${install_dir_flubio}/bin
#  LIBRARY DESTINATION  ${install_dir_flubio}/lib
#  ARCHIVE DESTINATION  ${install_dir_flubio}/lib
#  BUNDLE DESTINATION   ${install_dir_flubio}
#  #DIRECTORY ${CMAKE_Fortran_MODULE_DIRECTORY} DESTINATION ${install_dir_flubio}/include
#)
