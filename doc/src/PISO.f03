!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    program PISO_Alghorithm

    !==================================================================================================================
    ! Description:
    !! PISO algorithm by Issa et al. (1986).
    !==================================================================================================================

        use globalTimeVar
        use flubioMpi
        use flubioSettings
        use meshvar
        use correctFields
        use momentum
        use pressureCorrection
        use runTimeTasks, only: runTasks
        use turbulenceModels, only: trb, les_trb
        use energyEq

        implicit none

        integer :: iCorr, ierr
    !------------------------------------------------------------------------------------------------------------------

        !-------------------------!
        ! Initialize parallel run !
        !-------------------------!

        call PetscInitialize(PETSC_NULL_CHARACTER,ierr)

        ! Initialize MPI variables
        call setMPIvar()

        !---------------!
        ! Read settings !
        !---------------!

        call NavierStokesSettings()

        !-----------!
        ! Read mesh !
        !-----------!

        call mesh%readMeshFromFiles()

        call mesh%createMeshSearchTree()

        !--------------------!
        ! Allocate Variables !
        !--------------------!

        call initialiseNavierStokesSolver()

        !-----------------!
        ! Start time loop !
        !-----------------!

        totalTime = 0.0

        do while (time < tmax)

            ! Advance one time-step
            itime = itime+1
            time = time + dtime
            dtime00 = dtime0
            dtime0 = dtime

            ! Time-step clock
            telap(1) = mpi_Wtime()

            if(dtAdj==1) call adjustTimeStep()

            ! Momentum Equation
            call momentumEquation()

            ! Pressure Equation
            call pressureCorrectionEquation()

            ! correct fields
            call correctVelocityAndPressure()

            ! PISO loop PRIME corrections
            do iCorr=1,flubioOptions%numberOfMomCorr
                call primeCorrection()
            enddo

            ! Turbulence models
            if(flubioTurbModel%ransModel/=-1) call trb%turbulenceModel()
            if(flubioTurbModel%lesModel/=-1)  call les_trb%updateEddyViscosity()

            ! Energy equation, if required
            if(flubioControls%activateEnergyEquation==1) call energyEquation()

            ! update transport model (non-newtonian flows)
            call updateTransportModel()

            ! Run time processing
            call runTasks()

            ! write fields
            if(mod(itime,flubioControls%iflds)==0 .or. abs(flubioControls%iflds)==1) then
                call PISOWriteTofiles()
            end if

            ! Stop the clock
            telap(2) = mpi_Wtime()
            totalTime = totalTime +  (telap(2)-telap(1))

            ! Print info
            call PISOVerbose()

        enddo

        !-----------------------!
        ! End of the simulation !
        !-----------------------!

        if(id==0) then
            write(*,*) 'FLUBIO: Simulation compleated!',' ', ' Total run time =', totalTime
            write(*,*)
        endif

        ! Re-write fields at the end of the simulation
        call PISOWriteTofiles()

        ! Finilize MPI processing
        call PetscFinalize(ierr)

    end program PISO_Alghorithm

! *********************************************************************************************************************

    subroutine PISOVerbose

    !==================================================================================================================
    ! Description:
    !! PISOVerbose prints to screen some run time information. It can be customize to fit user's needs.
    !==================================================================================================================

        use flubioMpi
        use momentum
        use pressureCorrection
        use turbulenceModels

        implicit none
    !------------------------------------------------------------------------------------------------------------------

        ! Print info
        if(id==0) write(*,*)
        call velocity%fieldVerbose()
        call pressure%fieldVerbose()
        if(flubioTurbModel%ransModel > -1) call trb%verbose()
        if(flubioControls%activateEnergyEquation==1) call temperature%fieldVerbose()

        call Peqn%printDelimiter()

    end subroutine PISOVerbose

! *********************************************************************************************************************

    subroutine PISOWriteTofiles

    !==================================================================================================================
    ! Description:
    !! PISOWriteTofiles dumps fields for restart a simulation and post-processing.
    !==================================================================================================================

        use fieldvar
        use flubioSettings
        use turbulenceModels
        use energyEq, only : Eeqn, temperature

        implicit none

        call velocity%writeToFile()
        call pressure%writeToFile()
        call nu%writeToFile()
        call rho%writeToFile()
        if(flubioControls%activateEnergyEquation==1) call temperature%writeToFile()
        call writeMassFluxesToFile()
        if(flubioTurbModel%ransModel > -1)  call trb%writeToFile()
        call saveTimeQuantities()

        ! Delete folder you do not want to keep
        call deleteTimeFolders()

    end subroutine PISOWriteTofiles