module massFlowRate

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use fieldvar, only: mf
    use flubioMpi
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: massFlowRateObj
        character(len=:), dimension(:), allocatable :: boundaries
        integer :: nBoundaries
        real, dimension(:), allocatable :: massFlux
    contains
        procedure :: initialise => createmassFlowRateObj
        procedure :: run => computeFlux
        procedure :: appendValue
    end type massFlowRateObj

contains
        
subroutine createmassFlowRateObj(this)

    !==================================================================================================================
    ! Description:
    !! createmassFlowRateObj is the task constructor.
    !==================================================================================================================
    
        class(massFlowRateObj) :: this
    !-------------------------------------------------------------------------------------------------------------------
    
        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------
    
        type(json_core) :: jcore
    
        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iBoundary

        integer, dimension(:), allocatable :: ilen
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)
    
        call jcore%get(monitorPointer, 'runEvery', this%runAt, found)
        if(.not. found) this%runAt = 1

        call jcore%get(monitorPointer, 'saveEvery', this%saveEvery, found)
        call raiseKeyErrorJSON('saveEvery', found, 'settings/monitors/'//this%objName)
        
        call jcore%get(monitorPointer, 'boundaries', this%boundaries, ilen, found)
        call raiseKeyErrorJSON('boundaries', found, 'settings/monitors/'//this%objName)
    
        this%nBoundaries = size(this%boundaries)
        allocate(this%massFlux(this%nBoundaries))    

        ! Open a file to output the flux value
        if(id==0) then
            do iBoundary=1,this%nBoundaries
                open(1,file='postProc/monitors/'//trim(this%objName)//'_'//trim(this%boundaries(iBoundary))//'.dat')
                    write(1,*) '#   Time   massFlowRate'
                close(1)
            enddo
        endif

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine createmassFlowRateObj
    
! *********************************************************************************************************************

    subroutine computeFlux(this)

    !==================================================================================================================
    ! Description:
    !! computeFlux computes a field flux through a target boundary.
    !==================================================================================================================

        class(massFlowRateObj):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, is, ie, ierr

        integer :: boundaryIndex(3)
    !------------------------------------------------------------------------------------------------------------------
    
        real :: localFlux, globalFlux
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then

            do iBoundary=1,this%nBoundaries
                
                ! Hook up the boundary
                boundaryIndex = mesh%boundaries%findBoundaryByName(this%boundaries(iBoundary))

                is = boundaryIndex(1)
                ie = boundaryIndex(2)
                localFlux = 0.0
                do iBFace=is,ie 
                    localFlux = localFlux + mf(iBFace,1)
                enddo
                
                ! Sum contribution form all the processors
                call mpi_reduce(localFlux, globalFlux, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
                this%massFlux(iBoundary) = globalFlux

            enddo    
    
            call this%appendValue()

        endif    
        
    end subroutine computeFlux

!**********************************************************************************************************************

    subroutine appendValue(this)

        class(massFlowRateObj) this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iField
    !------------------------------------------------------------------------------------------------------------------

        if(id==0 .and. (mod(itime,this%saveEvery)==0 .or. abs(this%saveEvery)==1)) then

            do iBoundary=1,this%nBoundaries
                open(1,file='postProc/monitors/'//this%objName//'_'//trim(this%boundaries(iBoundary))//'.dat',access='append') 
                    write(1,*) time, this%massFlux(iBoundary)
                close(1)
            enddo

        endif    

    end subroutine appendValue    

end module massFlowRate