!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module kappaOmegaWilcox

!==================================================================================================================
! Description:
!! this module contains the implementation of the k-omega model by Wilcox (1988).
!==================================================================================================================

    use baseRansModel

    implicit none

    type, public, extends(baseModelClass) :: kw1988

        ! Coefficients
        real :: C_a1
        real :: C_a2
        real :: C_b1
        real :: C_b2
        real :: sigma_k1
        real :: sigma_o1
        real :: sigma_k2
        real :: sigma_o2
        real :: bstar

        ! Fields
        type(flubioField) :: tke
        type(flubioField) :: tdr

        type(flubioField) :: muEff_k
        type(flubioField) :: muEff_w

        ! Equations
        type(transportEqn) :: tkeEqn
        type(transportEqn) :: tdrEqn

    contains

            procedure :: createModel => createKappaOmega
            procedure :: destroyKappaOmega
            procedure :: modelCoeffs => modelCoeffsWilcox
            procedure :: overrideDefaultCoeffs
            procedure :: printCoeffs => printCoeffsKappaOmega

            procedure :: tkeDiffusivity => kappaDiffusivityWilcox
            procedure :: tdrDiffusivity => omegaDiffusivityWilcox

            procedure :: tkeSourceTerm => kappaSourceTermWilcox
            procedure :: tdrSourceTerm => omegaSourceTermWilcox
            procedure :: tkeSustTerm
            procedure :: tdrSustTerm

            procedure :: updateEddyViscosity  => updateEddyViscosityWilcox
            procedure :: nutWallFunction => nutWallFunctionWilcox

            procedure :: blendingFunction

            procedure :: turbulenceModel => kappaOmega

            procedure :: computeFrictionVelocityK => computeFrictionVelocityK_
            procedure :: turbulentProduction => turbulentProductionWrapper
            procedure :: turbulentProductionKappaOmega
            procedure :: turbulentProductionDev
            procedure :: vorticityProduction

            procedure :: tkeWallFunction
            procedure :: applyTkeWallFunction

            procedure :: tdrWallFunction => omegaWallFunction
            procedure :: tdrLowReWallFunction => omegaLowReWallFunction
            procedure :: applyTdrWallFunction => applyOmegaWallFunction
            procedure :: applyTdrLowReWallFunction => applyOmegaLowReWallFunction

            procedure :: printresidual => kappaOmegaresidual
            procedure :: writeToFile => kappaOmegaWriteToFile
            procedure :: verbose => kappaOmegaVerbose

    end type kw1988

contains

    subroutine createKappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! createKappaOmega is the model constructor.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: ksp_name
    !------------------------------------------------------------------------------------------------------------------

        ! Model coeffs
        call this%modelCoeffs()
        call this%createCommonQuantities()

        ! TKE
        call this%tke%setUpField('Tke', 'turbulence', 1)
        if(flubioControls%restFrom/=-1) call this%tke%readFromFile(flubioControls%restFrom)
        call computeGradient(this%tke)

        call this%tdrField%setUpWorkField('Tke', classType='turbulence', nComp=1)

        ! TDR
        call this%tdr%setUpField('Tdr', 'turbulence', 1, this%C_b1)
        if(flubioControls%restFrom/=-1) call this%tdr%readFromFile(flubioControls%restFrom)
        call computeGradient(this%tdr)

        call this%tdrField%setUpWorkField('Tdr', classType='turbulence', nComp=1)

        ! Effective diffuivities
        call this%muEff_k%setUpCoeff('muEff_k', appendToList='no')
        call this%muEff_w%setUpCoeff('muEff_w', appendToList='no')

        ! Update nut
        call this%updateEddyViscosity()
        call this%nut%appendFieldToList()

        ! Equations
        call this%tkeEqn%createEq('tkeEqn', this%tke%nComp)
        ksp_name = trim(this%tkeEqn%eqName)
        call this%tkeEqn%setEqOptions(ksp_name)

        call this%tdrEqn%createEq('tdrEqn', this%tdr%nComp)
        ksp_name = trim(this%tdrEqn%eqName)
        call this%tdrEqn%setEqOptions(ksp_name)

        call this%tke%writeToFile()
        call this%tdr%writeToFile()

    end subroutine createKappaOmega

!**********************************************************************************************************************

    subroutine destroyKappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! destroyKappaOmega is the model deconstructor.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tke%destroyField()
        call this%tdr%destroyField()

        call this%nut%destroyField()

        call this%tkeEqn%destroyEq()
        call this%tdrEqn%destroyEq()

    end subroutine destroyKappaOmega

!**********************************************************************************************************************

    subroutine kappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmega is the model runner.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        !---------------------------!
        ! Compute friction velocity !
        !---------------------------!

        call this%computeFrictionVelocity()

        !------------------------!
        ! Assemble and solve tke !
        !------------------------!

        call this%tkeDiffusivity()

        call this%tkeEqn%assembleEq(this%tke, this%muEff_k)

        call this%tkeSourceTerm()

        ! Tke wall function if any
        call this%applyTkeWallFunction()

        ! Relax tke equation
        if(steadySim==1) call this%tkeEqn%relaxEq(this%tke)

        call this%tkeEqn%solve(this%tke, setGuess=1, reusePC=0, reuseMat=0)

        call this%tke%updateBoundaryTurbulenceField()

        call computeGradient(this%tke)

        ! Bound tke
        if(flubioTurbModel%bounded==1) call bound(this%tke, 0.0, flubioTurbModel%boundType)

        !-----------------------!
        ! Assmble and solve tdr !
        !-----------------------!

        call this%tdrDiffusivity()

        call this%tdrEqn%assembleEq(this%tdr, this%muEff_w)

        call this%tdrSourceTerm()

        ! Low Re wall function if any
        call this%applyTdrLowReWallFunction()

        ! Relax tdr equation
        if(steadySim==1) call this%tdrEqn%relaxEq(this%tdr)

        ! High Re wall function if any
        call this%applyTdrWallFunction()

        call this%tdrEqn%solve(this%tdr, setGuess=1, reusePC=0, reuseMat=0)

        call this%tdr%updateBoundaryTurbulenceField(this%C_b1)

        call computeGradient(this%tdr)

        ! Bound tdr
        if(flubioTurbModel%bounded==1) call bound(this%tdr, 0.0, flubioTurbModel%boundType)

        !----------------------------!
        ! Update turbulent viscosity !
        !----------------------------!

        call this%updateEddyViscosity()

        ! Store the field into a dummy variable to be use in run time sampling
        this%tkeField%phi(:,1) = this%tke%phi(:,1)
        this%tdrField%phi(:,1) = this%tdr%phi(:,1)

    end subroutine kappaOmega

!**********************************************************************************************************************

    subroutine modelCoeffsWilcox(this)

    !==================================================================================================================
    ! Description:
    !! modelCoeffsWilcox sets the turbulence model coefficients according to NASA.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%C_a1=0.52d0 !5.0d0/9.0d0
        this%C_a2=0.4404d0

        this%C_b1=0.072d0 !0.075d0
        this%C_b2=0.0828d0

        this%bstar=0.09d0

        this%sigma_k1=0.5d0
        this%sigma_o1=0.5d0

        this%sigma_k2=1.0d0
        this%sigma_o2=0.856d0

        call this%overrideDefaultCoeffs()

        call this%laminarYPlus()

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine modelCoeffsWilcox

!**********************************************************************************************************************

    subroutine overrideDefaultCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! overrideDefaultCoeffs sets the coefficients as you might have defined in turbulenceModel dictionary.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! C_mu
        call flubioTurbModel%json%get('Cmu', coeff, found)
        if(found) this%Cmu = coeff

        ! kappa
        call flubioTurbModel%json%get('kappa', coeff, found)
        if(found) this%kappa = coeff

        ! E
        call flubioTurbModel%json%get('E', coeff, found)
        if(found) this%E = coeff

        ! B
        call flubioTurbModel%json%get('B', coeff, found)
        if(found) this%B = coeff

        ! Ca1
        call flubioTurbModel%json%get('Ca1', coeff, found)
        if(found) this%C_a1 = coeff

        ! Ca2
        call flubioTurbModel%json%get('Ca2', coeff, found)
        if(found) this%C_a2 = coeff

        ! Cb1
        call flubioTurbModel%json%get('Cb1', coeff, found)
        if(found) this%C_b1 = coeff

        ! Cb2
        call flubioTurbModel%json%get('Cb2', coeff, found)
        if(found) this%C_b2 = coeff

        ! bstar
        call flubioTurbModel%json%get('bstar', coeff, found)
        if(found) this%bstar = coeff

        ! sigma_k1
        call flubioTurbModel%json%get('sigma_k1', coeff, found)
        if(found) this%sigma_k1 = coeff

        ! sigma_o1
        call flubioTurbModel%json%get('sigma_o1', coeff, found)
        if(found) this%sigma_o1 = coeff

        ! sigma_k2
        call flubioTurbModel%json%get('sigma_k2', coeff, found)
        if(found) this%sigma_k2 = coeff

        ! sigma_o2
        call flubioTurbModel%json%get('sigma_o2', coeff, found)
        if(found) this%sigma_o2 = coeff

    end subroutine overrideDefaultCoeffs

! *********************************************************************************************************************

    subroutine kappaDiffusivityWilcox(this)

    !==================================================================================================================
    ! Description:
    !! kappaDiffusivityWilcox computes the diffusion coefficient for the tke equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: sigma
    !------------------------------------------------------------------------------------------------------------------

        sigma = this%sigma_k1

        !---------------------------------!
        ! Compute effective diffusivities !
        !---------------------------------!

        this%muEff_k%phi(:,1) = viscos + this%nut%phi(:,1)*sigma

        ! Update ghosts
        call this%muEff_k%updateGhosts()

    end subroutine kappaDiffusivityWilcox

!**********************************************************************************************************************

    subroutine omegaDiffusivityWilcox(this)

    !==================================================================================================================
    ! Description:
    !! kappaDiffusivityWilcox computes the diffusion coefficient for the tdr equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: sigma
    !------------------------------------------------------------------------------------------------------------------

        sigma = this%sigma_o1

        !---------------------------------!
        ! Compute effective diffusivities !
        !---------------------------------!

        this%muEff_w%phi(:,1) = viscos + this%nut%phi(:,1)*sigma

        ! Update ghosts
        call this%muEff_w%updateGhosts()

    end subroutine omegaDiffusivityWilcox

!**********************************************************************************************************************

    subroutine kappaSourceTermWilcox(this)

    !==================================================================================================================
    ! Description:
    !! kappaSourceTermWilcox computes the source term for the tke equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: bstar, tdr, Pk, density, vol

        real :: aC, bC
    !------------------------------------------------------------------------------------------------------------------

        bstar = this%bstar

        call this%turbulentProduction()

        !-------------------------!
        ! Compute tke source term !
        !-------------------------!

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            Pk = this%Pk(iElement)
            tdr = this%tdr%phi(iElement,1)

            vol = mesh%volume(iElement)

            aC = bstar*density*tdr*vol
            bC = Pk*vol

            this%tkeEqn%aC(iElement,1) = this%tkeEqn%aC(iElement,1) + aC
            this%tkeEqn%bC(iElement,1) = this%tkeEqn%bC(iElement,1) + bC

        enddo

         if(flubioTurbModel%sustTerm) call this%tkeSustTerm()

    end subroutine kappaSourceTermWilcox

!**********************************************************************************************************************

    subroutine omegaSourceTermWilcox(this)

    !==================================================================================================================
    ! Description:
    !! omegaSourceTermWilcox computes the source term for the tdr equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real ::  density, tdr, nut, Pk, vol, gamm, beta, SMALL

        real :: aC, bC
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 0.0000000001d0

        gamm = this%C_a1
        beta = this%C_b1

        !-------------------------!
        ! Compute bstar*rho*omega !
        !-------------------------!

        do iElement=1,numberOfElements

            tdr = this%tdr%phi(iElement,1)

            density = rho%phi(iElement,1)
            nut = this%nut%phi(iElement,1)
            Pk = this%Pk(iElement)

            vol = mesh%volume(iElement)

            aC = beta*density*tdr*vol
            bC = (gamm*Pk/(nut+small))*vol

            this%tdrEqn%aC(iElement,1) = this%tdrEqn%aC(iElement,1) + aC
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

        if(flubioTurbModel%sustTerm) call this%tdrSustTerm()

    end subroutine omegaSourceTermWilcox

!**********************************************************************************************************************

    subroutine TkeSustTerm(this)

    !==================================================================================================================
    ! Description:
    !! omegaSourceTermWilcox computes the source term for the tdr equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real ::  density, vol, beta

        real ::  bC
    !------------------------------------------------------------------------------------------------------------------

        beta = this%bstar

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            vol = mesh%volume(iElement)

            bC = beta*this%tke_amb*this%tdr_amb
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

    end subroutine tkeSustTerm

!**********************************************************************************************************************

    subroutine tdrSustTerm(this)

    !==================================================================================================================
    ! Description:
    !! omegaSourceTermWilcox computes the source term for the tdr equation.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: density, vol, beta

        real :: F1, F2, bC
    !------------------------------------------------------------------------------------------------------------------

        if(this%ransModel == 0)  beta = this%C_b1

        do iElement=1,numberOfElements

            if(this%ransModel /= 0) then
                call this%blendingFunction(F1, F2, iElement)
                beta = F1*this%C_b1 + (1-F1)*this%C_b2
            end if

            density = rho%phi(iElement,1)
            vol = mesh%volume(iElement)

            bC = beta*density*vol*this%tdr_amb**2
            this%tdrEqn%bC(iElement,1) = this%tdrEqn%bC(iElement,1) + bC

        enddo

    end subroutine tdrSustTerm

!**********************************************************************************************************************

    subroutine updateEddyViscosityWilcox(this)

    !==================================================================================================================
    ! Description:
    !! updateEddyViscosityWilcox computes the eddy viscosity according to the turbulence model.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: density, tke, tdr, m, F1, F2, SMALL
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        SMALL=0.0000000001

        !-------------------------!
        ! Compute bstar*rho*omega !
        !-------------------------!

        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            tke = max(this%tke%phi(iElement,1),0.0)
            tdr = this%tdr%phi(iElement,1)

            this%nut%phi(iElement,1) = density*tke/(tdr+SMALL)

        enddo

        nu%phi(1:numberOfElements,1) = viscos + this%nut%phi(1:numberOfElements,1)

        ! Correct eddy viscosity at wall boundary cells
        call this%nutWallFunction()

        ! Bound eddy viscosity
        call bound(this%nut, 0.0, flubioTurbModel%boundType)

        ! Update eddy and total viscosity ghost values
        call nu%updateGhosts()
        call this%nut%updateGhosts()

    end subroutine updateEddyViscosityWilcox

!**********************************************************************************************************************

    subroutine nutWallFunctionWilcox(this)

    !==================================================================================================================
    ! Description:
    !! nutWallFunctionWilcox computes the eddy viscosity at wall boundaries.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, iWall, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: density, tke, tdr, SMALL
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 0.00000000001d0

        !--------------------------------------!
        ! Correct viscosity at wall boundaries !
        !--------------------------------------!

        i = 0
        do iBoundary=1,numberOfRealBound

            iWall = 0
            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') i=i+1

            ! Wall function
            if(trim(mesh%boundaries%bcType(iBoundary))=='wall' .and. trim(this%tdr%bCond(iBoundary))=='omegaWallFunction') then

                iWall = iWall+1
                do iBFace=is,ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    nu%phi(patchFace,1) = this%nuWall(iWall,i)
                    this%nut%phi(patchFace,1) = this%nuWall(iWall,i) - viscos

                end do

            ! Resolved wall
            elseif(trim(mesh%boundaries%bcType(iBoundary))=='wall' .and. trim(this%tdr%bCond(iBoundary))/='omegaWallFunction') then

                iWall = iWall+1
                do iBFace=is,ie
                    patchFace = patchFace+1
                    nu%phi(patchFace,1) = viscos
                    this%nut%phi(patchFace,1) = 0.0
                end do

            ! Calculated from k and omega
            elseif(trim(mesh%boundaries%bcType(iBoundary))/='wall') then

                do iBFace=is,ie

                    patchFace = patchFace+1

                    density = rho%phi(patchFace,1)
                    tke = max(this%tke%phi(patchFace,1),0.0d0)
                    tdr = this%tdr%phi(patchFace,1) + SMALL

                    this%nut%phi(patchFace,1) = density*tke/tdr
                    nu%phi(patchFace,1) = viscos + density*tke/tdr

                end do

            end if

        end do

    end subroutine nutWallFunctionWilcox

! *********************************************************************************************************************

    subroutine computeFrictionVelocityK_(this)

    !==================================================================================================================
    ! Description:
    !! computeFrictionVelocityK computes the friction velocity from the kinetic energy at wall boundary cells.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iWall, iElement, is, ie, iOwner, boundaryFace
    !------------------------------------------------------------------------------------------------------------------

        real :: kappa, E, Cmu025, wallYplusLim

        real :: d, ypl, magUp, Up(3), nf(3), deltaU(3), dot, SMALL
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        SMALL = 0.00000001d0

        kappa = this%kappa
        E = this%E
        wallYplusLim = this%yplusLaminar
        Cmu025 = this%Cmu**0.25

        ! Loop over wall boundaries
        do iBoundary=1,numberOfWallBound

            iWall = 0
            i = mesh%boundaries%wallBound(iBoundary)

            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1
            boundaryFace = numberOfElements + (mesh%boundaries%startFace(i) - numberOfIntFaces) -1

            do iBFace=is,ie

                iWall = iWall+1
                iOwner = mesh%owner(iBFace)
                boundaryFace = boundaryFace + 1

                ! Velocity parallel to the wall
                nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)
                deltaU = velocity%phi(iOwner,:) - velocity%phi(boundaryFace,:)
                dot = dot_product(deltaU, nf)

                ! Centroid velocity - wall velocity parallel to the wall
                Up = deltaU-dot*nf
                magUp = sqrt(Up(1)**2+Up(2)**2+Up(3)**2)

                !------------!
                ! Find yplus !
                !------------!

                d = mesh%area(iBFace)/mesh%wallDist(iBFace)
                this%utau(iWall,iBoundary) = Cmu025*sqrt(max(this%tke%phi(iOwner,1), 0.0d0))
                ypl = d*this%utau(iWall,iBoundary)/viscos
                this%wallYplus(iWall,iBoundary) = ypl

                !--------------------!
                ! Modified viscosity !
                !--------------------!

                ! Total viscosity (nu+nut). nut = viscos*(nuWall-1)
                if(ypl > wallYplusLim) then
                    this%nuWall(iWall,iBoundary) = kappa*ypl*viscos/log(E*ypl)
                else
                    this%nuWall(iWall,iBoundary) = viscos
                end if

                this%tauWall(iWall,iBoundary) = this%nuWall(iWall,iBoundary)*magUp/d

            enddo

        enddo

    end subroutine computeFrictionVelocityK_

! *********************************************************************************************************************

    subroutine turbulentProductionWrapper(this)

    !==================================================================================================================
    ! Description:
    !! turbulentProduction computes the turbulent production Pk.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(this%productionType==0) then
            call this%turbulentProductionKappaOmega()
        elseif(this%productionType==1) then
            call this%turbulentProductionDev()
        elseif(this%productionType==2) then
            call this%vorticityProduction()
        else
            call flubioStopMsg('ERROR: please specify a method to compute the turbulent production')
        end if

    end subroutine turbulentProductionWrapper

! *********************************************************************************************************************

    subroutine turbulentProductionKappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! turbulentProduction computes the turbulent production Pk.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBoundary, iBFace, iOwner, patchFace, iWall, is, ie, iFace
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements,6), Ssum(numberOfElements), trace, S12, S13, S23

        real :: kappa, tw, ut, d, bstar, SMALL, tke, tdr
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 0.0000000001d0

        bstar = this%bstar
        kappa = this%kappa

        !-----------------------!
        ! Compute Strain tensor !
        !-----------------------!

        call strainTensor(S,Ssum)

        !--------------------!
        ! Compute production !
        !--------------------!

        do iElement=1,numberOfElements

            tke = max(this%tke%phi(iElement,1),0.0d0)
            tdr = this%tdr%phi(iElement,1)

            trace = S(iElement,1)**2 + S(iElement,2)**2 + S(iElement,3)**2

            S12 = 4.0*S(iElement,4)**2
            S13 = 4.0*S(iElement,5)**2
            S23 = 4.0*S(iElement,6)**2

            this%Pk(iElement) = this%nut%phi(iElement,1)*(2*trace + S12 + S13 + S23)

            ! Production limiter (BSL and SST)
            if(this%ransModel==1) call productionLimiter(this%Pk(iElement), 20.0*bstar*tdr*tke)

            if(this%ransModel==2) call productionLimiter(this%Pk(iElement), 10.0*bstar*tdr*tke)

        enddo

        !------------------------------------------------------------------!
        ! Correct production at boundaries, if wall function is activated  !
        !------------------------------------------------------------------!

        do iBoundary=1,numberOfWallBound

            iWall = mesh%boundaries%wallBound(iBoundary)

            if(this%tdr%bCond(iWall)==trim('omegaWallFunction')) then

                is = mesh%boundaries%startFace(iWall)
                ie = is + mesh%boundaries%nFace(iWall)-1
                patchFace = 0

                do iBFace=is,ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    tke = max(this%tke%phi(iOwner,1),0.0d0)
                    tdr = this%tdr%phi(iOwner,1)
                    d = mesh%area(iBFace)/mesh%wallDist(iBFace)

                    tw = abs(this%tauWall(patchFace,iBoundary))
                    ut = sqrt(max(0.0, this%tke%phi(iOwner,1)))*(this%Cmu)**0.25

                    this%Pk(iOwner) = tw*ut/(kappa*d)

                    !--------------------!
                    ! Production limiter !
                    !--------------------!

                    ! Production limiter (BSL and SST)
                    if(this%ransModel==1) call productionLimiter(this%Pk(iOwner), 20.0*bstar*tdr*tke)

                    if(this%ransModel==2) call productionLimiter(this%Pk(iOwner), 10.0*bstar*tdr*tke)

                enddo

            endif

        enddo

    end subroutine turbulentProductionKappaOmega

! *********************************************************************************************************************

    subroutine turbulentProductionDev(this)

    !==================================================================================================================
    ! Description:
    !! turbulentProductionDev computes the turbulent production Pk inculding the deviatoric term.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBoundary, iBFace, iOwner, patchFace, iWall, is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements,6), Ssum(numberOfElements), trace, S12, S13, S23

        real :: kappa, tw, ut, d, bstar, SMALL, tke, tdr, dev
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 0.0000000001d0

        bstar = this%bstar
        kappa = this%kappa

        !----------------------!
        ! Compute Strain tensor!
        !----------------------!

        call strainTensor(S,Ssum)

        !--------------------!
        ! Compute production !
        !--------------------!

        do iElement=1,numberOfElements

            tke = max(this%tke%phi(iElement,1),0.0d0)
            tdr = this%tdr%phi(iElement,1)

            dev = (2.0/3.0)*(velocity%phiGrad(iElement,1,1) + velocity%phiGrad(iElement,2,2)+ velocity%phiGrad(iElement,3,3))

            trace = (S(iElement,1)-dev)**2 + (S(iElement,2)-dev)**2 + (S(iElement,3)-dev)**2

            S12 = 4.0*S(iElement,4)**2
            S13 = 4.0*S(iElement,5)**2
            S23 = 4.0*S(iElement,6)**2

            this%Pk(iElement) = this%nut%phi(iElement,1)*(2*trace + S12 + S13 + S23)

            ! Production limiter (BSL and SST)
            if(this%ransModel==1) call productionLimiter(this%Pk(iElement), 20.0*bstar*tdr*tke)

            if(this%ransModel==2) call productionLimiter(this%Pk(iElement), 10.0*bstar*tdr*tke)

        enddo

        !------------------------------------------------------------------!
        ! Correct production at boundaries, if wall function is activated  !
        !------------------------------------------------------------------!

        do iBoundary=1,numberOfWallBound

            iWall = mesh%boundaries%wallBound(iBoundary)

            if(this%tdr%bCond(iWall)==trim('omegaWallFunction')) then

                is = mesh%boundaries%startFace(iWall)
                ie = is + mesh%boundaries%nFace(iWall)-1
                patchFace = numberOfElements + (is-numberOfIntFaces-1)

                do iBFace=is,ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    tke = max(this%tke%phi(iOwner,1),0.0d0)
                    tdr = this%tdr%phi(iOwner,1)
                    d = mesh%area(iBFace)/mesh%wallDist(iBFace)

                    tw = abs(this%tauWall(patchFace,iBoundary))
                    ut = sqrt(max(0.0, this%tke%phi(iOwner,1)))*(this%Cmu)**0.25

                    this%Pk(iOwner) = tw*ut/(kappa*d)

                    !--------------------!
                    ! Production limiter !
                    !--------------------!

                    ! BSL
                    if(this%ransModel==1) call productionLimiter(this%Pk(iOwner), 20.0*bstar*tdr*tke)

                    ! SST
                    if(this%ransModel==2) call productionLimiter(this%Pk(iOwner), 10.0*bstar*tdr*tke)

                enddo

            endif

        enddo

    end subroutine turbulentProductionDev

! *********************************************************************************************************************

    subroutine vorticityProduction(this)

    !==================================================================================================================
    ! Description:
    !! vorticityProduction computes the turbulent production Pk from the vorticity field as used in "-V" versions of k.omega.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBoundary, iBFace, iOwner, patchFace, iWall, is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: omega(numberOfElements)

        real :: kappa, tw, ut, d, bstar, SMALL, tke, tdr
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 0.0000000001d0
        bstar = this%bstar
        kappa = this%kappa

        !-------------------!
        ! Compute Vorticity !
        !-------------------!

        omega = rotationTensorModule()

        !--------------------!
        ! Compute production !
        !--------------------!

        do iElement=1,numberOfElements

            tdr = max(this%tke%phi(iElement,1),0.0d0)
            tke = this%tdr%phi(iElement,1)

            this%Pk(iElement) = this%nut%phi(iElement,1)*omega(iElement)**2

            ! Production limiter (BSL and SST)
            if(this%ransModel==1) call productionLimiter(this%Pk(iElement), 20.0*bstar*tdr*tke)

            if(this%ransModel==2) call productionLimiter(this%Pk(iElement), 10.0*bstar*tdr*tke)

        enddo

        !------------------------------------------------------------------!
        ! Correct production at boundaries, if wall function is activated  !
        !------------------------------------------------------------------!

        do iBoundary=1,numberOfWallBound

            iWall = mesh%boundaries%wallBound(iBoundary)

            if(this%tdr%bCond(iWall)==trim('omegaWallFunction')) then

                is = mesh%boundaries%startFace(iWall)
                ie = is + mesh%boundaries%nFace(iWall)-1
                patchFace = numberOfElements + (is-numberOfIntFaces-1)

                do iBFace=is,ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    tke = max(this%tke%phi(iOwner,1),0.0d0)
                    tdr = this%tdr%phi(iOwner,1)
                    d = mesh%area(iBFace)/mesh%wallDist(iBFace)

                    tw = abs(this%tauWall(patchFace,iBoundary))
                    ut = sqrt(max(0.0, this%tke%phi(iOwner,1)))*(this%Cmu)**0.25

                    this%Pk(iOwner) = tw*ut/(kappa*d)

                    !--------------------!
                    ! Production limiter !
                    !--------------------!

                    ! BSL
                    if(this%ransModel==1) call productionLimiter(this%Pk(iOwner), 20.0*bstar*tdr*tke)

                    ! SST
                    if(this%ransModel==2) call productionLimiter(this%Pk(iOwner), 10.0*bstar*tdr*tke)

                enddo

            endif

        enddo

    end subroutine vorticityProduction

! *********************************************************************************************************************

    subroutine omegaLowReWallFunction(this, aCb, bCb, omegaWall, iBFace, mu)

    !==================================================================================================================
    ! Description:
    !! omegaLowReWallFunction implements the low Re omega wall function. It sets a known value of omega at the wall boundaries.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBFace
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb, corr

        real :: omegaWall, d, mu, c_b1
    !------------------------------------------------------------------------------------------------------------------

        C_b1 = this%C_b1

        !---------------------------------!
        ! Set omega value at cell centers !
        !---------------------------------!

        d = mesh%area(iBFace)/mesh%wallDist(iBFace)

        if(flubioTurbModel%ransModel==2) then
            omegaWall = (60.0*viscos)/(C_b1*d**2)
        else
            omegaWall = (6.0*viscos)/(C_b1*d**2)
        endif

        ! note that mf = 0.0 where wall function is applied
        corr = -mf(iBFace,1) + mu*mesh%walldist(iBFace)

        aCb =  mu*mesh%walldist(iBFace)
        bCb =  corr*omegaWall

    end subroutine omegaLowReWallFunction

! *********************************************************************************************************************

    function omegaWallFunction(this, iBFace) result(bCb)

    !==================================================================================================================
    ! Description:
    !! omegaWallFunction sets the value of omega at the cell centers of boundary cells (only of type wall)
    !! using the automatic wall treatment.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBFace, iOwner
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb

        real :: C_b1, C_mu, Cmu025, kappa, yPlus

        real :: omegaVis, omegaLog, omegaWall, d, tke
    !------------------------------------------------------------------------------------------------------------------

        kappa = this%kappa
        C_b1 = this%C_b1
        C_mu = this%Cmu
        Cmu025 = C_mu**0.25

        !----------------------------------!
        ! find omega value at cell centers !
        !----------------------------------!

        iOwner = mesh%owner(iBFace)

        yPlus = this%yPlus(iOwner)
        d = mesh%area(iBFace)/mesh%wallDist(iBFace)
        tke = this%tke%phi(iOwner,1)

        omegaVis = (6.0*viscos)/(C_b1*d**2)
        omegaLog = sqrt(max(tke,0.0))/(Cmu025*kappa*d)

        ! Omega at walls, using advanced blending methods
        omegaWall = blend(omegaVis, omegaLog, this%wallFunctionBlending, yPlus, threshold=this%yPlusLaminar, n=2.0)

        bCb = omegaWall

    end function omegaWallFunction

! *********************************************************************************************************************

    subroutine applyOmegaWallFunction(this)

    !==================================================================================================================
    ! Description:
    !! applyOmegaWallFunction applies the wall function if they are required.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iOwner, iBFace, iComp, is ,ie, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            ! High Re wall Function
            if(trim(this%tdr%bCond(iBoundary))=='omegaWallFunction') then

                do iBFace=is, ie

                    iOwner = mesh%owner(iBFace)
                    bCb = this%tdrWallFunction(iBFace)

                    ! Set wall function value in the first cell
                    this%tdrEqn%aC(iOwner,1) = 1.0
                    this%tdrEqn%anb(iOwner)%coeffs = 0.0
                    this%tdrEqn%bC(iOwner,1) = bCb

                enddo

            endif

        enddo

    end subroutine applyOmegaWallFunction

! *********************************************************************************************************************

    subroutine applyOmegaLowReWallFunction(this)

    !==================================================================================================================
    ! Description:
    !! applyOmegaWallFunction applies the wall function if they are required.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iComp, is ,ie, patchFace, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb, omegaWall
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            if(trim(this%tke%bCond(iBoundary))=='omegaLowReWallFunction') then

                do iBFace=is, ie

                    patchFace = patchFace+1

                    ! Low Re wall Function
                    call this%tdrLowReWallFunction(aCb, bCb, omegaWall, iBFace, nu%phi(patchFace,1))

                    ! Set omega wall value
                    this%tdr%phi(patchFace,1) = omegaWall

                end do

            end if

        end do

    end subroutine applyOmegaLowReWallFunction

! *********************************************************************************************************************

    subroutine tkeWallFunction(this, aCb, bCb, tkeWall, iBFace, mu)

    !==================================================================================================================
    ! Description:
    !! tkeWallFunction implements the tke wall function (the wall treatment is automatic)
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBFace, iOwner
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb, corr

        real :: C, Ck, Bk, Cf, Ceps2, Cmu25

        real :: tkeWall, mu, d, utau, yPlus
    !------------------------------------------------------------------------------------------------------------------

        iOwner = mesh%owner(iBFace)

        Cmu25 = this%Cmu**0.25

        d = mesh%area(iBFace)/mesh%wallDist(iBFace)
        utau =  Cmu25*sqrt(this%tke%phi(iOwner,1))
        yPlus = utau*d/mu

        if(yPlus>this%yPlusLaminar) then

            Ck = -0.416;
            Bk = 8.366;
            tkeWall= (Ck/this%kappa)*log(yPlus) + Bk;
        else
             C = 11.0;
             Ceps2 = 1.9;
             Cf = (1.0/(yPlus + C)**2 + 2.0*yPlus/C**3 - 1.0/C**2);
             tkeWall = (2400.0/Ceps2**2)*Cf;
        end if

        tkeWall = tkeWall*utau**2

        ! note that mf = 0.0 where wall function is applied
        corr = -mf(iBFace,1) + mu*mesh%walldist(iBFace)

        aCb =  mu*mesh%walldist(iBFace)
        bCb =  corr*tkeWall

    end subroutine tkeWallFunction

! *********************************************************************************************************************

    subroutine applyTkeWallFunction(this)

    !==================================================================================================================
    ! Description:
    !! applyTkeWallFunction applies the tke wall function if required.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iOwner, iBFace, iComp, is ,ie, patchFace, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb, bCb, tkeWall
    !------------------------------------------------------------------------------------------------------------------

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            if(trim(this%tke%bCond(iBoundary))=='tkeWallFunction') then

                do iBFace=is, ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    ! Low Re wall Function
                    call this%tkeWallFunction(aCb, bCb, tkeWall, iBFace, nu%phi(patchFace,1))

                    ! Correct coefficients
                    this%tkeEqn%aC(iOwner,:) = this%tkeEqn%aC(iOwner,:) + aCb
                    this%tkeEqn%bC(iOwner,:) = this%tkeEqn%bC(iOwner,:) + bCb

                    ! Update wall value
                    this%tke%phi(patchFace,1) = tkeWall

                enddo

            endif
        enddo

    end subroutine applyTkeWallFunction

! *********************************************************************************************************************

    subroutine blendingFunction(this, F1, F2, iElement)

    !==================================================================================================================
    ! Description:
    !! blendingFunction computes the blending function F1 and F2 used in many turbulence models of the k-oemga family.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: F1, F2

        real :: a, b, c, c2, e, CD, gamma1, gamma2, SMALL

        real :: tke, d, tdr, density, gk(3), gw(3)
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        SMALL = 1e-10

        tke = this%tke%phi(iElement,1)
        tdr = this%tdr%phi(iElement,1)
        d = this%wd%dperp(iElement)
        density = rho%phi(iElement,1)
        gk = this%tke%phiGrad(iElement,:,1)
        gw = this%tdr%phiGrad(iElement,:,1)

        ! Compute blending functions for BSL and SST models
        a = sqrt(max(tke,0.0))/(this%bstar*tdr*d)
        b = 500.0d0*viscos/(tdr*d**2)
        c = max(2*a,b)
        c2 = max(a,b)
        gamma2 = c

        ! CD
        a = dot_product(gk, gw)
        a = 2.0*density*this%sigma_o2*a/(tdr+SMALL)
        CD = max(a,SMALL)

        ! Gamma1
        a = 4.0*density*this%sigma_o2*max(tke, 0.0)
        b = CD*d**2
        e = a/(CD+SMALL)
        gamma1 = min(c2, e)

        ! F1 and F2
        F1 = tanh(gamma1**4)
        F2 = tanh(gamma2**2)

    end subroutine blendingFunction

! *********************************************************************************************************************

    subroutine kappaOmegaWriteToFile(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmegaWriteToFile writes the k-omega fields to output.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tke%writeToFile()
        call this%tdr%writeToFile()
        call this%nut%writeToFile()
        if(this%outputYPlus==1) call this%writeWallYPlus()

    end subroutine kappaOmegaWriteToFile

! *********************************************************************************************************************

    subroutine kappaOmegaResidual(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmegaResidual writes the residuals to file and to standard output.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tkeEqn%printresidual()
        call this%tdrEqn%printresidual()

        call this%tkeEqn%writeresidual()
        call this%tdrEqn%writeresidual()

    end subroutine kappaOmegaResidual

! *********************************************************************************************************************

    subroutine kappaOmegaVerbose(this)

    !==================================================================================================================
    ! Description:
    !! kappaOmegaVerbose writes info to the standard output.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        call this%tke%fieldVerbose()
        call this%tdr%fieldVerbose()
        call this%nut%fieldVerbose()

    end subroutine kappaOmegaVerbose

! *********************************************************************************************************************

    subroutine printCoeffsKappaOmega(this)

    !==================================================================================================================
    ! Description:
    !! printCoeffsKappaOmega prints coefficients to screen.
    !==================================================================================================================

        class(kw1988) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,'(A)') '!----------------------------------!'
            write(*,'(A)') '! Kappa-Omega Wilcox coefficients: !'
            write(*,'(A)') '!----------------------------------!'
            write(*,*)
            write(*,'(A, f6.4)') ' Cmu = ', this%Cmu
            write(*,'(A, f6.4)') ' kappa = ', this%kappa
            write(*,'(A, f6.4)') ' Ca1 = ', this%C_a1
            write(*,'(A, f6.4)') ' Ca2 = ', this%C_a2
            write(*,'(A, f6.4)') ' Cb1 = ', this%C_b1
            write(*,'(A, f6.4)') ' Cb2 = ', this%C_b2
            write(*,'(A, f6.4)') ' bstar = ', this%bstar
            write(*,'(A, f6.4)') ' sigma_k1 = ', this%sigma_k1
            write(*,'(A, f6.4)') ' sigma_k2 = ', this%sigma_k2
            write(*,'(A, f6.4)') ' sigma_o1 = ', this%sigma_o1
            write(*,'(A, f6.4)') ' sigma_o2 = ', this%sigma_o2
            write(*,'(A)') '!---------------------------------!'
            write(*,*)
        end if

    end subroutine printCoeffsKappaOmega

end module kappaOmegaWilcox
