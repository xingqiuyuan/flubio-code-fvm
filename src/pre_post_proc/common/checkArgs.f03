!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

    subroutine check_np(args, nargs)

        implicit none

        character(*) :: args(nargs)
    !------------------------------------------------------------------------------------------------------------------

        integer :: p, nargs, flag
    !------------------------------------------------------------------------------------------------------------------

        flag=0

        do p=1,nargs

            if(args(p)=='-np') flag=1

        enddo

        if(flag==0) then
            write(*,*) 'FLUBIO: missing argument "np". Specify the number of process used in the simulation. '
            stop
        endif

    end subroutine check_np

! *********************************************************************************************************************

    subroutine check_format(args, nargs)

        implicit none

        character(*) :: args(nargs)
    !------------------------------------------------------------------------------------------------------------------

        integer :: p, nargs, flag
    !------------------------------------------------------------------------------------------------------------------

        flag=0

        do p=1,nargs

            if(args(p)=='-vtk' .or. args(p)=='-plt' ) flag=1

        enddo

        if(flag==0) then
            write(*,*) 'FLUBIO: missing argument "vtk" or "plt". Specify an output format'
            stop
        endif

    end subroutine check_format

! *********************************************************************************************************************

    subroutine check_field(args, nargs)

        implicit none

        character(*) :: args(nargs)
    !------------------------------------------------------------------------------------------------------------------

        integer :: p, nargs, flag
    !------------------------------------------------------------------------------------------------------------------

        flag=0

        do p=1,nargs

            if(args(p)=='-start' .or. args(p)=='-end' .or. args(p)=='-time' .or. args(p)=='-all') flag=1

        enddo

        if(flag==0) then
            write(*,*) 'FLUBIO: missing argument "time" or "start" or "end". Specify the field(s) to process.'
            stop
        endif

    end subroutine check_field

! *********************************************************************************************************************

    subroutine check_boundary(args, nargs)

        implicit none

        character(*) :: args(nargs)
    !------------------------------------------------------------------------------------------------------------------

        integer :: p, nargs, flag
    !------------------------------------------------------------------------------------------------------------------

        flag=0

        do p=1,nargs
            if(args(p)=='-boundary') flag=1
        enddo

        if(flag==0) then
            write(*,*) 'FLUBIO: missing argument "boundary". Specify the boundary to process'
            stop
        endif

    end subroutine check_boundary

! *********************************************************************************************************************