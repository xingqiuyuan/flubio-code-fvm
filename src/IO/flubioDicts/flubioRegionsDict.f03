!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module meshRegions

!===================================================================================================================
! Description:
!! This moudle implements the dictionary storing the entries of settings/meshRegions.
!===================================================================================================================

    use generalDictionaries
    use globalTimeVar

    implicit none

    type, extends(generalDict) :: meshRegionsDict

        integer :: numberOfRegions
        !! number of regions, if any

        logical :: found
        !! flag if regions exists

    contains
        procedure :: set => setRegions

    end type meshRegionsDict

contains

    subroutine setRegions(this)

    !===================================================================================================================
    ! Description:
    !! setRegions parses and stores the regions from settings/meshRegions.
    !==================================================================================================================

        class(meshRegionsDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name

        character(len=:), allocatable :: optName
        !! Option name
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/meshRegions'

       ! Initialise the JSON dictionary
        inquire(file=trim(dictName), exist=found)

        if (.not. found) then
            this%found = .false.
            this%numberOfRegions = 0
        else
            this%found = .true.
            call this%initJSON(dictName)
            call this%json%info('Regions', n_children=this%numberOfRegions)
        end if

    end subroutine setRegions

end module meshRegions