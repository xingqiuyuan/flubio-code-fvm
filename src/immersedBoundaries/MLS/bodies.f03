module bodies

    use fossil
    use lagrangianMarker
    use rigidBodyDynamics, only: eulerToAngular

    implicit none

    type, public :: prescribedMotion

        !character(len=:), dimension(:), allocatable  :: x
        !character(len=:), dimension(:), allocatable  :: xDot 
        !character(len=:), dimension(:), allocatable  :: omega
        !character(len=:), dimension(:), allocatable  :: quaternions
        !character(len=:), dimension(:), allocatable  :: eulerAngles 

        character(len=275) :: x(3)
        character(len=275) :: xDot(3) 
        character(len=275) :: omega(3)
        character(len=275) :: quaternions(4)
        character(len=275) :: eulerAngles (3)

    contains 
        procedure :: initToZero
    end type prescribedMotion

    type, public :: body2D

        character(len=:), allocatable :: name
        !! name of the body

        character(len=:), allocatable :: fileName
        !! file name

        real, dimension(:,:), allocatable :: bodyPoints
        !! lagrangian points composing the body
        
        real, dimension(:), allocatable :: ds
        !! lagrangian point spacing

        type(lagrangianPoint), dimension(:), allocatable :: xlag
        !! Lagrangian points
       
        type(flubioDynamicList) :: ownedPoints
        !! point hosted by the current processor rank

        integer :: numberOfLagrangianPoints
        !! number of lagrangian points hosted by the rank

        real, dimension(:,:), allocatable :: ulag
        !! Lagrangian velocity

        !! RIGID BODY MOTION !! To be implemented, not far away

        character(len=:), allocatable :: motionType
        !! motion type

        type(prescribedMotion) :: motionExpression
        !! motion expression as combination of x,y,z,t

        real :: mass 
        !! body mass 

        real :: InertiaTensor(3,3)
        !! tensor of intertia 

        real :: xg(3)
        !! center of mass

        real :: xcor(3)
        !! center of rotation

        real :: Force(3) 
        !! force on the body

        real :: Moment(3)
        !! Moment on the body

        real :: state(16,4)
        !! rigid body state, stored up to four time steps

    contains
        procedure :: loadPoints
        procedure :: computePointsCentroid
        procedure :: checkNumberOfLagrangianPoints => checkNumberOfLagrangianPoints2D
        procedure :: findOwnership => findMarkerOwnership
        procedure :: setLagrangianPointCoordinates2D
        procedure :: writeBodyPoints
        procedure :: findSupportCells => findSupportCells2D
        procedure :: interpolateField => interpolateField2D
        procedure :: lagrangianForces => lagrangianForces2D
        procedure :: eulerianForces => eulerianForces2D
        procedure :: bodyForces => bodyForces2D
        procedure :: moveBody => moveBody2D
        procedure :: updateBodyPosition => updateBodyPosition2D
        procedure :: updateBodyVelocity => updateBodyVelocity2D
        procedure :: getStateFromPrescribedMotion => getStateFromPrescribedMotion2D
        procedure :: getStateFromPrescribedPosition => getStateFromPrescribedPosition2D
        procedure :: writeForceToFile => writeForceToFile2D
        procedure :: writeStateToFile => writeStateToFile2D
    end type body2D

    type, public :: body3D

        character(len=:), allocatable :: name
        !! name of the body

        character(len=:), allocatable :: fileName
        !! STL file name

        type(lagrangianPoint), dimension(:), allocatable :: xlag
        !! Lagrangian points

        type(file_stl_object):: ibmSTLFile
        !! STL file object

        type(surface_stl_object) :: ibmSurface
        !! STL surface object

        type(flubioDynamicList) :: ownedPoints
        !! point hosted by the current processor rank

        integer :: numberOfLagrangianPoints
        !! number of lagrangian points hosted by the rank

        real, dimension(:,:), allocatable :: ulag
        !! Lagrangian velocity

        !! RIGID BODY MOTION !! To be implemented, not far away

        character(len=:), allocatable :: motionType
        !! motion type

        type(prescribedMotion) :: motionExpression
        !! motion expression as combination of x,y,z,t

        real :: mass 
        !! body mass 

        real :: InertiaTensor(3,3)
        !! tensor of intertia 

        real :: xg(3)
        !! center of mass

        real :: xcor(3)
        !! center of rotation

        real :: Force(3) 
        !! force on the body

        real :: Moment(3)
        !! Moment on the body

        real :: state(16,4)
        !! rigid body state, stored up to four time steps

    contains
        procedure :: initialiseBody3D
        procedure :: loadSTL
        procedure :: checkNumberOfLagrangianPoints
        procedure :: computeSTLCentroid
        procedure :: writeFieldsOnBody
        procedure :: findOwnership => findFacetOwnership
        procedure :: setLagrangianPointCoordinates
        procedure :: findSupportCells
        procedure :: interpolateField
        procedure :: lagrangianForces
        procedure :: eulerianForces
        procedure :: bodyForces
        procedure :: moveBody
        procedure :: updateBodyPosition
        procedure :: updateBodyVelocity
        procedure :: getStateFromPrescribedMotion
        procedure :: getStateFromPrescribedPosition 
        procedure :: writeForceToFile
        procedure :: writeStateToFile
    end type body3D

contains 

!**********************************************************************************************************************
    
    subroutine initialiseBody3D(this)
    
    !==================================================================================================================
    ! Description:
    !! initialiseBody3D initialize a 3D body.
    !==================================================================================================================
                    
        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: isize
    !------------------------------------------------------------------------------------------------------------------   

        ! Load STL file 
        call this%loadSTL()

        call this%ownedPoints%initDynList(listSizeOf=isize)                                    

        ! Rigid body
        this%xg = this%computeSTLCentroid()
        this%Force = 0.0 
        this%Moment = 0.0

        ! Compute the state vector
        if(this%motionType=="prescribed") then
            this%state = 0.0 
            this%state(:,1) = this%getStateFromPrescribedMotion() 
        elseif(lowercase(this%motionType)=="prescribedposition") then 
            this%state = 0.0 
            this%state(:,1) = this%getStateFromPrescribedPosition() 
        else 
            this%state = 0.0 
        endif

    end subroutine initialiseBody3D
                
!**********************************************************************************************************************    
  
    subroutine loadPoints(this)

    !==================================================================================================================
    ! Description:
    !! loadPoints loads lagrangian points for a target file.
    !==================================================================================================================
        
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------
            
        integer :: n, nPoints, stat
    !------------------------------------------------------------------------------------------------------------------

        real :: dx, dy

        real, dimension(:), allocatable :: xc, yc
    !------------------------------------------------------------------------------------------------------------------

        open(1, file="grid/stl/"//trim(this%fileName))
    
            read(1,*, iostat=stat) nPoints
            allocate(this%bodyPoints(nPoints,3))

            do n=1,nPoints
                read(1,*, iostat=stat) this%bodyPoints(n,1:3)
                if(stat/=0) exit
            enddo

        close(1)
    
        ! Allocate ds
        allocate(this%ds(nPoints))
        this%ds = 0.0

        ! find the central point between two consecutive lagrangian points
        allocate(xc(nPoints))
        allocate(yc(nPoints))
        xc = 0.0 
        yc = 0.0

        do n=1,nPoints-1
            xc(n) = 0.5*(this%bodyPoints(n+1,1) + this%bodyPoints(n,1)) 
            yc(n) = 0.5*(this%bodyPoints(n+1,2) + this%bodyPoints(n,2))
        enddo
        xc(nPoints) = 0.5*(this%bodyPoints(nPoints,1) + this%bodyPoints(1,1))
        yc(nPoints) = 0.5*(this%bodyPoints(nPoints,2) + this%bodyPoints(1,2))

        ! Compute ds to be use in the Cl calculation
        dx = xc(nPoints) - xc(1)
        dy = yc(nPoints) - yc(1)
        this%ds(1) = sqrt(dx**2 + dy**2)
        do n=2,nPoints
            dx = xc(n) - xc(n-1)   
            dy = yc(n) - yc(n-1)
            this%ds(n) = sqrt(dx**2 + dy**2)
            !write(*,*) 'ds = ', this%ds(n), 'xc = ', xc(n), yc(n), 'id = ', id, nPoints
        enddo 

        call this%ownedPoints%initDynList(listSizeOf=n)         

        ! Rigid body
        this%Force = 0.0 
        this%Moment = 0.0

        ! Compute the state vector
        if(this%motionType=="prescribed") then
            this%state = 0.0 
            this%state(:,1) = this%getStateFromPrescribedMotion()
        elseif(lowercase(this%motionType)=="prescribedposition") then 
            this%state = 0.0 
            this%state(:,1) = this%getStateFromPrescribedPosition() 
        else 
            this%state = 0.0 
        endif

        this%xg = this%computePointsCentroid()

    end subroutine loadPoints
    
!**********************************************************************************************************************

    subroutine checkNumberOfLagrangianPoints2D(this)

    !==================================================================================================================
    ! Description:
    !! checkNumberOfLagrangianPoints2D checks that the number of lagrangian points is conserved during the motion.
    !==================================================================================================================
        
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        integer :: ierr 
        !! error flag

        integer :: nPoints
        !! global number of lagrangian points
    !------------------------------------------------------------------------------------------------------------------

        call mpi_reduce(this%numberOfLagrangianPoints, nPoints, nid, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

        if(id==0 .and. .not. nPoints == size(this%bodyPoints(:,1))) then 
            call flubioStopMsg('ERROR: number of lagrangian points has changed during the simulation!')
        endif

    end subroutine checkNumberOfLagrangianPoints2D

!**********************************************************************************************************************  

    function computePointsCentroid(this) result(xg)

    !==================================================================================================================
    ! Description:
    !! computePointsCentroid computes the centroid of a 2D body.
    !==================================================================================================================
            
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        integer :: n, nPoints
    !------------------------------------------------------------------------------------------------------------------

        real :: xg(3)
    !------------------------------------------------------------------------------------------------------------------

        nPoints = size(this%bodyPoints(:,1)) 

        xg = 0.0
        do n=1,nPoints
            xg = xg + this%bodyPoints(n,1:3)
        enddo 
        
        xg = xg/nPoints

    end function computePointsCentroid

!**********************************************************************************************************************

    subroutine loadSTL(this)
    
    !==================================================================================================================
    ! Description:
    !! loadSTL loads stls files describing 3D bodies.
    !==================================================================================================================
            
        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------
                
        integer :: n, nPoints, stat
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg("FLUBIO: loading stl file '"// this%fileName //"'")
            
        call this%ibmSTLFile%load_from_file(facet=this%ibmSurface%facet, file_name="grid/stl/"//this%fileName, & 
                                            guess_format=.true.)
        call this%ibmSurface%analize()    
        call this%ibmSurface%sanitize_normals()

    end subroutine loadSTL
    
!**********************************************************************************************************************

    subroutine checkNumberOfLagrangianPoints(this)

    !==================================================================================================================
    ! Description:
    !! checkNumberOfLagrangianPoints2D checks that the number of lagrangian points is conserved during the motion.
    !==================================================================================================================
            
        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr 
        !! error flag

        integer :: nPoints
        !! global number of lagrangian points
    !------------------------------------------------------------------------------------------------------------------

        call mpi_reduce(this%numberOfLagrangianPoints, nPoints, nid, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD, ierr)

        if(id==0 .and. .not. nPoints == this%ibmSurface%facets_number) then 
            call flubioStopMsg('ERROR: number of lagrangian points has changed during the simulation!')
        endif

    end subroutine checkNumberOfLagrangianPoints

!**********************************************************************************************************************
    
    function computeSTLCentroid(this) result(xg)

    !===================================================================================================================
    ! Description:
    !! loadSTL loads stls files describing 3D bodies.
    !==================================================================================================================
            
        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------

        real :: xg(3)
    !------------------------------------------------------------------------------------------------------------------

        xg = 0.0
        do n=1,this%ibmSurface%facets_number
            xg(1) = xg(1) + this%ibmSurface%facet(n)%centroid%x
            xg(2) = xg(2) + this%ibmSurface%facet(n)%centroid%y
            xg(3) = xg(3) + this%ibmSurface%facet(n)%centroid%z
        enddo

        xg = xg/this%ibmSurface%facets_number

        ! Override fossi centroid, it looks buggy
        this%ibmSurface%centroid%x = xg(1)
        this%ibmSurface%centroid%y = xg(2)
        this%ibmSurface%centroid%z = xg(3)

    end function computeSTLCentroid

!**********************************************************************************************************************
    
    subroutine writeFieldsOnBody(this, surfaceFields, fieldNames, nFields)
    
        use penf
        use vtk_fortran

    !==================================================================================================================
    ! Description:
    !! loadSTL loads stls files describing 3D bodies.
    !==================================================================================================================

        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(vtk_file) :: vtk_field_file
        !! vtu file for each partition

        type(pvtk_file) :: pvtk_field_file
        !! pvtu file
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: file_number
		!! file number as string

        character(len=10) :: rank_number
		!! rank number as string

        character(len=:), allocatable :: fname
		!! file name

		character(len=:), allocatable :: dirName
		!! folder to save the output

        character(len=*) :: fieldNames(nFields)
		!! name of the fields
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
        !! target index

        integer :: n
        !! loop index

        integer :: k 
        !! loop index

        integer :: is 
        !! starting index 

        integer :: ie 
        !! ending index

        integer :: nf 
        !! number of facet, equal to the number of lagrangian points hosted by the rank

        integer :: ierr
        !! error flag

        integer :: nFields
        !! number of fields

        integer :: vrt(3)
        !! facet vertex id

        integer(I4P) :: connect(this%numberOfLagrangianPoints*3)
        !! connectivities

        integer(I1P) :: cellType(this%numberOfLagrangianPoints)
        !! cell type, always equal to 5 (triangular elements)

        integer(I4P) :: offset(this%numberOfLagrangianPoints)
        !! offset to read connectivity
    !------------------------------------------------------------------------------------------------------------------

        real :: listOfPoints(this%numberOfLagrangianPoints*3,3)
        !! list of all points composing the STL

        real :: surfaceFields(this%numberOfLagrangianPoints, nFields)
        !! fields to be plot on the surface
    !------------------------------------------------------------------------------------------------------------------

        logical :: success 
        !! error flag

		logical :: dirExists
		!! flag to check if a directory exists
	!------------------------------------------------------------------------------------------------------------------

        nf = this%numberOfLagrangianPoints

        ! To string
        write(file_number, '(i0)') itime
        write(rank_number, '(i0)') id

        ! Create a directory for the field. If it does not exists create it.
        dirName = 'postProc/VTKfields/Parallel/'//trim(file_number)
        inquire(file=trim(dirName)//'/.', exist=dirExists)

        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim(dirName)))

        fname = 'postProc/VTKfields/Parallel/'//trim(file_number)//'/'//trim(this%name)//'-d'//trim(rank_number)//'.vtu'
        
        ! Fill the list of points
        listOfPoints = 0.0
        connect = -1
        offset = -1
        cellType = 5
        ie = 0

        ! Loop over facet with the owner index
        do n=1,nf

            is = ie+1
            ie = is+2

            call this%ownedPoints%list%getat(n, i, success)

            ! Loop over facet vertex
            do k=1,3

                listOfPoints(is + k-1,1) = this%ibmSurface%facet(i)%vertex(k)%x
                listOfPoints(is + k-1,2) = this%ibmSurface%facet(i)%vertex(k)%y
                listOfPoints(is + k-1,3) = this%ibmSurface%facet(i)%vertex(k)%z
                
                vrt(k) = is+k-1

            enddo

            offset(n) = 3*n
            connect(is:ie) = vrt-1

        enddo

        ierr = vtk_field_file%initialize(format='ascii', filename=fname, mesh_topology='UnstructuredGrid')

        ! Write sizes
		ierr = vtk_field_file%xml_writer%write_piece(np=nf*3, nc=nf)

		! Write points
		ierr = vtk_field_file%xml_writer%write_geo(np=nf*3,   &
												   nc=nf, &
				     					           x=listOfPoints(:,1), &
											       y=listOfPoints(:,2), &
												   z=listOfPoints(:,3))

        ! Write connectivities
		ierr = vtk_field_file%xml_writer%write_connectivity(nc=nf,  &
				                                            connectivity=connect, &
														    offset=offset,        &
															cell_type=cellType)

        ! Define data
        ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='open')	

        ! Write Field
        do n=1,nFields
            ierr = vtk_field_file%xml_writer%write_dataarray(data_name=trim(fieldNames(n)), &
                                                             x=surfaceFields(1:nf, n))
        enddo

        ! Close vtu file
        ierr = vtk_field_file%xml_writer%write_dataarray(location='cell', action='close')
        ierr = vtk_field_file%xml_writer%write_piece()
        ierr = vtk_field_file%finalize()

        ! Write pvtu file to link all the vtus
        if(id == 0) then

            ! Write the pvtu file
            fname = 'postProc/VTKfields/Parallel/'//trim(file_number)//'/'//trim(this%name)//'.pvtu'
            ierr = pvtk_field_file%initialize(filename=fname, mesh_topology='PUnstructuredGrid', mesh_kind="Float64")

            ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='open')

            do n=1,nFields

                ierr = pvtk_field_file%xml_writer%write_parallel_dataarray(data_name=fieldNames(n), &
                                                                           data_type='Float64',     &
                                                                           number_of_components=1)
            end do

            ierr = pvtk_field_file%xml_writer%write_dataarray(location='cell', action='close')

            do i=1,nid
                write(rank_number,'(i0)') i-1
                fname = './'//trim(this%name)//'-d'//trim(rank_number)// '.vtu'
                ierr = pvtk_field_file%xml_writer%write_parallel_geo(source=fname)
            end do

            ierr = pvtk_field_file%finalize()

        end if

    end subroutine writeFieldsOnBody

!**********************************************************************************************************************

    subroutine findFacetOwnership(this)
    
    !==================================================================================================================
    ! Description:
    !! findFacetOwnership finds out the processor ownership of the facet.
    !==================================================================================================================
                
        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=20) :: fieldName(1)
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, nPoints
    
        integer :: index
    !------------------------------------------------------------------------------------------------------------------
    
        real :: point(3)

        real, dimension(:,:), allocatable :: surfaceField
    !------------------------------------------------------------------------------------------------------------------
       
        ! Clear points if any, the body might move in the domain
        call this%ownedPoints%clearList()
    
        ! Reallocate list
        call this%ownedPoints%initDynList(n)
    
        do n=1,this%ibmSurface%facets_number

            point(1) = this%ibmSurface%facet(n)%centroid%x
            point(2) = this%ibmSurface%facet(n)%centroid%y
            point(3) = this%ibmSurface%facet(n)%centroid%z

            ! N.B : if the body is very close to a boundary, you might end up picking a boundary point as closest point
            index = mesh%findPointOwnership(point)
            
            if(index>-1) call this%ownedPoints%addLast(n)
    
        enddo    

        this%numberOfLagrangianPoints = this%ownedPoints%getSize()

        if(allocated(this%ulag)) deallocate(this%ulag)
        allocate(this%ulag(this%numberOfLagrangianPoints,3-bdim))
        this%ulag = 0.0 

        ! Write the decomposition
        fieldName = "facet_ownership"
        allocate(surfaceField(this%numberOfLagrangianPoints,1))
        surfaceField(:,1) = real(id)

        if(mod(itime,25)==0) then 
            call this%writeFieldsOnBody(surfaceFields=surfaceField, fieldNames=fieldName, nFields=1)
        endif 

    end subroutine findFacetOwnership
            
!**********************************************************************************************************************
    
    subroutine findMarkerOwnership(this)
    
    !==================================================================================================================
    ! Description:
    !! findMarkerOwnership finds out the processor ownership of the point.
    !==================================================================================================================
                    
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------
                        
        integer :: n, nPoints
        
        integer :: index
    !------------------------------------------------------------------------------------------------------------------
        
        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------
        
        ! Clear points if any, the body might move in the domain
        call this%ownedPoints%clearList()
       
        ! Reallocate list, the argument just give sizeof(int)
        call this%ownedPoints%initDynList(n)

        do n=1,size(this%bodyPoints(:,1))
        
            point(1:3) = this%bodyPoints(n,1:3)
    
            ! N.B : if the body is very close to a boundary, you might end up picking a boundary point a closest point
            index = mesh%findPointOwnership(point)
            if(index>-1) call this%ownedPoints%addLast(n)
         
        enddo    
        
        ! Each processor takes his points
        this%numberOfLagrangianPoints = this%ownedPoints%getSize()
    
        ! Reallcoate lagrangian points velocity
        if(allocated(this%ulag)) deallocate(this%ulag)
        allocate(this%ulag(this%numberOfLagrangianPoints,3))
        this%ulag = 0.0 

    end subroutine findMarkerOwnership

!**********************************************************************************************************************

    subroutine setLagrangianPointCoordinates(this, lambdas, functionType)

    !==================================================================================================================
    ! Description:
    !! setLagrangianPointCoordinates assing the lagrangian point coordinates for each owned points
    !==================================================================================================================
             
        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------    
 
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: l, marker
    !------------------------------------------------------------------------------------------------------------------

        real :: xmarker(3)

        real :: lambdas

        real :: a
    !------------------------------------------------------------------------------------------------------------------

        logical :: success 
    !------------------------------------------------------------------------------------------------------------------        

        if(allocated(this%xlag)) deallocate(this%xlag)
        allocate(this%xlag(this%numberOfLagrangianPoints))
         
        do l = 1, this%numberOfLagrangianPoints
            
            call this%ownedPoints%list%getat(l, marker, success)

            xmarker(1) = this%ibmSurface%facet(marker)%centroid%x
            xmarker(2) = this%ibmSurface%facet(marker)%centroid%y
            xmarker(3) = this%ibmSurface%facet(marker)%centroid%z

            call this%xlag(l)%setLagrangianPoint(xmarker, this%ibmSurface%facet(marker)%det)
            this%xlag(l)%lambdas = lambdas
            this%xlag(l)%functionType = functionType
            this%xlag(l)%area = 0.5*sqrt(this%ibmSurface%facet(marker)%det)

        enddo     

    end subroutine setLagrangianPointCoordinates

!**********************************************************************************************************************

    subroutine setLagrangianPointCoordinates2D(this, lambdas, functionType)

    !==================================================================================================================
    ! Description:
    !! setLagrangianPointCoordinates2D assing the lagrangian point coordinates for each owned points.
    !==================================================================================================================
                 
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------    
     
        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: l, marker
    !------------------------------------------------------------------------------------------------------------------
    
        real :: xmarker(3)
    
        real :: lambdas
    
        real :: area
    !------------------------------------------------------------------------------------------------------------------
    
        logical :: success 
    !------------------------------------------------------------------------------------------------------------------        
    
        if(allocated(this%xlag)) deallocate(this%xlag)
        allocate(this%xlag(this%numberOfLagrangianPoints))
             
        do l = 1,this%numberOfLagrangianPoints
                
            call this%ownedPoints%list%getat(l, marker, success)
    
            xmarker(1:3) = this%bodyPoints(marker,1:3)
            area = this%ds(marker)

            call this%xlag(l)%setLagrangianPoint(xmarker, area)
            this%xlag(l)%lambdas = lambdas
            this%xlag(l)%functionType = functionType

        enddo     

    end subroutine setLagrangianPointCoordinates2D

!**********************************************************************************************************************

    subroutine writeBodyPoints(this)

    !==================================================================================================================
    ! Description:
    !! writeBodyPoints writes to file lagrangian points.
    !==================================================================================================================
                 
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: file_number
    !------------------------------------------------------------------------------------------------------------------

        integer :: l, marker
    !------------------------------------------------------------------------------------------------------------------
    
        logical :: dirExists
    !------------------------------------------------------------------------------------------------------------------

        write(file_number, '(i0)') itime

        inquire(file='postProc/ibm/2D/.', exist=dirExists)
        if(.not. dirExists) call execute_command_line ('mkdir -p ' // adjustl(trim('postProc/ibm/2D')))

        if(id==0) then
            open(1, file='postProc/ibm/2D/'//trim(this%name)//'-'//trim(file_number)//'.dat')
            do l=1,size(this%bodyPoints(:,1))
                write(1,*) this%bodyPoints(l,1:3)
            enddo
        endif

    end subroutine writeBodyPoints

!**********************************************************************************************************************

    subroutine findSupportCells(this)

    !==================================================================================================================
    ! Description:
    !! findSupportCells finds the support cells for every lagrangian point.
    !==================================================================================================================

        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------    

        integer :: l
    !------------------------------------------------------------------------------------------------------------------

            do l = 1,this%numberOfLagrangianPoints
                call this%xlag(l)%findSupport() 
            enddo     

    end subroutine findSupportCells

!**********************************************************************************************************************

    subroutine findSupportCells2D(this)

    !==================================================================================================================
    ! Description:
    !! findSupportCells2D finds the support cells for every lagrangian point in 2D.
    !==================================================================================================================

        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------    

        integer :: l
    !------------------------------------------------------------------------------------------------------------------

        do l = 1,this%numberOfLagrangianPoints
            call this%xlag(l)%findSupport() 
        enddo     

    end subroutine findSupportCells2D

!**********************************************************************************************************************

    function interpolateField(this, field, functionType) result(f)

    !==================================================================================================================
    ! Description:
    !! interpolateField interpolates a target field on the lagrangian points usig MLS interpolation.
    !==================================================================================================================

        class(body3D) :: this
        
        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------ 

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: l
    !------------------------------------------------------------------------------------------------------------------

        real :: f(this%numberOfLagrangianPoints,field%nComp)
        !! field value of the support cells

        real :: fi(field%nComp)
        !! interpolated field value
    !------------------------------------------------------------------------------------------------------------------

        ! For each lagrangian point, interpolate the target field
        do l = 1,this%numberOfLagrangianPoints
            fi = this%xlag(l)%interpolateFieldOnPoint(field, functionType)
            f(l,1:field%nComp) = fi
        enddo     

    end function interpolateField

!**********************************************************************************************************************

    function interpolateField2D(this, field, functionType) result(f)

    !==================================================================================================================
    ! Description:
    !! interpolateField2D interpolates a target field on the lagrangian points usig MLS interpolation in 2D.
    !==================================================================================================================        

        class(body2D) :: this
        
        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------ 

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: l, n
    !------------------------------------------------------------------------------------------------------------------

        real :: f(this%numberOfLagrangianPoints,field%nComp)
        !! field value of the support cells

        real :: fi(field%nComp)
        !! interpolated field value
    !------------------------------------------------------------------------------------------------------------------

        ! For each lagrangian point, interpolate the target field
        do l = 1,this%numberOfLagrangianPoints
            fi = this%xlag(l)%interpolateFieldOnPoint2D(field, functionType)
            f(l,1:field%nComp) = fi
        enddo     

    end function interpolateField2D

!**********************************************************************************************************************

    function lagrangianForces(this, u, ulag, functionType) result(Flag)

    !==================================================================================================================
    ! Description:
    !! lagrangianForces computes the lagrangian forces.
    !==================================================================================================================

        class(body3D) :: this
        
        type(flubioField) :: u
    !------------------------------------------------------------------------------------------------------------------ 

        character(len=*) :: functionType
        !! weighting function type
    !------------------------------------------------------------------------------------------------------------------

        integer :: l
        !! loop index
    !------------------------------------------------------------------------------------------------------------------

        real :: ui(this%numberOfLagrangianPoints,3)
        !! interpolated velocity

        real :: ulag(this%numberOfLagrangianPoints,3)
        !! velocity of the lagrangian point

        real :: Flag(this%numberOfLagrangianPoints,3)
        !! field value of the support cells
    !------------------------------------------------------------------------------------------------------------------

        ! Interpolate velocity on lagrangian points
        ui = 0.0
        ui = this%interpolateField(u, functionType)
        
        ! For each lagrangian point, compute the lagrangian force
        Flag = 0.0
        do l = 1,this%numberOfLagrangianPoints
            Flag(l,1:3-bdim) = (ulag(l,1:3-bdim) - ui(l,1:3-bdim))/dtime                 
        enddo     

    end function lagrangianForces

!**********************************************************************************************************************

    function lagrangianForces2D(this, u, ulag, functionType) result(Flag)

    !==================================================================================================================
    ! Description:
    !! lagrangianForces computes the lagrangian forces in 2D.
    !==================================================================================================================

        class(body2D) :: this
        
        type(flubioField) :: u
    !------------------------------------------------------------------------------------------------------------------ 

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: l
    !------------------------------------------------------------------------------------------------------------------

        real :: ui(this%numberOfLagrangianPoints,3)
        !! interpolated velocity

        real :: ulag(this%numberOfLagrangianPoints,3-bdim)
        !! velocity of the lagrangian point

        real :: Flag(this%numberOfLagrangianPoints,3-bdim)
        !! field value of the support cells
    !------------------------------------------------------------------------------------------------------------------

        ! Interpolate velocity on lagragina points
        ui = 0.0
        ui = this%interpolateField(u, functionType)

        ! For each lagrangian point, compute the lagrangian force
        Flag = 0.0
        do l = 1,this%numberOfLagrangianPoints
            Flag(l,1:3-bdim) = (ulag(l,1:3-bdim) - ui(l,1:3-bdim))/dtime     
        enddo     

    end function lagrangianForces2D

!**********************************************************************************************************************

    subroutine eulerianForces(this, u, ulag, f, fGhost, functionType)

    !==================================================================================================================
    ! Description:
    !! eulerianForces computes the eulerian forces.
    !==================================================================================================================

        class(body3D) :: this
        
        type(flubioField) :: u

        type(flubioField) :: f

        type(flubioField) :: fGhost
    !------------------------------------------------------------------------------------------------------------------ 

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: l
    !------------------------------------------------------------------------------------------------------------------

        real :: ulag(this%numberOfLagrangianPoints,3)
        !! velocity of the lagrangian point

        real :: Flag(this%numberOfLagrangianPoints,3)
        !! field value of the support cells
    !------------------------------------------------------------------------------------------------------------------

        Flag = this%lagrangianForces(u, ulag, functionType)

        do l = 1,this%numberOfLagrangianPoints
            call this%xlag(l)%eulerianForce(f, fGhost, Flag(l,:))
        enddo

        ! Body forces
        call this%bodyForces(Flag)

    end subroutine eulerianForces

!**********************************************************************************************************************

    subroutine eulerianForces2D(this, u, ulag, f, fGhost, functionType)

    !==================================================================================================================
    ! Description:
    !! eulerianForces computes the eulerian forces in 2D.
    !==================================================================================================================

        class(body2D) :: this
        
        type(flubioField) :: u

        type(flubioField) :: f

        type(flubioField) :: fGhost
    !------------------------------------------------------------------------------------------------------------------ 

        character(len=*) :: functionType
    !------------------------------------------------------------------------------------------------------------------

        integer :: l
    !------------------------------------------------------------------------------------------------------------------

        real :: ulag(this%numberOfLagrangianPoints,3-bdim)
        !! velocity of the lagrangian point

        real :: Flag(this%numberOfLagrangianPoints,3-bdim)
        !! field value of the support cells
    !------------------------------------------------------------------------------------------------------------------

        Flag = this%lagrangianForces(u, ulag, functionType)

        do l = 1,this%numberOfLagrangianPoints
            call this%xlag(l)%eulerianForce(f, fGhost, Flag(l,1:3-bdim))
        enddo

        ! Body forces
        call this%bodyForces(Flag)

    end subroutine eulerianForces2D

!**********************************************************************************************************************

    subroutine bodyForces(this, Flag)

    !==================================================================================================================
    ! Description:
    !! bodyForces computes the forces on the body.
    !! Fb = - Flag\Delta V_l
    !! Note: I am not 100% sure this is correct, paper's by De tullio suggest to interpolate pressure and stress on 
    !! the facet and compute forces ans standard. However, I am afraid that way will be not clean as internal body
    !! might be not perfect. Maybe I can implement a mask if that's the way.  
    !==================================================================================================================

        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        integer :: l
        !! loop index

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        real :: Fbl(3) 
        !! local force on the body

        real :: Fb(3) 
        !! global force on the body

        real :: Mbl(3) 
        !! local force on the body

        real :: Mb(3) 
        !! global force on the body

        real :: r(3)
        !! force arm

        real :: Vl 
        !! forcing volume

        real :: Flag(this%numberOfLagrangianPoints,3)
        !! field value of the support cells
    !------------------------------------------------------------------------------------------------------------------

        Fb = 0.0
        Fbl = 0.0
        Mb = 0.0
        Mbl = 0.0
        do l = 1,this%numberOfLagrangianPoints

            ! Compute Vl 
            Vl = this%xlag(l)%hl()* this%xlag(l)%area

            ! Force arm 
            r = this%xlag(l)%x - this%xcor

            ! Force
            Fbl = Fbl - Flag(l,:)*Vl

            ! Moment 
            Mbl = cross_product(Fbl, r) 

        enddo

        ! Sum up contributions
        call mpi_Allreduce(Fbl, Fb, 3, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

        ! Sum up contributions
        call mpi_Allreduce(Mbl, Mb, 3, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

        ! Set force and moments 
        this%Force = Fb 
        this%Moment = Mb

    end subroutine bodyForces

!**********************************************************************************************************************

    subroutine bodyForces2D(this, Flag)

    !==================================================================================================================
    ! Description:
    !! bodyForces computes the forces on the body in 2D.
    !! Fb = - Flag\Delta V_l
    !! Note: I am not 100% sure this is correct, paper's by De tullio suggest to interpolate pressure and stress on 
    !! the facet and compute forces as standard. However, I am afraid that way will be not clean as internal body
    !! might be not perfect. Maybe I can implement a mask if that's the way.    
    !==================================================================================================================

        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        integer :: l
        !! loop index

        integer :: ierr
        !! error flag
    
        real :: Fbl(3) 
        !! local force on the body

        real :: Fb(3) 
        !! global force on the body

        real :: Mbl(3) 
        !! local force on the body

        real :: Mb(3) 
        !! global force on the body

        real :: r(3)
        !! force arm

        real :: Vl 
        !! forcing volume

        real :: Flag(this%numberOfLagrangianPoints,3-bdim)
        !! field value of the support cells
    !------------------------------------------------------------------------------------------------------------------

        Fb = 0.0
        Fbl = 0.0
        Mb = 0.0
        Mbl = 0.0
        do l = 1,this%numberOfLagrangianPoints

            ! Compute Vl 
            Vl = this%xlag(l)%hl()*this%xlag(l)%area

            ! Force arm 
            r = this%xlag(l)%x - this%xcor 
            r(3) = 0.0
            
            ! Force
            Fbl(1:3-bdim) = Fbl(1:3-bdim) - Flag(l,1:3-bdim)*Vl
            Fbl(3) = 0.0

            ! Moment 
            Mbl = cross_product(Fbl, r) 

        enddo

        ! Sum up contributions
        call mpi_Allreduce(Fbl, Fb, 3, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

        ! Sum up contributions
        call mpi_Allreduce(Mbl, Mb, 3, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)

        ! Set force and moments 
        this%Force = Fb 
        this%Moment = Mb

        ! Just in case, put to zero force in z direction and moments in x and y direction
        this%Moment(1) = 0.0
        this%Moment(2) = 0.0
        this%Force(3) = 0.0  
        
    end subroutine bodyForces2D

!**********************************************************************************************************************

    subroutine writeForceToFile(this)

    !==================================================================================================================
    ! Description:
    !! writeForceToFile writes the force acting on the body to file.
    !==================================================================================================================

        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        if(id==0) then
            if(itime==1) then
                open(1, file='postProc/monitors/'//trim(this%name)//'-forces.dat')
                    write(1,*) time, this%Force, this%Moment
                close(1)
            else 
                open(1, file='postProc/monitors/'//trim(this%name)//'-forces.dat', access='append')
                    write(1,*) time, this%Force, this%Moment
                close(1)
            endif 
        endif 

    end subroutine writeForceToFile

!**********************************************************************************************************************

    subroutine writeForceToFile2D(this)

    !==================================================================================================================
    ! Description:
    !! writeForceToFile writes the force acting on the body to file.
    !==================================================================================================================
    
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        if(id==0) then
            if(itime==1) then
                open(1, file='postProc/monitors/'//trim(this%name)//'-forces.dat')
                    write(1,*) time, this%Force, this%Moment
                close(1)
            else 
                open(1, file='postProc/monitors/'//trim(this%name)//'-forces.dat', access='append')
                    write(1,*) time, this%Force, this%Moment
                close(1)
            endif 
        endif

    end subroutine writeForceToFile2D

!**********************************************************************************************************************
    
    subroutine writeStateToFile(this)

    !==================================================================================================================
    ! Description:
    !! writeStateToFile writes the force acting on the body to file.
    !==================================================================================================================

        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        if(id==0) then
            if(itime==1) then
                open(1, file='postProc/monitors/'//trim(this%name)//'-state.dat')
                    write(1,*) time, this%state(:,1)
                close(1)
            else 
                open(1, file='postProc/monitors/'//trim(this%name)//'-state.dat', access='append')
                    write(1,*) time, this%state(:,1)
                close(1)
            endif 
        endif

    end subroutine writeStateToFile

!**********************************************************************************************************************

    subroutine writeStateToFile2D(this)

    !==================================================================================================================
    ! Description:
    !! writeStateToFile2D writes the body state to file.
    !==================================================================================================================
    
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        if(id==0) then
            if(itime==1) then
                open(1, file='postProc/monitors/'//trim(this%name)//'-state.dat')
                    write(1,*) time, this%state(:,1)
                close(1)
            else 
                open(1, file='postProc/monitors/'//trim(this%name)//'-state.dat', access='append')
                    write(1,*) time, this%state(:,1)
                close(1)
            endif 
        endif

    end subroutine writeStateToFile2D    

!**********************************************************************************************************************

    subroutine moveBody(this)

    !==================================================================================================================
    ! Description:
    !! moveBody wraps the method prescribing the body motion. Each method has to return a state vector for the body.
    !==================================================================================================================
        
        class(body3D) :: this
    !------------------------------------------------------------------------------------------------------------------ 

        real :: state(16)
        !! state vector + euler angles
    !------------------------------------------------------------------------------------------------------------------

        if(this%motionType=="prescribed") then 

            ! Store old states
            this%state(:,4) = this%state(:,3)
            this%state(:,3) = this%state(:,2) 
            this%state(:,2) = this%state(:,1)
            
            ! Compute the state vector
            this%state(:,1) = this%getStateFromPrescribedMotion()

            ! Update body position and velocity
            call this%updateBodyPosition()

        elseif(lowercase(this%motionType)=="prescribedposition") then 

            ! Store old states
            this%state(:,4) = this%state(:,3)
            this%state(:,3) = this%state(:,2) 
            this%state(:,2) = this%state(:,1)
            
            ! Compute the state vector
            this%state(:,1) = this%getStateFromPrescribedPosition()

            ! Update body position and velocity
            call this%updateBodyPosition()

        elseif(this%motionType=="6DoF") then
            call flubioStopMsg('ERROR: this method is not implmented yet')
        else 

        endif
 
    end subroutine moveBody
            
!**********************************************************************************************************************

    subroutine moveBody2D(this)

    !==================================================================================================================
    ! Description:
    !! moveBody wraps the method prescribing the body motion in 2D. Each method has to return a state vector for the body.
    !==================================================================================================================
            
        class(body2D) :: this
    !------------------------------------------------------------------------------------------------------------------ 
    
        real :: state(16)
        !! state vector + euler angles
    !------------------------------------------------------------------------------------------------------------------
        
        if(this%motionType=="prescribed") then 

            ! Store old states
            this%state(:,4) = this%state(:,3)
            this%state(:,3) = this%state(:,2) 
            this%state(:,2) = this%state(:,1)
            
            ! Compute the state vector
            this%state(:,1) = this%getStateFromPrescribedMotion()
     
            ! Update body position and velocity
            call this%updateBodyPosition()
            
        elseif(lowercase(this%motionType)=="prescribedposition") then 

            ! Store old states
            this%state(:,4) = this%state(:,3)
            this%state(:,3) = this%state(:,2) 
            this%state(:,2) = this%state(:,1)
            
            ! Compute the state vector
            this%state(:,1) = this%getStateFromPrescribedPosition()

            ! Update body position and velocity
            call this%updateBodyPosition()

        elseif(this%motionType=="6DoF") then
            call flubioStopMsg('ERROR: this method is not implmented yet')
        else 
          
        endif

    end subroutine moveBody2D

!**********************************************************************************************************************

    subroutine updateBodyPosition(this)

        use rigidBodyDynamics, only: invReu

    !==================================================================================================================
    ! Description:
    !! updateBodyPosition updates the body position and lagrangia points velocity
    !==================================================================================================================

        class(body3D) :: this 
    !------------------------------------------------------------------------------------------------------------------

        real :: dx(3)
        !! delta position vector 

        real :: dtheta(3)
        !! delta rotation vector

        real :: xg(3)
        !! STL center

        real :: R(3,3)
        !! rotation matrix from euler angles
    !------------------------------------------------------------------------------------------------------------------

        ! Translation and rotation deltas
        dx = this%state(1:3,1) - this%state(1:3,2)
        dtheta = this%state(14:16,1) - this%state(14:16,2)
        
        ! STL centroid
        xg = this%xcor

        ! Rotation matrix
        R = invReu(dtheta)
        
        ! Rotate around the center of mass 
        call this%ibmSurface%translate(x=-xg(1), y=-xg(2), z=-xg(3))
        call this%ibmSurface%rotate(matrix=R)
        call this%ibmSurface%translate(x=xg(1), y=xg(2), z=xg(3))

        ! Translate lagrangian points 
        call this%ibmSurface%translate(x=dx(1), y=dx(2), z=dx(3))  

        ! Update STL centroid
        this%xg = this%computeSTLCentroid()
        this%xcor = this%xcor + dx

    end subroutine updateBodyPosition

!**********************************************************************************************************************

    subroutine updateBodyVelocity(this)

    !==================================================================================================================
    ! Description:
    !! updateBodyVelocity updates the body velocity combing rotation and translation. 
    !==================================================================================================================
            
        class(body3D) :: this 
    !------------------------------------------------------------------------------------------------------------------

        integer :: l 
        !! loop index
    !------------------------------------------------------------------------------------------------------------------  

        real :: vcm(3)
        !! velocity and the center of mass

        real :: omega(3)
        !! angular velocity

        real :: r(3)
        !! distance vector
    !------------------------------------------------------------------------------------------------------------------
        
        vcm = this%state(4:6,1)
        omega = this%state(7:9,1)

        do l=1,this%numberOfLagrangianPoints
            r = this%xlag(l)%x - this%xcor
            this%ulag(l,:) = vcm + cross_product(omega, r)
        enddo

    end subroutine updateBodyVelocity

!**********************************************************************************************************************

    subroutine updateBodyPosition2D(this)

    !==================================================================================================================
    ! Description:
    !! updateBodyPosition2D updates the body position and lagrangia points velocity for a 2D body
    !==================================================================================================================
    
        use rigidBodyDynamics,only: invReu2D

        class(body2D) :: this 
    !------------------------------------------------------------------------------------------------------------------

        integer :: p
    !------------------------------------------------------------------------------------------------------------------

        real :: dx(3)
        !! delta position vector 

        real :: dtheta(3)
        !! deltaa rotation vector

        real :: R(3,3)
        !! rotation matrix from euler angles

        real :: rotatedPoint(3)
    !-----------------------------------------------------------------------------------------------------------------
    
        ! Translation and rotation deltas
        dx = this%state(1:3,1) - this%state(1:3,2)
        dtheta = this%state(14:16,1) - this%state(14:16,2)

        ! Rotation matrix, z-axis only be sure your geometry is defined in the x-y plane
        this%bodyPoints(:,1) = this%bodyPoints(:,1) - this%xcor(1)
        this%bodyPoints(:,2) = this%bodyPoints(:,2) - this%xcor(2)
        this%bodyPoints(:,3) = this%bodyPoints(:,3) - this%xcor(3)

        R = invReu2D(dtheta(3))
        do p=1,size(this%bodyPoints(:,1))
            rotatedPoint = matmul(R,this%bodyPoints(p,1:3))
            this%bodyPoints(p,1:3) = rotatedPoint
        enddo 

        this%bodyPoints(:,1) = this%bodyPoints(:,1) + this%xcor(1)
        this%bodyPoints(:,2) = this%bodyPoints(:,2) + this%xcor(2)
        this%bodyPoints(:,3) = this%bodyPoints(:,3) + this%xcor(3)

        ! Translate
        this%bodyPoints(:,1) = this%bodyPoints(:,1) + dx(1)
        this%bodyPoints(:,2) = this%bodyPoints(:,2) + dx(2)
        this%bodyPoints(:,3) = this%bodyPoints(:,3) + dx(3) 
       
        ! Update center of mass position
        this%xg = this%computePointsCentroid()
        this%xcor = this%xcor + dx

    end subroutine updateBodyPosition2D

!**********************************************************************************************************************

    subroutine updateBodyVelocity2D(this)

    !==================================================================================================================
    ! Description:
    !! updateBodyVelocity2D updates the 2D body velocity. 
    !==================================================================================================================
        
        class(body2D) :: this 
    !------------------------------------------------------------------------------------------------------------------
 
        integer :: l 
        !! loop index
    !------------------------------------------------------------------------------------------------------------------  

        real :: vcm(3)
        !! velocity and the center of mass

        real :: omega(3)
        !! angular velocity

        real :: r(3)
        !! distance vector
        
        real :: omegar(3)
        !! cross product between omega and r
    !------------------------------------------------------------------------------------------------------------------
        
        vcm = this%state(4:6,1)
        omega = this%state(7:9,1)

        do l=1,this%numberOfLagrangianPoints
            r = this%xlag(l)%x - this%xcor
            omegar = cross_product(omega, r)
            this%ulag(l,1:3-bdim) = vcm(1:3-bdim) + omegar(1:3-bdim)
        enddo

    end subroutine updateBodyVelocity2D

!********************************************************************************************************************** 

    function getStateFromPrescribedMotion(this) result(state)

    !==================================================================================================================
    ! Description:
    !! getStateFromPrescribedMotion parses the motion and converts it to a number.
    !==================================================================================================================

        use parseFromFile, only: getExprValue 

        class(body3D) :: this 
    !------------------------------------------------------------------------------------------------------------------

        character(len=275) :: expr

        character(len = 10), dimension(1) :: variables
        !! variables in string
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target component

        integer :: nComp
        !! number of componets
    !------------------------------------------------------------------------------------------------------------------

        real :: variablesValues(1)
        !! name of the variables to be interpreted

        real :: state(16)
        ! state vector as real number
    !------------------------------------------------------------------------------------------------------------------

        variables(1) = 't'

        ! Variables
        variablesValues(1) = time 

        ! Position
        expr = this%motionExpression%x(1)
        state(1) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%x(2)
        state(2) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%x(3)
        state(3) = getExprValue(expr, variables, variablesValues, 1)

        ! Velocity
        expr = this%motionExpression%xDot(1)
        state(4) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%xDot(2)
        state(5) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%xDot(3)
        state(6) = getExprValue(expr, variables, variablesValues, 1)

        ! Angular Velocity
        expr = this%motionExpression%omega(1)
        state(7) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%omega(2)
        state(8) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%omega(3)
        state(9) = getExprValue(expr, variables, variablesValues, 1)

        ! Quaternoins
        expr = this%motionExpression%quaternions(1)
        state(10) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%quaternions(2)
        state(11) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%quaternions(3)
        state(12) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%quaternions(4)
        state(13) = getExprValue(expr, variables, variablesValues, 1)

        ! Euler angles
        expr = this%motionExpression%eulerAngles(1)
        state(14) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%eulerAngles(2)
        state(15) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%eulerAngles(3)
        state(16) = getExprValue(expr, variables, variablesValues, 1)

    end function getStateFromPrescribedMotion
    
!**********************************************************************************************************************
    
    function getStateFromPrescribedMotion2D(this) result(state)

    !==================================================================================================================
    ! Description:
    !! getStateFromPrescribedMotion2D parses the motion and converts it to a number.
    !==================================================================================================================

        use parseFromFile, only: getExprValue

        class(body2D) :: this 
    !-----------------------------------------------------------------------------------------------------------------

        character(len=275) :: expr

        character(len = 10), dimension(1) :: variables
        !! variables in string
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target component

        integer :: nComp
        !! number of componets
    !-----------------------------------------------------------------------------------------------------------------

        real :: variablesValues(1)
        !! name of the variables to be interpreted

        real :: state(16)
        ! state vector as real number
    !-----------------------------------------------------------------------------------------------------------------

        variables(1) = 't'

        ! variables
        variablesValues(1) = time ! t

        ! Position
        expr = this%motionExpression%x(1)
        state(1) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%x(2)
        state(2) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%x(3)
        state(3) = getExprValue(expr, variables, variablesValues, 1)

        ! Velocity
        expr = this%motionExpression%xDot(1)
        state(4) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%xDot(2)
        state(5) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%xDot(3)
        state(6) = getExprValue(expr, variables, variablesValues, 1)

        ! Angular Velocity
        expr = this%motionExpression%omega(1)
        state(7) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%omega(2)
        state(8) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%omega(3)
        state(9) = getExprValue(expr, variables, variablesValues, 1)

        ! Quaternoins
        expr = this%motionExpression%quaternions(1)
        state(10) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%quaternions(2)
        state(11) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%quaternions(3)
        state(12) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%quaternions(4)
        state(13) = getExprValue(expr, variables, variablesValues, 1)

        ! Euler angles
        expr = this%motionExpression%eulerAngles(1)
        state(14) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%eulerAngles(2)
        state(15) = getExprValue(expr, variables, variablesValues, 1)
        expr = this%motionExpression%eulerAngles(3)
        state(16) = getExprValue(expr, variables, variablesValues, 1)

    end function getStateFromPrescribedMotion2D

!********************************************************************************************************************** 

    function getStateFromPrescribedPosition(this) result(state)

    !==================================================================================================================
    ! Description:
    !! getStateFromPrescribedPosition parses the motion and computes velocity and angular velocity of the body.
    !==================================================================================================================

        use parseFromFile, only: getExprValue 

        class(body3D) :: this 
    !------------------------------------------------------------------------------------------------------------------
    
        character(len=275) :: expr(3)
    
        character(len = 10), dimension(1) :: variables
        !! variables in string
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iComp
        !! target component
    
        integer :: nComp
        !! number of componets
    !------------------------------------------------------------------------------------------------------------------
    
        real :: variablesValues(1)
        !! name of the variables to be interpreted
    
        real :: state(16)
        ! state vector as real number

        real :: xn(3)
        !! position at time t

        real :: xn1(3)
        !! position at time t+dt

        real :: xn2(3)
        !! position at time t+2*dt

        real :: xn3(3)
        !! postion at time t+3*dt

        real :: euDot(3)
        !! Euler angle derivatives

        real coeffs(4)
        !! Finite difference coefficients

        real :: R(3,3)
        !! Rotation matrix to compute angular velocities
    !------------------------------------------------------------------------------------------------------------------
    
        variables(1) = 't'
    
        coeffs(1) = -11.0/6.0
        coeffs(2) = 3.0
        coeffs(3) = -3.0/2.0
        coeffs(4) = 1.0/3.0

        ! Variables
        variablesValues(1) = time 

        expr(1) = this%motionExpression%x(1)
        expr(2) = this%motionExpression%x(2)
        expr(3) = this%motionExpression%x(3)

        state(1) = getExprValue(expr(1), variables, variablesValues, 1)
        state(2) = getExprValue(expr(2), variables, variablesValues, 1)
        state(3) = getExprValue(expr(3), variables, variablesValues, 1)

        ! Use 3th order forward finite difference to compute velocity from position
        xn = state(1:3)
        
        variablesValues(1) = time + dtime 
        xn1(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn1(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn1(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 2.0*dtime 
        xn2(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn2(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn2(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 3.0*dtime 
        xn3(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn3(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn3(3) = getExprValue(expr(3), variables, variablesValues, 1)
        
        state(4:6) = (coeffs(1)*xn + coeffs(2)*xn1 + coeffs(3)*xn2 + coeffs(4)*xn3)/dtime

        ! Use RK3 to compute the angular velocity from euler angles
        expr(1) = this%motionExpression%eulerAngles(1)
        expr(2) = this%motionExpression%eulerAngles(2)
        expr(3) = this%motionExpression%eulerAngles(3)

        variablesValues(1) = time 
        state(14) = getExprValue(expr(1), variables, variablesValues, 1)
        state(15) = getExprValue(expr(2), variables, variablesValues, 1)
        state(16) = getExprValue(expr(3), variables, variablesValues, 1)

        xn = state(14:16)

        variablesValues(1) = time + dtime 
        xn1(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn1(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn1(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 2.0*dtime 
        xn2(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn2(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn2(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 3.0*dtime 
        xn3(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn3(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn3(3) = getExprValue(expr(3), variables, variablesValues, 1)

        euDot = (coeffs(1)*xn + coeffs(2)*xn1 + coeffs(3)*xn2 + coeffs(4)*xn3)/dtime

        R = eulerToAngular(xn)
        state(7:9) = matmul(R, euDot)

        ! Zeros quaternions 
        state(10:13) = 0.0

    end function getStateFromPrescribedPosition

!********************************************************************************************************************** 

    function getStateFromPrescribedPosition2D(this) result(state)

    !==================================================================================================================
    ! Description:
    !! getStateFromPrescribedPosition parses the motion and computes velocity and angular velocity of the body.
    !==================================================================================================================

        use parseFromFile, only: getExprValue 

        class(body2D) :: this 
    !------------------------------------------------------------------------------------------------------------------
    
        character(len=275) :: expr(3)
    
        character(len = 10), dimension(1) :: variables
        !! variables in string
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iComp
        !! target component
    
        integer :: nComp
        !! number of componets
    !------------------------------------------------------------------------------------------------------------------
    
        real :: variablesValues(1)
        !! name of the variables to be interpreted
    
        real :: state(16)
        ! state vector as real number

        real :: xn(3)
        !! position at time t

        real :: xn1(3)
        !! position at time t+dt

        real :: xn2(3)
        !! position at time t+2*dt

        real :: xn3(3)
        !! postion at time t+3*dt

        real :: euDot(3)
        !! Euler angle derivatives

        real coeffs(4)
        !! Finite difference coefficients

        real :: R(3,3)
        !! Rotation matrix to compute angular velocities
    !------------------------------------------------------------------------------------------------------------------
    
        variables(1) = 't'
    
        coeffs(1) = -11.0/6.0
        coeffs(2) = 3.0
        coeffs(3) = -3.0/2.0
        coeffs(4) = 1.0/3.0

        ! Variables
        variablesValues(1) = time 

        expr(1) = this%motionExpression%x(1)
        expr(2) = this%motionExpression%x(2)
        expr(3) = this%motionExpression%x(3)

        state(1) = getExprValue(expr(1), variables, variablesValues, 1)
        state(2) = getExprValue(expr(2), variables, variablesValues, 1)
        state(3) = getExprValue(expr(3), variables, variablesValues, 1)

        ! Use 3th order forward finite difference to compute velocity from position
        xn = state(1:3)
        
        variablesValues(1) = time + dtime 
        xn1(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn1(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn1(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 2.0*dtime 
        xn2(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn2(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn2(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 3.0*dtime 
        xn3(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn3(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn3(3) = getExprValue(expr(3), variables, variablesValues, 1)
        
        state(4:6) = (coeffs(1)*xn + coeffs(2)*xn1 + coeffs(3)*xn2 + coeffs(4)*xn3)/dtime

        ! Compute the angular velocity from euler angles
        expr(1) = this%motionExpression%eulerAngles(1)
        expr(2) = this%motionExpression%eulerAngles(2)
        expr(3) = this%motionExpression%eulerAngles(3)

        variablesValues(1) = time 
        state(14) = getExprValue(expr(1), variables, variablesValues, 1)
        state(15) = getExprValue(expr(2), variables, variablesValues, 1)
        state(16) = getExprValue(expr(3), variables, variablesValues, 1)

        xn = state(14:16)

        variablesValues(1) = time + dtime 
        xn1(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn1(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn1(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 2.0*dtime 
        xn2(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn2(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn2(3) = getExprValue(expr(3), variables, variablesValues, 1)

        variablesValues(1) = time + 3.0*dtime 
        xn3(1) = getExprValue(expr(1), variables, variablesValues, 1)
        xn3(2) = getExprValue(expr(2), variables, variablesValues, 1)
        xn3(3) = getExprValue(expr(3), variables, variablesValues, 1)

        euDot = (coeffs(1)*xn + coeffs(2)*xn1 + coeffs(3)*xn2 + coeffs(4)*xn3)/dtime

        R = eulerToAngular(xn)
        state(7:9) = matmul(R, euDot)

        ! Zeros quaternions 
        state(10:13) = 0.0

    end function getStateFromPrescribedPosition2D

!**********************************************************************************************************************

    subroutine initToZero(this)

        class(prescribedMotion) :: this 
    !------------------------------------------------------------------------------------------------------------------

        !allocate(character(len=275) :: this%x(3))
        this%x(1) = "0.0"
        this%x(2) = "0.0"
        this%x(3) = "0.0"

        !allocate(character(len=275) :: this%xDot(3))
        this%xDot(1) = "0.0"
        this%xDot(2) = "0.0"
        this%xDot(3) = "0.0"

        !allocate(character(len=275) :: this%omega(3))
        this%omega(1) = "0.0"
        this%omega(2) = "0.0"
        this%omega(3) = "0.0"

        !allocate(character(len=275) :: this%quaternions(4))
        this%quaternions(1) = "0.0"
        this%quaternions(2) = "0.0"
        this%quaternions(3) = "0.0"
        this%quaternions(4) = "0.0"

        !allocate(character(len=275) :: this%eulerAngles(3))
        this%eulerAngles(1) = "0.0"
        this%eulerAngles(2) = "0.0"
        this%eulerAngles(3) = "0.0"

    end subroutine initToZero

!**********************************************************************************************************************

end module bodies
