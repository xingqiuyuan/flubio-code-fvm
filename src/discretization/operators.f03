module operators
  
    use diffusionFluxes 
    use gradient
    use meshvar
    use tensorMath
    use flubioTensors

    implicit none

    interface div
        module procedure :: divField
        module procedure :: divPhi
        module procedure :: divPhiField
    end interface div

    interface lapla
        module procedure :: laplaField
        module procedure :: laplaCoeffField
        module procedure :: laplaConstField
    end interface lapla

    !interface grad 
    !end interface grad

contains 
    
!**********************************************************************************************************************

    function divField(field, opt) result(phi)

    !==================================================================================================================
    ! Description:
    !! div returns the diverge of a vector field.
    !==================================================================================================================

        type(flubioField) :: field
        !! input field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iOwner, iNeighbour

        integer :: opt
        !! interpolation option
    !-----------------------------------------------------------------------------------------------------------------

        real :: F
        !! work varailbe to store a flux

        real :: phi(numberOfElements)
        !! output array
    !-----------------------------------------------------------------------------------------------------------------

        ! Check field componentes
        if(field%nComp==1) call flubioStopMsg('ERROR: expected vector field, but got a scalar field instead.')
        phi = 0.0

        if(opt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(opt >= 2 .and. opt <= 4) then
            call interpolate(field, interpType='gradientbased', opt=opt)
        else
            call interpolate(field, interpType='tvd', opt=opt)
        end if

        ! Internal faces
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            F = dot_product(field%phif(iFace,:), mesh%Sf(iFace,:))
            phi(iOwner) = phi(iOwner) + F
            phi(iNeighbour) = phi(iNeighbour) - F

        end do

        ! Boundary faces
        do iBFace=numberofIntFaces+1,numberOfFaces

            iOwner = mesh%owner(iBFace)

            F = dot_product(field%phif(iBFace,:), mesh%Sf(iBFace,:))
            phi(iOwner) = phi(iOwner) + F

        end do

    end function divField

!**********************************************************************************************************************

    function divPhi(F) result(phi)

    !==================================================================================================================
    ! Description:
    !! divPhi returns the diverge of a vector field, given in input its face flux.
    !==================================================================================================================

        integer :: iFace, iBFace, iOwner, iNeighbour
    !-----------------------------------------------------------------------------------------------------------------

        real :: F(numberOfFaces,1)
        !! work varailbe to store a flux

        real :: phi(numberOfElements)
        !! output array
    !-----------------------------------------------------------------------------------------------------------------

        phi = 0.0

        ! Internal faces
        do iFace=1,numberOfIntFaces
            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)
            phi(iOwner) = phi(iOwner) + F(iFace,1)
            phi(iNeighbour) = phi(iNeighbour) - F(iFace,1)
        end do

        ! Boundary faces
        do iBFace=numberofIntFaces+1,numberOfFaces
            iOwner = mesh%owner(iBFace)
            phi(iOwner) = phi(iOwner) + F(iBFace,1)
        end do

    end function divPhi

!**********************************************************************************************************************

    function divPhiField(F, field, opt) result(phi)

    !==================================================================================================================
    ! Description:
    !! divPhifield returns the diverge of a vector field from a face flux, multiplied by a field.
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iElement, iFace, iBFace, iOwner, iNeighbour, opt
    !-----------------------------------------------------------------------------------------------------------------

        real :: F(numberOfFaces,1)
        !! work variable to store a flux

        real :: phi(numberOfElements, field%nComp)
        !! output array
    !-----------------------------------------------------------------------------------------------------------------

        phi = 0.0

        if(opt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(opt >= 2 .and. opt <= 4) then
            call interpolate(field, interpType='gradientbased', opt=opt)
        else
            call interpolate(field, interpType='tvd', opt=opt)
        end if

        ! Internal faces
        do iFace=1,numberOfIntFaces
            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)
            phi(iOwner,:) = phi(iOwner,:) + F(iFace,1)*field%phif(iFace,:)
            phi(iNeighbour,:) = phi(iNeighbour,:) - F(iFace,1)*field%phif(iFace,:)
        end do

        ! Boundary faces
        do iBFace=numberOfIntFaces+1,numberOfFaces
            iOwner = mesh%owner(iBFace)
            phi(iOwner,:) = phi(iOwner,:) + F(iBFace,1)*field%phif(iBFace,:)
        end do

    end function divPhiField

!**********************************************************************************************************************

    function laplaField(field) result(phi)

    !==================================================================================================================
    ! Description:
    !! laplaField returns the laplacian of a field.
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iElement, iFace, iBFace, iOwner, iNeighbour, iProc, is, ie, pNeigh, iComp, nComp
    !-----------------------------------------------------------------------------------------------------------------

        real :: phiC, phiN, F, orthCorr, nonOrthCorr

        real :: phi(numberOfElements, field%nComp)

        real :: phiGrad_f(numberOfFaces, 3, field%nComp)
    !-----------------------------------------------------------------------------------------------------------------

        phi = 0.0

        call computeGradient(field)
        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=field%nComp)

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            do iComp =1,field%nComp

                phiC = field%phi(iOwner,iComp)
                phiN = field%phi(iNeighbour,iComp)

                orthCorr = mesh%gDiff(iFace)*(phiN - phiC)
                nonOrthCorr = dot_product(phiGrad_f(iFace, :,iComp), mesh%Tf(iFace, :))
                F = (orthCorr + nonOrthCorr)

                phi(iOwner, iComp) = phi(iOwner, iComp) + F
                phi(iNeighbour, iComp) = phi(iNeighbour, iComp) - F

            end do

        enddo

        ! Processor boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh + 1
                iOwner = mesh%owner(iBFace)

                do iComp =1,field%nComp

                    phiC = field%phi(iOwner,iComp)
                    phiN = field%ghosts(pNeigh, iComp ,iBoundary)

                    orthCorr = mesh%gDiff(iBFace)*(phiN - phiC)
                    nonOrthCorr = dot_product(phiGrad_f(iBFace, :,iComp), mesh%Tf(iBFace, :))
                    F = (orthCorr + nonOrthCorr)

                    phi(iOwner, iComp) = phi(iOwner,iComp) + F

                end do

            enddo

        enddo

        ! Real boundaries
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                do iComp =1,field%nComp

                    phiC = field%phi(iOwner,iComp)
                    phiN = field%phif(iBFace,iComp)

                    orthCorr = mesh%gDiff(iBFace)*(phiN - phiC)
                    nonOrthCorr = dot_product(phiGrad_f(iBFace, :,iComp), mesh%Tf(iBFace, :))
                    F = (orthCorr + nonOrthCorr)

                    phi(iOwner, iComp) = phi(iOwner,iComp) + F

                end do

            enddo

        enddo

    end function laplaField

!**********************************************************************************************************************

    function laplaConstField(coeff, field) result(phi)

    !==================================================================================================================
    ! Description:
    !! laplaConstField returns the laplacian of a field multipliedby a constant.
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iElement, iFace, iBFace, iOwner, iNeighbour, iProc, is, ie, pNeigh, iComp, nComp
    !-----------------------------------------------------------------------------------------------------------------

        real :: coeff
        !! constant coefficients multiplied the laplacian

        real :: phiC, phiN, F, orthCorr, nonOrthCorr

        real :: phi(numberOfElements, field%nComp)

        real :: phiGrad_f(numberOfFaces, 3, field%nComp)
    !-----------------------------------------------------------------------------------------------------------------

        phi = 0.0

        call computeGradient(field)
        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=field%nComp)

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            do iComp =1,field%nComp

                phiC = field%phi(iOwner,iComp)
                phiN = field%phi(iNeighbour,iComp)

                orthCorr = mesh%gDiff(iFace)*(phiN - phiC)
                nonOrthCorr = dot_product(phiGrad_f(iFace, :,iComp), mesh%Tf(iFace, :))
                F = coeff*(orthCorr + nonOrthCorr)

                phi(iOwner, iComp) = phi(iOwner, iComp) + F
                phi(iNeighbour, iComp) = phi(iNeighbour, iComp) - F

            end do

        enddo

        ! Processor boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh + 1
                iOwner = mesh%owner(iBFace)

                do iComp =1,field%nComp

                    phiC = field%phi(iOwner,iComp)
                    phiN = field%ghosts(pNeigh, iComp ,iBoundary)

                    orthCorr = mesh%gDiff(iBFace)*(phiN - phiC)
                    nonOrthCorr = dot_product(phiGrad_f(iBFace, :,iComp), mesh%Tf(iBFace, :))
                    F = coeff*(orthCorr + nonOrthCorr)

                    phi(iOwner, iComp) = phi(iOwner,iComp) + F

                end do

            enddo

        enddo

        ! Real boundaries
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                do iComp =1,field%nComp

                    phiC = field%phi(iOwner,iComp)
                    phiN = field%phif(iBFace,iComp)

                    orthCorr = mesh%gDiff(iBFace)*(phiN - phiC)
                    nonOrthCorr = dot_product(phiGrad_f(iBFace, :,iComp), mesh%Tf(iBFace, :))
                    F = coeff*(orthCorr + nonOrthCorr)

                    phi(iOwner, iComp) = phi(iOwner,iComp) + F

                end do

            enddo

        enddo

    end function laplaConstField

!**********************************************************************************************************************

    function laplaCoeffField(coeff, field) result(phi)

    !==================================================================================================================
    ! Description:
    !! lapla returns the laplacian of a field.
    !==================================================================================================================

        type(flubioField) :: field, coeff
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iElement, iFace, iBFace, iOwner, iNeighbour, iProc, is, ie, pNeigh, iComp, nComp
    !-----------------------------------------------------------------------------------------------------------------

        real :: phiC, phiN, F, orthCorr, nonOrthCorr

        real :: phi(numberOfElements, field%nComp)

        real :: phiGrad_f(numberOfFaces, 3, field%nComp)
    !-----------------------------------------------------------------------------------------------------------------

        phi = 0.0

        call interpolate(coeff, interpType='harmonic', opt=-1)
        call computeGradient(field)
        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=field%nComp)

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            do iComp=1,field%nComp

                phiC = field%phi(iOwner,iComp)
                phiN = field%phi(iNeighbour,iComp)

                orthCorr = mesh%gDiff(iFace)*(phiN - phiC)
                nonOrthCorr = dot_product(phiGrad_f(iFace, :,iComp), mesh%Tf(iFace, :))
                F = coeff%phif(iFace,1)*(orthCorr + nonOrthCorr)
                !F = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                phi(iOwner, iComp) = phi(iOwner, iComp) + F
                phi(iNeighbour, iComp) = phi(iNeighbour, iComp) - F

            end do

        enddo

        ! Processor boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh + 1
                iOwner = mesh%owner(iBFace)

                do iComp =1,field%nComp

                    phiC = field%phi(iOwner,iComp)
                    phiN = field%ghosts(pNeigh, iComp ,iBoundary)

                    orthCorr = mesh%gDiff(iBFace)*(phiN - phiC)
                    nonOrthCorr = dot_product(phiGrad_f(iBFace, :,iComp), mesh%Tf(iBFace, :))
                    F = coeff%phif(iBFace,1)*(orthCorr + nonOrthCorr)
                    !F = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Sf(iBFace,:))

                    phi(iOwner, iComp) = phi(iOwner,iComp) + F

                end do

            enddo

        enddo

        ! Real boundaries
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                do iComp =1,field%nComp

                    phiC = field%phi(iOwner,iComp)
                    phiN = field%phif(iBFace,iComp)

                    orthCorr = mesh%gDiff(iBFace)*(phiN - phiC)
                    nonOrthCorr = dot_product(phiGrad_f(iBFace, :,iComp), mesh%Tf(iBFace, :))
                    F = coeff%phif(iBFace,1)*(orthCorr + nonOrthCorr)
                    !F = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Sf(iBFace,:))

                    phi(iOwner, iComp) = phi(iOwner,iComp) + F

                end do

            enddo

        enddo

    end function laplaCoeffField
    
!**********************************************************************************************************************

    function grad(field, recompute) result(phi)

    !==================================================================================================================
    ! Description:
    !! grad returns the gradient of a field. This is the function version of compute gradients.
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        logical, optional :: recompute

        logical :: recomputeGradient
    !-----------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements+numberOfBFaces, 3, field%nComp)
   !-----------------------------------------------------------------------------------------------------------------

        if(present(recompute)) then 
            recomputeGradient = recompute
        else
            recomputeGradient = .true.
        endif

        if(recomputeGradient) call computeGradient(field)

        phi = field%phiGrad

    end function grad

!**********************************************************************************************************************

    function internalGrad(field, recompute) result(phi)

    !==================================================================================================================
    ! Description:
    !! grad returns the gradient of a field. This is the function version of compute gradients.
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        logical, optional :: recompute

        logical :: recomputeGradient
    !-----------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements, 3, field%nComp)
   !-----------------------------------------------------------------------------------------------------------------

        if(present(recompute)) then 
            recomputeGradient = recompute
        else
            recomputeGradient = .true.
        endif

        if(recomputeGradient) call computeGradient(field)

        phi = field%phiGrad(1:numberofElements, 1:3, 1:3)

    end function internalGrad

!**********************************************************************************************************************

    function snGrad(field, opt) result(phi)

    !==================================================================================================================
    ! Description:
    !! snGrad returns the surface normal gradient $\nabla\phi\cdot n_f$.
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iFace, iComp

        integer, optional :: opt
    !-----------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfFaces, field%nComp)

        real :: phiGrad_f(numberOfFaces, 3, field%nComp)

        real :: dot
    !-----------------------------------------------------------------------------------------------------------------

        if(.not. present(opt)) opt = 0

        phi = 0.0
        phiGrad_f = 0.0

        call computeGradient(field)
        call interpolateGradientCorrected(phiGrad_f, field, opt=opt, fComp=field%nComp)

        do iFace=1,numberOfFaces
            do iComp=1,field%nComp
                dot = dot_product(phiGrad_f(iFace, :, iComp), mesh%Sf(iFace,:)/mesh%area(iFace))
                phi(iFace, iComp) = dot
            enddo
        end do

    end function snGrad

!**********************************************************************************************************************

    function curl(field) result(phi)

    !==================================================================================================================
    ! Description:
    !! curl returns the curl of a field.
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !-----------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements, field%nComp)

        real :: c(3)
    !-----------------------------------------------------------------------------------------------------------------

        phi = 0.0

        call computeGradient(field)

        do iElement=1,numberOfElements + numberOfBFaces
            c(1) = field%phiGrad(iElement,2,3) - field%phiGrad(iElement,3,2)
            c(2) = field%phiGrad(iElement,3,1) - field%phiGrad(iElement,1,3)
            c(3) = field%phiGrad(iElement,1,2) - field%phiGrad(iElement,2,1)
            phi(iElement, :) = c
        end do

    end function curl

! *********************************************************************************************************************

    function totalDerivative(field) result(phi)

    !==================================================================================================================
    ! Description:
    !! totalDeivative returns the total derivative of a field.
    !! \begin{equation}
    !!  \dfrac{D\phi}{Dt} = \dfrac{\partial\phi}{\partial t} + u\cdot\nabla\phi
    !! \end{equation}
    !==================================================================================================================

        type(flubioField) :: field
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp
    !-----------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements, field%nComp)

        real :: dphidt, dphidx, dphidy, dphidz
    !-----------------------------------------------------------------------------------------------------------------

        phi = 0.0

        call computeGradient(field)

        do iComp=1,field%nComp

            do iElement=1,numberOfElements + numberOfBFaces

                dphidt = (field%phi(iElement,iComp) - field%phio(iElement,iComp))/dtime

                dphidx = field%phiGrad(iElement, 1, iComp)
                dphidy = field%phiGrad(iElement, 2, iComp)
                dphidz = field%phiGrad(iElement, 3, iComp)

                phi(iElement, iComp) = dphidt + velocity%phi(iElement,1)*dphidx  &
                        + velocity%phi(iElement,2)*dphidy  &
                        + velocity%phi(iElement,3)*dphidz
            end do

        end do

    end function totalDerivative

! *********************************************************************************************************************

    function gradGradMag(field, recompute) result(phi)

    !==================================================================================================================
    ! Description:
    !! grad returns the gradient of a field. This is the function version of compute gradients.
    !==================================================================================================================

        type(flubioField) :: field

        type(flubioField) :: tmp

        type(ttbTensor2) :: G
    !-----------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp

        integer :: iBoundary, is, ie, iBFace, patchFace
    !-----------------------------------------------------------------------------------------------------------------

        logical, optional :: recompute

        logical :: recomputeGradient
    !-----------------------------------------------------------------------------------------------------------------

        real :: phi(numberOfElements+numberOfBFaces)
   !-----------------------------------------------------------------------------------------------------------------

        if(present(recompute)) then 
            recomputeGradient = recompute
        else
            recomputeGradient = .true.
        endif

        if(recomputeGradient) call computeGradient(field)

        phi = 0.0

        call tmp%createWorkField(fieldName='tmp', classType='scalar', nComp=3)
        tmp%phi(1:numberOfElements+numberOfBfaces,1) = field%phiGrad(:,1,1)
        tmp%phi(1:numberOfElements+numberOfBfaces,2) = field%phiGrad(:,2,1)
        tmp%phi(1:numberOfElements+numberOfBfaces,3) = field%phiGrad(:,3,1)

        call computeGradient(tmp)

        call G%create(dim=numberOfElements+numberOfBFaces, phi=tmp%phiGrad(1:numberOfElements+numberOfBFaces,1:3,1:3))
        phi = norm(G)**2

        do iComp=2,field%nComp

            tmp%phi(1:numberOfElements+numberOfBfaces,1) = field%phiGrad(:,1,iComp)
            tmp%phi(1:numberOfElements+numberOfBfaces,2) = field%phiGrad(:,2,iComp)
            tmp%phi(1:numberOfElements+numberOfBfaces,3) = field%phiGrad(:,3,iComp)

            call computeGradient(tmp)

            call G%destroy()
            call G%create(dim=numberOfElements+numberOfBFaces, phi=tmp%phiGrad)
            phi = phi + norm(G)**2

        enddo

    end function gradGradMag

!**********************************************************************************************************************

    function parsedSourceTerm(field) result(bC)

    !==================================================================================================================
    ! Description:
    !! sourceTerm returns a source term array as specified in settings/volumeSource.
    !==================================================================================================================

        use parseFromFile

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: sourceName
        !! expression value

        character(len=:), allocatable :: sourceType
        !! source type

        character(len=:), allocatable :: expr
        !! expression value
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, fComp
    !------------------------------------------------------------------------------------------------------------------

        real ::  bC(numberOfElements, field%nComp)
        !! return array

        real :: sourceTermValue(numberOfElements, field%nComp)
        !! source term to add
   !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        sourceTermValue = 0.0

        bC = 0.0

        ! Get the source terms to be added
        do i=1,size(flubioOptions%volumeSourceNames)

            sourceName = trim(adjustl(flubioOptions%volumeSourceNames(i)))

            ! Check the source type
            call flubioOptions%json%get('VolumeSources%'//sourceName//'%type', sourceType, found)

            call flubioOptions%json%get('VolumeSources%'//sourceName//'%expression', expr, found)
            if(.not. found) call flubioStopMsg('ERROR: cannot find the expression for the volume source "'//sourceName//'"')

            call expressionParsingWithField(expr, sourceTermValue, field)

            bC = bC + sourceTermValue

        end do

    end function parsedSourceTerm

! *********************************************************************************************************************

    function hardCodedSourceTerm(nComp) result(bC)

    !==================================================================================================================
    ! Description:
    !! hardCodeSourceTerm returns a source term array as specified in settings/volumeSource.
    !==================================================================================================================

        character(len=:), allocatable :: sourceName
        !! expression value

        character(len=:), allocatable :: sourceType
        !! source type
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, nComp
    !------------------------------------------------------------------------------------------------------------------

        real ::  bC(numberOfElements, nComp)
        !! return array

        real :: sourceTermValue(numberOfElements, nComp)
        !! source term to add

        real, dimension(:), allocatable :: rvec
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        sourceTermValue = 0.0
        bC = 0.0

        ! Get the source terms to be added
        do i=1,size(flubioOptions%volumeSourceNames)

            sourceName = trim(adjustl(flubioOptions%volumeSourceNames(i)))

            ! Check the source type
            call flubioOptions%json%get('VolumeSources%'//sourceName//'%type', sourceType, found)
            if(.not. found) call flubioStopMsg('ERROR: cannot find the source type "'//sourceType//'"')

            if(sourceType == 'constantForcing') then

                call flubioOptions%json%get('VolumeSources%'//sourceName//'%expression', rvec, found)
                if(.not. found) cycle

                do iElement=1,numberOfElements
                    sourceTermValue(iElement,:) = rvec*mesh%volume(iElement)
                end do

            ! Add here more
            else
                sourceTermValue = 0.0
            end if

            bC = bC + sourceTermValue

        end do

    end function hardCodedSourceTerm

! *********************************************************************************************************************

end module operators
