module QField

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: velocity
    use flubioMpi
    use json_module
    use runTimeObject
    implicit none

    type, public, extends(runTimeObj) :: Q

        character(len=30) :: fieldName
        !! Name of the field

        integer :: nComp
        !! number of components

        real, dimension(:,:), allocatable :: phi
        !! field variable

    contains

        procedure :: initialise => createQ
        procedure :: run =>  computeQ
        procedure :: writeFieldFunction

    end type Q

contains

    subroutine createQ(this)

    !==================================================================================================================
    ! Description:
    !! createQ is the field constructor and it allocates the memory for a new flubioField.
    !==================================================================================================================

        class(Q) :: this
    !-------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)

        call jcore%get(monitorPointer, 'saveEvery', this%runAt, found)
        call raiseKeyErrorJSON('saveEvery', found)

        this%fieldName = 'Q'
        this%nComp = 1
        allocate(this%phi(numberOfElements+numberOfBFaces,this%nComp))

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createQ

! *********************************************************************************************************************

    subroutine computeQ(this)

    !==================================================================================================================
    ! Description:
    !! computeQcriterion computes Q criterion as:
    !! \begin{equation*}
    !!  Q = 0.5*(|\Omega|^2 + |S|^2)
    !! \end{equation*}
    !==================================================================================================================

        class(Q):: this
	!------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: trGradU, omega(3), Q
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then

            do i=1,numberOfElements+numberOfBFaces

		        trGradU = velocity%phiGrad(i,1,1) + velocity%phiGrad(i,3,2) + velocity%phiGrad(i,3,3)

		        omega(1) = velocity%phiGrad(i,1,2)*velocity%phiGrad(i,2,1)
		        omega(2) = velocity%phiGrad(i,1,3)*velocity%phiGrad(i,3,1)
		        omega(3) = velocity%phiGrad(i,3,2)*velocity%phiGrad(i,2,3)

		        Q = 0.5*(trGradU**2 -omega(1)-omega(2)-omega(3))

		        this%phi(i,1) = Q

            end do

            call this%writeFieldFunction

        endif

    end subroutine computeQ

!**********************************************************************************************************************

    subroutine writeFieldFunction(this)

    !==================================================================================================================
    ! Description:
    !! writeToFile writes the field to a file to be post processed.
    !==================================================================================================================

        class(Q) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.

        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp = 1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp=1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        end if

    end subroutine writeFieldFunction

end module QField
