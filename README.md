Please refer to the following files to install the code and obtain some software reference:

- Install: FLUBIO_INSTALL.pdf

- Mesh: FLUBIO relies on OpenFOAM mesh format and the mesh can be partitioned using our partitioner taking advantage of MEtis or kahip

- Software reference paper: "FLUBIO - An unstructured, parallel, finite-volume based Navier-Stokes and convection-diffusion like equations solver for teaching and research purposes", 2020, submitted to softwareX

- Software documentation (generated with FORD): FLUBIO/doc/index.htm

- FLUBIO tutorials: FLUBIO/examples/tutorials.pdf

FLUBIO is on constant development, the suggestion is to check out the latest version (or master branch) as it might contain many fixes and additons. If you are experimenting issues, you have a question or need a bit of support, please use the issue tracker on this repository. Thanks! 


