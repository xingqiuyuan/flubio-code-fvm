!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module pressureEq

!==================================================================================================================
! Description:
!! This module contains the data structure and the methods for the pressure-correction equation.
!==================================================================================================================

    use flubioDictionaries
    use fieldvar
    use momentum
    use physicalConstants

    implicit none

    type, public, extends(transportEqn) :: pressureEqn

        type(flubioField) :: Dv
        !! It is used to build the pressure correction coefficients (Moukalled et al., pp. 594-597).

        real, dimension(:,:), allocatable :: massResidual
        !! mass imbalance (i.e. pressure-correction eq rhs)

        real :: res5
        !! Maxiumum continuity residual after 5 iterations

        real :: resCont
        !! Continuity residualScaled with res5

        contains
                
            procedure :: createEq => createPressureEq
            procedure :: destroyEq => destroyPressureEq
            procedure :: createEqCoeffs => createPressureEqCoeffs
            procedure :: destroyEqCoeffs => destroyPressureEqCoeffs
            procedure :: setPressureUnderRelaxationFactor
            procedure::  setPressureEqOptions
        
            procedure :: assemblePressureCorrectionFluxes
            procedure :: assemblePressureFluxes
            procedure :: assemblePressureCorrectionEq
            procedure :: assemblePressureEq
            procedure :: pressureBoundaryConditions
            procedure :: pressureBoundaryConditionsFS
            procedure :: updatePressureCrossTerm
            procedure :: updatePressureCrossTermAndRC
            procedure :: rhieChowInterpolation
            procedure :: setPressureReference

            procedure :: bodyForceCorrection

            procedure :: writeMassImbalance
            procedure :: printMassImbalance

    end type pressureEqn

contains

! *********************************************************************************************************************

    subroutine createPressureEq(this, eqName, nComp)

    !==================================================================================================================
    ! Description:
    !! createPressureEq is the class contructor.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: nComp
    !------------------------------------------------------------------------------------------------------------------

        this%nComp = nComp

        call this%createEqCoeffs()

        call this%Dv%setUpWorkField('Dv', classType='scalar', nComp=3)

        allocate(this%massResidual(numberOfElements,1))
        this%massResidual = 0.0

        allocate(mesh%Tfp(numberOfFaces,3))
        mesh%Tfp = 0.0

        allocate(this%res(1))
        allocate(this%scalingRef(1))
        allocate(this%unscaledRes(1))
        allocate(this%convergenceResidual(1))
        allocate(this%resmax(1))

        this%res = 0.0
        this%scalingRef = 0.0
        this%unscaledRes = 0.0
        this%convergenceResidual = 0.0
        this%resmax = 0.0

        if(flubioControls%restFrom==-1) this%res5 = 0.0
        this%resCont = 0.0

        this%eqName = eqName

        this%isAllocated = .false.

    end subroutine createPressureEq

! *********************************************************************************************************************

    subroutine destroyPressureEq(this)

    !==================================================================================================================
    ! Description:
    !! destroyPressureEq is the class de-contructor.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        call this%destroyEqCoeffs()

        deallocate(this%massResidual)

        deallocate(this%res)
        deallocate(this%scalingRef)
        deallocate(this%unscaledRes)
        deallocate(this%convergenceResidual)
        deallocate(this%resmax)

        call MatDestroy(this%A, ierr)
        call VecDestroy(this%rhs, ierr)

    end subroutine destroyPressureEq

! *********************************************************************************************************************

    subroutine createPressureEqCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! createPressureEq is the class contructor.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        if(.not. this%coeffsAllocated) then
            allocate(this%aC(numberOfElements, this%nComp))
            allocate(this%anb(numberOfElements))
            allocate(this%bC(numberOfElements, this%nComp))

            do iElement=1,numberOfElements
                this%anb(iElement)%csize_i = mesh%numberOfNeighbours(iElement)
                this%anb(iElement)%csize_j = this%nComp
                call this%anb(iElement)%initRealMat(mesh%numberOfNeighbours(iElement), this%nComp)
            end do

            this%aC = 0.0
            this%bC = 0.0

            this%coeffsAllocated = .true.
        endif

    end subroutine createPressureEqCoeffs

! *********************************************************************************************************************

    subroutine destroyPressureEqCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! destroyPressureEq is the class de-contructor.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(this%coeffsAllocated) then
            deallocate(this%aC)
            deallocate(this%anb)
            deallocate(this%bC)

            this%coeffsAllocated = .false.
        endif

    end subroutine destroyPressureEqCoeffs

! *********************************************************************************************************************

    subroutine setPressureEqOptions(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setPressureEqOptions sets the option for the pressure-correction equation.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: kspName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, key, solverType, pcType, convOpt
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------!
        ! Solver options !
        !----------------!

        call flubioSolvers%json%get(this%eqName//'%solverType', solverType, found)
        if(found) then 
            this%solverType = solverType
        else
            this%solverType = 'petsc'
        endif

        ! Add additional options as command line, if any
        call this%setSolverOptions()
        if(this%solverType == 'amgcl') call this%getAMGCLOptions()

        call flubioSolvers%json%get(this%eqName//'%solver', solverType, found)
        call raiseKeyErrorJSON(this%eqName//'%solver', found, 'settings/solvers')
        this%solverName = solverType

        call flubioSolvers%json%get(this%eqName//'%preconditioner', pcType, found)
        call raiseKeyErrorJSON(this%eqName//'%preconditioner', found, 'settings/solvers')
        this%pcName = pcType

        call flubioSolvers%setPreconditionerOption(lowercase(this%pcName), this%pc,  this%solverType)

        !--------------------!
        ! Numerical settings !
        !--------------------!

        ! Absolute solver tolerance
        call flubioSolvers%json%get(this%eqName//'%absTol', this%absTol, found)
        call raiseKeyErrorJSON(this%eqName//'%absTol', found, 'settings/solvers')

        ! Relative solver tolerance
        call flubioSolvers%json%get(this%eqName//'%relTol', this%relTol, found)
        call raiseKeyErrorJSON(this%eqName//'%relTol', found, 'settings/solvers')

        !-----------------------!
        ! Steady state controls !
        !-----------------------!

        if(steadySim==1) then
            call flubioSteadyStateControls%json%get(this%eqName//'%urf', this%urf, found)
            call raiseKeyErrorJSON(this%eqName//'%urf', found, 'settings/steadyStateControls')
            this%resmax(:) = flubioSteadyStateControls%massResidual
        else
            this%urf = 1.0
        endif

        if(steadySim==0) call flubioOptions%setPrimeCorrections()

    end subroutine setPressureEqOptions

! *********************************************************************************************************************

    subroutine setPressureUnderRelaxationFactor(this)

    !===================================================================================================================
    ! Description:
    !! setPressureUnderRelaxationFactor sets the explicit relaxation factor for the pressure.
    !===================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        if(steadySim==1) then

            key = this%eqName//'%urf'
            call flubioSteadyStateControls%json%get(key, this%urf, found)
            call raiseKeyErrorJSON(key, found, 'settings/steadyStateControls')

            call flubioSteadyStateControls%json%get(this%eqName//'%massResidual',  this%resmax(1), found)
            call raiseKeyErrorJSON(this%eqName//'%massResidual', found, 'settings/steadyStateControls')
            this%resmax(:) = anumber

        else
            this%urf = 1.0
        endif

    end subroutine setPressureUnderRelaxationFactor

!**********************************************************************************************************************

    function assemblePressureCorrectionFluxes(this) result(fluxC1f)

    !==================================================================================================================
    ! Description:
    !! AssemblePressureCorrectionTerms computes pressure-correction fluxes to be used during the linear system assembling.
    !! Three quantities can be defined and their expression depends on the non-orthogonal correction approach chosen.
    !! For the under-relaxed, approach we have (see Moukalled et al. pp. 597):
    !! \begin{eqnarray}
    !!     FluxC_1_f = -\rho_f D_f \\
    !!     FluxC_2_f = -\rho_f D_f  \\
    !!     FluxV_f =  -m_f -\nabla p^{'}_f \cdot \textbf{T}_f
    !! \begin{eqnarray}
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iFace, iBFace, iBoundary, iProc, is, ie, iComp, nn
    !------------------------------------------------------------------------------------------------------------------

        real :: Df, Sf(3), Efp(3), sumAnb(3)

        real :: fluxC1f(numberOfFaces,1)
    !------------------------------------------------------------------------------------------------------------------
       
        ! Compute Dv
        do iElement=1, numberOfElements
            if(flubioSteadyStateControls%SIMPLEC=='yes' .or. flubioSteadyStateControls%SIMPLEC=='on') then
                nn = mesh%numberOfNeighbours(iElement)
                sumAnb(1) = sum(Ueqn%anb(iElement)%coeffs(1:nn,1))
                sumAnb(2) = sum(Ueqn%anb(iElement)%coeffs(1:nn,2))
                sumAnb(3) = sum(Ueqn%anb(iElement)%coeffs(1:nn,3))
                this%Dv%phi(iElement,:) = mesh%volume(iElement)/(Ueqn%aC(iElement,:) + sumAnb(:))
            else
                this%Dv%phi(iElement,:) = mesh%volume(iElement)/Ueqn%aC(iElement,:)
            endif
        enddo

        ! Destroy Ueqn
        call Ueqn%destroyEqCoeffs()

        call this%Dv%boundaryExtrapolation()
        call this%Dv%linearInterpolation()

        ! Mass fluxes
        call this%rhieChowInterpolation()

        ! Intenal faces
        do iFace=1,numberOfIntFaces

            ! Pressure Correction equation coeffs
            Sf(:) = this%Dv%phif(iFace,:)*mesh%Sf(iFace,:)

            ! Non-Orthogonal correction
            call mesh%pressureNonOrthogonalCorrection(iFace, Sf, Df)

            Efp = Df*mesh%CN(iFace,:)
            mesh%Tfp(iFace,:) = Sf-Efp

            fluxC1f(iFace,1) = rho%phif(iFace,1)*Df

        enddo

        ! Processor Boundaries
        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie

                ! Pressure Correction equation coeffs
                Sf(:) = this%Dv%phif(iBFace,:)*mesh%Sf(iBFace,:)

                ! Non-Orthogonal correction
                call mesh%pressureNonOrthogonalCorrection(iBFace, Sf, Df)

                Efp = Df*mesh%CN(iBFace,:)
                mesh%Tfp(iBFace,:) = Sf-Efp

                fluxC1f(iBFace,1) = rho%phif(iBFace,1)*Df

            enddo

        enddo

    end function assemblePressureCorrectionFluxes

! *********************************************************************************************************************

    function assemblePressureFluxes(this) result(fluxC1f)

    !==================================================================================================================
    ! Description:
    !! AssemblePressureTerms computes pressure fluxes to be used during the linear system assembling of thre pressure equation.
    !! Please note that the pressure equation is aslightly modified with respect to the standand SIMPLE algorithm and it follows
    !! The metodology used in OpenFoam.
    !! Three quantities can be defined and their expression depends on the non-orthogonal correction approach chosen.
    !! For the under-relaxed, approach we have (see Moukalled et al. pp. 597):
    !! \begin{eqnarray}
    !!     FluxC_1_f = -\rho_f D_f \\
    !!     FluxC_2_f = -\rho_f D_f  \\
    !!     FluxV_f =  -m_f -\nabla p^{'}_f \cdot \textbf{T}_f
    !! \begin{eqnarray}
    !==================================================================================================================

        class(pressureEqn) :: this

        type(flubioField) :: ustar
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iFace, iBFace, iBoundary, iOwner, iProc, is, ie, iComp, patchFace, pNeigh, nn, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: corr(3)
        !! auxiliary vector

        real :: H_by_A(numberOfElements,3)
        !! working varialbe to store HbyA values
    !------------------------------------------------------------------------------------------------------------------

        real :: Df, Sf(3), Efp(3), sumAnb(3), dot

        real :: fluxC1f(numberOfFaces,1)
    !------------------------------------------------------------------------------------------------------------------

        !---------!
        ! Prepare !
        !---------!

        vf = 0.0
        mf = 0.0
        H_by_A = 0.0

        ! Override predicted velocity with HbyA (no pressure gradient effect)
        H_by_A = Ueqn%HbyA(velocity)
        velocity%phi(1:numberOfElements, :) = H_by_A

        ! interpolate density at cell faces
        call interpolate(rho, interpType=phys%interpType, opt=-1)

        !----------------------------!
        ! Pressure-Correction Coeffs !
        !----------------------------!

        do iElement=1, numberOfElements
            if(flubioSteadyStateControls%SIMPLEC=='yes' .or. flubioSteadyStateControls%SIMPLEC=='on') then
                nn = mesh%numberOfNeighbours(iElement)
                sumAnb(1) = sum(Ueqn%anb(iElement)%coeffs(1:nn,1))
                sumAnb(2) = sum(Ueqn%anb(iElement)%coeffs(1:nn,2))
                sumAnb(3) = sum(Ueqn%anb(iElement)%coeffs(1:nn,3))
                this%Dv%phi(iElement,:) = rho%phi(iElement,1)*mesh%volume(iElement)/(Ueqn%aC(iElement,:) + sumAnb(:))
            else
                this%Dv%phi(iElement,:) = rho%phi(iElement,1)*mesh%volume(iElement)/Ueqn%aC(iElement,:)
            endif
        enddo

        call this%Dv%boundaryExtrapolation()
        call this%Dv%linearInterpolation()

        ! Destroy Ueqn, free memory 
        call Ueqn%destroyEqCoeffs()

        !---------------------------------!
        ! Interpolate velocities at faces !
        !---------------------------------!

        call interpolate(velocity, interpType='linear', opt=-1)
        vf = velocity%phif

        !-------------------------!
        ! Assemble Interior Faces !
        !-------------------------!

        do iFace=1,numberOfIntFaces

            ! Compute Mass flux
            dot = dot_product(vf(iFace,:),mesh%Sf(iFace,:))
            mf(iFace,1) = rho%phif(iFace,1)*dot

            ! Pressure Correction equation coeffs
            Sf(:) = this%Dv%phif(iFace,:)*mesh%Sf(iFace,:)

            ! Non-Orthogonal correction
            call mesh%pressureNonOrthogonalCorrection(iFace, Sf, Df)

            Efp = Df*mesh%CN(iFace,:)
            mesh%Tfp(iFace,:) = Sf-Efp

            fluxC1f(iFace,1) = rho%phif(iFace,1)*Df

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie

                ! Compute Mass flux
                dot = dot_product(vf(iBFace,:),mesh%Sf(iBFace,:))
                mf(iBFace,1) = rho%phif(iBFace,1)*dot

                ! Pressure Correction equation coeffs
                Sf(:) = this%Dv%phif(iBFace,:)*mesh%Sf(iBFace,:)

                ! Non-Orthogonal correction
                call mesh%pressureNonOrthogonalCorrection(iBFace, Sf, Df)

                Efp = Df*mesh%CN(iBFace,:)
                mesh%Tfp(iBFace,:) = Sf-Efp

                fluxC1f(iBFace,1) = rho%phif(iBFace,1)*Df

            enddo

        enddo

    end function assemblePressureFluxes

! *********************************************************************************************************************

    subroutine assemblePressureCorrectionEq(this)

    !==================================================================================================================
    ! Description:
    !! assemblePressureCorrectionEq assembles the fluxes in the pressure-correction matrix and rhs.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement,iFace, iBoundary, iBFace, iProc, is, ie
    !------------------------------------------------------------------------------------------------------------------

        integer :: iOwner, iNeighbour, iOwnerNeighbourCoef, iNeighbourOwnerCoef
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces,1)

        real :: fluxC2f(1)
    !------------------------------------------------------------------------------------------------------------------

        ! Pressure fluxes
        fluxC1f = this%assemblePressureCorrectionFluxes()

        !  Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            fluxC2f(1) = -fluxC1f(iFace,1)

            ! Assemble fluxes for owner cell
            this%aC(iOwner,1) = this%aC(iOwner,1) + fluxC1f(iFace,1)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(1))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,1) = this%aC(iNeighbour,1) - fluxC2f(1)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace,1))

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                fluxC2f(1) = -fluxC1f(iBFace,1)

                ! Assemble fluxes for owner cell
                this%aC(iOwner,1) = this%aC(iOwner,1) + fluxC1f(iBFace,1)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(1))

            enddo

        enddo

    end subroutine assemblePressureCorrectionEq

! *********************************************************************************************************************

    subroutine assemblePressureEq(this)

    !==================================================================================================================
    ! Description:
    !! assemblePressureEq assembles the fluxes in the pressure matrix and rhs.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement,iFace, iBoundary, iBFace, iProc, is, ie
    !------------------------------------------------------------------------------------------------------------------

        integer :: iOwner, iNeighbour, iOwnerNeighbourCoef, iNeighbourOwnerCoef
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces,1)

        real :: fluxC2f(1)
    !------------------------------------------------------------------------------------------------------------------

        ! Pressure fluxes
        fluxC1f = this%assemblePressureFluxes()

        !  Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            fluxC2f(1) = -fluxC1f(iFace,1)

            ! Assemble fluxes for owner cell
            this%aC(iOwner,1) = this%aC(iOwner,1) + fluxC1f(iFace,1)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(1))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,1) = this%aC(iNeighbour,1) - fluxC2f(1)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, fluxC1f(iFace,1))

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                fluxC2f(1) = -fluxC1f(iBFace,1)

                ! Assemble fluxes for owner cell
                this%aC(iOwner,1) = this%aC(iOwner,1) + fluxC1f(iBFace,1)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, fluxC2f(1))

            enddo

        enddo

    end subroutine assemblePressureEq

! *********************************************************************************************************************

    subroutine updatePressureCrossTerm(this, p)

    !==================================================================================================================
    ! Description:
    !! updatePressureCrossTerm computes the cross diffusion term for the non-orthogonal
    !! corrections during solution of pressure-correction equation.
    !==================================================================================================================

        use gradient

        class(pressureEqn) :: this

        type(flubioField) :: p
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iFace, iBFace, iOwner, iNeighbour, iBoundary, iProc, is, ie, iElement, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,1), dot, dot2, limiter

        real :: fluxVf(numberOfFaces, 1)
    !------------------------------------------------------------------------------------------------------------------

        ! Update p' at boundaries
        call p%boundaryExtrapolation()

        ! Compute pressure correction gradient
        call computeGradient(p)

        ! Interpolate Gradient to Faces
        call interpolateGradientCorrected(phiGrad_f, p, opt=1, fComp=1)

        ! Account for cell non-orthogonality
        do iFace = 1,numberOfFaces

            dot = dot_product(phiGrad_f(iFace,:,1), mesh%Tfp(iFace,:))
            dot2 = dot_product(phiGrad_f(iFace,:,1), this%Dv%phif(iFace,:)*mesh%Sf(iFace,:))

            if(flubioOptions%lambda==1.0) then
                limiter = 1.0
            else
                limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
            end if

            fluxVf(iFace,1) = -rho%phif(iFace,1)*dot*limiter

        enddo

        ! Assemble in pressure equation rhs
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble fluxes for owner and neighbour cell
            this%bC(iOwner,1) = this%bC(iOwner,1) - fluxVf(iFace,1)
            this%bC(iNeighbour,1) =  this%bC(iNeighbour,1) + fluxVf(iFace,1)

        enddo

        ! Processor Boundaries
        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie
                iOwner = mesh%owner(iBFace)
                this%bC(iOwner,1) = this%bC(iOwner,1) - fluxVf(iBFace,1)
            enddo

        enddo

        ! Real Boundaries
        do iBoundary = 1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            
            do iBFace = is,ie

                iOwner = mesh%owner(iBFace)

                ! Do not add non-orthogonal corrections to zero gradient boundaries
                if(trim(pressure%bCond(iBoundary)) /= "neumann0") then
                    this%bC(iOwner,1) = this%bC(iOwner,1) - fluxVf(iBFace,1)
                endif

            enddo

        enddo

    end subroutine updatePressureCrossTerm
    
! *********************************************************************************************************************

    subroutine updatePressureCrossTermAndRC(this, p)

    !==================================================================================================================
    ! Description:
    !! updatePressureCrossTermAndRC computes the cross diffusion term for the non-orthogonal
    !! corrections during solution of pressure-correction equation.
    !==================================================================================================================

        use gradient

        class(pressureEqn) :: this

        type(flubioField) :: p
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iFace, iBFace, iOwner, iNeighbour, iBoundary, iProc, is, ie, iElement, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,1)

        real :: gradPbar(numberOfFaces,3,1)
        
        real :: dot, dot2, limiter

        real :: fluxVf(numberOfFaces, 1)

        real :: fluxVfRC(numberOfFaces, 1)
    !------------------------------------------------------------------------------------------------------------------

        ! Reset pressure equation rhs
        this%bC = 0.0

        ! Update p' at boundaries
        call p%boundaryExtrapolation()

        ! Compute pressure correction gradient
        call computeGradient(p)

        ! Interpolate Gradient to Faces
        call interpolateGradientCorrected(phiGrad_f, p, opt=1, fComp=1)
        call interpolateGradientCorrected(gradPbar, p, opt=0, fComp=1)

        ! Account for cell non-orthogonality
        do iFace = 1,numberOfFaces

            dot = dot_product(phiGrad_f(iFace,:,1),mesh%Tfp(iFace,:))
            dot2 = dot_product(phiGrad_f(iFace,:,1),mesh%Sf(iFace,:))

            if(flubioOptions%lambda==1.0) then
                limiter = 1.0
            else
                limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
            end if

            fluxVf(iFace,1) = -rho%phif(iFace,1)*dot*limiter

            dot = dot_product(this%Dv%phif(iFace,:)*gradPbar(iFace,:,1), mesh%Sf(iFace,:))
            fluxVfRC(iFace,1) = rho%phif(iFace,1)*dot

        enddo

        ! Assemble in pressure equation rhs
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble fluxes for owner and neighbour cell
            this%bC(iOwner,1) = this%bC(iOwner,1) - fluxVf(iFace,1) - fluxVfRC(iFace,1)
            this%bC(iNeighbour,1) =  this%bC(iNeighbour,1) + fluxVf(iFace,1) + fluxVfRC(iFace,1)

        enddo

        ! Processor Boundaries
        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie
                iOwner = mesh%owner(iBFace)
                this%bC(iOwner,1) = this%bC(iOwner,1) - fluxVf(iBFace,1) - fluxVfRC(iBFace,1)
            enddo

        enddo

        ! Real Boundaries
        do iBoundary = 1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace = is,ie

                iOwner = mesh%owner(iBFace)

                ! Do not add the non-orthogonal correction for zero gradient patches
                if(trim(pressure%bCond(iBoundary)) /= "neumann0") then
                    this%bC(iOwner,1) = this%bC(iOwner,1) - fluxVf(iBFace,1)
                endif

                ! Add the RC correction for fixed pressure patches only
                if(trim(pressure%bCond(iBoundary)) == "fixedPressure") then
                    this%bC(iOwner,1) = this%bC(iOwner,1) - fluxVfRC(iBFace,1)
                endif

            enddo

        enddo

    end subroutine updatePressureCrossTermAndRC

! *********************************************************************************************************************

    subroutine setPressureReference(this)

    !==================================================================================================================
    ! Description:
    !! setPressureReference simply sets the value of pressure at cell number 1. Actually this is done by default and automatically.
    !! if you need to set the cell in a specific way, change the index here or add a dictonary entry.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: opt
        !! option value
    !------------------------------------------------------------------------------------------------------------------

        integer :: flag, iBoundary

        integer :: pCell
        !! cell number where to set the pressure
    !------------------------------------------------------------------------------------------------------------------

        real :: pRef
        !! pressure reference value
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! Look for boundary conditions
        flag = 0

        do iBoundary=1, numberOfRealBound

            if(pressure%bcFlag(iBoundary)==2 .or. &
                    pressure%bcFlag(iBoundary)==0 .or. &
                    pressure%bcFlag(iBoundary)==-1) flag=flag+1

        enddo

        if(id==0 .and. flag==numberOfRealBound) then

            call flubioSolvers%json%get('pCell', opt, found)
            if(.not. found) opt='none'

            if(trim(opt)/='none') then

                call flubioSolvers%json%get('pCell', pCell, found)
                call raiseKeyErrorJSON('pCell', found, 'settings/solvers')

                call flubioSolvers%json%get('pRef', pRef, found)
                call raiseKeyErrorJSON('pRef', found, 'settings/solvers')

            else
                pCell = 1
                pRef = 0.0
            end if

                this%aC(pCell,1) = 1.0
                this%anb(pCell)%coeffs(:,1) = 0.0
                this%bC(pCell,1) = pRef

            ! Sparisty pattern preserving
            !this%bC(pCell,1) = this%bC(pCell,1) + this%aC(pCell,1)*pRef
            !this%aC(pCell,1) = 2.0*this%aC(pCell,1)

            if(id==0 .and. itime==1) write(*,*) 'FLUBIO: Setting pressure reference'

        endif

    end subroutine setPressureReference

! *********************************************************************************************************************

    subroutine pressureBoundaryConditions(this)

    !==================================================================================================================
    ! Description:
    !! pressureBoundaryConditions applies the boundary condition to the pressure-correction equation.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBFace, iBoundary, iOwner, iProc, is, ie, iComp, patchFace, pNeigh, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: Df, Sf(3), Efp(3), dot, dot1, fluxCb
    !------------------------------------------------------------------------------------------------------------------

        ! Loop over boundaries
        do iBoundary = 1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace = is,ie

                iOwner = mesh%owner(iBFace)

                ! Non-Orthogonal correction
                Sf(:) = this%Dv%phi(iOwner,:)*mesh%Sf(iBFace,:)
                call mesh%pressureNonOrthogonalCorrection(iBFace, Sf, Df)

                Efp = Df*mesh%CN(iBFace,:)
                mesh%Tfp(iBFace,:) = Sf - Efp

                ! void
                if(pressure%bcFlag(iBoundary)==0) then

                    call voidPressureBoundary(fluxCb)

                ! Dirichlet
                elseif(pressure%bcFlag(iBoundary)==1) then

                    call dirichletPressureBoundary(rho%phif(iBFace,1)*Df, fluxCb)

                ! Zero Gradient
                elseif (pressure%bcFlag(iBoundary)==2) then

                    call neumann0PressureBoundary(fluxCb)

                elseif (pressure%bcFlag(iBoundary)==3) then

                    call farFieldPressureBoundary(iBFace, rho%phif(iBFace,1)*Df, fluxCb)

                else
                    call flubioStopMsg( 'FLUBIO: Boundary condition for pressure not found')
                endif

                this%aC(iOwner,1) = this%aC(iOwner,1) + fluxCb
     
            enddo

        enddo

    end subroutine pressureBoundaryConditions

! *********************************************************************************************************************

    subroutine pressureBoundaryConditionsFS(this)

    !==================================================================================================================
    ! Description:
    !! pressureBoundaryConditionsFS applies the boundary condition to the pressure equation in the fractional step method.
    !==================================================================================================================
    
        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: iBFace, iBoundary, iOwner, iProc, is, ie, iComp, patchFace, pNeigh, ierr
    !------------------------------------------------------------------------------------------------------------------
    
        real :: aC
        !! diagonal coefficient correction
        
        real :: bC
        !! rhs correction
    !------------------------------------------------------------------------------------------------------------------
    
        ! Loop over boundaries
        do iBoundary = 1,numberOfRealBound
    
            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
    
            do iBFace = is,ie
    
                iOwner = mesh%owner(iBFace)
        
                ! void
                if(pressure%bcFlag(iBoundary)==0) then
    
                    call voidBoundary(aC, bC, 1)

                ! Fixed Pressure
                elseif(pressure%bcFlag(iBoundary)==1) then
    
                    call dirichletBoundary(iBFace, aC, bC,  pressure%boundaryValue(iBoundary,1), 1.0, 0.0, 1)
    
                ! Zero Gradient
                elseif (pressure%bcFlag(iBoundary)==2) then
    
                    call neumann0Boundary(iBFace, aC, bC, 0.0, 1)

                elseif (pressure%bcFlag(iBoundary)==3) then
    
                    call flubioStopMsg('FLUBIO: Sorry not tested yet.')

                elseif (pressure%bcFlag(iBoundary)==4) then

                    call dirichletBoundary(iBFace, aC, bC,  pressure%boundaryValue(iBoundary,1), 1.0, 0.0, 1)

                else
                    call flubioStopMsg( 'FLUBIO: Boundary condition for pressure not found')
                endif
    
                this%aC(iOwner,1) = this%aC(iOwner,1) + aC
                this%bC(iOwner,1) = this%bC(iOwner,1) + bC
         
            enddo
     
        enddo
    
    end subroutine pressureBoundaryConditionsFS
    
!**********************************************************************************************************************

    function underRelaxationCorrection() result(relaxCorr)

    !==================================================================================================================
    ! Description:
    !! underRelaxationCorrection computes a correction for the face velocity which depends on the under-relaxation factor.
    !! \begin{eqnarray}
    !! v_f^urf = (1-\alpha^U)*(v_f^o-\bar{v_f^o}),
    !! \begin{eqnarray}
    !! where $*^o$ stands for the old time level values of the velocity field.
    !==================================================================================================================

        type(flubioField) :: tmp
    !------------------------------------------------------------------------------------------------------------------

        real :: relaxCorr(numberOfFaces,3)
        !! under-relaxation factor correction for Rhie and Chow interpolation

        real :: vold_f(numberOfFaces,3)

    !------------------------------------------------------------------------------------------------------------------

        !--------------------------------!
        ! Interpolate vold at mesh faces !
        !--------------------------------!

        !call interpolateElementToFace(vold_f, velocity%phio, velocity%ghosts0, 3, 0)
        !relaxCorr = (1.0-Ueqn%urf)*(vf - vold_f)*real(steadySim)

        call tmp%copyField("copyVelocity", velocity%nComp, velocity)
        tmp%phi = velocity%phio
        call tmp%linearInterpolation()
        relaxCorr = (1.0-Ueqn%urf)*(vf - tmp%phif)*real(steadySim)
        call tmp%destroyField() 

        call flubioStopMsg('This function needs to be reviesed. Ghost0 is not available anymore')

    end function underRelaxationCorrection

!**********************************************************************************************************************

    subroutine rhieChowInterpolation(this)

    !==================================================================================================================
    ! Description:
    !! rhieChowInterpolation compute the mass flow according to the Rhie and Chow formulation.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, iBFace, iProc, is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: pressGrad_f(numberOfFaces,3,1)
        !!  pressure gradient interpolated (with correction) at mesh faces

        real :: gradPbar(numberOfFaces,3,1)
        !! pressure gradient linearly interpolated at mesh faces

        real :: relaxCorr(numberOfFaces,3)
        !! under-relaxation factor correction for Rhie and Chow interpolation

        real :: dot, corr(3)
    !------------------------------------------------------------------------------------------------------------------

        !---------!
        ! Prepare !
        !---------!

        vf = 0.0
        mf = 0.0
        relaxCorr = 0.0

        call interpolateGradientCorrected(pressGrad_f, pressure, opt=1, fComp=1)

        call interpolateGradientCorrected(gradPbar, pressure, opt=0, fComp=1)

        call interpolate(rho, interpType=phys%interpType, opt=-1)

        if(flubioOptions%urfCorr==1) relaxCorr = underRelaxationCorrection()

        call interpolate(velocity, interpType='linear', opt=-1)
        vf = velocity%phif - relaxCorr

        !-------------------------!
        ! Assemble Interior Faces !
        !-------------------------!

        do iFace=1,numberOfIntFaces

            ! Correct face velocities with Rhie and Chow interpolation
            corr = pressGrad_f(iFace,:,1) - gradPbar(iFace,:,1)

            vf(iFace,:) = vf(iFace,:) - this%Dv%phif(iFace,:)*corr

            ! Compute Mass flux
            dot = dot_product(vf(iFace,:),mesh%Sf(iFace,:))
            mf(iFace,1) = rho%phif(iFace,1)*dot

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary = 1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace = is,ie

                ! Correct face velocities with Rhie and Chow interpolation
                corr = pressGrad_f(iBFace,:,1) - gradPbar(iBFace,:,1)
                vf(iBFace,:) = vf(iBFace,:) - this%Dv%phif(iBFace,:)*corr

                ! Compute Mass flux
                dot = dot_product(vf(iBFace,:), mesh%Sf(iBFace,:))
                mf(iBFace,1) = rho%phif(iBFace,1)*dot

            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        do iBoundary = 1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace = is,ie

                ! Compute Mass flux
                dot = dot_product(vf(iBFace,:), mesh%Sf(iBFace,:))
                mf(iBFace,1) = rho%phif(iBFace,1)*dot

            enddo

        enddo

    end subroutine rhieChowInterpolation

!**********************************************************************************************************************

    function bodyForceCorrection(this) result(relaxCorr)

    !==================================================================================================================
    ! Description:
    !! underRelaxationCorrection computes a correction for the face velocity depending on the under-relaxation factor.
    !! \begin{eqnarray}
    !! \begin{eqnarray}
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iFace, iBFace, iBoundary, iOwner, iProc, is, ie, iComp, patchFace, pNeigh, nn, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: relaxCorr(numberOfFaces,3)
        !! under-relaxation factor correction for Rhie and Chow interpolation
    !------------------------------------------------------------------------------------------------------------------

        ! TO BE WRITTEN YET

    end function bodyForceCorrection

!**********************************************************************************************************************

    subroutine writeMassImbalance(this)

    !==================================================================================================================
    ! Description:
    !! writeresiduals writes the residual in a data file ready to plot.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: header
    !------------------------------------------------------------------------------------------------------------------

        !-----------------!
        ! Write residuals !
        !-----------------!

        header = '# Iteration      massImbalance      ScaledResidual      UnscaledResidual'

        if(id==0 .and. flubioControls%saveResiduals==1 .and. mod(itime,flubioControls%resOutputFrequency)==0) then

            if(itime<=1) then

                open(1,file='postProc/monitors/res-'//trim(this%eqName)//'.dat',status='replace')
                    write(1,*) header
                    write(1,*) itime, this%resCont, this%convergenceResidual/this%scalingRef, this%convergenceResidual
                close(1)

            else

                open(1,file='postProc/monitors/res-'//trim(this%eqName)//'.dat',access='append')
                    write(1,*) itime, this%resCont, this%convergenceResidual/this%scalingRef, this%convergenceResidual
                close(1)

            endif

        endif

    end subroutine writeMassImbalance

! *********************************************************************************************************************

    subroutine printMassImbalance(this)

    !==================================================================================================================
    ! Description:
    !! printMassImbalance prints to screeb the actual mass imbalance.
    !==================================================================================================================

        class(pressureEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=80) :: FMT
    !------------------------------------------------------------------------------------------------------------------

        integer :: fComp
    !------------------------------------------------------------------------------------------------------------------

        FMT = "(A30, ES13.6)"

        if(id == 0) then
            write(*,FMT) '  '//trim(this%eqName)//' initial mass imbalance: ', this%resCont
        endif

    end subroutine printMassImbalance

end module pressureEq
