!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module diffusionFluxes

!==================================================================================================================
! Description:
!! DiffusionFluxes contains the data structure and the methods to assemble the diffusion fluxes at
!! mesh faces, arising from the numerical approximation of the diffusion term.
!! The fluxes are then used to assemble the diffusion term in the equation matrix and rhs.
!==================================================================================================================

    use flubioFields
    use meshvar
    use interpolation
    use physicalConstants

    implicit none
    interface assembleDiffusionFluxes
        module procedure :: assembleDiffusionFluxesFieldDiffusivity
        module procedure :: assembleDiffusionFluxesWithConstantDiffusivity
        module procedure :: assembleDiffusionFluxesWithoutCrossDiffusionConst
        module procedure :: assembleDiffusionFluxesWithoutCrossDiffusionField   
    end interface assembleDiffusionFluxes
    interface updateCrossDiffusion
        module procedure :: updateCrossDiffusionStd
        module procedure :: updateCrossDiffusionWithConstantDiffusivity         
    end interface updateCrossDiffusion

contains

! *********************************************************************************************************************

    subroutine assembleDiffusionFluxesFieldDiffusivity(field, nu_, fluxC1f, fluxC2f, fluxVf)

    !==================================================================================================================
    ! Description:
    !! AssembleDiffusionTerm computes the diffusion fluxes to be used in linear system assembly.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = \mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxC_2_f = -\mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \begin{eqnarray}
    !==================================================================================================================

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation

        type(flubioField) :: nu_
        !! diffusion field (might be constant or variable)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, field%nComp)
        !! flux owner side
        
        real :: fluxC2f(numberOfFaces, field%nComp)
        !! flux neighbour side
        
        real :: fluxVf(numberOfFaces, field%nComp)
        !! correction flux (i.e. non-orthogonal corrections)
    !------------------------------------------------------------------------------------------------------------------

        fluxVf = 0.0

        fComp = field%nComp

        ! Interpolate diffusivity
        call interpolate(nu_, interpType=phys%interpType, opt= -1)

        ! Assemble fluxes
        do iComp=1,fComp

            if(nu_%nComp > 1) then 
                fluxC1f(:, iComp) = nu_%phif(:,iComp)*mesh%gDiff
                fluxC2f(:, iComp) = -nu_%phif(:,iComp)*mesh%gDiff
            else 
                fluxC1f(:, iComp) = nu_%phif(:,1)*mesh%gDiff
                fluxC2f(:, iComp) = -nu_%phif(:,1)*mesh%gDiff
            endif 

        end do

        ! Cross diffusion
        call updateCrossDiffusion(field, nu_, fluxVf)

    end subroutine assembleDiffusionFluxesFieldDiffusivity

!**********************************************************************************************************************

    subroutine assembleDiffusionFluxesWithoutCrossDiffusionField(nu_, fComp, fluxC1f, fluxC2f)

    !==================================================================================================================
    ! Description:
    !! assembleDiffusionFluxesWithoutCrossDiffusion computes the diffusion fluxes to be used in linear system assembly
    !! without cross-diffusion.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = \mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxC_2_f = -\mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxV_f = 0.0
    !! \begin{eqnarray}
    !==================================================================================================================

        type(flubioField) :: nu_
        !! diffusion field (might be constant or variable)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, fComp)
        !! flux owner side
        
        real :: fluxC2f(numberOfFaces, fComp)
        !! flux neighbour side
        
        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product
    !------------------------------------------------------------------------------------------------------------------

        ! Interpolate diffusivity
        call interpolate(nu_,  interpType=phys%interpType, opt= -1)

        ! Assemble fluxes
        do iComp=1,fComp

            if(nu_%nComp > 1) then 
                fluxC1f(:, iComp) = nu_%phif(:,iComp)*mesh%gDiff
                fluxC2f(:, iComp) = -nu_%phif(:,iComp)*mesh%gDiff
            else 
                fluxC1f(:, iComp) = nu_%phif(:,1)*mesh%gDiff
                fluxC2f(:, iComp) = -nu_%phif(:,1)*mesh%gDiff
            endif

        end do

    end subroutine assembleDiffusionFluxesWithoutCrossDiffusionField

!**********************************************************************************************************************

    subroutine assembleDiffusionFluxesWithoutCrossDiffusionConst(nu_, fComp, fluxC1f, fluxC2f)

    !==================================================================================================================
    ! Description:
    !! assembleDiffusionFluxesWithoutCrossDiffusionConst computes the diffusion fluxes to be used in linear system assembly
    !! without cross-diffusion.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = \mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxC_2_f = -\mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxV_f = 0.0
    !! \begin{eqnarray}
    !==================================================================================================================
    
        integer :: iFace, iComp, fComp, opt
    !------------------------------------------------------------------------------------------------------------------
    
        real :: fluxC1f(numberOfFaces, fComp)
        !! flux owner side
        
        real :: fluxC2f(numberOfFaces, fComp)
        !! flux neighbour side

        real :: nu_
        !! diffusion coefficient
            
        real :: nu_f
        !! face viscosity
    
        real :: dot
        !! auxilairy variable to store a scalar product
    !------------------------------------------------------------------------------------------------------------------
    
        ! Assemble fluxes
        do iComp=1,fComp
            fluxC1f(:, iComp) = nu_*mesh%gDiff
            fluxC2f(:, iComp) = -nu_*mesh%gDiff
        end do
    
    end subroutine assembleDiffusionFluxesWithoutCrossDiffusionConst
    
!**********************************************************************************************************************
    
    subroutine updateCrossDiffusionStd(field, nu_, fluxVf)

    !==================================================================================================================
    ! Description:
    !! updateCrossDiffusion computes the cross-diffusion term arising from mesh non-orthogonality.
    !! Cross-diffsuion is computed as:
    !! \begin{equation*}
    !!    FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \end{equation*}
    !==================================================================================================================

        type(flubioField) :: field
        !! Field targeted by the transport equation of which the gradient at mesh faces is required

        type(flubioField) :: nu_
        !! Diffusion coefficient field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt, fsize
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxVf(numberOfFaces, field%nComp)
        !! correction flux (i.e. non-orthogonal corrections)

        real :: phiGrad_f(numberOfFaces,3,field%nComp)
        !! field gradient interpolated at mesh faces

        real :: nu_f
        !! diffusion coefficient interpolated at the cell face

        real :: dot, dot2
        !! dot product

        real :: limiter
        !! non-orthogonal correction limiter
    !------------------------------------------------------------------------------------------------------------------

        fsize = field%nComp

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=fsize)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,fSize

            ! Internal faces
            do iFace=1,numberOfIntFaces

                if(nu_%nComp > 1) then 
                    nu_f = nu_%phif(iFace,iComp)
                else 
                    nu_f = nu_%phif(iFace,1)
                endif 

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(flubioOptions%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                end if

                fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

            ! Processor faces
            do iBoundary = 1,numberOfProcBound

                iProc = mesh%boundaries%procBound(iBoundary)
                is = mesh%boundaries%startFace(iProc)
                ie = is+mesh%boundaries%nFace(iProc)-1
    
                do iBFace = is,ie

                    if(nu_%nComp > 1) then 
                        nu_f = nu_%phif(iBFace,iComp)
                    else 
                        nu_f = nu_%phif(iBFace,1)
                    endif 
                    
                    dot = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Tf(iBFace,:))
                    dot2 = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Sf(iBFace,:))
    
                    if(flubioOptions%lambda==1.0) then
                        limiter = 1.0
                    else
                        limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                    end if
    
                    fluxVf(iBFace,iComp) = -nu_f*dot*limiter

                enddo
    
            enddo

            ! Boundary faces
            do iBoundary = 1,numberOfRealBound

                is = mesh%boundaries%startFace(iBoundary)
                ie = is+mesh%boundaries%nFace(iBoundary)-1

                do iBFace = is,ie

                    if(nu_%nComp > 1) then 
                        nu_f = nu_%phif(iBFace,iComp)
                    else 
                        nu_f = nu_%phif(iBFace,1)
                    endif 

                    dot = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Tf(iBFace,:))
                    dot2 = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Sf(iBFace,:))
    
                    if(flubioOptions%lambda==1.0) then
                        limiter = 1.0
                    else
                        limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                    end if

                    ! Do not use non-orthogonal correction for fixed gradient type boundary conditions 
                    if(trim(field%bCond(iBoundary)) == "neumann" .or. & 
                       trim(field%bCond(iBoundary)) == "neumann0" .or. & 
                       trim(field%bCond(iBoundary)) == "robin") then 
                        fluxVf(iBFace,iComp) = 0.0
                    else 
                        fluxVf(iBFace,iComp) = -nu_f*dot*limiter
                    endif
                   
                enddo

            enddo

        enddo

    end subroutine updateCrossDiffusionStd

! *********************************************************************************************************************

    subroutine assembleDiffusionFluxesWithConstantDiffusivity(field, nu_f, fluxC1f, fluxC2f, fluxVf)

    !==================================================================================================================
    ! Description:
    !! AssembleDiffusionTerm computes the diffusion fluxes to be used in linear system assembly.
    !! For each face, we can define three quantities:
    !! \begin{eqnarray}
    !!  FluxC_1_f = \mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxC_2_f = -\mu_f\frac{|\textbf{S}|_f}{d_{CN}} \\
    !!  FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \begin{eqnarray}
    !==================================================================================================================

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt, fsize
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxC1f(numberOfFaces, field%nComp)
        !! flux owner side
        
        real :: fluxC2f(numberOfFaces, field%nComp)
        !! flux neighbour side
        
        real :: fluxVf(numberOfFaces, field%nComp)
        !! correction flux (i.e. non-orthogonal corrections)

        real :: nu_f
        !! constant diffusivity
    !------------------------------------------------------------------------------------------------------------------
        
        fluxVf = 0.0

        fComp = field%nComp

        ! Assemble fluxes
        do iComp=1,fComp
            fluxC1f(:, iComp) = nu_f*mesh%gDiff
            fluxC2f(:, iComp) = -nu_f*mesh%gDiff
        end do

        ! Cross diffusion
        call updateCrossDiffusionWithConstantDiffusivity(field, nu_f, fluxVf)

    end subroutine assembleDiffusionFluxesWithConstantDiffusivity

!**********************************************************************************************************************

    subroutine updateCrossDiffusionWithConstantDiffusivity(field, nu_f, fluxVf)

    !==================================================================================================================
    ! Description:
    !! updateCrossDiffusion computes the cross-diffusion term arising from mesh non-orthogonality.
    !! Cross-diffsuion is computed as:
    !! \begin{equation*}
    !!    FluxV_f = \mu_f\nabla\phi_f \cdot \textbf{T}_f
    !! \end{equation*}
    !==================================================================================================================

        type(flubioField) :: field
        !! Field targeted by the transport equation of which the gradient at mesh faces is required
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iComp, fComp, is, ie, iProc, pNeigh, opt, fsize
    !------------------------------------------------------------------------------------------------------------------

        real :: fluxVf(numberOfFaces, field%nComp)
        !! correction flux (i.e. non-orthogonal corrections)

        real :: phiGrad_f(numberOfFaces,3,field%nComp)
        !! field gradient interpolated at mesh faces

        real :: nu_f
        !! diffusion coefficient interpolated at the cell face

        real :: dot, dot2
        !! a dot prduct

        real :: limiter
        !! non-orthogonal correction limiter
    !------------------------------------------------------------------------------------------------------------------

        fsize = field%nComp

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=fsize)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,fSize

            do iFace=1,numberOfIntFaces

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(flubioOptions%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                end if

                fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

            ! Processor faces
            do iBoundary = 1,numberOfProcBound

                iProc = mesh%boundaries%procBound(iBoundary)
                is = mesh%boundaries%startFace(iProc)
                ie = is+mesh%boundaries%nFace(iProc)-1
    
                do iBFace = is,ie
                    
                    dot = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Tf(iBFace,:))
                    dot2 = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Sf(iBFace,:))
    
                    if(flubioOptions%lambda==1.0) then
                        limiter = 1.0
                    else
                        limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                    end if
    
                    fluxVf(iBFace,iComp) = -nu_f*dot*limiter

                enddo
    
            enddo

            ! Boundary faces
            do iBoundary = 1,numberOfRealBound

                is = mesh%boundaries%startFace(iBoundary)
                ie = is+mesh%boundaries%nFace(iBoundary)-1

                do iBFace = is,ie

                    dot = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Tf(iBFace,:))
                    dot2 = dot_product(phiGrad_f(iBFace,:,iComp), mesh%Sf(iBFace,:))
    
                    if(flubioOptions%lambda==1.0) then
                        limiter = 1.0
                    else
                        limiter = limitCrossDiffusion(dot2, dot, flubioOptions%lambda)
                    end if

                    ! Do not use non-orthogonal correction for fixed gradient type boundary conditions 
                    if(trim(field%bCond(iBoundary)) == "neumann" .or. & 
                       trim(field%bCond(iBoundary)) == "neumann0" .or. & 
                       trim(field%bCond(iBoundary)) == "robin") then 
                        fluxVf(iBFace,iComp) = 0.0
                    else 
                        fluxVf(iBFace,iComp) = -nu_f*dot*limiter
                    endif
                   
                enddo

            enddo

        enddo

    end subroutine updateCrossDiffusionWithConstantDiffusivity

! *********************************************************************************************************************

    subroutine totalDiffusionFluxes(field, nu_, fluxTf)

    !==================================================================================================================
    ! Description:
    !! TotalDiffusionFluxes computes the total diffusion fluxes at internal faces only.
    !! This method must be called after assembleDiffsuionFluxes, once fluxC1f and fluxC2f are known.
    !! the total flux reads:
    !! \begin{eqnarray}
    !! FluxT_f = FluxC_1_f\phi_C - FluxC_2_f\phi_N + FluxV_f
    !! \begin{eqnarray}
    !==================================================================================================================

        type(flubioField) :: field
        !! Field targeted by the transport equation of which the gradient at mesh faces is required

        type(flubioField) :: nu_
        !! Diffusion coefficient field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iOwner, iNeighbour, iComp, fComp, is, ie, iProc, pNeigh, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product

        real :: phiC(field%nComp)
        !! owner field value

        real :: phiN(field%nComp)
        !! neighbour field value
    
        real :: fluxC1f(numberOfFaces, field%nComp)
        !! flux owner side
        
        real :: fluxC2f(numberOfFaces, field%nComp)
        !! flux neighbour side

        real :: fluxVf(numberOfFaces, field%nComp)
        !! correction flux (i.e. non-orthogonal correction)

        real :: fluxTf(numberOfFaces, field%nComp)
        !! total flux
    !------------------------------------------------------------------------------------------------------------------

        call assembleDiffusionFluxes(field, nu_, fluxC1f, fluxC2f, fluxVf)

        !-------------------------!
        ! Assemble Interior Faces !
        !-------------------------!

        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            phiC = field%phi(iOwner,:)
            phiN = field%phi(iNeighbour,:)

            fluxTf(iFace,:) = fluxC1f(iFace,:)*phiC + fluxC2f(iFace,:)*phiN + fluxVf(iFace,:)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh + 1
                iOwner = mesh%owner(iBFace)

                phiC = field%phi(iOwner,:)
                phiN = field%ghosts(pNeigh,:,iBoundary)

                fluxTf(iBFace,:) = fluxC1f(iBFace,:)*phiC + fluxC2f(iBFace,:)*phiN + fluxVf(iBFace,:)

            enddo

        enddo

        !-----------------!
        ! Real boundaries !
        !-----------------!

        call boundaryDiffusionFluxes(field, nu_, fluxC1f, fluxC2f, fluxVf, fluxTf)

    end subroutine totalDiffusionFluxes

! *********************************************************************************************************************

    subroutine boundaryDiffusionFluxes(field, nu_, fluxC1f, fluxC2f, fluxVf, fluxTf)

    !==================================================================================================================
    ! Description:
    !! BoundaryDiffusionFluxes computes the total diffusion fluxes at boundary faces only.
    !==================================================================================================================

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation

        type(flubioField) :: nu_
        !! diffusion coefficient
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iOwner, iComp, fComp, is, ie, iProc, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product

        real :: phiC(field%nComp)
        !! owner field value

        real :: phib(field%nComp)
        !! neighbour field value

        real :: gDiff(field%nComp)
        !! boundary flux

        real :: fluxC1f(numberOfFaces, field%nComp)
        !! flux owner side
        
        real :: fluxC2f(numberOfFaces, field%nComp)
        !! flux neighbour side

        real :: fluxVf(numberOfFaces, field%nComp)
        !! correction flux (i.e. non-orthogonal correction)

        real :: fluxTf(numberOfFaces, field%nComp)
        !! total flux
    !------------------------------------------------------------------------------------------------------------------

        fComp = field%nComp

        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace + 1
                iOwner = mesh%owner(iBFace)

                phiC = field%phi(iOwner,:)
                phib = field%phi(patchFace,:)

                gDiff = fluxC1f(iBFace, :)

                do iComp = 1,fComp
                    fluxTf(iBFace,iComp) = gDiff(iComp)*phiC(iComp) - gDiff(iComp)*phib(iComp) + fluxVf(iBFace,iComp)
                enddo

            enddo

        enddo

    end subroutine boundaryDiffusionFluxes

! *********************************************************************************************************************

    subroutine boundaryDiffusionFluxes_gDiff(field, nu_)

    !==================================================================================================================
    ! Description:
    !! BoundaryDiffusionFluxes_gDiff computes the total diffusion fluxes at boundary faces only using gDiff as flux.
    !==================================================================================================================

        type(flubioField) :: field
        !! field to be used during cross diffsuion term evaluation

        type(flubioField) :: nu_
        !! diffusion coefficient
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBFace, iBoundary, iOwner, iComp, fComp, is, ie, iProc, patchFace
        !! index to loop over boundaries
    !------------------------------------------------------------------------------------------------------------------

        real :: nu_f
        !! face viscosity

        real :: dot
        !! auxilairy variable to store a scalar product

        real :: phiC(field%nComp)
        !! owner field value

        real :: phib(field%nComp)
        !! neighbour field value

        real :: gDiff
        !! nu*S/dperp

        real :: fluxC1f(numberOfFaces, field%nComp)
        !! flux owner side
        
        real :: fluxC2f(numberOfFaces, field%nComp)
        !! flux neighbour side

        real :: fluxVf(numberOfFaces, field%nComp)
        !! correction flux (i.e. non-orthogonal correction)

        real :: fluxTf(numberOfFaces, field%nComp)
        !! total flux
    !------------------------------------------------------------------------------------------------------------------

        patchFace = numberOfElements
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                patchFace = patchFace + 1
                iOwner = mesh%owner(iBFace)

                phiC = field%phi(iOwner,:)
                phib = field%phi(patchFace,:)

                gDiff = nu_%phif(iBFace,1)*mesh%gDiff(iBFace)

                fluxTf(iBFace,:) = gDiff*phiC - gDiff*phib + fluxVf(iBFace,:)

            enddo

        enddo

    end subroutine boundaryDiffusionFluxes_gDiff

!**********************************************************************************************************************

    function limitCrossDiffusion(dot1, dot2, lambda) result(limiter)

    !==================================================================================================================
    ! Description:
    !! limitCrossDiffusion clips the amount of non-orthogonal correction which might creates instabilities.
    !==================================================================================================================

        real :: dot1, dot2
        !!

        real :: lambda
        !! limiter in input

        real :: limiter
        !! limiter to apply

        real :: small
        !! limiter to apply
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-8

        limiter = lambda*abs(dot1)/((1-lambda)*abs(dot2)+small)

        limiter = min(limiter, 1.0)

    end function limitCrossDiffusion

! *********************************************************************************************************************

end module diffusionFluxes