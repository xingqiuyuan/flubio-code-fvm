module Cp0Field

    use flubioDictionaries, only: flubioMonitors, raiseKeyErrorJSON
    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: velocity, pressure
    use flubioMpi
    use physicalConstants, only: rho
    use json_module
    use runTimeObject

    implicit none

    type, public, extends(runTimeObj) :: CP0

        character(len=30) :: fieldName
        !! Name of the field

        integer :: nComp
        !! number of components

        real, dimension(:,:), allocatable :: phi
        !! field variable

    contains

        procedure :: initialise => createCp0
        procedure :: run => computeCp0
        procedure :: writeFieldFunction

    end type CP0

contains
    
    subroutine createCp0(this)

    !==================================================================================================================
    ! Description:
    !! createCp0 is the field constructor and it allocates the memory for a new flubioField.
    !==================================================================================================================

        class(CP0) :: this
    !-------------------------------------------------------------------------------------------------------------------

        logical :: found, monitorsFound, monitorFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Get monitor pointer
        call jcore%get_child(monitorsPointer, this%objName, monitorPointer, monitorFound)

        call jcore%get(monitorPointer, 'saveEvery', this%runAt, found)
        call raiseKeyErrorJSON('saveEvery', found)

        this%fieldName = 'CP0'
        this%nComp = 1
        allocate(this%phi(numberOfElements+numberOfBFaces,this%nComp))

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine createCp0

! *********************************************************************************************************************

    subroutine computeCp0(this)

    !==================================================================================================================
    ! Description:
    !! computeCp0 computes the total pressure as:
    !! \begin{equation*}
    !! P_t= p + 0.5\rho|U|^2
    !! \end{equation*}
    !==================================================================================================================

        class(CP0):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: u, v, w, p
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then
            do i=1,numberOfElements+numberOfBFaces
                u = velocity%phi(i,1)
                v = velocity%phi(i,2)
                w = velocity%phi(i,3)
                p = pressure%phi(i,1)
                this%phi(i,1) = p + 0.5*rho%phi(i,1)*(u**2+v**2+w**2)
            end do
            call this%writeFieldFunction()
        endif

    end subroutine computeCp0

!**********************************************************************************************************************

    subroutine writeFieldFunction(this)

    !==================================================================================================================
    ! Description:
    !! writeToFile writes the field to a file to be post processed.
    !==================================================================================================================

        class(CP0) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=20) :: timeStamp
        !! time stamp

        character(len=20) ::  procID
        !! processor ID
    !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
        !! target components
   !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        postDir='postProc/fields/'
        write(timeStamp,'(i0)') itime
        fieldDir = postDir//trim(timeStamp)//'/'

        ! Create a directory for the field. If it does not exists create it.

        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp = 1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        elseif(.not. dirExists) then

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(this%fieldName)//'-d'//trim(procID)//'.bin', form='unformatted')

            write(1) this%nComp
            write(1) this%fieldName

            do iComp=1,this%nComp
                write(1) this%phi(:,iComp)
                write(1) iComp
                write(1) iComp
            end do

            close(1)

        end if

    end subroutine writeFieldFunction
    
end module Cp0Field