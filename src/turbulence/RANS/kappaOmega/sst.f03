!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module kappaOmegaSST

!==================================================================================================================
! Description:
!! this module contains the implementation of the k-omega SST model.
!==================================================================================================================

    use kappaOmegaBSL

    implicit none

    type, public, extends(kwBSL) :: kwSST

        real :: a1
        !! a1 coefficient 

    contains

        procedure :: modelCoeffs => modelCoeffsSST
        procedure :: overrideDefaultCoeffs_SST
        procedure :: printCoeffs => printCoeffsKappaOmegaSST

        procedure :: updateEddyViscosity => updateEddyViscositySST
        procedure :: nutWallFunctionSST

    end type kwSST

contains

    subroutine modelCoeffsSST(this)

    !==================================================================================================================
    ! Description:
    !! modelCoeffsBSL sets the turbulence model coefficients according to NASA.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: val
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------------!
        ! Default Coefficients !
        !----------------------!

        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%a1 = 0.31

        this%C_a1 = 5.0/9.0
        this%C_a2 = 0.4404

        this%C_b1 = 0.075 !0.072
        this%C_b2 = 0.0828

        this%bstar = 0.09

        this%sigma_k1 = 0.85
        this%sigma_o1 = 0.5

        this%sigma_k2 = 1.0
        this%sigma_o2 = 0.856

        !----------------------------!
        ! Override from dictionaries !
        !----------------------------!

        call this%overrideDefaultCoeffs()

        this%gamma_1 = this%C_b1/this%bstar - this%sigma_o1*this%kappa**2/sqrt(this%bstar)
        this%gamma_2 = this%C_b2/this%bstar - this%sigma_o2*this%kappa**2/sqrt(this%bstar)

        call this%laminarYPlus()

        !-------------------!
        ! Print if required !
        !-------------------!

        call flubioTurbModel%json%get('PrintCoefficients', val, found)
        if(found .and. (lowercase(val)=='yes' .or. lowercase(val)=='on')) call this%printCoeffs()

    end subroutine modelCoeffsSST

! *********************************************************************************************************************

    subroutine overrideDefaultCoeffs_SST(this)

    !==================================================================================================================
    ! Description:
    !! overrideDefaultCoeffs_SST sets the coefficients as you might have defined in turbulenceModel dictionary.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! C_mu
        call flubioTurbModel%json%get('Cmu', coeff, found)
        if(found) this%Cmu = coeff

        ! kappa
        call flubioTurbModel%json%get('kappa', coeff, found)
        if(found) this%kappa = coeff

        ! E
        call flubioTurbModel%json%get('E', coeff, found)
        if(found) this%E = coeff

        ! B
        call flubioTurbModel%json%get('B', coeff, found)
        if(found) this%B = coeff

        ! a1
        call flubioTurbModel%json%get('a1', coeff, found)
        if(found) this%a1 = coeff

        ! Ca1
        call flubioTurbModel%json%get('Ca1', coeff, found)
        if(found) this%C_a1 = coeff

        ! Ca2
        call flubioTurbModel%json%get('Ca2', coeff, found)
        if(found) this%C_a2 = coeff

        ! Cb1
        call flubioTurbModel%json%get('Cb1', coeff, found)
        if(found) this%C_b1 = coeff

        ! Cb2
        call flubioTurbModel%json%get('Cb2', coeff, found)
        if(found) this%C_b2 = coeff

        ! bstar
        call flubioTurbModel%json%get('bstar', coeff, found)
        if(found) this%bstar = coeff

        ! sigma_k1
        call flubioTurbModel%json%get('sigma_k1', coeff, found)
        if(found) this%sigma_k1 = coeff

        ! sigma_o1
        call flubioTurbModel%json%get('sigma_o1', coeff, found)
        if(found) this%sigma_o1 = coeff

        ! sigma_k2
        call flubioTurbModel%json%get('sigma_k2', coeff, found)
        if(found) this%sigma_k2 = coeff

        ! sigma_o2
        call flubioTurbModel%json%get('sigma_o2', coeff, found)
        if(found) this%sigma_o2 = coeff

        ! CRC
        call flubioTurbModel%json%get('crc', coeff, found)
        if(found) this%crc = coeff

        ! divCoeff
        call flubioTurbModel%json%get('divCoeff', coeff, found)
        if(found) this%divCoeff = coeff

    end subroutine overrideDefaultCoeffs_SST

!**********************************************************************************************************************

    subroutine printCoeffsKappaOmegaSST(this)

    !==================================================================================================================
    ! Description:
    !! printCoeffsKappaOmegaSST prints coefficients to screen.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then
            write(*,'(A)') '!----------------------------------!'
            write(*,'(A)') '! Kappa-Omega SST coefficients:    !'
            write(*,'(A)') '!----------------------------------!'
            write(*,*)
            write(*,'(A, f6.4)') ' Cmu = ', this%Cmu
            write(*,'(A, f6.4)') ' kappa = ', this%kappa
            write(*,'(A, f6.4)') ' Ca1 = ', this%C_a1
            write(*,'(A, f6.4)') ' Ca2 = ', this%C_a2
            write(*,'(A, f6.4)') ' Cb1 = ', this%C_b1
            write(*,'(A, f6.4)') ' Cb2 = ', this%C_b2
            write(*,'(A, f6.4)') ' a1 = ', this%a1
            write(*,'(A, f6.4)') ' bstar = ', this%bstar
            write(*,'(A, f6.4)') ' sigma_k1 = ', this%sigma_k1
            write(*,'(A, f6.4)') ' sigma_k2 = ', this%sigma_k2
            write(*,'(A, f6.4)') ' sigma_o1 = ', this%sigma_o1
            write(*,'(A, f6.4)') ' sigma_o2 = ', this%sigma_o2
            write(*,'(A, f6.4)') ' divCoeff = ', this%divCoeff
            write(*,'(A)') ' !---------------------------------!'
            write(*,*)
        end if

    end subroutine printCoeffsKappaOmegaSST

!**********************************************************************************************************************

    subroutine updateEddyViscositySST(this)

    !==================================================================================================================
    ! Description:
    !! updateEddyViscositySST updates the eddy viscosity for the k-w SST model.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(ttbTensor2) :: G
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements + numberOfBFaces)

        real :: density, tke, tdr, m, F2kw, small
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        small = 1.0e-15

        call G%create(dim=numberOfElements+numberOfBFaces, phi=velocity%phiGrad)
        S = sqrt(2.0)*norm(symm(G))

        ! Alternative
        !S = strainTensorModule()

        ! Eddy viscosity
        do iElement=1,numberOfElements

            density = rho%phi(iElement,1)
            tke = max(this%tke%phi(iElement,1),0.0)
            tdr = this%tdr%phi(iElement,1)

            F2kw = this%F2(iElement)
            m = max(this%a1*tdr, S(iElement)*F2kw)
            this%nut%phi(iElement,1) = this%a1*density*tke/max(m, small)

        enddo

        ! Correct eddy viscosity at wall boundary cells
        call this%nutWallFunctionSST(S)

        ! Update processor boundaries
        call this%nut%updateGhosts()

        ! Bound eddy viscosity
        call bound(this%nut, 0.0, flubioTurbModel%boundType)

        ! Physical viscosity
        nu%phi(1:numberOfElements,1) = viscos + this%nut%phi(1:numberOfElements,1)

        ! Update eddy and total viscosity
        call nu%updateGhosts()

    end subroutine updateEddyViscositySST

!**********************************************************************************************************************

    subroutine nutWallFunctionSST(this, S)

    !==================================================================================================================
    ! Description:
    !! nutWallFunctionSST computes the eddy viscosity at wall boundaries for SST model.
    !==================================================================================================================

        class(kwSST) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, iWall, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements+numberOfBFaces)

        real :: density, tke, tdr, m, F2kw, small
    !------------------------------------------------------------------------------------------------------------------

        small = 1.e-15

        !--------------------------------------!
        ! Correct viscosity at wall boundaries !
        !--------------------------------------!

        i = 0
        do iBoundary=1,numberOfRealBound

            iWall = 0
            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            patchFace = numberOfElements + (is-numberOfIntFaces-1)

            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') i=i+1

            ! Wall function
            if(trim(mesh%boundaries%bcType(iBoundary))=='wall' .and. trim(this%tdr%bCond(iBoundary))=='omegaWallFunction') then

                iWall = iWall+1
                do iBFace=is,ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    nu%phi(patchFace,1) = this%nuWall(iWall,i)
                    this%nut%phi(patchFace,1) = this%nuWall(iWall,i) - viscos

                end do

            ! Resolved wall
            elseif((trim(mesh%boundaries%bcType(iBoundary))=='wall' .and. &
                   trim(this%tdr%bCond(iBoundary))/='omegaWallFunction')) then

                iWall = iWall+1
                do iBFace=is,ie
                    patchFace = patchFace+1
                    nu%phi(patchFace,1) = viscos
                    this%nut%phi(patchFace,1) = 0.0
                end do

            ! Calculated from k and omega
            elseif(trim(mesh%boundaries%bcType(iBoundary))/='wall') then

                do iBFace=is,ie

                    patchFace = patchFace+1
                    iOwner = mesh%owner(iBFace)

                    density = rho%phi(patchFace,1)
                    tke = max(this%tke%phi(patchFace,1),0.0)
                    tdr = this%tdr%phi(patchFace,1)

                    F2kw = this%F2(iOwner)
                    m = max(this%a1*tdr, S(patchFace)*F2kw)

                    this%nut%phi(patchFace,1) = this%a1*density*tke/max(m, small)
                    nu%phi(patchFace,1) = viscos + this%a1*density*tke/max(m, small)

                end do

            end if

        end do

    end subroutine nutWallFunctionSST

!**********************************************************************************************************************

end module kappaOmegaSST