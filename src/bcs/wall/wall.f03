!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!
                                          
!*******************************************************************************************!			
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine wallBoundary(iBFace, aC, bC, vc, vb, mu)
	!! Description : wallBoundary computes the corretion to be applied at the diagonal coefficients and righ hand sides
	!! of the momentum equation when a wall boundary condition is applied to a taget boundary.
	!! in : iBFace, vb, mu
	!! out: aC, bC

		! modules
		use meshvar
		use massFlowVar

		! arguments
		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: iBFace
		!! target boundary face

		integer :: iOwner
		!! owner of the target boundary face

		integer ::fComp
		!! number of field components

		integer :: patchFace
		!! auxiliary loop index
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(3)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(3)
		!! right hand side corretion due to the velocity inlet boundary condition

		real :: vb(3)
		!! velocity value prescribed at the target boundary

		real :: vc(3)
		!! cell-center velocity

		real :: nf(3)
		!! surface normal at a target boundary face

		real :: mu
		!! dinamic viscosity at a target boundary face

		real :: a, b, c
		!! auxliary variables
	!------------------------------------------------------------------------------------------------------------------

		iOwner = mesh%owner(iBFace)

		! Get the surface normal
		nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)

		! Diagonal coefficient correction
		aC = mu*mesh%walldist(iBFace)*(1-nf**2)

	    !u-Component
		a = vb(1)*(1-nf(1)**2)
		b = (vc(2)-vb(2))*nf(1)*nf(2)
		c = (vc(3)-vb(3))*nf(1)*nf(3)

		bC(1) = mu*mesh%walldist(iBFace)*(a+b+c)

		! v-Component
		a = vb(2)*(1-nf(2)**2)
		b = (vc(1)-vb(1))*nf(1)*nf(2)
		c = (vc(3)-vb(3))*nf(2)*nf(3)

		bC(2) = mu*mesh%walldist(iBFace)*(a+b+c)

		! w-Component
		a = vb(3)*(1-nf(3)**2)
		b = (vc(1)-vb(1))*nf(1)*nf(3)
		c = (vc(2)-vb(2))*nf(2)*nf(3)

		bC(3) = mu*mesh%walldist(iBFace)*(a+b+c)

	end subroutine wallBoundary

! *********************************************************************************************************************

	subroutine updateWallBoundaryField(phi, phi0, iBoundary)
	!! Description : updateWallBoundaryField updates the field values at a target wall boundary
	!! in : iBoundary
	!! out: velocity%phi(numberOfElements+1:numberOfElements+numberOfBFaces,:)

		! modules
		use meshvar
		use massFlowVar
		
		! arguments
		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: iBFace
		!! target boundary face

		integer :: is
		!! start face of the target boundary

		integer :: ie
		!! end face of the target boundary

		integer :: iOwner
		!! owner of the target boundary face

		integer ::fComp
		!! number of field components

		integer :: patchFace
		!! auxiliary loop index
	!------------------------------------------------------------------------------------------------------------------

		real :: phi(numberOfElements+numberOfBFaces, fComp)
		!! field values

		real :: phi0(fComp)
		!! value to be assigned at the boundary

	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace = numberOfElements +(is-numberOfIntFaces-1)

		do iBFace=is,ie
			patchFace = patchFace+1
			phi(patchFace,:) = phi0
		enddo

	end subroutine updateWallBoundaryField

! *********************************************************************************************************************
