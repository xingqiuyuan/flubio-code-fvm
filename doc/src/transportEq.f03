!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module transportEq

!==================================================================================================================
! Description:
!! This modules implements the general transport equation and its data structure. It is particularly importat
!! since all other equation types are derived from this class.
!==================================================================================================================

#include "petsc/finclude/petscksp.h"
	use petscksp
    
    use flubioDictionaries
    use math
    use meshvar
    use diffusionFluxes
    use convectiveFluxes
    use timeSchemes
    use sourceTerms

    implicit none

    type, public :: transportEqn

        Mat A
        !!  PETSc matrix.

        Vec rhs
        !!  PETSc right hand side

        KSP ksp
        !! PETSc ksp to use

        character(len=:), allocatable :: eqName
        !! Name of the choosen convective scheme

        real, dimension(:,:), allocatable :: aC
        !! Diagonal coefficients for a transport equation matrix

        type(realCellStructMat), dimension(:), allocatable :: anb
        !! Off-diagonal coefficients for a transport equation matrix

        real, dimension(:,:), allocatable :: bC, bCDdtQ
        !! Right hand side for a transport equation

        type(diffusive_fluxes) :: diffFluxes
        !! diffusion fluxes for a transport equation

        type(convective_fluxes) :: convFluxes
        !! diffusion fluxes for a transport equation

        type(sourceTermsArray), dimension(:), allocatable :: fvSources

        character(len=:), allocatable :: convOptName
        !! Name of the choosen convective scheme

        character(len=:), allocatable :: timeOptName
        !! Name of the choosen time scheme

        character(len=:), allocatable :: solverName
        !! Name of the choosen linear solver

        character(len=:), allocatable :: pcName
        !! Name of the choosen preconditioner

        integer :: convImp
        !! Flag for the implcit convection

        integer :: convOpt
        !! Flag for the selected convective scheme

        integer :: timeOpt
        !! Flag for the selected time scheme

        integer :: solver
        !! Flag for the solver

        integer :: pc
        !! Flag for preconditioner

        real :: urf
        !! under relaxation factor for a transport equation

        real, dimension(:), allocatable :: res
        !! Actual equation residual

        real, dimension(:), allocatable :: scalingRef
        !! Actual equation scaled residual

        real, dimension(:), allocatable :: unscaledRes
        !! Actual equation unscaled residual

        real, dimension(:), allocatable :: convergenceResidual
        !! Actual equation unscaled residual

        real, dimension(:), allocatable :: resmax
        !! Maximum residual for the equation

        integer :: boundedCorr
        !! Flag for the bounded correction -div(U)*phi

        real :: absTol
        !! Linear solver maxium absolute tolerace for the equation

        real :: relTol
        !! Linear solver maxium relative tolerace for the equation

        integer :: nComp
        !! number of componets of the equation

        contains
            procedure :: createEq => createTransportEq
            procedure :: destroyEq => destroyTransportEq
            procedure :: resetCoeffs

            procedure :: setNumericalOptions
            procedure :: setUnderRelaxationFactor
            procedure :: setEqOptions
            procedure :: setEqOptionsWithoutConvection
            procedure :: setSolverOptions
            procedure :: setEquationSources

            procedure :: checkSteadyState

            generic ::   addTransientTerm => addTransientTermStd, addTransientTermWithCoeff
            procedure :: addTransientTermStd
            procedure :: addTransientTermWithCoeff
            procedure :: addDiffusionTerm
            procedure :: addConvectiveTerm
            procedure :: addConvectiveTermImplicit
            procedure :: addBoundedTerm
            procedure :: relaxEq
            procedure :: addBodyForce
            procedure :: addBalancedBodyForce
            procedure :: addDevTerm
            procedure :: addSubstantialDerivative
            procedure :: addExplicitSourceTerm
            procedure :: addLinearizedSourceTerm
            procedure :: addCustomSourceTerm
            procedure :: addVolumeSourceTerms
            procedure :: updateRhsWithCrossDiffusion
            procedure :: updateRhsWithCrossDiffusionWithConstantDiffusivity
            procedure :: applyBoundaryConditions => applyBoundaryConditionsTransport
            procedure :: assembleEq
            procedure :: assembleEqWithCoeff
            procedure :: assembleMat
            procedure :: assembleRhs
            procedure :: setReferenceValue
            procedure :: setMatReferenceValue

            procedure :: setUpDiffusionTerm
            procedure :: setUpConvectiveTerm

            procedure :: allocateMatrices
            procedure :: setLinearSolver
            procedure :: solve
            procedure :: explicitSolve
            procedure :: scaledResidual
            procedure :: scaledResidual2
            procedure :: getNorm
            procedure :: checkConvergence
            procedure :: getResidualForConvergenceCheck
            procedure :: writeResidual
            procedure :: printResidual
            procedure :: viewKspSettings

            procedure :: H
            procedure :: H1

            procedure :: verbose
            procedure :: kspVerbose
            procedure :: exportMatrixToFile
            procedure :: printCurrentIteration
            procedure :: printDelimiter

    end type transportEqn

contains

! *********************************************************************************************************************

    subroutine createTransportEq(this, eqName, nComp)

    !==================================================================================================================
    ! Description:
    !! createTransportEq is the class constructor.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: eqName
        !! equation name
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: nComp
        !! number of components
    !------------------------------------------------------------------------------------------------------------------

        this%nComp = nComp

        call this%diffFluxes%create(nComp)
        call this%convFluxes%create(nComp)

        allocate(this%aC(numberOfElements, nComp))
        allocate(this%anb(numberOfElements))
        allocate(this%bC(numberOfElements, nComp))
        allocate(this%bCDdtQ(numberOfElements, nComp))

        this%aC = 0.0
        this%bC = 0.0
        this%bCDdtQ = 0.0

        do iElement=1,numberOfElements
            this%anb(iElement)%csize_i = mesh%numberOfNeighbours(iElement)
            this%anb(iElement)%csize_j = this%nComp
            call this%anb(iElement)%initRealMat(mesh%numberOfNeighbours(iElement), nComp)
        end do

        allocate(this%res(nComp))
        allocate(this%resmax(nComp))
        allocate(this%scalingRef(nComp))
        allocate(this%unscaledRes(nComp))
        allocate(this%convergenceResidual(nComp))

        this%scalingRef = 0.0
        this%unscaledRes = 0.0
        this%convergenceResidual = 0.0
        this%resmax = 0.0

        this%eqName = eqName
        call this%allocateMatrices()

    end subroutine createTransportEq

! *********************************************************************************************************************

    subroutine destroyTransportEq(this)

    !==================================================================================================================
    ! Description:
    !! createTransportEq is the class de-constructor.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        call this%diffFluxes%destroy()
        call this%convFluxes%destroy()

        deallocate(this%aC)
        deallocate(this%anb)
        deallocate(this%bC)
        deallocate(this%bCDdtQ)

        deallocate(this%res)
        deallocate(this%scalingRef)
        deallocate(this%unscaledRes)
        deallocate(this%convergenceResidual)
        deallocate(this%resmax)

        call MatDestroy(this%A, ierr)
        call VecDestroy(this%rhs, ierr)

    end subroutine destroyTransportEq

! *********************************************************************************************************************

    subroutine setEqOptions(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setEqOptions sets the solver and discretization options for the target transport equation by parsing the settings from
    !! the relevant dictionaries.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: kspName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, key, solverType, pcType, convOpt, relax
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------!
        ! Solver options !
        !----------------!

        ! Add additional options as command line, if any
        call this%setSolverOptions()

        call flubioSolvers%json%get(this%eqName//'%solver', solverType, found)
        call raiseKeyErrorJSON(this%eqName//'%solver', found)
        this%solverName = solverType

        call flubioSolvers%json%get(this%eqName//'%preconditioner', pcType, found)
        call raiseKeyErrorJSON(this%eqName//'%preconditioner', found)
        this%pcName = pcType

        call flubioSolvers%setPreconditionerOption(lowercase(this%pcName), this%pc)

        !--------------------!
        ! Numerical settings !
        !--------------------!

        call flubioOptions%setTimeSchemeOption(this%eqName, this%timeOpt, this%timeOptName)

        call flubioOptions%json%get(this%eqName//'%convectiveScheme', convOpt, found)
        if(.not. found) convOpt = 'none'
        this%convOptName = lowercase(convOpt)
        call flubioOptions%setConvectionOption(this%convOptName, this%convOpt)

        ! Check or assign a default
        if(this%convOpt==-1 .and. flubioOptions%convOpt==-1) then
            call printConvectiveSchemes(this%eqname)
        elseif(this%convOpt==-1 .and. flubioOptions%convOpt/=-1) then
            call flubioOptions%json%get('Default%convectiveScheme', convOpt, found)
            this%convOptName = convOpt
            this%convOpt = flubioOptions%convOpt
        end if

        call flubioOptions%setConvectionImplicit(this%eqName, this%convImp)
        call flubioOptions%setBoundedCorrection(this%eqName, this%boundedCorr)

        ! Absolute solver tolerance
        call flubioSolvers%json%get(this%eqName//'%absTol', this%absTol, found)
        call raiseKeyErrorJSON(this%eqName//'%absTol', found)

        ! Relative solver tolerance
        call flubioSolvers%json%get(this%eqName//'%relTol', this%relTol, found)
        call raiseKeyErrorJSON(this%eqName//'%relTol', found)

        !-----------------------!
        ! Steady state controls !
        !-----------------------!

        if(steadySim==1) then
            key = this%eqName//'%urf-'
            call flubioSteadyStateControls%json%get(this%eqName//'%urf', this%urf, found)
            call raiseKeyErrorJSON(this%eqName//'%urf', found)

            call flubioSteadyStateControls%json%get(this%eqName//'%residual', anumber, found)
            call raiseKeyErrorJSON(this%eqName//'%residual', found)
            this%resmax(:) = anumber
        end if

        !-------------------!
        ! set linear solver !
        !-------------------!

        call this%setLinearSolver(kspName)

    end subroutine setEqOptions

! *********************************************************************************************************************

    subroutine setEqOptionsWithoutConvection(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setEqOptions sets the solver and discretization options for the target transport equation by parsing the settings from
    !! the relevant dictionaries.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(*) :: kspName
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, key, solverType, pcType, relax
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        !----------------!
        ! Solver options !
        !----------------!

        ! Add additional options as command line, if any
        call this%setSolverOptions()

        call flubioSolvers%json%get(this%eqName//'%solver', solverType, found)
        call raiseKeyErrorJSON(this%eqName//'%solver', found)
        this%solverName = solverType

        call flubioSolvers%json%get(this%eqName//'%preconditioner', pcType, found)
        call raiseKeyErrorJSON(this%eqName//'%preconditioner', found)
        this%pcName = pcType

        call flubioSolvers%setPreconditionerOption(lowercase(this%pcName), this%pc)

        !--------------------!
        ! Numerical settings !
        !--------------------!

        call flubioOptions%setTimeSchemeOption(this%eqName, this%timeOpt, this%timeOptName)

        ! Absolute solver tolerance
        call flubioSolvers%json%get(this%eqName//'%absTol', this%absTol, found)
        call raiseKeyErrorJSON(this%eqName//'%absTol', found)

        ! Relative solver tolerance
        call flubioSolvers%json%get(this%eqName//'%relTol', this%relTol, found)
        call raiseKeyErrorJSON(this%eqName//'%relTol', found)

        !-----------------------!
        ! Steady state controls !
        !-----------------------!

        if(steadySim==1) then
            key = this%eqName//'%urf-'
            call flubioSteadyStateControls%json%get(this%eqName//'%urf', this%urf, found)
            call raiseKeyErrorJSON(this%eqName//'%urf', found)

            call flubioSteadyStateControls%json%get(this%eqName//'%residual', anumber, found)
            call raiseKeyErrorJSON(this%eqName//'%residual', found)
            this%resmax(:) = anumber
        end if

        !-------------------!
        ! set linear solver !
        !-------------------!

        call this%setLinearSolver(kspName)

    end subroutine setEqOptionsWithoutConvection

! *********************************************************************************************************************

    subroutine setNumericalOptions(this)

    !==================================================================================================================
    ! Description:
    !! setNumericalOptions sets the numrical option only for the transport equation.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, convOpt
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call flubioOptions%setTimeSchemeOption(this%eqName, this%timeOpt, this%timeOptName)

        call flubioOptions%json%get(this%eqName//'%convectiveScheme', convOpt, found)
        if(.not. found) convOpt = 'none'
        this%convOptName = lowercase(convOpt)

        call flubioOptions%setConvectionOption(this%convOptName, this%convOpt)

        ! Check or assign a default
        if(this%convOpt==-1 .and. flubioOptions%convOpt==-1) then
            call printConvectiveSchemes(this%eqname)
        elseif(this%convOpt==-1 .and. flubioOptions%convOpt/=-1) then
            this%convOpt = flubioOptions%convOpt
        end if

        call flubioOptions%setConvectionImplicit(this%eqName, this%convImp)
        call flubioOptions%setBoundedCorrection(this%eqName, this%boundedCorr)

    end subroutine setNumericalOptions

! *********************************************************************************************************************

    subroutine setUnderRelaxationFactor(this)

    !==================================================================================================================
    ! Description:
    !! setUnderRelaxationFactor stes the implicit under-relaxation factor for the transport equation.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: key, opt
    !------------------------------------------------------------------------------------------------------------------

        real :: anumber
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        if(steadySim==1) then
            key = this%eqName//'%urf'
            call flubioSteadyStateControls%json%get(key, this%urf, found)
            call raiseKeyErrorJSON(key, found)
        end if

    end subroutine setUnderRelaxationFactor

!------------------------------------------------------------------------------------------------------------------
!                                           linear system assembly methods
!------------------------------------------------------------------------------------------------------------------

    subroutine addConvectiveTerm(this)

    !==================================================================================================================
    ! Description:
    !! addConvectiveTerm adds the convective term to the equation's coeffiecients and rhs.
    !! The assembling procedure is carried out looping on the mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer  :: iElement, iBoundary, iFace, iBFace, iProc, iComp, is, ie

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef
    !------------------------------------------------------------------------------------------------------------------

        real :: ones(this%nComp)

        real :: corr
    !------------------------------------------------------------------------------------------------------------------

        ones = 1.0

        !------------------------------------!
        !  Assemble fluxes of interior faces !
        !------------------------------------!

        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef = mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner,:) = this%aC(iOwner,:) + this%convFluxes%fluxC1f(iFace,:)
            this%bC(iOwner,:) = this%bC(iOwner,:) - this%convFluxes%fluxVf(iFace,:)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, this%convFluxes%fluxC2f(iFace,:))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,:) = this%aC(iNeighbour,:) - this%convFluxes%fluxC2f(iFace,:)
            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + this%convFluxes%fluxVf(iFace,:)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, this%convFluxes%fluxC1f(iFace,:))

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner,:) = this%aC(iOwner,:) + this%convFluxes%fluxC1f(iBFace,:)
                this%bC(iOwner,:) = this%bC(iOwner,:) - this%convFluxes%fluxVf(iBFace,:)

                call this%anb(iOwner)%subtract(iOwnerNeighbourCoef, this%convFluxes%fluxC2f(iBFace,:))

            enddo

        enddo

        ! Bounded correction and deviatoric term
        if(this%boundedCorr==1) call this%addBoundedTerm()

        ! if(this%devTerm==1) call addDevTerm()

    end subroutine addConvectiveTerm

! *********************************************************************************************************************

    subroutine addConvectiveTermImplicit(this)

    !==================================================================================================================
    ! Description:
    !! addConvectiveTermImplicit adds the convective part to the equation's coefficients.
    !! The assembling procedure is carried out looping on the mesh faces and taking advantage of the owner-neighbour definition.
    !! This routine is the same as assembleConvectionMomentumEq, but it does not include the rhs,
    !! since the deferred correction is not used (i.e. the whole HR term is treated implicitly).
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iBoundary, iFace, iBFace, iProc, iComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        real :: corr
        !! correction
    !------------------------------------------------------------------------------------------------------------------

        !  Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            iOwnerNeighbourCoef=mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef=mesh%iNeighbourOwnerCoef(iFace)
            
            ! Assemble fluxes for owner cell
            this%aC(iOwner,:) = this%aC(iOwner,:) + this%convFluxes%fluxC1f(iFace,:)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, this%convFluxes%fluxC2f(iFace,:))

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour,:) = this%aC(iNeighbour,:) - this%convFluxes%fluxC2f(iFace,:)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, this%convFluxes%fluxC1f(iFace,:))

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef=mesh%iOwnerNeighbourCoef(iBFace)
                
                !   Assemble fluxes for owner cell
                this%aC(iOwner,:) = this%aC(iOwner,:) + this%convFluxes%fluxC1f(iBFace,:)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, this%convFluxes%fluxC2f(iBFace,:))
                !this%anb(iOwner,iOwnerNeighbourCoef,:) = this%anb(iOwner,iOwnerNeighbourCoef,:) + this%convFluxes%fluxC2f(iBFace,:)
            enddo

        enddo

        ! Bounded correction
        if(this%boundedCorr==1) call this%addBoundedTerm()

!        if(this%devTerm==1) call addDevTerm

    end subroutine addConvectiveTermImplicit

! *********************************************************************************************************************

    subroutine addDiffusionTerm(this)

    !==================================================================================================================
    ! Description:
    !! addDiffusionTerm adds the diffusion term in the equation's coefficients and right hand side.
    !! The assembling procedure is carried out looping on mesh faces and taking advantage of the owner-neighbour definition.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: iElement, iFace, iBFace, iBoundary, iProc, iComp, nComp

        integer :: iOwner, iNeighbour,  iOwnerNeighbourCoef, iNeighbourOwnerCoef

        integer :: is, ie
    !------------------------------------------------------------------------------------------------------------------

        nComp = size(this%aC(1,:))

        ! Assemble fluxes of interior faces
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)
            
            iOwnerNeighbourCoef=mesh%iOwnerNeighbourCoef(iFace)
            iNeighbourOwnerCoef=mesh%iNeighbourOwnerCoef(iFace)

            ! Assemble fluxes for owner cell
            this%aC(iOwner, :) = this%aC(iOwner, :) + this%diffFluxes%fluxC1f(iFace, :)
            call this%anb(iOwner)%add(iOwnerNeighbourCoef, this%diffFluxes%fluxC2f(iFace, :))

            this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iFace,:)

            ! Assemble fluxes for neighbour cell
            this%aC(iNeighbour, :) = this%aC(iNeighbour, :) - this%diffFluxes%fluxC2f(iFace, :)
            call this%anb(iNeighbour)%subtract(iNeighbourOwnerCoef, this%diffFluxes%fluxC1f(iFace, :))

            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + this%diffFluxes%fluxVf(iFace,:)

        enddo

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)

            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)
                iOwnerNeighbourCoef = mesh%iOwnerNeighbourCoef(iBFace)

                ! Assemble fluxes for owner cell
                this%aC(iOwner, :) = this%aC(iOwner, :) + this%diffFluxes%fluxC1f(iBFace, :)
                call this%anb(iOwner)%add(iOwnerNeighbourCoef, this%diffFluxes%fluxC2f(iBFace, :))

                this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iBFace,:)

            enddo

        enddo

    end subroutine addDiffusionTerm

! *********************************************************************************************************************

    subroutine addTransientTermStd(this, field, ddtCoef)

    !==================================================================================================================
    ! Description:
    !! addTransientTerm adds the transient term to the equantion's diagonal coefficient and rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, ddtCoef
    !------------------------------------------------------------------------------------------------------------------

        integer :: fComp, timeOpt
    !------------------------------------------------------------------------------------------------------------------
        
        timeOpt = this%timeOpt
        fComp = this%nComp

        if(timeOpt==0) then

            call Euler(field%phio, ddtCoef%phio(1:numberOfElements,1), fComp, this%aC, this%bC)

        elseif(timeOpt==1) then

            if(itime==1) then
                call Euler(field%phio, ddtCoef%phio, fComp, this%aC, this%bC)
            else
                call AdamsMoulton(field%phio, field%phioo, ddtCoef%phi(1:numberOfElements,1), & 
                                  ddtCoef%phio(1:numberOfElements,1), ddtCoef%phioo(1:numberOfElements,1), & 
                                  fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==2) then

            if(itime==1) then
                call Euler(field%phio, ddtCoef%phi(1:numberOfElements,1), fComp, this%aC, this%bC)
            else
                call crankNicholson(field%phio, field%phioo, ddtCoef%phi(1:numberOfElements,1), & 
                                    ddtCoef%phio(1:numberOfElements,1), ddtCoef%phioo(1:numberOfElements,1), & 
                                    fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==-1) then

            ! Steady state, do nothing

        else

            call flubioStopMsg('ERROR: Unknown time scheme ')

        endif

    end subroutine addTransientTermStd

! *********************************************************************************************************************

    subroutine addTransientTermWithCoeff(this, field, ddtCoef, coef)

    !==================================================================================================================
    ! Description:
    !! addTransientTermWithCoeff adds the transient term to the equantion's diagonal coefficient and rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, ddtCoef, coef
    !------------------------------------------------------------------------------------------------------------------

        integer :: fComp, timeOpt
    !------------------------------------------------------------------------------------------------------------------

        real :: cEff(numberOfElements), cEff0(numberOfElements), cEff00(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        cEff = ddtCoef%phi(:,1)*coef%phi(:,1)
        cEff0 = ddtCoef%phio(:,1)*coef%phio(:,1)
        cEff00 = ddtCoef%phioo(:,1)*coef%phioo(:,1)

        timeOpt = this%timeOpt
        fComp = this%nComp

        if(timeOpt==0) then

            call Euler(field%phio, cEff0, fComp, this%aC, this%bC)

        elseif(timeOpt==1) then

            if(itime==1) then
                call Euler(field%phio, cEff0, fComp, this%aC, this%bC)
            else
                call AdamsMoulton(field%phio, field%phioo,&
                                  cEff, cEff0, cEff00, &
                                  fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==2) then

            if(itime==1) then
                call Euler(field%phio, cEff0, fComp, this%aC, this%bC)
            else
                call crankNicholson(field%phio, field%phioo, &
                        cEff, cEff0, cEff00, &
                        fComp, this%aC, this%bC)
            endif

        elseif(timeOpt==-1) then
            ! Steady state, do nothing
        else
            call flubioStopMsg('ERROR: Unknown time scheme ')
        endif

    end subroutine addTransientTermWithCoeff

! *********************************************************************************************************************


    subroutine relaxEq(this, field)

    !==================================================================================================================
    ! Description:
    !! relaxEq relaxes implixitly the target equation:
    !! \begin{eqnarray}
    !!  a_C = \dfrac{a_C}{urf} \\
    !!  b_C = b_C + \dfrac{1-urf}{urf}a_C\phi
    !! \end{eqnarray}
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
    !------------------------------------------------------------------------------------------------------------------

        real relFactor
        !! relaxation factor

        real :: rel
        !! auxiliary variable
    !------------------------------------------------------------------------------------------------------------------

        rel = (1.0-this%urf)/this%urf

        ! Modify the central coefficients and rhs according to the implicit under-relaxation method
        do iComp=1,field%nComp
            this%bC(:,iComp) = this%bC(:,iComp) + rel*this%aC(:,iComp)*field%phi(1:numberOfElements,iComp)
            this%aC(:,iComp) = this%aC(:,iComp)/this%urf
        end do

    end subroutine relaxEq

! *********************************************************************************************************************

    subroutine updateRhsWithCrossDiffusion(this, field, diffCoeff, nComp)

    !==================================================================================================================
    ! Description:
    !! updateRhsWithCrossDiffusion computes the cross-diffusion term for the non-orthogonal
    !! correction term and adds it to the equation's rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, diffCoeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBoundary, iBFace, iOwner, iNeighbour, iProc, is, ie, iComp, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,nComp)
        !! gradient interpolated at mesh faces

        real :: dot
        !! dot product

        real :: dot2
        !! dot product

        real :: limiter
        !! cross diffusion limiter

        real :: nu_f
    !------------------------------------------------------------------------------------------------------------------

        !----------------------!
        ! Interpolate viscosty !
        !----------------------!

        call interpolate(diffCoeff, phys%interpType, -1)

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=nComp)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,nComp

            do iFace=1,numberOfFaces

                nu_f = diffCoeff%phif(iFace,1)

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(flubioOptions%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, this%diffFluxes%lambda)
                end if

                this%diffFluxes%fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

        enddo

        !------------------------------!
        ! Assemble in the equation rhs !
        !------------------------------!

        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble fluxes for owner and neighbour cell
            this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iFace,:)
            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + this%diffFluxes%fluxVf(iFace,:)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                ! Assemble fluxes for owner and neighbour cell
                this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iBFace,:)

            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iOwner = mesh%owner(iBFace)
                this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iBFace,:)
            enddo

        enddo

    end subroutine updateRhsWithCrossDiffusion

! *********************************************************************************************************************

    subroutine updateRhsWithCrossDiffusionWithConstantDiffusivity(this, field, nu_f, nComp)

    !==================================================================================================================
    ! Description:
    !! updateRhsWithCrossDiffusion computes the cross-diffusion term for the non-orthogonal
    !! correction term and adds it to the equation's rhs.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, diffCoeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iBoundary, iBFace, iOwner, iNeighbour, iProc, is, ie, iComp, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: phiGrad_f(numberOfFaces,3,nComp)
        !! gradient interpolated at mesh faces

        real :: nu_f
        !! diffusion coefficient

        real :: dot
        !! dot product

        real :: dot2
        !! dot product

        real :: limiter
        !! cross diffusion limiter
    !------------------------------------------------------------------------------------------------------------------

        !-----------------------!
        ! Interpolate gradients !
        !-----------------------!

        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=nComp)

        !------------------------------------!
        ! Account for cell non-orthogonality !
        !------------------------------------!

        do iComp=1,nComp

            do iFace=1,numberOfFaces

                dot = dot_product(phiGrad_f(iFace,:,iComp), mesh%Tf(iFace,:))
                dot2 = dot_product(phiGrad_f(iFace,:,iComp), mesh%Sf(iFace,:))

                if(flubioOptions%lambda==1.0) then
                    limiter = 1.0
                else
                    limiter = limitCrossDiffusion(dot2, dot, this%diffFluxes%lambda)
                end if

                this%diffFluxes%fluxVf(iFace,iComp) = -nu_f*dot*limiter

            enddo

        enddo

        !------------------------------!
        ! Assemble in the equation rhs !
        !------------------------------!

        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble fluxes for owner and neighbour cell
            this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iFace,:)
            this%bC(iNeighbour,:) = this%bC(iNeighbour,:) + this%diffFluxes%fluxVf(iFace,:)

        enddo

        !----------------------!
        ! Processor Boundaries !
        !----------------------!

        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                ! Assemble fluxes for owner and neighbour cell
                this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iBFace,:)

            enddo

        enddo

        !-----------------!
        ! Real Boundaries !
        !-----------------!

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iOwner = mesh%owner(iBFace)
                this%bC(iOwner,:) = this%bC(iOwner,:) - this%diffFluxes%fluxVf(iBFace,:)
            enddo

        enddo

    end subroutine updateRhsWithCrossDiffusionWithConstantDiffusivity

!------------------------------------------------------------------------------------------------------------------
!                                           PETSc Linear system settings
!------------------------------------------------------------------------------------------------------------------

    subroutine allocateMatrices(this)
        
    !==================================================================================================================
    ! Description:
    !! allocateMatrices generates the PETSc matrix and rhs.
    !==================================================================================================================
        
        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: sbuf, rbuf, ierr
    !------------------------------------------------------------------------------------------------------------------

        MatType myType

        VecType myVec
    !------------------------------------------------------------------------------------------------------------------
        
        mytype = MATMPIAIJ
        myVec = VECMPI

        flubioSolvers%lm = numberOfElements
        sbuf = flubioSolvers%lm

        ! Prepare the sizes
        call mpi_allReduce(sbuf, rbuf, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD, ierr)
        flubioSolvers%M = rbuf

        !-----------------------------------!
        ! Create Matrix and right hand side !
        !-----------------------------------!

        ! Matrix
        call MatCreate(PETSC_COMM_WORLD, this%A, ierr)

        call MatSetSizes(this%A, flubioSolvers%lm, flubioSolvers%lm, flubioSolvers%M, flubioSolvers%M, ierr)

        call MatSetType(this%A, myType, ierr)

        call MatMPIAIJSetPreallocation(this%A, flubioSolvers%d_nz, PETSC_NULL_INTEGER, flubioSolvers%o_nz, PETSC_NULL_INTEGER, ierr)

        call MatSetUp(this%A, ierr)

        ! Rhs
        call VecCreate(PETSC_COMM_WORLD, this%rhs, ierr)

        call VecSetSizes(this%rhs, flubioSolvers%lm, flubioSolvers%M, ierr)

        call VecSetType(this%rhs, myVec, ierr)

        end subroutine allocateMatrices

!**********************************************************************************************************************

    subroutine solve(this, field, setGuess, reusePC, reuseMat)
        
    !==================================================================================================================
    ! Description:
    !! solve assembles the matrix and rhs in PETSc format and  solves the linear system Ax=rhs.
    !==================================================================================================================
        
        class(transportEqn) :: this
        
        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=30) :: itStr, initResStr, finalResStr

        character(len=:), allocatable :: str1, str2, final
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, setGuess, reusePC, reuseMat
    
        integer :: lm, nn, it, fComp
    
        integer :: iG, jG(maxNeigh), ii(numberOfElements)
    
        integer :: ierr, nComp
    !------------------------------------------------------------------------------------------------------------------
        
        real :: res, initRes, finalRes, scalingFactor, small
    !------------------------------------------------------------------------------------------------------------------
    
        PetscReal auxD, auxNb(maxNeigh)

        ! Linear system objects
        Vec  x, y
        Vec vres
        PC  pc
        KSP ksp
        KSPConvergedReason reason
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        !-------------------------------------------!
        ! Fill the matrix with the equation coeffs  !
        ! Do this many times as the number of comp  !
        !-------------------------------------------!

        fComp = this%nComp
        if(this%nComp>1) fComp = fComp - bdim

        do iComp=1,fComp

            if(reuseMat<=1) then

                call MatZeroEntries(this%A, ierr)

                do iElement=1,numberOfElements

                    ! Diagonal value
                    iG = mesh%cellGlobalAddr(iElement)-1
                    auxD = this%aC(iElement,iComp)
                    call MatSetValue(this%A, iG, iG, auxD, INSERT_VALUES, ierr)

                    ! Neighbours values
                    nn = mesh%numberOfNeighbours(iElement)

                    jG(1:nn) = mesh%conn(iElement)%col(1:nn)-1
                    auxNb(1:nn) = this%anb(iElement)%coeffs(1:nn,iComp)
                    call MatSetValues(this%A, 1, iG, nn, jG(1:nn), auxNb(1:nn), INSERT_VALUES, ierr)

                enddo

                call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
                call MatAssemblyEnd(this%A, MAT_FINAL_ASSEMBLY, ierr)

            endif

            !-------------------------------------!
            ! Fill a vector with the equation rhs !
            !-------------------------------------!

            call VecZeroEntries(this%rhs,ierr)

            ! PETSC counts from 0
            do i=1,numberOfElements
                ii(i)=mesh%cellGlobalAddr(i)-1
            enddo

            call VecSetValues(this%rhs, numberOfElements, ii, this%bC(:,iComp), INSERT_VALUES, ierr)

            call VecAssemblyBegin(this%rhs, ierr)
            call VecAssemblyEnd(this%rhs, ierr)

            !-------------------------!
            ! Solve the linear system !
            !-------------------------!

            ! duplicate vector
            call VecDuplicate(this%rhs, x, ierr)
            call VecZeroEntries(x, ierr)

            call VecDuplicate(this%rhs, vres, ierr)

            ! Use the same preconditioner according to user's setting. This might or might not improve the performace.
            if(reusePC==1) then
                call KSPSetReusePreconditioner(this%ksp, PETSC_TRUE, ierr)
            else
                call KSPSetReusePreconditioner(this%ksp, PETSC_FALSE, ierr)
            endif

            ! Update the matrix and recompute the preconditioner if needed
            call KSPSetOperators(this%ksp, this%A, this%A, ierr)

            ! Set a non-zero guess for the iterative solver. The guess is the latest available solution obtained for phi
            if(setGuess==1) then

                call VecSetValues(x, numberOfElements, ii, field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)

                call VecAssemblyBegin(x, ierr)
                call VecAssemblyEnd(x, ierr)

                call KSPSetInitialGuessNonZero(this%ksp,PETSC_TRUE,ierr)

            endif

            ! Get initial residual norm
            call MatMult(this%A, x, vres, ierr)
            call VecAXPY(vres, -1.0, this%rhs, ierr)
            call VecNorm(vres, NORM_1, initRes, ierr)

            this%res(iComp) = initRes

            ! scale the residual
            if(steadySim==1) call this%scaledResidual2(field, iComp, scalingFactor)

            ! Solve the system
            call KSPSolve(this%ksp, this%rhs, x, ierr)

            ! Check convergence
            call KSPGetConvergedReason(this%ksp, reason, ierr)

            if (reason < 0) then
                if(id==0) write(*,*) reason
                call flubioStopMsg('ERROR: KSP has not converged. Simulations stopped.')
                return
            endif

            ! Extract values
            call VecGetValues(x, flubioSolvers%lm, ii, field%phi(1:numberOfElements,iComp), ierr)

            ! residual norm and iterations
            call KSPGetResidualNorm(this%ksp, res, ierr)
            call KSPGetIterationNumber(this%ksp, it, ierr)

            ! Store the residuals
            this%unscaledRes(iComp) = initRes
            if(reuseMat<=1) then
                this%convergenceResidual(iComp) = initRes
                this%scalingRef(iComp) = scalingFactor
            end if

            ! KSP verbosity
            call this%kspVerbose(field%fieldName(iComp), it, reuseMat, initRes, res, scalingFactor)

            ! ALWAYS deallocate memory for petsc objects created in any subroutine!
            call VecDestroy(x,ierr)
            call VecDestroy(vres,ierr)

        end do

    end subroutine solve

! *********************************************************************************************************************

    subroutine explicitSolve(this, field)

    !==================================================================================================================
    ! Description:
    !! explicitMomentumSolve solves explicitely the target equation. It assembles the matrix, but does not call the linear solver.
    !! Instead, It perform just a matrix-vector multiplication with the last known values of the cell centered field.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        PetscReal auxD, auxNb(maxNeigh)

        Vec y
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, fComp, ierr

        integer :: lm, nn, it

        integer :: iG, jG(maxNeigh), ii(numberOfElements), conn(numberOfElements, maxNeigh)

        real :: sumAnb(numberOfElements,1)
    !------------------------------------------------------------------------------------------------------------------

        fComp = this%nComp
        if(fComp>1) fComp = fComp - 1

        do iComp=1,fComp

            !---------------------!
            ! Assemble the matrix !
            !---------------------!

            call MatZeroEntries(this%A, ierr)
            call VecZeroEntries(this%rhs, ierr)
            sumAnb = 0.0

            do iElement=1,numberOfelements

                iG = mesh%cellGlobalAddr(iElement)-1 ! PETSC counts from 0

                ! Diagonal value
                auxD = this%aC(iElement,iComp)
                call MatSetValue(this%A,iG,iG,auxD,INSERT_VALUES,ierr)

                ! Neighbours values
                nn = mesh%numberOfNeighbours(iElement)
                auxNb(1:nn) = this%anb(iElement)%coeffs(1:nn,iComp)

                jG(1:nn) = mesh%conn(iElement)%col(1:nn)-1
                call MatSetValues(this%A,1,iG,nn,jG(1:nn),auxNb,INSERT_VALUES,ierr)

            enddo

            call MatAssemblyBegin(this%A,MAT_FINAL_ASSEMBLY,ierr)
            call MatAssemblyEnd(this%A,MAT_FINAL_ASSEMBLY,ierr)

            !-------------------------------------!
            ! Fill a vector with the equation rhs !
            !-------------------------------------!

            ! duplicate the vector
            call VecDuplicate(this%rhs,y,ierr)

            do i=1,numberOfElements
                ii(i) = mesh%cellGlobalAddr(i)-1 ! PETSC counts from 0
            enddo

            call VecSetValues(this%rhs,numberOfElements,ii,field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)

            call VecAssemblyBegin(this%rhs, ierr)
            call VecAssemblyEnd(this%rhs, ierr)

            !------------------------------------------------------------------------!
            ! Matrix multiplication, remember to subtract aC(iElement)*phi(iElement) !
            !------------------------------------------------------------------------!

            call MatMult(this%A, this%rhs, y, ierr)
            call VecGetValues(y, flubioSolvers%lm, ii, sumAnb(:,1), ierr)

            ! Subtract off the diagonal term
            do iElement=1,numberOfElements

                this%bC(iElement,iComp) = this%bC(iElement,iComp) - (sumAnb(iElement,1) - &
                        this%aC(iElement,iComp)*field%phi(iElement,iComp))

                field%phi(iElement,iComp) = this%bC(iElement,iComp)/this%aC(iElement,icomp)

            enddo

            ! Destroy PETSc object
            call VecDestroy(y,ierr)

        enddo

    end subroutine explicitSolve

!**********************************************************************************************************************

    subroutine setLinearSolver(this, kspName)

    !==================================================================================================================
    ! Description:
    !! setLinearSolvers sets the linear solvers options according to the user's choices.
    !! Please note that all the solvers use the default settings. To custumize your application you should use
    !! the command line database as specified in PETSc.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        PetscReal div

        KSPType  mySolver

        PC  mypc
    
        character(*) kspName
        !! KSP name used to target this KSP object
    !------------------------------------------------------------------------------------------------------------------

        integer :: myPrecond
        !! preconditioner flag

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        mySolver = this%solverName
        myPrecond = this%pc
        kspName = kspName

        div=10000.0 ! or PETSC_DEFAULT_REAL

        !  linear solver options for velocity
        call KSPCreate(PETSC_COMM_WORLD, this%ksp, ierr)
        call KSPSetOptionsPrefix(this%ksp, kspName ,ierr)
        call KSPSetType(this%ksp, mySolver, ierr)
        call KSPSetTolerances(this%ksp, this%relTol, this%absTol, div, flubioSolvers%itmax, ierr)
        call KSPSetErrorIfNotConverged(this%ksp, PETSC_TRUE, ierr)

        ! test this, just to see I can exit first from the linear system loop
        call KSPSetNormType(this%ksp, KSP_NORM_UNPRECONDITIONED, ierr)

        if(this%solverName=='ibcgs') call KSPSetLagNorm(this%ksp, PETSC_TRUE, ierr)
    
        ! Set this preconditioner
        call KSPGetPC(this%ksp, mypc, ierr)
    
        if(myPrecond==0) then
    
            ! Block Jacobi
            call PCSetType(mypc,PCBJACOBI,ierr)
    
        elseif(myPrecond==1) then
    
            ! Additive  Schwarz
            call PCSetType(mypc,PCASM,ierr)
    
        elseif(myPrecond==2) then
    
            ! GAMG
            call PCSetType(mypc,PCGAMG,ierr)
    
        elseif(myPrecond==3) then
    
            ! Hypre
            call PCSetType(mypc,PCHYPRE,ierr)
    
        elseif(myPrecond==4) then
    
            ! Point Jacobi
            call PCSetType(mypc,PCPBJACOBI,ierr)
    
        elseif(myPrecond==5) then
    
            ! SOR
            call PCSetType(mypc,PCSOR,ierr)
    
        elseif(myPrecond==6) then
    
            ! Sparse Approximate Inverse method (SPAI)
            call PCSetType(mypc,PCSPAI,ierr)
    
        elseif(myPrecond==7) then
    
            ! Kaczmarz
            ! call PCSetType(mypc,PCKACZMARZ,ierr)

            call flubioMsg('FLUBIO: this preconditioner is in the PETSc list, but appears to be not working')

        elseif(myPrecond==8) then

            ! ML
            call PCSetType(mypc, PCML, ierr)

        elseif(myPrecond==-1) then
    
            ! No preconditioning
            call PCSetType(mypc,PCNONE,ierr)
    
        else
    
            ! Default
            call PCSetType(mypc,PCBJACOBI,ierr)
    
        endif

        ! Set preconditioner reordering type
        call PCFactorSetMatOrderingType(mypc, flubioSolvers%matOrderingType, ierr)

        ! Enable command line option
        call KSPSetFromOptions(this%ksp, ierr)

    end subroutine setLinearSolver

! *********************************************************************************************************************

    subroutine addBoundedTerm(this)

    !==================================================================================================================
    ! Description:
    !! addBoundedTerm adds an extra stabilzation term to the matrix diagonal as function of the velocity divergence:
    !! \begin{equation*}
    !!   a_C = a_C + \nabla\cdot u
    !! \end{equation*}
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp, iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: div(numberOfElements + numberOfBFaces)
        !! divergence of \texbf{u}
    !------------------------------------------------------------------------------------------------------------------

        call divU(div)

        do iComp=1,this%nComp
            this%aC(:,iComp) = this%aC(:,iComp) - div(1:numberOfElements)*mesh%volume
        enddo

    end subroutine addBoundedTerm

! *********************************************************************************************************************

    subroutine resetCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! resetCoeffs makes zero the coeffs and rhs of the target equation.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Reset coefficients
        this%aC = 0.0
        call resetNonUniformMatrix(this%anb, numberOfElements)
        this%bC = 0.0

    end subroutine resetCoeffs

! *********************************************************************************************************************

    subroutine addBodyForce(this, f, nComp)

    !==================================================================================================================
    ! Description:
    !! addBodyForce adds a body force to the target equation. Please note that the body force is added with a negative sign!
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: f(numberOfElements+numberOfBFaces, nComp)
        !! body force to add
    !------------------------------------------------------------------------------------------------------------------

        do iComp=1,this%nComp
           this%bC(:, iComp) = this%bC(:,iComp) - f(1:numberOfElements,iComp)*mesh%volume(:)
        enddo

    end subroutine addBodyForce

! *********************************************************************************************************************

    subroutine balancedBodyForce(fb, f, nComp)

    !==================================================================================================================
    ! Description:
    !! addBalancedBodyForce takes a body force, perform a stabilization procedure and then it adds it to the equation.
    !! This subroutine is experimental and its definition is found is eq. (15.211) Moukalleld et al., 1st edition.
    !==================================================================================================================

        type(flubioField) :: bodyForce
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iFace, iElement, iComp, nComp

        integer :: iProc, pNeigh, is, ie
    !------------------------------------------------------------------------------------------------------------------

        integer :: iOwner, iNeighbour
    !------------------------------------------------------------------------------------------------------------------

        real :: Svol(3)
        !! Surface normal vector scaled by the cell volume

        real :: dot
        !! auxiliary variable for a dot product

        real :: f(numberOfElements+numberOfBFaces, nComp)
        !! body force to balance

        real :: fb(numberOfElements+numberOfBFaces, nComp)
        !! balanced body force
    !------------------------------------------------------------------------------------------------------------------

        fb = 0.0

        if(nComp==1) call flubioStopMsg('FLUBIO: this routine works only with vector fields')

        ! Create a field containing a body force
        call bodyForce%createWorkField(fieldName='bodyForce', classType='scalar', nComp=nComp)

        ! Interpolate the body force at mesh faces
        call interpolate(bodyForce, 'linear', -1)

        ! Internal Faces
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ! Assemble for owner cell
            Svol = mesh%Sf(iFace,:)/mesh%volume(iOwner)
            dot = dot_product(bodyForce%phif(iFace,:), mesh%CN(iFace,:))
            fb(iOwner,:) = fb(iOwner,:) + (1.0-mesh%gf(iFace))*Svol*dot

            ! Assemble for neighbour cell
            Svol = -mesh%Sf(iFace,:)/mesh%volume(iNeighbour)
            dot = dot_product(bodyForce%phif(iFace,:), -mesh%CN(iFace,:))
            fb(iNeighbour,:) = fb(iNeighbour,:) + mesh%gf(iFace)*Svol*dot

        end do

        ! Processor Boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh=pNeigh+1
                iOwner = mesh%owner(iBFace)

                ! Assemble for owner cell
                Svol = mesh%Sf(iBFace,:)/mesh%volume(iOwner)
                dot = dot_product(bodyForce%phif(iBFace,:), mesh%CN(iBFace,:))
                fb(iOwner,:) = fb(iOwner,:) + (1.0-mesh%gf(iBFace))*Svol*dot

            enddo

        enddo

        ! Real Boundaries
        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                iOwner = mesh%owner(iBFace)

                ! Assemble for owner cell
                Svol = mesh%Sf(iBFace,:)/mesh%volume(iOwner)
                dot = dot_product(bodyForce%phif(iBFace,:), mesh%CN(iBFace,:))
                fb(iOwner,:) = fb(iOwner,:) + (1.0-mesh%gf(iBFace))*Svol*dot

            enddo

        enddo

        ! Destroy the work field
        call bodyForce%destroyField()

    end subroutine balancedBodyForce

! *********************************************************************************************************************

    subroutine addbalancedBodyForce(this, f, nComp)

    !==================================================================================================================
    ! Description:
    !! addBalancedBodyForce takes a body force, perform a stabilization procedure and then it adds it back to the equation.
    !! This function is experimental and its definition is found is eq. (15.211) Moukalleld et al., 1st edition.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp, iElement, nComp
    !------------------------------------------------------------------------------------------------------------------

        integer :: iOwner, iNeighbour
    !------------------------------------------------------------------------------------------------------------------

        real :: f(numberOfElements+numberOfBFaces, nComp)
        !! body force to balance

        real :: fb(numberOfElements+numberOfBFaces, nComp)
        !! balanced body force to add
    !------------------------------------------------------------------------------------------------------------------

        call balancedBodyForce(fb, f, nComp)

        do iComp=1,this%nComp
            this%bC(:,iComp) = this%bC(:,iComp) - fb(1:numberOfElements,iComp)*mesh%volume
        enddo

    end subroutine addBalancedBodyForce

! *********************************************************************************************************************

    subroutine addDevTerm(this, mu, nComp)

    !==================================================================================================================
    ! Description:
    !! addDevTerm adds the deviatoric term $2/3\mu div(U) \phi$ to the transport equation
    !! implicitly if the sign is positive, explictly if negative.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: div(numberOfElements + numberOfBFaces)
        !! auxiliary variable

        real :: aC
        !! matrix contribution

        real :: bC
        !! rhs contribution

        real :: vol
        !! cell volume

        real :: mu
        !! cell diffusivity

        real :: dev
    !------------------------------------------------------------------------------------------------------------------

        ! divergence of U
        call divU(div)

        do iElement=1,numberOfElements

            vol = mesh%volume(iElement)

            dev = (2.0/3.0)*mu*div(iElement)*vol

            call doubleBar(dev, 0.0, aC)
            call doubleBar(-dev, 0.0, bC)

            this%aC(iElement,:) = this%aC(iElement,:) + aC
            this%bC(iElement,:) = this%bC(iElement,:) + bC

        enddo

    end subroutine addDevTerm

! *********************************************************************************************************************

    subroutine addSubstantialDerivative(this, field)

    !==================================================================================================================
    ! Description:
    !! addSubstantialDerivative adds the substantial derivative of a field as source term to the equation rhs:
    !! \begin{equation*}
    !!  S = \dfrac{D\phi}{Dt}V_C=(\dfrac{\partial \phi -\phi^\circ}{\partial t}+\textbf{u}\cdot\nabla\phi)V_C
    !! \end{equation*}
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement

        integer :: iComp

    !------------------------------------------------------------------------------------------------------------------

        real :: vol
        !! cell volume

        real :: timeTerm(field%nComp)
        !! time detivative

        real :: advTerm(field%nComp)
        !! convective term

        real :: DphiDt(field%nComp)
        !! substantial derivative
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements

            vol = mesh%volume(iElement)

            do iComp=1,field%nComp
                advTerm(iComp) = dot_product(velocity%phi(iElement,:),field%phiGrad(iElement,:,iComp))
            end do

            if(steadySim==0) then
                timeTerm = (field%phi(iElement,:)-field%phio(iElement,:))/dtime
                DphiDt = timeTerm + advTerm
            else
                DphiDt = advTerm
            end if

            this%bC(iElement,:) = this%bC(iElement,:) + DphiDt*vol

        enddo

    end subroutine addSubstantialDerivative

! *********************************************************************************************************************

    subroutine addCustomSourceTerm(this, field)

    !==================================================================================================================
    ! Description:
    !! computeCustomSourceTerm adds a source term for the target equation as specified in settings/volumeSource.
    !==================================================================================================================

        use parseFromFile

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: sourceName
        !! expression value

        character(len=:), allocatable :: sourceType
        !! source type

        character(len=:), allocatable :: linearPart
        !! expression value for the linear part

        character(len=:), allocatable :: nonLinearPart
        !! expression value for the non linear part

        character(len=:), allocatable :: expr
        !! expression value
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iRegion, iSource, nSources, fComp
    !------------------------------------------------------------------------------------------------------------------

        real ::  aC(field%nComp), bC(field%nComp), SMALL

        real :: linearSourceTerm(numberOfElements, field%nComp)
        !! source term to add

        real :: nonLinearsourceTerm(numberOfElements, field%nComp)
        !! source term to add

        real :: sourceTerm(numberOfElements, field%nComp)
        !! source term to add
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, customSourcesFound, eqnFound, sourceFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, customSourcesPointer, eqnPointer, sourcePointer
    !------------------------------------------------------------------------------------------------------------------

        SMALL = 0.0000000001d0

        fComp = field%nComp

        sourceTerm = 0.0
        linearSourceTerm = 0.0
        nonLinearSourceTerm = 0.0

        call flubioSources%json%info('customVolumeSources%'//this%eqName, n_children=nSources)

        ! Get the pointer to the dictionary
        call flubiosources%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'customVolumeSources', customSourcesPointer, customSourcesFound)
        call raiseKeyErrorJSON('customVolumeSources%', customSourcesFound)

        call jcore%get(customSourcesPointer, this%eqName, eqnPointer, eqnFound)
        call raiseKeyErrorJSON(this%eqName, eqnFound)

        do iSource=1,nSources

            ! Get monitors pointer
            call jcore%get_child(eqnPointer, iSource, sourcePointer, sourceFound)

            ! Get monitor type
            call jcore%get(sourcePointer, 'type', sourceType, found)
            call raiseKeyErrorJSON('type', found)

            if(lowercase(sourceType) == 'explicitsource') then
                
                call jcore%get(sourcePointer, 'expression', expr, sourceFound)
                if(.not. found) call flubioStopMsg('ERROR: cannot find the expression for the volume source "'//sourceName//'"')

                call expressionParsingWithField(expr, sourceTerm, field)

                !! add the volume source to the rhs
                do iElement=1,numberOfElements
                    aC = 0.0
                    bC = sourceTerm(iElement,:)*mesh%volume(iElement)
                    call applyToSelectedRegions(aC, bC, iElement, sourceName, this%nComp)
                    this%bC(iElement,:) = this%bC(iElement,:) + bC(:)
                enddo

            elseif(lowercase(sourceType) == 'nonlinearsource') then

                call flubioOptions%json%get('VolumeSources%'//sourceName//'%implictPart', linearPart, found)
                if(.not. found) call flubioStopMsg('ERROR: cannot find the linear part for the volume source "'//sourceName//'"')

                call expressionParsingWithField(linearPart, linearSourceTerm, field)

                call flubioOptions%json%get('VolumeSources%'//sourceName//'%explicitPart', nonLinearPart, found)
                if(.not. found) call flubioStopMsg('ERROR: cannot find the non-linear part for the volume source "'//sourceName//'"')

                call expressionParsingWithField(nonLinearPart, nonLinearSourceTerm, field)

                !! Add the volume source to diagonal and rhs
                do iElement=1,numberOfElements

                    aC = linearSourceTerm(iElement,:)*mesh%volume(iElement)
                    bC = nonLinearSourceTerm(iElement,:)*mesh%volume(iElement)
                    call applyToSelectedRegions(aC, bC, iElement, sourceName, this%nComp)

                    this%aC(iElement,:) = this%aC(iElement,:) - aC(:)
                    this%bC(iElement,:) = this%bC(iElement,:) + bC(:)

                enddo

            else

                call flubioStopMsg('ERROR: unknown source type. Either use explicitSource &
                        to an explicit source term or nonLinearSource to provide a linear + non linear &
                        source decomposition.')
            end if

        end do

        ! Clean up
        nullify(dictPointer)
        if(customSourcesFound) nullify(customSourcesPointer)
        if(eqnFound) nullify(eqnPointer)
        if(sourceFound) nullify(sourcePointer)
        call jcore%destroy()

    end subroutine addCustomSourceTerm

! *********************************************************************************************************************

    subroutine addVolumeSourceTerms(this)

    !==================================================================================================================
    ! Description:
    !! computeCustomSourceTerm adds a source term for the target equation as specified in settings/volumeSource.
    !==================================================================================================================

        use parseFromFile

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSource, nSources
    !------------------------------------------------------------------------------------------------------------------

        real :: aC(numberOfElements,3), bC(numberOfElements,3)
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        do iSource =1,size(this%fvSources)
            call this%fvSources(iSource)%source%computeSource(aC, bC)
            this%aC(:,1:this%nComp) = this%aC(:,1:this%nComp) + aC(:,1:this%nComp)
            this%bC(:,1:this%nComp) = this%bC(:,1:this%nComp) + bC(:,1:this%nComp)
        enddo

    end subroutine addVolumeSourceTerms

! *********************************************************************************************************************

    subroutine addExplicitSourceTerm(this, source, fComp)

    !==================================================================================================================
    ! Description:
    !! addExplicitSourceTerm adds a source term wrapped in field form.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, fComp
    !------------------------------------------------------------------------------------------------------------------

        real :: source(numberOfElements, fComp)

        real :: bC(fComp)
    !------------------------------------------------------------------------------------------------------------------

        !! add the rhs with the volume source
        do iElement=1,numberOfElements
            bC = source(iElement,:)*mesh%volume(iElement)
            this%bC(iElement,:) = this%bC(iElement,:) + bC(:)
        enddo

    end subroutine addExplicitSourceTerm

! *********************************************************************************************************************

    subroutine addLinearizedSourceTerm(this, linearPart, nonLinearPart, fComp)

    !==================================================================================================================
    ! Description:
    !! addLinearizedSourceTerm adds a source term to the equation. The linear part contributes to the diagonal coefficient.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, fComp
    !------------------------------------------------------------------------------------------------------------------

        real :: linearPart(numberOfElements, fComp)
        !! linear part

        real :: nonLinearPart(numberOfElements, fComp)
        !! non-linear part

        real :: aC(fComp), bC(fComp)
    !------------------------------------------------------------------------------------------------------------------

        !! add the rhs with the volume source
        do iElement=1,numberOfElements

            aC = linearPart(iElement,:)*mesh%volume(iElement)
            bC = nonLinearPart(iElement,:)*mesh%volume(iElement)

            this%aC(iElement,:) = this%aC(iElement,:) - aC(:)
            this%bC(iElement,:) = this%bC(iElement,:) + bC(:)

        enddo

    end subroutine addLinearizedSourceTerm

! *********************************************************************************************************************

    subroutine applyBoundaryConditionsTransport(this, field, diffCoeff, convCoeff)

    !==================================================================================================================
    ! Description:
    !! applyBoundaryConditionsTransport applies the boundary conditions for the target transport equation.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field

        type(flubioField) :: diffCoeff

        type(flubioField), optional :: convCoeff
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iOwner, iBFace, iComp, is ,ie, patchFace, bFlag
    !------------------------------------------------------------------------------------------------------------------

        real :: c
        !! convection coeffcient

        real :: mu
        !! diffusion coeffcient

        real phi0(this%nComp)
        !! boundary calue to be eventully assigned

        real, dimension(:), allocatable :: slipLength
        !! slip length for robin bC

        real, dimension(:), allocatable :: phiInf
        !! coefficient in robin boundary condition
    !------------------------------------------------------------------------------------------------------------------

        real :: aCb(field%nComp)
        !! diagonal coefficient boundary correction

        real :: bCb(field%nComp)
        !! rhs boundary correction
    !------------------------------------------------------------------------------------------------------------------

        patchFace = numberOfElements

        ! Interpolate physical quantities
        if(present(convCoeff)) then
            call interpolate(diffCoeff, phys%interpType, -1)
            call interpolate(convCoeff, phys%interpType, -1)
        else
            call interpolate(diffCoeff, phys%interpType, -1)
        end if

        do iBoundary=1,numberOfRealBound

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            bFlag = field%bcFlag(iBoundary)

            do iBFace=is, ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                c = 1.0
                if(present(convCoeff)) c = convCoeff%phif(iBFace,1)
                mu = diffCoeff%phif(iBFace,1)

                ! void bc
                if(bFlag==0) then

                    call voidBoundary(aCb, bCb, this%nComp)

                ! Dirichlet bc
                elseif (bFlag==1) then

                    call dirichletBoundary(iBFace, aCb, bCb, field%phi(patchFace,:), mu, c, this%nComp)

                ! Zero Gradient bc
                elseif (bFlag==2) then

                    call neumann0Boundary(iBFace, aCb, bCb, c, this%nComp)

                ! Fixed Gradient bc
                elseif (bFlag==3) then

                    call neumannBoundary(iBFace, aCb, bCb, field%boundaryValue(iBoundary,:), mu, c, this%nComp)

                ! Fixed Flux bc
                elseif (bFlag==4) then

                    call prescribedFluxBoundary(iBFace, aCb, bCb, field%boundaryValue(iBoundary,:), mu, c, this%nComp)

                ! Robin bc
                elseif (bFlag==5) then

                    call field%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%slipLength', slipLength)
                    call field%bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%phiInf', phiInf)
                    call robinBoundary(iBoundary, iBFace, aCb, bCb, field%phiGrad(patchFace,:,:), field%phi(iBFace,:), mu, slipLength, phiInf, this%nComp)

                ! farField
                elseif(bFlag==6) then

                    phi0 = field%boundaryValue(iBoundary,1)
                    call farFieldBoundary(iBFace, aCb, bCb, phi0, mu, c,this%nComp)

                ! User defined dirichlet
                elseif (bFlag==1000) then

                    call dirichletBoundary(iBFace, aCb, bCb, field%phi(patchFace,:), mu, c, this%nComp)

                ! Turbulent Intensity
                elseif (bFlag==7) then

                    call dirichletBoundary(iBFace, aCb, bCb, field%phi(patchFace,:), mu, c, this%nComp)

                ! Viscosity ratio
                elseif (bFlag==8) then

                    call dirichletBoundary(iBFace, aCb, bCb, field%phi(patchFace,:), mu, c, this%nComp)

                elseif (bFlag==9) then

                    ! wall function, do nothing

                elseif (bFlag==10) then

                    ! wall function, do nothing

                else

                    write(*,*) 'FLUBIO: transportEq.f03::applyBoundaryConditionsTransport - Boundary Condition not implemented at',' ', mesh%boundaries%userName(iBoundary)
                    stop

                endif

                ! Correct coefficients
                this%aC(iOwner,:) = this%aC(iOwner,:) + aCb
                this%bC(iOwner,:) = this%bC(iOwner,:) + bCb

            enddo

        enddo

    end subroutine applyBoundaryConditionsTransport

! *********************************************************************************************************************

    subroutine assembleEq(this, field, muEff)

    !==================================================================================================================
    ! Description:
    !! assembleEq wraps all the necessary operations needed to assemble an equation. It is suefull to quicly build an equation
    !! in one shot.
    !==================================================================================================================

        use physicalConstants
    !------------------------------------------------------------------------------------------------------------------

        class(transportEqn) :: this

        type(flubioField) :: field, muEff
    !------------------------------------------------------------------------------------------------------------------

        !------------------------------------!
        ! Reset coeffs and update old fields !
        !------------------------------------!

        call this%resetCoeffs()

        call field%updateOldFieldValues()

        !--------------------------!
        ! Assemble diffusion term  !
        !--------------------------!

        call this%diffFluxes%assembleDiffusionFluxes(field, muEff)

        !--------------------------!
        ! Assemble convective term !
        !--------------------------!

        if(this%convImp==0) then
            call this%convFluxes%assembleConvectiveTerm(field, this%convOpt)
        else
            call this%convFluxes%assembleConvectiveTermImplicit(field, this%convOpt)
        endif

        !--------------------------!
        ! Assemble linear system   !
        !--------------------------!

        ! Transient
        if(steadySim==0) call this%addTransientTerm(field, rho)

        ! Diffusion
        call this%addDiffusionTerm()

        ! Convection
        if(this%convImp==0) then
            call this%addConvectiveTerm()
        else
            call this%addConvectiveTermImplicit()
        end if

        ! Boundary conditions
         call this%applyBoundaryConditions(field, muEff)

    end subroutine assembleEq

! *********************************************************************************************************************

    subroutine assembleEqWithCoeff(this, field, muEff, cEff)

    !==================================================================================================================
    ! Description:
    !! assembleEqWithCoeff wraps all the necessary operations needed to assemble an equation. It is suefull to quicly build an equation
     !! in one shot.
    !==================================================================================================================

        use physicalConstants
    !------------------------------------------------------------------------------------------------------------------

        class(transportEqn) :: this

        type(flubioField) :: field, muEff, cEff
    !------------------------------------------------------------------------------------------------------------------

        !------------------------------------!
        ! Reset coeffs and update old fields !
        !------------------------------------!

        call this%resetCoeffs()

        call field%updateOldFieldValues()

        !--------------------------!
        ! Assemble diffusion term  !
        !--------------------------!

        call this%diffFluxes%assembleDiffusionFluxes(field, muEff)

        !--------------------------!
        ! Assemble convective term !
        !--------------------------!

        if(this%convImp==0) then
            call this%convFluxes%assembleConvectiveTerm(field, cEff, this%convOpt)
        else
            call this%convFluxes%assembleConvectiveTermImplicit(field, cEff, this%convOpt)
        endif

        !--------------------------!
        ! Assemble linear system   !
        !--------------------------!

        ! Transient
        if(steadySim==0) call this%addTransientTerm(field, rho, cEff)

        ! Diffusion
        call this%addDiffusionTerm()

        ! Convection
        if(this%convImp==0) then
            call this%addConvectiveTerm()
        else
            call this%addConvectiveTermImplicit()
        end if

        ! Boundary conditions
        call this%applyBoundaryConditions(field, muEff, cEff)

    end subroutine assembleEqWithCoeff

! *********************************************************************************************************************

    subroutine setUpDiffusionTerm(this, field, muEff, crossDiffusionTerm)

    !==================================================================================================================
    ! Description:
    !! setUpDiffusionTerm wraps all the necessary operations needed to assemble diffusionTerm.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field, muEff

        character(len=*) :: crossDiffusionTerm
    !------------------------------------------------------------------------------------------------------------------

        ! Assemble diffsion fluxes
        if(lowercase(crossDiffusionTerm)=='yes') then
            call this%diffFluxes%assembleDiffusionFluxes(field, muEff)
        else
            call this%diffFluxes%assembleDiffusionFluxesWithoutCrossDiffusion(field, muEff)
        end if

        ! Add to matrix
        call this%addDiffusionTerm()

    end subroutine setUpDiffusionTerm

! *********************************************************************************************************************

    subroutine setUpConvectiveTerm(this, field)

    !==================================================================================================================
    ! Description:
    !! setUpConvectiveTerm wraps all the necessary operations needed to assemble the convective term.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        ! Assemble convective fluxes
        if(this%convImp==0) then
            call this%convFluxes%assembleConvectiveTerm(field, this%convOpt)
        else
            call this%convFluxes%assembleConvectiveTermImplicit(field, this%convOpt)
        endif

        ! Add to matrix
        if(this%convImp==0) then
            call this%addConvectiveTerm()
        else
            call this%addConvectiveTermImplicit()
        end if

    end subroutine setUpConvectiveTerm

! *********************************************************************************************************************

    subroutine assembleMat(this, iComp)

    !==================================================================================================================
    ! Description:
    !! assembleMat assembles the matrix for the ith component of the equation in PETSc format.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp

        integer :: lm, nn, it, fComp

        integer :: iG, jG(maxNeigh), ii(numberOfElements)

        integer :: ierr, nComp
    !------------------------------------------------------------------------------------------------------------------

        PetscReal auxD, auxNb(maxNeigh)
    !------------------------------------------------------------------------------------------------------------------

        call MatZeroEntries(this%A, ierr)

        do iElement=1,numberOfElements

            ! Diagonal value, PETSC counts from 0
            iG = mesh%cellGlobalAddr(iElement)-1
            auxD = this%aC(iElement,iComp)
            call MatSetValue(this%A, iG, iG, auxD, INSERT_VALUES, ierr)

            ! Neighbours values
            nn=mesh%numberOfNeighbours(iElement)
            auxNb(1:nn) = this%anb(iElement)%coeffs(1:nn,iComp)
            jG(1:nn) = mesh%conn(iElement)%col(1:nn)-1
            call MatSetValues(this%A, 1, iG, nn, jG(1:nn), auxNb(1:nn), INSERT_VALUES, ierr)

        enddo

        call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
        call MatAssemblyEnd(this%A, MAT_FINAL_ASSEMBLY, ierr)

    end subroutine assembleMat

!**********************************************************************************************************************

    subroutine assembleRhs(this, iComp)

    !==================================================================================================================
    ! Description:
    !! assembleRhs assembles the ith-component of equation rhs.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, ierr

        integer :: ii(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        call VecZeroEntries(this%rhs,ierr)

        ! PETSC counts from 0
        do i=1,numberOfElements
            ii(i) = mesh%cellGlobalAddr(i)-1
        enddo

        call VecSetValues(this%rhs, numberOfElements, ii, this%bC(:, iComp), INSERT_VALUES, ierr)

        call VecAssemblyBegin(this%rhs, ierr)
        call VecAssemblyEnd(this%rhs, ierr)

    end subroutine assembleRhs

!**********************************************************************************************************************

    subroutine setReferenceValue(this, iElement, value, iComp)

    !==================================================================================================================
    ! Description:
    !! setReferenceValue sets the value of field at target local cell number.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: aC, bC, aC0, value
    !------------------------------------------------------------------------------------------------------------------

        aC0 = this%aC(iElement, iComp)
        this%bC(iElement, iComp) = this%bC(iElement, iComp) + aC0*value
        this%aC(iElement, iComp) = this%aC(iElement, iComp) + aC0

    end subroutine setReferenceValue

! *********************************************************************************************************************

    subroutine setMatReferenceValue(this, iG, value, iComp)

    !==================================================================================================================
    ! Description:
    !! setMatReferenceValue sets the value of field at target GLOBAL cell number.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iG, iComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: aC, bC, value
    !------------------------------------------------------------------------------------------------------------------

        bC = this%bC(iG, iComp) + this%aC(iG, iComp)*value
        aC = 2.0*this%aC(iG, iComp)

        call MatSetValue(this%A, iG, iG, aC, INSERT_VALUES, ierr)
        call MatAssemblyBegin(this%A, MAT_FINAL_ASSEMBLY, ierr)
        call MatAssemblyEnd(this%A, MAT_FINAL_ASSEMBLY, ierr)

        call VecSetValue(this%rhs, iG, bC, INSERT_VALUES, ierr)
        call VecAssemblyBegin(this%rhs, ierr)
        call VecAssemblyEnd(this%rhs, ierr)

    end subroutine setMatReferenceValue

! *********************************************************************************************************************

    subroutine computeEquationresiduals(this, field)

    !==================================================================================================================
    ! Description:
    !! computeEquationresiduals calculates the scaled residual for an arbitrary transport equation.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        Vec x, y
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp, ii(numberOfElements), ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: sumAnb(numberOfElements,1)

        real :: local_ref, local_res, ref, res, cell_res, scaledRes, small

        real :: sbuf, rbuf
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        call VecDuplicate(this%rhs, x, ierr)

        ! Loop over elements
        do iElement=1,numberOfElements
            ii(iElement) = mesh%cellGlobalAddr(iElement)-1 ! PETSC counts from 0
        enddo

        do iComp=1,this%nComp

            sumAnb = 0.0

            call VecSetValues(x, numberOfElements, ii, field%phi(1:numberOfElements,iComp), INSERT_VALUES, ierr)

            call VecAssemblyBegin(x, ierr)
            call VecAssemblyEnd(x, ierr)

            !-----------------------!
            ! Matrix multiplication !
            !-----------------------!

            call MatMult(this%A, x, y, ierr)
            call VecGetValues(y, flubioSolvers%lm, ii, sumAnb(:,1), ierr)

            ! Reference value to scale the residuals
            res = 0.0
            ref = 0.0
            local_res = 0.0
            local_ref = 0.0

            do iElement=1,numberOfElements
                cell_res = sumAnb(iElement,1)-this%bC(iElement,iComp)
                local_res = local_res + abs(cell_res)
                local_ref = local_ref + abs(this%aC(iElement,iComp)*field%phi(iElement,iComp))
            enddo

            ! SumUp contribution for

            sbuf = 0.0
            rbuf = 0.0

            ! Fill the buffer array
            sbuf = local_res
            call mpi_allReduce(sbuf, rbuf ,1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
            res = rbuf

            sbuf = local_ref
            call mpi_allReduce(sbuf, rbuf ,1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
            ref = rbuf

            ! Final residual
            scaledRes = res/(ref + small)
            this%res(iComp) = scaledRes

         end do

        ! Destroy vectors
        call VecDestroy(x, ierr)
        call VecDestroy(y, ierr)

    end subroutine computeEquationresiduals

!**********************************************************************************************************************

    subroutine scaledResidual(this, field)

    !==================================================================================================================
    ! Description:
    !! scaledResidual scales the equation residual according to the following formula:
    !! \begin{equation*}
    !!    res= \frac{|\sum_{cells} A\phi - rhs|}{|\sum_{cells} a_C\phi_C|}
    !! \end{equation*}
    !! This normalization does not lead at a unitary scaled residual, but follows the literature.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real ::  initRes, localRef, ref, sbuf, rbuf, small
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        ! Reference value to scale the residuals
        do iComp = 1, this%nComp

            localRef = 0.0
            localRef=sum(abs(this%aC(1:numberOfElements,iComp)*field%phi(1:numberOfElements,iComp)))

            sbuf = 0.0
            rbuf = 0.0

            sbuf = localRef
            call mpi_allReduce(sbuf, rbuf ,1, MPI_REAL8, MPI_SUM, MPI_COMM_WORLD, ierr)
            ref = rbuf

            this%res(iComp) = this%res(iComp)/(ref + small)

        end do

    end subroutine scaledResidual

! *********************************************************************************************************************

    subroutine scaledResidual2(this, field, iComp, ref)

    !==================================================================================================================
    ! Description:
    !! scaledResidual scales the equation residual according to the following formula:
    !! \begin{equation*}
    !!    res= \frac{|\sum_{cells} A\phi - rhs|}{|\sum_{cells} a_C\phi_C|}
    !! \end{equation*}
    !! This normalization does not lead at a unitary scaled residual, but follows the literature.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, ierr

        integer :: ii(numberOfElements)

    !------------------------------------------------------------------------------------------------------------------

        real :: ref, small, fieldMean(field%nComp)

        real :: n1, n2

        real :: dummy(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        Vec ax, axBar, resBar, x, xBar, ax_axBar, b
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        call meanField(field, fieldMean)

        call VecDuplicate(this%rhs, x, ierr)
        call VecDuplicate(this%rhs, xBar, ierr)
        call VecDuplicate(this%rhs, ax, ierr)
        call VecDuplicate(this%rhs, axBar, ierr)
        call VecDuplicate(this%rhs, resBar, ierr)
        call VecDuplicate(this%rhs, ax_axBar, ierr)
        call VecDuplicate(this%rhs, b, ierr)

        ! PETSC counts from 0
        do i=1,numberOfElements
            ii(i)=mesh%cellGlobalAddr(i)-1
        enddo

        ! b
        call VecZeroEntries(b,ierr)
        call VecSetValues(b, numberOfElements, ii, this%bC(1:numberOfElements,iComp), INSERT_VALUES, ierr)
        call VecAssemblyBegin(b, ierr)
        call VecAssemblyEnd(b, ierr)

        ! x
        call VecZeroEntries(x,ierr)
        call VecSetValues(x, numberOfElements, ii, field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)
        call VecAssemblyBegin(x, ierr)
        call VecAssemblyEnd(x, ierr)

        ! xBar
        dummy = fieldMean(iComp)

        call VecZeroEntries(xbar,ierr)
        call VecSetValues(xBar, numberOfElements, ii, dummy, INSERT_VALUES, ierr)
        call VecAssemblyBegin(xbar, ierr)
        call VecAssemblyEnd(xbar, ierr)

        ! Ax
        call MatMult(this%A, x, ax, ierr)

        !AxBar
        call MatMult(this%A, xBar, axBar, ierr)

        !resBar
        call VecAXPY(b, -1.0, axBar, ierr)
        call VecAXPY(ax, -1.0, axBar, ierr)

        ! norms
        call VecNorm(b, NORM_1, n1, ierr)
        call VecNorm(ax, NORM_1, n2, ierr)

        ! scale the residual
        ref = n1 + n2
        this%res(iComp) = this%res(iComp)/(ref + small)

        ! destroy all the working vectors
        call VecDestroy(x, ierr)
        call VecDestroy(xBar, ierr)
        call VecDestroy(ax, ierr)
        call VecDestroy(axBar, ierr)
        call VecDestroy(resBar, ierr)
        call VecDestroy(ax_axBar, ierr)
        call VecDestroy(b, ierr)

    end subroutine scaledResidual2

!**********************************************************************************************************************

    function getNorm(this, x) result(initRes)

    !==================================================================================================================
    ! Description:
    !! getNorm scomputes the residual norm of the linear system.
    !==================================================================================================================

        class(transportEqn) :: this

        integer :: i, iElement, iComp, ierr
    !------------------------------------------------------------------------------------------------------------------

        real :: initRes
        !! Residual Norm !Ax-b|

        Vec x
        !! Field values vector

        Vec vres
        !! Working vecotr
    !------------------------------------------------------------------------------------------------------------------

        call VecDuplicate(this%rhs, vres, ierr)

        ! Get initial residual norm
        call MatMult(this%A, x, vres, ierr)
        call VecAXPY(vres, -1.0, this%rhs, ierr)
        call VecNorm(vres, NORM_1, initRes, ierr)

        ! Clean up
        call VecDestroy(vres, ierr)

    end function getNorm

!**********************************************************************************************************************

    subroutine writeResidual(this)

    !==================================================================================================================
    ! Description:
    !! writeresiduals writes the residual in a data file ready to plot.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: header
    !------------------------------------------------------------------------------------------------------------------

        real :: timeElapsed
        !! time Elapsed per iteration
    !------------------------------------------------------------------------------------------------------------------

        ! File Header
        timeElapsed = telap(2)-telap(1)

        if(this%nComp==1) then
            header = '# Iteration      ScaledResidual      UnscaledResidual'
        elseif(this%nComp==3) then
            header = '# Iteration      ScaledResidual-x    ScaledResidual-y      ScaledResidual-z      &
                      UnscaledResidual-x    UnscaledResidual-y      UnscaledResidual-z'
        elseif(this%nComp==3 .and. bdim==1) then
            header = '# Iteration      ScaledResidual-x    ScaledResidual-y      UnscaledResidual-x    UnscaledResidual-y'
        end if

        !-----------------!
        ! Write residuals !
        !-----------------!

        if(id==0 .and. flubioControls%saveResiduals==1 .and. mod(itime,flubioControls%resOutputFrequency)==0) then

            if(itime<=1) then

                open(1,file='postProc/monitors/res-'//trim(this%eqName)//'.dat',status='replace')
                    write(1,*) header
                    write(1,*) itime, this%convergenceResidual(:)/(this%scalingRef(:) + 1e-10), this%convergenceResidual(:)
                close(1)
            else
                open(1,file='postProc/monitors/res-'//trim(this%eqName)//'.dat',access='append')
                    write(1,*) itime, this%convergenceResidual(:)/(this%scalingRef(:) + 1e-10), this%convergenceResidual(:)
                close(1)

            endif

        endif

    end subroutine writeResidual

! *********************************************************************************************************************

    subroutine verbose(this, field)

    !==================================================================================================================
    ! Description:
    !! verbose prints at screen some information related to the simulation. You can custumize the output as you retain
    ! more appropriate.
    !==================================================================================================================

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: str, final

        character(len=30) max, min

        integer :: iComp, fcomp
    !------------------------------------------------------------------------------------------------------------------

        real, dimension(:), allocatable :: minField, maxField
    !------------------------------------------------------------------------------------------------------------------

        if(flubioControls%verbosityLevel==1) call minMaxField(field, minField, maxField)

        if(id==0 .and. flubioControls%verbosityLevel==1) then

            fcomp =  field%nComp
            if(fcomp>1) fcomp = fcomp-bdim

            do iComp=1,fcomp
                write(min,'(ES12.5)') minField(iComp)
                write(max,'(ES12.5)') maxField(iComp)
                str = 'min('//trim(field%fieldName(iComp))//'), max('//trim(field%fieldName(iComp))//'): '
                final = ' ' // str //' ('//trim(min) // ', ' //trim(adjustl(trim(max)))//')'
                write(*,*) final
            end do

        endif

    end subroutine verbose

!**********************************************************************************************************************

    subroutine kspVerbose(this, fieldName, it, iCorr, initRes, res, scalingFactor)

    !==================================================================================================================
    ! Description:
    !! kspVerbose prints at screen some information related to the ksp solve.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len = *) fieldName

        character(len=:), allocatable :: str1, str2, final, optName

        character(len=20) :: string1, string2, string3

        character(len=30) :: itStr, initResStr, finalResStr
    !------------------------------------------------------------------------------------------------------------------

        integer :: it, iCorr, ierr, outputMatrixEvery, outputCorrEvery
    !------------------------------------------------------------------------------------------------------------------

        real :: initRes, res, scalingFactor, small
    !------------------------------------------------------------------------------------------------------------------

        PetscBool set, setOutputFreq, setCorrOutputFreq
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        if(id==0) then

            write(itStr,'(i0)') it
            if(lowercase(flubioSolvers%scalingType) == 'yes' .or. lowercase(flubioSolvers%scalingType) == 'on') then
                write(initResStr,'(ES12.5)') initRes/(scalingFactor + small)
                write(finalResStr,'(ES12.5)') res/(scalingFactor + small)
            else
                write(initResStr,'(ES12.5)') initRes
                write(finalResStr,'(ES12.5)') res
            end if

            str1 = ' ' //  trim(fieldName) // ' converged in '// adjustl(trim(itStr))
            str2 = ' iterations, initial norm: ' // trim(adjustl(trim(initResStr))) // ', final norm: ' // adjustl(trim(finalResStr))
            final = str1 // str2
            write(*,*) final

        endif

        outputMatrixEvery = 1
        optName = '-'//this%eqName//'_'//'save_linear_system_every'
        call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, optName, string1, setOutputFreq, ierr)
        if(setOutputFreq) read(string1,*) outputMatrixEvery

        if((mod(itime, outputMatrixEvery)==0 .or. outputMatrixEvery==1) .and. setOutputFreq) then
            outputCorrEvery = 1
            optName = '-'//this%eqName//'_'//'save_linear_system_ncorr_every'
            call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, optName, string3, setCorrOutputFreq, ierr)
            if(setCorrOutputFreq) read(string3,*) outputCorrEvery

            if(mod(iCorr, outputCorrEvery)==1 .or. outputCorrEvery==1) then
                ! Print matrix if required
                optName = '-'//this%eqName//'_'//'save_linear_system_ncorr_every'
                call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, optName, string2, set, ierr)
                call this%exportMatrixToFile(iCorr)
            endif

        end if

    end subroutine kspVerbose

!**********************************************************************************************************************

    subroutine exportMatrixToFile(this, iCorr)

    !==================================================================================================================
    ! Description:
    !! exportMatrixToFile save the matrix to file.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=20):: iter, nonOrthCorr, string

        character(len=:), allocatable :: fileName, viewerType
    !------------------------------------------------------------------------------------------------------------------

        integer :: iCorr, ierr
    !------------------------------------------------------------------------------------------------------------------

        PetscBool set

        PetscViewer matrixViewer, rhsViewer
    !------------------------------------------------------------------------------------------------------------------

        ! Find out the viewer to use
        viewerType = '-'//this%eqName//'_'//'viewer_type'
        call PetscOptionsGetString(PETSC_NULL_OPTIONS,PETSC_NULL_CHARACTER, viewerType, string, set, ierr)

        write(iter,'(i0)') itime
        write(nonOrthCorr,'(i0)') iCorr

        if(lowercase(trim(string))=='ascii') then
            fileName = this%eqName//'_Matrix_it_'//trim(iter)//'_nOrthCorr_'//trim(nonOrthCorr)//'.dat'
            call PetscViewerASCIIOpen(PETSC_COMM_WORLD, fileName, matrixViewer, ierr)
            call MatView(this%A, matrixViewer,ierr)

            fileName = this%eqName//'_rhs_it_'//trim(iter)//'_nOrthCorr_'//trim(nonOrthCorr)//'.dat'
            call PetscViewerASCIIOpen(PETSC_COMM_WORLD, fileName, rhsViewer, ierr)
            call VecView(this%rhs, rhsViewer,ierr)
        else
            fileName = this%eqName//'_Matrix_it_'//trim(iter)//'.bin'
            call PetscViewerBinaryOpen(PETSC_COMM_WORLD, fileName, FILE_MODE_WRITE, matrixViewer, ierr)
            call MatView(this%A, matrixViewer,ierr)

            fileName = this%eqName//'_rhs_it_'//trim(iter)//'_nOrthCorr_'//trim(nonOrthCorr)//'.bin'
            call PetscViewerBinaryOpen(PETSC_COMM_WORLD, fileName, FILE_MODE_WRITE , rhsViewer, ierr)
            call VecView(this%rhs, rhsViewer,ierr)
        end if

        ! clean up
        call  PetscViewerDestroy(matrixViewer, ierr)
        call  PetscViewerDestroy(rhsViewer, ierr)

    end subroutine exportMatrixToFile

!**********************************************************************************************************************

    subroutine printResidual(this)

    !==================================================================================================================
    ! Description:
    !! printresidual prints the equation residual.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(:), allocatable :: str, res_str, final

        character(len=30) :: res(3)
    !------------------------------------------------------------------------------------------------------------------

        integer :: fComp
    !------------------------------------------------------------------------------------------------------------------

        fComp = this%nComp
        if(fComp>1) fComp = fComp - bdim

        if(fComp==1) then
            write(res(1),'(ES13.6)') this%convergenceResidual(1)/(this%scalingRef(1) + 1e-10)
            res_str = trim(res(1))
        elseif(fComp==2) then
            write(res(1),'(ES13.6)') this%convergenceResidual(1)/(this%scalingRef(1) + 1e-10)
            write(res(2),'(ES13.6)') this%convergenceResidual(2)/(this%scalingRef(2) + 1e-10)
            res_str = trim(res(1)) //' '// res(2)
        else
            write(res(1),'(ES13.6)') this%convergenceResidual(1)/(this%scalingRef(1) + 1e-10)
            write(res(2),'(ES13.6)') this%convergenceResidual(2)/(this%scalingRef(2) + 1e-10)
            write(res(3),'(ES13.6)') this%convergenceResidual(3)/(this%scalingRef(3) + 1e-10)
            res_str = trim(res(1)) //' '// trim(res(2)) //' '// trim(res(3))
        end if

        if(id == 0) then
            str = ' '//trim(this%eqName)//' initial residual: '
            final = str // res_str
            write(*,*) final
        endif

    end subroutine printResidual

!**********************************************************************************************************************

    subroutine printDelimiter(this)

    !==================================================================================================================
    ! Description:
    !! printDelimiter prints a delimiter with time information.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=80) :: FMT
    !------------------------------------------------------------------------------------------------------------------

        if(id==0) then

            write(*,*)
            FMT = "(A16, ES12.5, A8)"
            write(*,FMT) ' Time elapsed: ', telap(2)-telap(1), ' seconds'
            write(*,*) '***************************************************************************************************'
            write(*,*)

            FMT = "(A14, ES13.6, A1, A19, i0)"

            if(steadySim==0 .and. time<tmax) write(*,FMT) ' Time = ', time, ',' ,' Time-step number:   ', itime

            write(*,*)

        end if

    end subroutine printDelimiter

!**********************************************************************************************************************

    subroutine printCurrentIteration(this)

    !==================================================================================================================
    ! Description:
    !! printCurrentIteration prints the current iteration.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: itimeStr

        character(len=:), allocatable :: final
    !------------------------------------------------------------------------------------------------------------------

        write(itimeStr,'(i0)') itime

        if(id==0) then
            final = ' Iteration number: ' // adjustl(trim(itimeStr))
            if(steadySim==1) write(*,*) final
            write(*,*)
        end if

    end subroutine printCurrentIteration

!**********************************************************************************************************************

    subroutine checkSteadyState(this)

    !==================================================================================================================
    ! Description:
    !! checkSteadyState checks if the simulation is steady.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName, opt
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! check default option
        call flubioOptions%json%get('Default%TimeScheme', opt, found)
        if(.not. found) opt = 'none'

        if(lowercase(opt)=='steady') then
            steadySim=1
        else
            steadySim=0
        end if

        ! Check equation option, if default is not present
        if(.not. found) then

            call flubioOptions%json%get(this%eqName//'%TimeScheme', opt, found)
            if(.not. found) opt = 'none'

            if(lowercase(opt)=='steady') then
                steadySim=1
            else
                steadySim=0
            end if

        end if

    end subroutine checkSteadyState

! *********************************************************************************************************************

    subroutine checkConvergence(this)

    !==================================================================================================================
    ! Description:
    !! checkConvergence checks if the convergence criteria are satified or if the simulation has exceeded
    !! the maximum number of iterations.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        !-------------------!
        ! Check Convergence !
        !-------------------!

        if(itime>=nint(tmax)) then

            flubioControls%convergence=1

            call flubioMsg('Equation has reached the maxium number of Iterations, simulation will be stopped at the end of the current iteration...')

        elseif(steadySim==0) then

            flubioControls%convergence=1

        elseif(this%convergenceResidual(1)/this%scalingRef(1)<=this%resmax(1)) then

            flubioControls%convergence=1

            if(id==0) then
                call flubioMsg('Equation has CONVERGED!')
                write(*,*) 'Residual for '// trim(this%eqName) // ' is: ', this%convergenceResidual/this%scalingRef
                write(*,*)
            endif

        endif

    end subroutine checkConvergence

! *********************************************************************************************************************

    subroutine getResidualForConvergenceCheck(this)

    !==================================================================================================================
    ! Description:
    !! getResidualForConvergenceCheck store the residual after the first non-orthogonal correction
    !! and uses it to check the equation convergence.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%convergenceResidual = this%res

    end subroutine getResidualForConvergenceCheck

! *********************************************************************************************************************

    subroutine viewKspSettings(this)

    !==================================================================================================================
    ! Description:
    !! viewKspSettings print the ksp settings to the screen.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: ierr
    !------------------------------------------------------------------------------------------------------------------

        call KSPview(this%ksp, PETSC_VIEWER_STDOUT_WORLD, ierr)

    end subroutine viewKspSettings

! *********************************************************************************************************************

    subroutine setSolverOptions(this)

    !==================================================================================================================
    ! Description:
    !! setSolverOptions parse options from the lsolver dictionary and set the as petsc options.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        TYPE(JSON_CORE) :: jcore

        type(JSON_VALUE), pointer :: objPointer, dictPointer, subdict_pointer, keyPointer
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictValue, path, optionPath, petscOpt

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, opt_size, ierr
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, keyFound, found_subdict
    !------------------------------------------------------------------------------------------------------------------

        call jcore%initialize()
        call  flubioSolvers%json%get(dictPointer)
        call jcore%get_child(dictPointer, this%eqName, objPointer, found)

        if(found) call jcore%get_child(objPointer, 'options', subdict_pointer, found_subdict)

        if(found_subdict) then

            optionPath = trim(this%eqName)//'%options'
            call flubioSolvers%json%info(optionPath, n_children=opt_size)

            do i = 1, opt_size

                call jcore%get_child(subdict_pointer, i, keyPointer, keyFound)

                if(keyFound)then

                    call jcore%get_path(keyPointer, path, keyFound)
                    call jCore%get(keyPointer, dictValue)

                    call split_in_array(path, splitPath, '.')
                    petscOpt = "-"//trim(this%eqName)//'_'//trim(splitPath(size(splitPath)))
                    call PetscOptionsSetValue(PETSC_NULL_OPTIONS, petscOpt, dictValue, ierr)

                else
                    call flubioStopMsg("ERROR: cannot parse option correctly. Please check your json dictionary.")
                endif

            end do

        end if

        ! Clean up
        nullify(dictPointer)
        if(found_subdict) nullify(objPointer)
        if(keyFound) nullify(keyPointer)
        call jcore%destroy()

    end subroutine setSolverOptions

! *********************************************************************************************************************

    function H(this, field) result(H_)

    !==================================================================================================================
    ! Description:
    !! H returns the H operator, commonly in SIMPLE algorithm description, i.e. :
    !! \begin{equation}
    !!    H = [b_C -\sum a_{nb}*\phi_{anb}]/a_C
    !!  \end{equation}
    !==================================================================================================================

#include "petsc/finclude/petscksp.h"
        use petscksp

        class(transportEqn) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        PetscReal auxD, auxNb(maxNeigh)

        Vec y
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, fComp, ierr, nn

        integer :: iG, jG(maxNeigh), ii(numberOfElements), conn(numberOfElements, maxNeigh)
    !------------------------------------------------------------------------------------------------------------------

        real :: H_(numberOfElements, this%nComp), sumAnb(numberOfElements,1)

        real :: val, gradP
    !------------------------------------------------------------------------------------------------------------------

        H_ = 0.0

        fComp = this%nComp

        do iComp=1,fComp

            !---------------------!
            ! Assemble the matrix !
            !---------------------!

            call MatZeroEntries(this%A, ierr)
            call VecZeroEntries(this%rhs, ierr)
            sumAnb = 0.0

            do iElement=1,numberOfelements

                iG = mesh%cellGlobalAddr(iElement)-1 ! PETSC counts from 0

                ! Diagonal value
                auxD = this%aC(iElement,iComp)
                call MatSetValue(this%A,iG,iG,auxD,INSERT_VALUES,ierr)

                ! Neighbours values
                nn = mesh%numberOfNeighbours(iElement)
                auxNb(1:nn) = this%anb(iElement)%coeffs(1:nn,iComp)

                jG(1:nn) = mesh%conn(iElement)%col(1:nn)-1
                call MatSetValues(this%A,1,iG,nn,jG(1:nn),auxNb,INSERT_VALUES,ierr)

            enddo

            call MatAssemblyBegin(this%A,MAT_FINAL_ASSEMBLY,ierr)
            call MatAssemblyEnd(this%A,MAT_FINAL_ASSEMBLY,ierr)

            !-------------------------------------!
            ! Fill a vector with the equation rhs !
            !-------------------------------------!

            ! duplicate the vector
            call VecDuplicate(this%rhs,y,ierr)

            do i=1,numberOfElements
                ii(i)=mesh%cellGlobalAddr(i)-1 ! PETSC counts from 0
            enddo

            call VecSetValues(this%rhs,numberOfElements,ii,field%phi(1:numberOfElements,iComp),INSERT_VALUES,ierr)

            call VecAssemblyBegin(this%rhs, ierr)
            call VecAssemblyEnd(this%rhs, ierr)

            !------------------------------------------------------------------------!
            ! Matrix multiplication, remember to subtract aC(iElement)*phi(iElement) !
            !------------------------------------------------------------------------!

            call MatMult(this%A, this%rhs, y, ierr)
            call VecGetValues(y, flubioSolvers%lm, ii, sumAnb(:,1), ierr)

            ! Subtract off the diagonal term and add the pressure gradient again
            do iElement=1,numberOfElements

                val = this%bC(iElement,iComp) -(sumAnb(iElement,1) &
                        -this%aC(iElement,iComp)*field%phi(iElement,iComp))

                H_(iElement,iComp) = val/this%aC(iElement,iComp)

            enddo

            ! Destroy PETSc object
            call VecDestroy(y, ierr)

        enddo

    end function H

! *********************************************************************************************************************

    function H1(this) result(H1_)

    !==================================================================================================================
    ! Description:
    !! H1 returns the H1 operator, commonly in SIMPLE algorithm description, i.e. :
    !! \begin{equation}
    !!    H_1 = \sum a_{nb}
    !!  \end{equation}
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iElement, iComp, fComp, nn
    !------------------------------------------------------------------------------------------------------------------

        real :: H1_(numberOfElements, this%nComp)
    !------------------------------------------------------------------------------------------------------------------

        H1_ = 0.0

        fComp = this%nComp

        do iComp=1,fComp

            do iElement=1,numberOfelements
                nn = mesh%numberOfNeighbours(iElement)
                H1_(iElement, iComp) = sum(this%anb(iElement)%coeffs(1:nn, iComp))
            enddo

        enddo

    end function H1

! *********************************************************************************************************************

    subroutine applyToSelectedRegions(aC, bC, iElement, sourceName, nComp)

    !==================================================================================================================
    ! Description:
    !! applyToSelectedRegions applies the source only to the selected regions, if they are defined
    !==================================================================================================================

        character(len=*) sourceName
        !! Name of the source

        character(len=:), dimension(:), allocatable :: regionNames
        !! region name
    !------------------------------------------------------------------------------------------------------------------

        integer iElement, iRegion, regionIndex, nComp

        integer, dimension(:),allocatable :: ilen
        !! first array element lenght
    !------------------------------------------------------------------------------------------------------------------

        real :: aC(nComp), bC(nComp), applySource
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! Check if where the source is to be applied
        call flubioOptions%json%get('VolumeSources%'//sourceName//'%regions', regionNames, ilen, found)

        ! Filter only if you find the region where to do so, otherwise apply the source in the whole domain
        if(found) then

            ! everywhere will apply the source everything by skipping the check
            if(regionNames(1) == 'everywhere') return

            ! sum up the sources position flag. If the sum is 0, then do not apply the source,
            ! while if it is bigger than 1.0 apply the source
            applySource = 0.0
            do iRegion=1,size(regionNames)
                regionIndex = mesh%getRegionIndexByName(adjustl(trim(regionNames(iRegion))))
                applySource = applySource + mesh%meshRegions(regionIndex)%phi(iElement,1)
            end do

            if(applySource < 0.5) then
                aC = 0.0
                bC = 0.0
            end if
        end if

    end subroutine applyToSelectedRegions

!**********************************************************************************************************************

    subroutine setEquationSources(this)

    !==================================================================================================================
    ! Description:
    !! setEqnSources fills the run time processing dictionary.
    !==================================================================================================================

        class(transportEqn) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, typeValue, sourceName, path
        !! First array element lenght

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: iSource, nSources
    !------------------------------------------------------------------------------------------------------------------

        logical :: keyFound, typeFound, sourceFound, sourcesFound, eqnFound, nameFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, sourcesPointer, eqnPointer, sourcePointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioSources%json%get(dictPointer)

        call flubioSources%sourcesTable%get(this%eqName, nSources, keyFound)
        call raiseKeyErrorJSON(this%eqName, keyFound)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)

        ! Get sources pointer
        call jcore%get_child(sourcesPointer, this%eqName, eqnPointer, eqnFound)

        ! Allocate the monitors array
        allocate(this%fvSources(nSources))

        do iSource=1,nSources

            ! Get monitors pointer
            call jcore%get_child(eqnPointer, iSource, sourcePointer, sourceFound)

            ! Get monitor name
            call jcore%get_path(sourcePointer, path, nameFound)
            if(nameFound) then
                call split_in_array(path, splitPath, '.')
                sourceName = splitPath(3)
            else
                call flubioStopMsg('ERROR: cannot find the source name, please check if the dictionary is correct.')
            end if

            ! Get monitor type
            call jcore%get(sourcePointer, 'type', typeValue, typeFound)

            if(typeFound) then

                if(lowercase(trim(typeValue))=='constantsource') then
                    allocate(constantSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='semiimplicitsource') then
                    allocate(semiImplicitSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='boussinesqsource') then
                    allocate(boussinesqSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='bouyancysource') then
                    allocate(bouyancySourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                elseif(lowercase(trim(typeValue))=='poroussource') then
                    allocate(porousSourceTerm::this%fvSources(iSource)%source)
                    this%fvSources(iSource)%source%sourceName = sourceName
                else
                    call flubioStopMsg('ERROR: unknown source type: '//trim(typeValue))
                end if
            else
                call flubioStopMsg("ERROR: cannot find the source type. Please either specify it or check the spelling.")
            end if

        enddo

        ! Initialise sources
        do iSource =1,nSources
            call this%fvSources(iSource)%source%set(trim(this%eqName))
        enddo

        ! Clean up
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(sourceFound) nullify(sourcePointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setEquationSources

!**********************************************************************************************************************

end module transportEq
