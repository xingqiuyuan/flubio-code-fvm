# Library name
set(TARGET serialvtkformat)

# Collect files
file(GLOB src "*.f03")
set_source_files_properties(${src} PROPERTIES LANGUAGE Fortran)

# Add the library
add_library(${TARGET} ${src})
set_target_properties(${TARGET} PROPERTIES COMPILE_FLAGS ${CMAKE_Fortran_FLAGS})
target_include_directories(${TARGET}  PRIVATE "${PENF_INC_PATH};${VTK_IO_INC_PATH};${CMAKE_Fortran_MODULE_DIRECTORY}")
target_link_libraries(${TARGET}  PRIVATE ${PENF_LIBRARY} ${VTK_IO_LIBRARY} strings serialmeshvar commonutils flubiompi)