!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module bcDicts

!==================================================================================================================
! Description:
!! This modules contains the definition of the dictionaries needed to define the boundary conditons for each equation type.
!! Too learn more about equation types, see the realted documentation in the "sources/equations" folder.
!! The dictionary associates to a each boundary condition name a flag,used later to call the correct boundary condition subroutine.
!==================================================================================================================

    use generalDictionaries

    implicit none

    type(generalDict) :: momentumBC
    !! momentum equation dictionary

    type(generalDict) :: pressureBC
    !! pressure equations dictionary

    type(generalDict) :: scalarBC
    !! scalar transport equations dictionary. This is the deafualt for each transport equation.

    type(generalDict) :: turbulentBC
    !! turbulent equations dictionary

contains

    subroutine setMomentumBcList

    !==================================================================================================================
    ! Description:
    !! setMomentumBcList associates a boundary condition flag corresponding to a boundary condition type for the momentum equation.
    !==================================================================================================================

        integer :: bcFlag
        !! auxilairy varaible used to assign the boundary flag in the dictionary

        integer :: isize
        !! auxilary variable used to define the size of the dictionary
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(bcFlag)/8
        dictName = 'momentumBoundaryConditions'
        call momentumBC%dict%new(15,isize)
        call momentumBC%setDictName(dictName)

        ! Void
        bCFlag = 0
        call momentumBC%dict%put("void", bCFlag)

        ! Wall
        bCFlag = 1
        call momentumBC%dict%put("wall", bCFlag)

        ! Slip
        bCFlag = 2
        call momentumBC%dict%put("slip", bCFlag)

        ! Symmetry
        bCFlag = 3
        call momentumBC%dict%put("symmetry", bCFlag)

        ! VelocityInlet
        bCFlag = 4
        call momentumBC%dict%put("velocityInlet", bCFlag)

        ! Neumann0
        bCFlag =  5
        call momentumBC%dict%put("neumann0", bCFlag)

        ! Neumann
        bCFlag =  6
        call momentumBC%dict%put("neumann", bCFlag)

        ! NavierSlip
        bCFlag = 8
        call momentumBC%dict%put("navierSlip", bCFlag)

        ! Dirichlet
        bCFlag = 9
        call momentumBC%dict%put("dirichlet", bCFlag)

        ! Robin
        bCFlag = 10
        call momentumBC%dict%put("robin", bCFlag)

        ! Far Field
        bCFlag = 11
        call momentumBC%dict%put("farField", bCFlag)

        ! User defined dirichelt with inline expression parsing
        bCFlag = 1000
        call momentumBC%dict%put("userDefinedDirichlet", bCFlag)

        ! User defined inlet with inline expression parsing
        bCFlag = 1001
        call momentumBC%dict%put("userDefinedInlet", bCFlag)

        ! Periodic
        bCFlag = -1
        call momentumBC%dict%put("periodic", bCFlag)

    end subroutine setMomentumBcList

! *********************************************************************************************************************

    subroutine setPressureBcList()

    !==================================================================================================================
    ! Description:
    !! setPressureBcList associates a boundary condition flag corresponding to a boundary condition type for the pressure equation.
    !==================================================================================================================

        integer :: bcFlag
        !! auxilairy varaible used to assign the boundary flag in the dictionary

        integer :: isize
        !! auxilary variable used to define the size of the dictionary
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(bcFlag)/8
        dictName = 'pressureBoundaryConditions'
        call pressureBC%dict%new(5,isize)
        call pressureBC%setDictName(dictName)

        ! void
        bCFlag = 0
        call pressureBC%dict%put("void", bCFlag)

        ! FixedPressure
        bCFlag = 1
        call pressureBC%dict%put("fixedPressure", bCFlag)

        ! neumann0
        bCFlag = 2
        call pressureBC%dict%put("neumann0", bCFlag)

        ! neumann0
        bCFlag = 3
        call pressureBC%dict%put("farFieldPressure", bCFlag)

        ! Periodic
        bCFlag = -1
        call pressureBC%dict%put("periodic", bCFlag)


    end subroutine setPressureBcList

! *********************************************************************************************************************

    subroutine setScalarTransportBcList()

    !==================================================================================================================
    ! Description:
    !! setScalarTransportBcList associates a boundary condition flag
    !! corresponding to a boundary condition type for the transport equation.
    !==================================================================================================================

        integer :: bcFlag
        !! auxilairy varaible used to assign the boundary flag in the dictionary

        integer :: isize
        !! auxilary variable used to define the size of the dictionary
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(bcFlag)/8
        call scalarBC%dict%new(13,isize)
        dictName = 'scalarBoundaryConditions'
        call scalarBC%setDictName(dictName)

        ! void
        bCFlag = 0
        call scalarBC%dict%put("void", bCFlag)

        ! FixedValue
        bCFlag = 1
        call scalarBC%dict%put("fixedValue", bCFlag)

        ! Dirichlet
        bCFlag = 1
        call scalarBC%dict%put("dirichlet", bCFlag)

        ! neumann0
        bCFlag = 2
        call scalarBC%dict%put("neumann0", bCFlag)

        ! neumann
        bCFlag = 3
        call scalarBC%dict%put("neumann", bCFlag)

        ! Fixed Flux
        bCFlag = 4
        call scalarBC%dict%put("prescribedFlux", bCFlag)

        ! Robin
        bCFlag = 5
        call scalarBC%dict%put("robin", bCFlag)

        ! farField
        bCFlag = 6
        call scalarBC%dict%put("farField", bCFlag)

        ! User defined
        bCFlag = 1000
        call scalarBC%dict%put("userDefinedDirichlet", bCFlag)

        ! Periodic
        bCFlag = -1
        call scalarBC%dict%put("periodic", bCFlag)

    end subroutine setScalarTransportBcList

! *********************************************************************************************************************

    subroutine setTurbulentBcList()

    !==================================================================================================================
    ! Description:
    !! setTurbulentBcList associates a boundary condition flag to a boundary condition type for
    !! a transport equation equation used for turbulence modelling.
    !==================================================================================================================

        integer :: bcFlag
        !! auxilairy varaible used to assign the boundary flag in the dictionary

        integer :: isize
        !! auxilary variable used to define the size of the dictionary
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(bcFlag)/8
        call turbulentBC%dict%new(15,isize)
        dictName = 'turbulentBoundaryConditions'
        call turbulentBC%setDictName(dictName)

        ! void
        bCFlag = 0
        call turbulentBC%dict%put("void", bCFlag)

        ! FixedValue
        bCFlag = 1
        call turbulentBC%dict%put("fixedValue", bCFlag)

        ! FixedValue
        bCFlag = 1
        call turbulentBC%dict%put("dirichlet", bCFlag)

        ! neumann0
        bCFlag = 2
        call turbulentBC%dict%put("neumann0", bCFlag)

        ! neumann
        bCFlag = 3
        call turbulentBC%dict%put("neumann", bCFlag)

        ! Fixed Flux
        bCFlag = 4
        call turbulentBC%dict%put("prescribedFlux", bCFlag)

        ! Robin
        bCFlag = 5
        call turbulentBC%dict%put("robin", bCFlag)

        ! farField
        bCFlag = 6
        call turbulentBC%dict%put("farField", bCFlag)

        ! User defined
        bCFlag = 1000
        call turbulentBC%dict%put("userDefinedDirichlet", bCFlag)

        ! Specified turbulent intensity
        bCFlag = 7
        call turbulentBC%dict%put("turbulentIntensity", bCFlag)

        ! Specified viscosity ratio
        bCFlag = 8
        call turbulentBC%dict%put("viscosityRatio", bCFlag)

        ! Omega low Re wall function
        bCFlag = 9
        call turbulentBC%dict%put("omegaLowReWallFunction", bCFlag)

        ! Omega wall function
        bCFlag = 10
        call turbulentBC%dict%put("omegaWallFunction", bCFlag)

        ! Omega wall function
        bCFlag = 11
        call turbulentBC%dict%put("tkeWallFunction", bCFlag)

        ! Periodic
        bCFlag = -1
        call turbulentBC%dict%put("periodic", bCFlag)


    end subroutine setTurbulentBcList

end module bcDicts
