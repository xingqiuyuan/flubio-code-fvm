!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module flubioBoundaries

!==================================================================================================================
! Description:
!! This module implements the data structure and methods for the mesh boundaries.
!==================================================================================================================

    use globalMeshVar
    use flubioMpi
    use strings
    use m_strings
    use nodes, only: skipHeader, findStreamSize, checkFormat

    implicit none

    type, public :: fvBoundaries
        
        integer :: numberOfBoundaries
        !! Number of boundaries in the domain
        
        integer :: numberOfPatches
        !! Number of boundaries in the domain
        
        integer :: numberOfRealBound
        !! Number of physical boundaries in the domain (preocessor boundaries are excluded)
        
        integer :: numberOfProcFaces
        !! Not used.
        
        integer :: maxProcFaces
        !! Maxium number of faces in the processor boundaries. It is needed to  determin the buffer size during MPI communications
        
        integer :: maxPerFaces
        !! Not used
        
        integer :: maxWallFaces
        !! Maximum number of wall faces
        
        character(30), dimension(:), allocatable :: userName
        !! Boundary name provided by te user
        
        character(30), dimension(:), allocatable :: neighPatch
        !! Name of the neighbour patch for a periodic boundary
        
        character(30), dimension(:), allocatable :: refPatch
        !! reference patch

        character(30), dimension(:), allocatable :: bcType
        !! Base type of boundary condition (wall, inlet, outlet)
        
        integer, dimension(:), allocatable :: startFace
        !! Location of the starting face in the faces array for a target patch
        
        integer, dimension(:), allocatable :: startFaceOrg
        !! Location of the starting face in the faces array for a target patch (backup)
        
        integer, dimension(:), allocatable :: nFace
        !! Number of faces compising a target patch

        integer, dimension(:), allocatable :: realBound
        !! Array containing the position of the physical boundaries in the boundary file
        
        integer, dimension(:), allocatable :: procBound
        !! Array containing the position of the processor boundaries in the boundary file
        
        integer, dimension(:), allocatable :: myProc
        !! For a periodic boundary, myProc is the id of the rank hosting the patch

        integer, dimension(:), allocatable :: neighProc
        !! For a periodic boundary, neighProc is the id of the rank neighbouring with the patch

        integer, dimension(:), allocatable :: perBound
        !!  Array containing the position of the periodic boundaries in the boundary file

        integer, dimension(:), allocatable :: wallBound
        !! Array containing the position of the wall boundaries in the boundary file

        integer, dimension(:,:), allocatable :: periodicBoundaryMapping

        integer, dimension(:), allocatable :: getCyclicPatch
        !! position of the periodic boundary in the processor boundary array

        integer, dimension(:,:), allocatable :: ghostBoundaryMap
        !! Mapping array for the processor boundary.

        contains
            procedure :: readBoundAddr
            procedure :: readBoundAddrOFAscii
            procedure :: readBoundAddrOFBinary
            procedure :: readBoundAddrFlubioAscii
            procedure :: readBoundAddrFlubioBinary

            procedure :: readBoundariesFromFiles
            procedure :: readBoundariesFromFilesOFAscii
            procedure :: readBoundariesFromFilesFlubioAscii
            procedure :: readBoundariesFromFilesFlubioBinary

            procedure :: setupCyclicBoundaries
            
            procedure :: findBoundaryIndices
            procedure :: findBoundaryByName
            procedure :: findWallBoundaries
            procedure :: getPeriodicFacesSize
            procedure :: getProcessorBoundaryMap
            procedure :: findNumberOfRealBoundaryFaces

    end type fvBoundaries

contains

    subroutine readBoundAddr(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID

        character(len=:), allocatable :: fileFormat
        !! file format
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, dummy, counter1, nFaces

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        call checkFormat('grid/processor'//trim(procID)//'/boundaryProcAddressing', fileFormat)

        if (fileFormat == 'openfoam_ascii') then
            call this%readBoundAddrOFAscii()
        elseif(fileFormat == 'openfoam_binary') then
            call this%readBoundAddrOFBinary()
        elseif(fileFormat == 'flubio_ascii') then 
            call this%readBoundAddrFlubioAscii()
        elseif(fileFormat == 'flubio_binary') then 
            call this%readBoundAddrFlubioBinary()
        else
            call flubioStopMsg('ERROR: cannot recognize file format for faces.')
        endif    

    end subroutine readBoundAddr

! **********************************************************************************************************************

    subroutine readBoundAddrOFAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID

        character(len=80) :: line
        !! parsed line

        character(len=:), allocatable :: croppedLine
        !! cropped line

        character(len=:), allocatable :: oneLine
        !! cropped line

        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iFace, dummy, counter1, nFaces, ierr

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        logical :: oneLineFlag
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1,file='grid/processor'//trim(procID)//'/boundaryProcAddressing')
              
            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=numberOfBoundaries, oneLine = oneLine)

            ! Allocate arrays
            allocate(dummyVec1(numberOfBoundaries))

            if (.not. allocated(oneLine)) then 
                oneLineFlag = .false.
            else
                oneLineFlag = matchw(oneLine, '*(*')
            endif   

            if(oneLineFlag) then

                call substitute(oneLine,'(',' ')
                call substitute(oneLine,')',' ')
                call split_in_array(oneLine, carray, ' ')
            
                do i=2,size(carray)
                    
                    call string_to_value(carray(i), dummy, ierr)
                    
                    if(dummy/=-1) then
                        counter1 = counter1+1
                    endif
                    dummyVec1(i-1) = dummy
                enddo
               
            else

                do iBoundary=1,numberOfBoundaries

                    call readLine(1, line, ierr)
                    croppedLine = crop(line)
    
                    call substitute(croppedLine,'(',' ')
                    call substitute(croppedLine,')',' ')
                    call split_in_array(croppedLine, carray, ' ')
                
                    call string_to_value(carray(1), dummy, ierr)
                    if(dummy/=-1) then
                        counter1 = counter1+1
                    endif
                    dummyVec1(iBoundary) = dummy
    
                enddo 

            endif        

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries
        numberOfRealBound = this%numberOfRealBound

    end subroutine readBoundAddrOFAscii

! **********************************************************************************************************************

    subroutine readBoundAddrOFBinary(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID

        character(len=:), allocatable :: oneLine
        !! cropped line
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, dummy, counter1, nFaces, ierr

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1, file='grid/processor'//trim(procID)//'/boundaryProcAddressing', form = 'UNFORMATTED', access='STREAM')

            call skipHeader(aunit=1, fileFormat='binary')
            call findStreamSize(aunit=1, fileFormat='binary', streamSize=numberOfBoundaries, oneLine = oneLine)
        
            allocate(dummyVec1(numberOfBoundaries))

            do iBoundary=1,numberOfBoundaries

                read(1) dummy

                if(dummy/=-1) then
                    counter1 = counter1+1
                endif

                dummyVec1(iBoundary) = dummy
            enddo

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries

        numberOfRealBound = this%numberOfRealBound

        deallocate(dummyVec1)

    end subroutine readBoundAddrOFBinary

! **********************************************************************************************************************

    subroutine readBoundAddrFlubioAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, dummy, counter1, nFaces

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1, file='grid/processor'//trim(procID)//'/boundaryProcAddressing')

            read(1,*) numberOfBoundaries

            allocate(dummyVec1(numberOfBoundaries))

            do iBoundary=1,numberOfBoundaries

                read(1,*) dummy

                if(dummy/=-1) then
                    counter1 = counter1+1
                endif

                dummyVec1(iBoundary) = dummy
            enddo

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries

        numberOfRealBound = this%numberOfRealBound

        deallocate(dummyVec1)

    end subroutine readBoundAddrFlubioAscii

! **********************************************************************************************************************

    subroutine readBoundAddrFlubioBinary(this)

    !==================================================================================================================
    ! Description:
    !! readBoundAddr reads the boundary address file, telling how the patches are decomposed across the processors.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10):: procID
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, dummy, counter1, nFaces

        integer, dimension(:), allocatable :: dummyVec1
        !! auxiliary integer vector
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id

        ! -------------------------------------------------------!
        ! Find out which boundaries are real boundaries          !
        ! (boudaries flagged with -1 are processors' boundaries) !
        ! -------------------------------------------------------!

        counter1 = 0

        open(1, file='grid/processor'//trim(procID)//'/boundaryProcAddressing', form="unformatted")

            read(1) numberOfBoundaries

            allocate(dummyVec1(numberOfBoundaries))

            do iBoundary=1,numberOfBoundaries

                read(1) dummy

                if(dummy/=-1) then
                    counter1 = counter1+1
                endif

                dummyVec1(iBoundary) = dummy
            enddo

        close(1)

        allocate(this%realBound(counter1))

        this%realBound = dummyVec1
        this%numberOfRealBound = counter1
        this%numberOfBoundaries = numberOfBoundaries

        numberOfRealBound = this%numberOfRealBound

        deallocate(dummyVec1)

    end subroutine readBoundAddrFlubioBinary

! **********************************************************************************************************************

    subroutine readBoundariesFromFiles(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(len=:), allocatable :: fileFormat
        !! file format
    !------------------------------------------------------------------------------------------------------------------

        call readBoundAddr(this)

        write(procID,'(i0)') id

        call checkFormat('grid/processor'//trim(procID)//'/boundary', fileFormat)

        if (fileFormat == 'openfoam_ascii') then
            call this%readBoundariesFromFilesOFAscii()
        elseif(fileFormat == 'openfoam_binary') then
            call this%readBoundariesFromFilesOFAscii()
        elseif(fileFormat == 'flubio_ascii') then 
            call this%readBoundariesFromFilesFlubioAscii()
        elseif(fileFormat == 'flubio_binary') then 
            call this%readBoundariesFromFilesFlubioBinary()
        else
            call flubioStopMsg('ERROR: cannot recognize file format for faces.')
        endif   

        call this%findNumberOfRealBoundaryFaces()

    end subroutine readBoundariesFromFiles

! *********************************************************************************************************************

    subroutine readBoundariesFromFilesOFAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(10) :: procID
        !! processor ID

        character(30) :: info(5)
        !! auxilairy vector to store boundary information

        character(30) :: info2(7)
        !! auxilairy vector to store boundary information

        character(30), dimension(:), allocatable :: dummyChar
        !! auxilairy sting vector

        character(len=100) :: line
        !! parsed line

        character(len=:), allocatable :: croppedLine
        !! cropped line

        character(len=:), allocatable :: oneLine
        !! cropped line

        character(len=30), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iProc, pseudoFaces
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: dummy
        !! dummy integer

        integer, dimension(:), allocatable :: allCyclic
        !! cyclic boudnaries

        integer :: sbuf(nid)
        !! sent buffer

        integer :: rbuf(nid)
        !! received buffer

        integer :: ierr
        !! error flag

        integer :: nb 
        !! number of boundaries
    !------------------------------------------------------------------------------------------------------------------
        
        logical :: isWord
    !------------------------------------------------------------------------------------------------------------------

        write(procID, '(i0)' ) id

        pseudoFaces = 0
        numberOfPeriodicBound = 0
        totalPerBound = 0

        allocate(this%userName(numberOfBoundaries))
        allocate(this%bcType(numberOfBoundaries))
        allocate(this%startFace(numberOfBoundaries))
        allocate(this%nFace(numberOfBoundaries))
        allocate(this%myProc(numberOfBoundaries))
        allocate(this%neighProc(numberOfBoundaries))
        allocate(this%neighPatch(numberOfBoundaries))
        allocate(this%refPatch(numberOfBoundaries))

        this%neighPatch = 'none'
        this%refPatch = 'none'

        this%myProc = -1
        this%neighProc = -1

        open(1,file='grid/processor'//trim(procID)//'/boundary')

            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=nb, oneLine=oneLine)

            iBoundary = 0
            ! loop pver the file to identify boundaries and keywords
            do
                call readLine(1, line, ierr)
                croppedLine = crop(line)
                call substitute(line,';','')   
                isWord = isalpha(line)
                call split_in_array(line, carray, ' ')
                
                ! That's the patch name!
                if(isWord .and. size(carray) ==1) then
                    iBoundary = iBoundary +1
                    this%userName(iBoundary) = carray(1)
                endif

                ! Find type
                if(size(carray) >1) then

                    if(carray(1) == 'type') this%bcType(iBoundary) = carray(2)
                    if(carray(1) == 'nFaces') call string_to_value(carray(2), this%nFace(iBoundary), ierr)
                    if(carray(1) == 'startFace') call string_to_value(carray(2), this%startFace(iBoundary), ierr)   
                    if(carray(1) == 'neighbourPatch') this%neighPatch(iBoundary) = carray(2)
                    if(carray(1) == 'myProcNo') call string_to_value(carray(2),  this%myProc(iBoundary), ierr) 
                    if(carray(1) == 'neighbProcNo') call string_to_value(carray(2), this%neighProc(iBoundary), ierr)
                    if(carray(1) == 'referPatch') this%refPatch(iBoundary) = carray(2)

                    if(this%realBound(iBoundary) == -1) pseudoFaces = pseudoFaces + this%nFace(iBoundary)

                endif    
            
                if(ierr /= 0) exit

            enddo   

        close(1)

        ! Set up cyclic boundaries, check if there are void patches, set maxium processor size for excahnge buffers
        call this%setupCyclicBoundaries()

    end subroutine readBoundariesFromFilesOFAscii

! **********************************************************************************************************************

    subroutine readBoundariesFromFilesFlubioAscii(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(30) :: info(5)
        !! auxilairy vector to store boundary information

        character(30) :: info2(7)
        !! auxilairy vector to store boundary information

        character(30), dimension(:), allocatable :: dummyChar
        !! auxilairy sting vector
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iProc, nread, nskip, endLine, pseudoFaces
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: dummy
        !! dummy integer

        integer, dimension(:), allocatable :: allCyclic
        !! cyclic boudnaries

        integer :: sbuf(nid)
        !! sent buffer

        integer :: rbuf(nid)
        !! received buffer

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        nskip = 1
        pseudoFaces = 0
        numberOfPeriodicBound = 0
        totalPerBound = 0

        allocate(this%userName(numberOfBoundaries))
        allocate(this%bcType(numberOfBoundaries))
        allocate(this%startFace(numberOfBoundaries))
        allocate(this%nFace(numberOfBoundaries))
        allocate(this%myProc(numberOfBoundaries))
        allocate(this%neighProc(numberOfBoundaries))
        allocate(this%neighPatch(numberOfBoundaries))
        allocate(this%refPatch(numberOfBoundaries))

        this%neighPatch = 'none'
        this%refPatch = 'none'

        this%myProc = -1
        this%neighProc = -1

        do iBoundary=1,numberOfBoundaries

            if (this%realBound(iBoundary) /= -1) then

                ! Read info
                call readLines(info, endLine, 5, 7, nskip)
                nskip = endLine

                ! Store info
                this%userName(iBoundary) = info(1)
                this%bcType(iBoundary) = info(2)

                read(info(3),'(i10)') this%nFace(iBoundary)
                read(info(4),'(i10)') this%startFace(iBoundary)
                if(info(2) == 'cyclic') this%neighPatch(iBoundary) = info(5)

            else

                ! Read info
                call readLines(info2, endLine, 7, 11, nskip)
                nskip = endLine

                ! Store info
                this%userName(iBoundary) = info2(1)
                this%bcType(iBoundary) = info2(2)

                read(info2(3),'(i10)') this%nFace(iBoundary)
                read(info2(4),'(i10)') this%startFace(iBoundary)
                read(info2(5),'(i10)') this%myProc(iBoundary)
                read(info2(6),'(i10)') this%neighProc(iBoundary)
                if(info2(2) == 'processorCyclic') this%refPatch(iBoundary) = info2(7)

                pseudoFaces = pseudoFaces + this%nFace(iBoundary)

            endif

        enddo

        ! Set up cyclic boundaries, check if there are void patches, set maxium processor size for excahnge buffers
        call this%setupCyclicBoundaries()

    end subroutine readBoundariesFromFilesFlubioAscii

! **********************************************************************************************************************

    subroutine readBoundariesFromFilesFlubioBinary(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(30) :: info(5)
        !! auxilairy vector to store boundary information

        character(30) :: info2(7)
        !! auxilairy vector to store boundary information

        character(30), dimension(:), allocatable :: dummyChar
        !! auxilairy sting vector
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iProc, nread, nskip, endLine, pseudoFaces
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: dummy
        !! dummy integer

        integer, dimension(:), allocatable :: allCyclic
        !! cyclic boudnaries

        integer :: sbuf(nid)
        !! sent buffer

        integer :: rbuf(nid)
        !! received buffer

        integer :: ierr
        !! error flag
    !------------------------------------------------------------------------------------------------------------------

        call flubioStopMSg('ERROR: this feature is to be coded yet, but it is not fundamental.')

    end subroutine readBoundariesFromFilesFlubioBinary

!**********************************************************************************************************************

    subroutine setupCyclicBoundaries(this)

    !==================================================================================================================
    ! Description:
    !! readBoundaries reads the boundary mesh files and the boundary conditions.
    !==================================================================================================================
    
            class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

            character(30), dimension(:), allocatable :: dummyChar
            !! auxilairy sting vector
    !------------------------------------------------------------------------------------------------------------------
    
            integer :: i, j, k, nEmpty, iBoundary, iProc, is, ie, pseudoFaces, max_faces, new_size
    !------------------------------------------------------------------------------------------------------------------
    
            integer, dimension(:), allocatable :: dummy
            !! dummy integer
    
            integer, dimension(:), allocatable :: allCyclic
            !! cyclic boudnaries
    
            integer :: sbuf(nid)
            !! sent buffer
    
            integer :: rbuf(nid)
            !! received buffer
    
            integer :: ierr
            !! error flag
    !------------------------------------------------------------------------------------------------------------------
    
            i = 0
            j = 0
            k = 0

            pseudoFaces = 0
            numberOfPeriodicBound = 0
            totalPerBound = 0

            allocate(dummy(numberOfBoundaries))
            allocate(cyclic(numberOfBoundaries))
            allocate(allCyclic(numberOfBoundaries))
            allocate(dummyChar(numberOfBoundaries))
    
            ! Count the number of periodic boundaries
            do iBoundary=1,numberOfBoundaries
    
                if(this%bCtype(iBoundary)=='cyclic' .and. this%nFace(iBoundary)/=0)  then
                    numberOfPeriodicBound = numberOfPeriodicBound +1
                    totalPerBound = totalPerBound +1
                endif
    
                if(this%bCtype(iBoundary)=='processorCyclic') totalPerBound = totalPerBound +1
    
            enddo

            ! Resize elements to account for periodic patches. They will be treated as process boundaries.
            if(numberOfPeriodicBound /= 0) then
    
                new_size = numberOfBoundaries+numberOfPeriodicBound
                dummy = 0
                dummy = this%startFace
    
                deallocate(this%startFace)
                allocate(this%startFace(new_size))
    
                this%startFace = 0
                this%startFace(1:numberOfBoundaries) = dummy
    
                dummy = 0
                dummy = this%nFace
    
                deallocate(this%nFace)
                allocate(this%nFace(new_size))
    
                this%nFace = 0
                this%nFace(1:numberOfBoundaries) = dummy
    
                dummy = 0
                dummy = this%neighProc
    
                deallocate(this%neighProc)
                allocate(this%neighProc(new_size))
    
                this%neighProc(1:numberOfBoundaries) = -1
                this%neighProc(1:numberOfBoundaries) = dummy
                dummyChar = this%userName
    
                deallocate(this%userName)
                allocate(this%userName(new_size))
    
                this%userName(1:numberOfBoundaries) = dummyChar
                dummyChar=this%bcType
    
                deallocate(this%bcType)
                allocate(this%bcType(new_size))
    
                this%bcType(1:numberOfBoundaries) = dummyChar
                dummy = 0
                dummy = this%realBound
    
                deallocate(this%realBound)
                allocate(this%realBound(new_size))
    
                this%realBound(1:numberOfBoundaries)=-1
                this%realBound(1:numberOfBoundaries) = dummy
                dummy = 0
                dummy = this%myProc
    
                deallocate(this%myProc)
                allocate(this%myProc(new_size))
    
                this%myProc(1:numberOfBoundaries) = -1
                this%myProc(1:numberOfBoundaries) = dummy
    
                deallocate(dummy)
                deallocate(dummyChar)
    
                ! Allocate original disposition of start faces to be use in calculation of the offset vector
                allocate(this%startFaceOrg(new_size))
                this%startFaceOrg=this%startFace+1
    
            else
    
                ! Allocate original disposition of start faces to be use in calculation of the offset vector
                allocate(this%startFaceOrg(numberOfBoundaries))
                this%startFaceOrg = this%startFace+1
            
            endif
    
            ! Locate processor's boundaries in the boundary vector
            allocate(this%procBound(numberOfBoundaries-this%numberOfRealBound+numberOfPeriodicBound))
            this%procBound = -1
            cyclic = -1
            allcyclic = -1
         
            do iBoundary=1,numberOfBoundaries
               
                if(this%realBound(iBoundary)==-1) then
                    i = i+1
                    this%procBound(i) = iBoundary
                endif
    
                if(this%bcType(iBoundary)=='cyclic' .and. this%nFace(iBoundary)/=0) then
                    j = j+1
                    this%procBound(numberOfBoundaries-this%numberOfRealBound+j) = numberOfBoundaries+j
                    cyclic(j) = iBoundary
                    allCyclic(j) = numberOfBoundaries+j
                endif
    
                if(this%bcType(iBoundary)=='processorCyclic' .and. this%nFace(iBoundary)/=0) then
                    k = k+j+1
                    allCyclic(k) = iBoundary
                endif
    
            enddo
    
            this%startFace = this%startFace+1
            this%numberOfProcFaces = pseudoFaces

            ! Add the cyclic patch to the processor boundaries
            j = numberOfBoundaries
            do iBoundary=1,numberOfPeriodicBound
               
                i = cyclic(iBoundary)
                j = j+1
    
                this%startFace(j) = this%startFace(i)
                this%nFace(j ) =this%nFace(i)
                this%myProc(j) = id
                this%neighProc(j) = id
                this%userName(j) = this%userName(i)
                this%bcType(j) = this%bcType(i)
                this%realBound(j) = -1
    
                ! void the faces in cyclic patches (avoid useless loops later)
                this%nFace(i) = 0
    
            enddo

            ! Look for the max buffer size
            sbuf = 0
            rbuf = 0
            max_faces = -1

            do iBoundary=1,numberOfBoundaries-this%numberOfRealBound
                iProc = this%procBound(iBoundary)
                if(this%nFace(iProc)>=max_faces) max_faces = this%nFace(iProc)
            enddo
                
            sbuf(id+1) = max_faces

            call mpi_allReduce(sbuf, rbuf, nid, MPI_INT, MPI_SUM, MPI_COMM_WORLD, ierr)
            this%maxProcFaces = maxval(rbuf)
            maxProcFaces = maxval(rbuf)
            
            ! Look for bimensionality and components of the problem
            bdim = 0
            nEmpty = 0
            do iBoundary=1,numberOfboundaries
                if(lowercase(this%bcType(iBoundary)) == 'empty' .or. lowercase(this%bcType(iBoundary)) == 'void') then
                    bdim = 1
                    nEmpty = nEmpty + 1
                    exit
                endif
            enddo
    
            if(nid==1) this%maxProcFaces=1
    
            deallocate(cyclic)
            deallocate(allCyclic)
    
            ! Update numberOfBoundaries
            numberOfBoundaries = numberOfBoundaries + numberOfPeriodicBound
    
            ! Locate periodic boundaries
            allocate(this%perBound(totalPerBound))
    
            ! Not Owned Faces
            do iBoundary=1,numberOfBoundaries
                if(this%bCtype(iBoundary)=='processorCyclic') 	then
                    numberOfProcCyclic = numberOfProcCyclic+1
                    this%perBound(numberOfProcCyclic) = iBoundary
                endif
            enddo
    
            ! Owned Faces
            i = numberOfProcCyclic
            do iBoundary=1,numberOfPeriodicBound
                i = i+1
                this%perBound(i)=this%procBound(numberOfBoundaries-this%numberOfRealBound-numberOfPeriodicBound+iBoundary)
            enddo
    
            ! find out the position of the cyclic patches in procBound vector.
            allocate(this%getCyclicPatch(totalPerBound))
    
            j = 0
            this%getCyclicPatch = -1
    
            do iBoundary=1, numberOfBoundaries - this%numberOfRealBound - numberOfPeriodicBound
                i = this%procBound(iBoundary)
                if(this%bCType(i)=='processorCyclic') then
                    j = j+1
                    this%getCyclicPatch(j) = iBoundary
                endif
            enddo
    
            ! Add owned cyclic patches to getCyclicPatch
            do i=1,numberOfPeriodicBound
                this%getCyclicPatch(numberOfProcCyclic+i) = numberOfBoundaries-this%numberOfRealBound - numberOfPeriodicBound + i
            enddo
    
    end subroutine setupCyclicBoundaries

! *********************************************************************************************************************

    subroutine findBoundaryIndices(this, patchNames, is, ie, nFaces, pIndex, n)

    !==================================================================================================================
    ! Description:
    !! findBoundaryIndices finds the index of a target boundary.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=50) :: patchNames(n)
        !! name of the boundaries

        character(len=50) :: actualBoundary
        !! actual inspected boundary

        character(len=50) :: targetBoundary
        !! target boundary
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, n, iBoundary

        integer :: is(n)
        !! start index for each boundary

        integer :: ie(n)
        !! end index for each boundary

        integer :: nFaces(n)
        !! number of faces for each boundary

        integer :: pIndex(n)
        !! patch indices
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! boolean
    !------------------------------------------------------------------------------------------------------------------

        pIndex = 0
        is = 0
        ie = -1

        !i = numberOfElements

        do j=1,n

            found = .false.

            targetBoundary = lowercase(adjustl(trim(patchNames(j))))
            i = numberOfElements
            do iBoundary=1, numberOfRealBound

                actualBoundary = lowercase(adjustl(trim(this%userName(iBoundary))))

                if(trim(actualBoundary)==trim(targetBoundary)) then

                    nFaces(j) = this%nFace(iBoundary)
                    is(j) = this%startFace(iBoundary)
                    ie(j) = is(j) + nFaces(j)-1
                    pIndex(j) = i

                    found = .true.

                else

                    i=i+this%nFace(iBoundary)

                endif

            enddo

            if(.not. found) call flubioStopMsg(' ERROR: Boundary '//trim(targetBoundary)//' not found in the boundaries list!')

        enddo

    end subroutine findBoundaryIndices

! *********************************************************************************************************************

    function findBoundaryByName(this, boundaryName) result(targetBoundary)

    !==================================================================================================================
    ! Description:
    !! findBoundaryByName returns boundary starting and ending indices.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: boundaryName
        !! name of the boundaries

        character(len=50) :: actualBoundary
        !! actual inspected boundary
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary

        integer :: is
        !! start index for each boundary

        integer :: ie
        !! end index for each boundary

        integer :: nFaces
        !! number of faces for each boundary

        integer :: targetBoundary(3)
        !! Boundary indices array (is,ie,nFaces)
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
        !! boolean
    !------------------------------------------------------------------------------------------------------------------

        is = 0
        ie = -1
        targetBoundary = -1

        found = .false.
        do iBoundary=1, numberOfRealBound

            actualBoundary = lowercase(adjustl(trim(this%userName(iBoundary))))

            if(actualBoundary==lowercase(boundaryName)) then

                nFaces = this%nFace(iBoundary)
                is = this%startFace(iBoundary)
                ie = is + nFaces-1

                targetBoundary(1) = is
                targetBoundary(2) = ie
                targetBoundary(3) = nFaces

                found = .true.

            endif

        enddo

        if(.not. found) call flubioStopMsg('ERROR: Boundary '//boundaryName// ' not found in the boundaries list!')

    end function findBoundaryByName

! *********************************************************************************************************************

    subroutine findWallBoundaries(this)

    !==================================================================================================================
    ! Description:
    !! findWallBoundaries find the index of the boundaries which are wall boundaries.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary
        !! target boundary

        integer :: iWall
        !! wall boundary index
    !------------------------------------------------------------------------------------------------------------------

        !----------------------------------------!
        ! Find out the number of wall boundaries !
        !----------------------------------------!

        allocate(this%wallBound(numberOfRealBound))

        numberOfWallBound = 0
        this%wallBound = -1
        this%maxWallFaces = -1

        do iBoundary=1,numberOfRealBound
            if(trim(this%bcType(iBoundary)) == 'wall') then
                numberOfWallBound = numberOfWallBound+1
                this%wallBound(numberOfWallBound) = iBoundary
            endif
        enddo

        ! Define maxWallFaces for each processor domain
        do iBoundary=1,numberOfWallBound
            iWall = this%wallBound(iBoundary)
            if(this%nFace(iWall) > this%maxWallFaces) this%maxWallFaces = this%nFace(iWall)
        enddo

    end subroutine findWallBoundaries

! *********************************************************************************************************************

    subroutine getPeriodicFacesSize(this)

    !==================================================================================================================
    ! Description:
    ! getPeriodicFacesSize counts the number of periodic faces.
    !==================================================================================================================

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, ierr

        integer :: sbuf(nid), rbuf(nid), max_faces
    !------------------------------------------------------------------------------------------------------------------

        ! look for the max buffer size
        sbuf = 0
        rbuf = 0

        max_faces=-1
        do iBoundary=1,numberOfProcCyclic+numberOfPeriodicBound
            i = this%perBound(iBoundary)
            if(this%nFace(i) >= max_faces) max_faces = this%nFace(i)
        enddo

        sbuf(id+1) = max_faces
        call mpi_allReduce(sbuf, rbuf, nid, MPI_INT, MPI_SUM, MPI_COMM_WORLD, ierr)
        this%maxPerFaces=maxval(rbuf)

        if(totalPerBound/=0) allocate(this%periodicBoundaryMapping(this%maxPerFaces,totalPerBound))

    end subroutine getPeriodicFacesSize

! *********************************************************************************************************************

    subroutine readLines(info, endLine, ninfo, nread, nskip)

    !==================================================================================================================
    ! Description:
    !! readLines parses the lines in the boundary file.
    !==================================================================================================================
    
        character(10) :: procID
        !! processor ID
    
        character(30) :: auxchar(nread+2)
        !! auxiliary string
    
        character(30) ::  info(ninfo)
        !! auxiliary string to stor info
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: i, is, ie, j, ninfo, nread, nskip, endLine
    !------------------------------------------------------------------------------------------------------------------
    
        write(procID,'(i0)') id
    
        open(1,file='grid/processor'//trim(procID)//'/boundary')
    
            do is=1,nskip
                read(1,*)
            enddo
    
            is = nskip+1
            ie = is+nread
            j = 0
    
            do i=1,nread
                j=j+1
                read(1,*) auxchar(j)
            enddo
    
            ! If the bondary is a periodic, read one more line
            if (auxchar(3)=='processorCyclic' .or. auxchar(3)=='cyclic') then
                j=j+1
                read(1,*)  auxchar(j) ! keyword name
    
                j=j+1
                read(1,*) auxchar(j)  ! keyword
            endif
    
        close(1)
    
        j = 1
        info(1) = auxchar(1)
    
        do i=3,nread,2
            j = j+1
            info(j)=auxchar(i)
        enddo
    
        i = nread
    
        ! If a boundary is periodic read neighProc or refPatch
        if (auxchar(3)=='processorCyclic' .or. auxchar(3)=='cyclic') then
            j = j+1
            info(j) = auxchar(i+2)
            endLine = ie-1+2
        else
            endLine = ie-1
        endif
    
    end subroutine readLines

! *********************************************************************************************************************

    subroutine findNumberOfRealBoundaryFaces(this)

    !==================================================================================================================
    ! Description:
    !! findNumberOfRealBoundaryFaces finds the number of real boundary faces, excluding processor boundaries.
    !==================================================================================================================
        
        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: iBoundary
        !! target boundary
        
        integer :: iWall
        !! wall boundary index
    !------------------------------------------------------------------------------------------------------------------
            
        numberOfRealBFaces = 0
        do iBoundary=1,numberOfBoundaries
            if(trim(this%bcType(iBoundary)) /= 'processor') then
                numberOfRealBFaces = numberOfRealBFaces + this%nFace(iBoundary)
            endif
        enddo
        
    end subroutine findNumberOfRealBoundaryFaces
        
! *********************************************************************************************************************
    
    subroutine getProcessorBoundaryMap(this)

        class(fvBoundaries) :: this
    !------------------------------------------------------------------------------------------------------------------       
    
        integer :: n, iBoundary, iBFace, iProc, is, ie, pNeigh
    !------------------------------------------------------------------------------------------------------------------

        ! Find out the size of the processor boundaries
        pNeigh = 0
        do iBoundary=1,numberOfProcBound
            iProc = this%procBound(iBoundary)
            pNeigh = pNeigh + this%nFace(iProc)  
        enddo

        allocate(this%ghostBoundaryMap(pNeigh,3))
        this%ghostBoundaryMap = -1

        ! Fill the mapping array
        n = 0
        do iBoundary=1,numberOfProcBound

            iProc = this%procBound(iBoundary)
            is = this%startFace(iProc)
            ie = is+this%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie
                n = n+1
                pNeigh = pNeigh+1
                this%ghostBoundaryMap(n,1) = iBoundary 
                this%ghostBoundaryMap(n,2) = pNeigh
                this%ghostBoundaryMap(n,3) = iBFace 
            enddo

        enddo

    end subroutine getProcessorBoundaryMap    

! *********************************************************************************************************************

end module flubioBoundaries
