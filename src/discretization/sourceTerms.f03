module sourceTerms

    use meshvar
    use flubioDictionaries

    implicit none

    ! Source terms containters
    type, public:: sourceTerm
        character(len=:), allocatable :: sourceType
        character(len=:), allocatable :: sourceName
        integer :: nComp
    contains
        procedure :: set => setBaseSource
        procedure :: computeSource => computeBaseSource
    end type sourceTerm

    type, public, extends(sourceTerm) :: constantSourceTerm
        real, dimension(:), allocatable :: constantValue
        !! constant value to apply
    contains
        procedure :: set => setConstantSourceTerm
        procedure :: computeSource => computeConstantSourceTerm
    end type constantSourceTerm

    type, public, extends(sourceTerm) :: semiImplicitSourceTerm

        real, dimension(:), allocatable :: aC
        !! implict contribuition (linear part)

        real, dimension(:), allocatable :: bC
        !! explicitContribution (non linear part)

    contains
        procedure :: set => setSemiImplicitSourceTerm
        procedure :: computeSource => computeSemiImplicitSourceTerm
    end type semiImplicitSourceTerm

    type, public, extends(sourceTerm) :: boussinesqSourceTerm
        real, dimension(:), allocatable :: g
        real :: alpha
        real :: rho0
        real :: T0
    contains
        procedure :: set => setBoussinesqSourceTerm
        procedure :: computeSource => computeBoussinesqSourceTerm
    end type boussinesqSourceTerm

    type, public, extends(sourceTerm) :: bouyancySourceTerm
        real, dimension(:), allocatable :: g
    contains
        procedure :: set => setBouyancySourceTerm
        procedure :: computeSource => computeBouyancySourceTerm
    end type bouyancySourceTerm

    type, public, extends(sourceTerm) :: porousSourceTerm

        character(len=:), allocatable :: regionName
        !! where to apply

        real, dimension(:), allocatable :: D
        !!Darcy tensor

        real, dimension(:), allocatable :: F
        !! Forchheimer tensor

    contains
        procedure :: set => setPorousSourceTerm
        procedure :: computeSource => computePorousSourceTerm
    end type porousSourceTerm

    ! Source term polymorphic array
    type sourceTermsArray
        class(sourceTerm), allocatable :: source
    end type sourceTermsArray

!    type(sourceTermsArray), dimension(:), allocatable :: fvSources

contains

    subroutine setBaseSource(this, eqName)

        class(sourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        call flubioMsg('FLUBIO: this is the base abstract class, you should not land here.')

    end subroutine setBaseSource

!**********************************************************************************************************************

    subroutine computeBaseSource(this, aC, bC)

        class(sourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        real :: aC(numberOfElements,3), bC(numberOfElements,3)
    !------------------------------------------------------------------------------------------------------------------

        aC = 0.0
        bC = 0.0

        call flubioMsg('FLUBIO: this is the base abstract class, you should not land here.')

    end subroutine computeBaseSource

!**********************************************************************************************************************

    subroutine setSemiImplicitSourceTerm(this, eqName)

        class(semiImplicitSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, sourcesFound, sourceFound, eqnFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, sourcesPointer, sourcePointer, eqnPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubiosources%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)

        ! Get eqn pointer
        call jcore%get_child(sourcesPointer, eqName, eqnPointer, eqnFound)
        call raiseKeyErrorJSON(eqName, eqnFound, "settings/volumeSources")

        ! Get source pointer
        call jcore%get_child(eqnPointer, this%sourceName, sourcePointer, sourceFound)
        call raiseKeyErrorJSON(this%sourceName, sourceFound, "settings/volumeSources")

        call jcore%get(sourcePointer, 'aC', this%aC, found)
        call raiseKeyErrorJSON('aC', found, "settings/volumeSources")

        call jcore%get(sourcePointer, 'bC', this%bC, found)
        call raiseKeyErrorJSON('bC', found, "settings/volumeSources")

        ! Clean up
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(sourceFound) nullify(sourcePointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setSemiImplicitSourceTerm

!**********************************************************************************************************************

    subroutine computeSemiImplicitSourceTerm(this, aC, bC)

        class(semiImplicitSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: aC(numberOfElements,3), bC(numberOfElements,3)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,numberOfElements
            aC(iElement,:) = -this%aC(1:3)*mesh%volume(iElement)
            bC(iElement,:) = this%bC(1:3)*mesh%volume(iElement)
        end do

    end subroutine computeSemiImplicitSourceTerm

!**********************************************************************************************************************

    subroutine setConstantSourceTerm(this, eqName)

        class(constantSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, sourcesFound, sourceFound, eqnFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, sourcesPointer, sourcePointer, eqnPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubiosources%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)

        ! Get eqn pointer
        call jcore%get_child(sourcesPointer, eqName, eqnPointer, eqnFound)
        call raiseKeyErrorJSON(eqName, eqnFound, "settings/volumeSources")

        ! Get source pointer
        call jcore%get_child(eqnPointer, this%sourceName, sourcePointer, sourceFound)
        call raiseKeyErrorJSON(this%sourceName, sourceFound, "settings/volumeSources")

        call jcore%get(sourcePointer, 'forcing', this%constantValue, found)
        call raiseKeyErrorJSON('forcing', found, "settings/volumeSources")

        ! Clean up
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(sourceFound) nullify(sourcePointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setConstantSourceTerm

!**********************************************************************************************************************

    subroutine computeConstantSourceTerm(this, aC, bC)

        class(constantSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: aC(numberOfElements,3), bC(numberOfElements,3)
    !------------------------------------------------------------------------------------------------------------------

        aC = 0.0
        do iElement=1,numberOfElements
            bC(iElement,:) = this%constantValue(1:3)*mesh%volume(iElement)
        end do

    end subroutine computeConstantSourceTerm

!**********************************************************************************************************************

    subroutine setBoussinesqSourceTerm(this, eqName)

        class(boussinesqSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, sourcesFound, sourceFound, eqnFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, sourcesPointer, sourcePointer, eqnPointer
     !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubiosources%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)

        ! Get eqn pointer
        call jcore%get_child(sourcesPointer, eqName, eqnPointer, eqnFound)
        call raiseKeyErrorJSON(eqName, eqnFound, "settings/volumeSources")

        ! Get source pointer
        call jcore%get_child(eqnPointer, this%sourceName, sourcePointer, sourceFound)
        call raiseKeyErrorJSON(this%sourceName, sourceFound, "settings/volumeSources")

        call jcore%get(sourcePointer, 'rho0', this%rho0, found)
        call raiseKeyErrorJSON('rho0', found, "settings/volumeSources")

        call jcore%get(sourcePointer, 'g', this%g, found)
        call raiseKeyErrorJSON('g', found, "settings/volumeSources")

        call jcore%get(sourcePointer, 'alpha', this%alpha, found)
        call raiseKeyErrorJSON('alpha', found, "settings/volumeSources")

        call jcore%get(sourcePointer, 'T0', this%T0, found)
        call raiseKeyErrorJSON('T0', found, "settings/volumeSources")

        ! Clean up
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(sourceFound) nullify(sourcePointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setBoussinesqSourceTerm

!**********************************************************************************************************************

    subroutine computeBoussinesqSourceTerm(this, aC, bC)

        use fieldvar, only: temperature

        class(boussinesqSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff(3)

        real :: aC(numberOfElements, 3), bC(numberOfElements, 3)
    !------------------------------------------------------------------------------------------------------------------

        aC = 0.0

        coeff = this%rho0*this%alpha*this%g
     
        do iElement=1,numberOfElements
            bC(iElement,:) = -coeff*(temperature%phi(iElement,1)-this%T0)*mesh%volume(iElement)
        end do

    end subroutine computeBoussinesqSourceTerm

!**********************************************************************************************************************

    subroutine setBouyancySourceTerm(this, eqName)

        class(bouyancySourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, sourcesFound, sourceFound, eqnFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, sourcesPointer, sourcePointer, eqnPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubiosources%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)

        ! Get eqn pointer
        call jcore%get_child(sourcesPointer, eqName, eqnPointer, eqnFound)
        call raiseKeyErrorJSON(eqName, eqnFound, "settings/volumeSources")

        ! Get source pointer
        call jcore%get_child(eqnPointer, this%sourceName, sourcePointer, sourceFound)
        call raiseKeyErrorJSON(this%sourceName, sourceFound, "settings/volumeSources")

        call jcore%get(sourcePointer, 'g', this%g, found)
        call raiseKeyErrorJSON('g', found, "settings/volumeSources")

        ! Clean up
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(sourceFound) nullify(sourcePointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setBouyancySourceTerm

!**********************************************************************************************************************

    subroutine computeBouyancySourceTerm(this, aC, bC)

        use physicalConstants, only : rho

        class(bouyancySourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: coeff(3)

        real :: aC(numberOfElements, 3), bC(numberOfElements, 3)
    !------------------------------------------------------------------------------------------------------------------

        aC = 0.0

        do iElement=1,numberOfElements
            bC(iElement,:) = -this%g*rho%phi(iElement,1)*mesh%volume(iElement)
        end do

    end subroutine computeBouyancySourceTerm

!**********************************************************************************************************************

    subroutine setPorousSourceTerm(this, eqName)

        class(porousSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: eqName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, sourcesFound, sourceFound, eqnFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, sourcesPointer, sourcePointer, eqnPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubiosources%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'volumeSources', sourcesPointer, sourcesFound)

        ! Get eqn pointer
        call jcore%get_child(sourcesPointer, eqName, eqnPointer, eqnFound)
        call raiseKeyErrorJSON(eqName, eqnFound, "settings/volumeSources")

        ! Get source pointer
        call jcore%get_child(eqnPointer, this%sourceName, sourcePointer, sourceFound)
        call raiseKeyErrorJSON(this%sourceName, sourceFound, "settings/volumeSources")

        call jcore%get(sourcePointer, 'applyTo', this%regionName, found)
        call raiseKeyErrorJSON('applyTo', found, "settings/volumeSources")

        call jcore%get(sourcePointer, 'D', this%D, found)
        call raiseKeyErrorJSON('D', found, "settings/volumeSources")

        call jcore%get(sourcePointer, 'F', this%F, found)
        call raiseKeyErrorJSON('F', found, "settings/volumeSources")

        ! Clean up
        nullify(dictPointer)
        if(sourcesFound) nullify(sourcesPointer)
        if(sourceFound) nullify(sourcePointer)
        if(eqnFound) nullify(eqnPointer)
        call jcore%destroy()

    end subroutine setPorousSourceTerm

!**********************************************************************************************************************

    subroutine computePorousSourceTerm(this, aC, bC)

        use fieldvar, only : velocity
        use physicalConstants, only : rho, nu

        class(porousSourceTerm) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: darcyTerm(3)

        real :: forchheimerTerm(3)

        real :: ukk

        real :: aC(numberOfElements, 3), bC(numberOfElements, 3)
    !------------------------------------------------------------------------------------------------------------------

        call flubioStopMsg('ERROR: Sorry, I need to finish the implementation for this paticular source term')

        aC = 0.0

        ! -mu*(Dij*uj)+ 0.5*rho*(Fij*uj)*abs(ukk)

        do iElement=1,numberOfElements

            ! Darcy part
            darcyTerm(1) = nu%phi(iElement,1)*(this%D(1)*velocity%phi(iElement,1) + &
                    this%D(2)*velocity%phi(iElement,3) + &
                    this%D(3)*velocity%phi(iElement,3))

            darcyTerm(2) = nu%phi(iElement,1)*(this%D(4)*velocity%phi(iElement,1) + &
                    this%D(5)*velocity%phi(iElement,3) + &
                    this%D(6)*velocity%phi(iElement,3))

            darcyTerm(3) = nu%phi(iElement,1)*(this%D(7)*velocity%phi(iElement,1) + &
                    this%D(8)*velocity%phi(iElement,3) + &
                    this%D(9)*velocity%phi(iElement,3))

            ! forchheimer part
            ukk = abs(velocity%phi(iElement,1) + velocity%phi(iElement,2) + velocity%phi(iElement,3))

            forchheimerTerm(1) = 0.5*rho%phi(iElement,1)*ukk*(this%F(1)*velocity%phi(iElement,1) + &
                    this%F(2)*velocity%phi(iElement,3) + &
                    this%F(3)*velocity%phi(iElement,3))

            forchheimerTerm(2) = 0.5*rho%phi(iElement,1)*ukk*(this%F(4)*velocity%phi(iElement,1) + &
                    this%F(5)*velocity%phi(iElement,3) + &
                    this%F(6)*velocity%phi(iElement,3))

            forchheimerTerm(3) = 0.5*rho%phi(iElement,1)*ukk*(this%F(7)*velocity%phi(iElement,1) + &
                    this%F(8)*velocity%phi(iElement,3) + &
                    this%F(9)*velocity%phi(iElement,3))

            ! Assmeble all
            bC(iElement,:) = (darcyTerm + forchheimerTerm)*mesh%volume(iElement)

        end do

    end subroutine computePorousSourceTerm

end module sourceTerms
