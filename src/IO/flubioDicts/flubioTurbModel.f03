!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module turbModel

!==================================================================================================================
! Description:
!! This module implements the dictionary storing the entries of settings/turbModel and associated flags.
!==================================================================================================================

    use generalDictionaries
    use globalTimeVar

    implicit none

    type, extends(generalDict) :: turbModelDict

        character(len=:), allocatable :: modelName
        !! name of the model

        character(len=:), allocatable :: boundType
        !! field bounding method

        integer :: lesModel
        !! falg for les models

        integer :: ransModel
        !! flag for rans models

        integer :: productionType
        !! turbulent production calcualtion method

        integer :: curvatureCorr
        !! curvature correction flag

        integer :: wallFunctionBlending
        !! wall function blending between viscous and logarithmic layers

        integer :: wallDistCorr
        !! number of wall distance non-orthogonal corrections

        integer :: bounded
        !! flag to active bounding procedure

        integer :: frictionVelocityMethod
        !! Friction velocity method

        integer :: outputYPlus
        !! flag to output yplus

        real :: tke_amb
        !! free stream tdr

        real :: tdr_amb
        !! free stream tdr

        logical :: sustTerm
        !! Flag to add ambient turbulence

    contains
        procedure :: set => setTurbModel

    end type turbModelDict

contains

    subroutine setTurbModel(this)

    !==================================================================================================================
    ! Description:
    !! setTurbModel sets the turbulence model type.
    !==================================================================================================================

        class(turbModelDict) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! dictionary name

        character(len=:), allocatable :: optName
        !! option name

        character(len=:), allocatable :: val, val2
        !! value
    !------------------------------------------------------------------------------------------------------------------

        logical :: found, found2
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/turbulenceModel'

        ! Initialise JSON
        call this%initJSON(dictName)

        ! Check if the keyword exists and set its value

        call this%json%get('ModelName', val, found)
        this%modelName = lowercase(val)

        if(trim(this%modelName) == 'smagorinsky') then

            this%lesModel = 0
            this%ransModel = -1

        elseif(trim(this%modelName) == 'wale') then

            this%lesModel = 2
            this%ransModel = -1

        elseif(trim(this%modelName) == 'kappaomega') then

            this%lesModel = -1
            this%ransModel = 0

        elseif(trim(this%modelName) == 'kappaomegabsl') then

            this%lesModel = -1
            this%ransModel = 1

        elseif(trim(this%modelName) == 'kappaomegasst') then

            this%lesModel = -1
            this%ransModel = 2

        elseif(trim(this%modelName) =='kappaepsilon') then

            this%lesModel = -1
            this%ransModel = 3

        elseif(trim(this%modelName) =='spalartallmaras') then

            this%lesModel = -1
            this%ransModel = 7

        elseif(trim(this%modelName) =='qzeta') then

            this%lesModel = - 1
            this%ransModel = 8

        elseif(trim(this%modelName)=='dns') then

            this%lesModel = -1
            this%ransModel = -1
            this%modelName = 'dns'

        else

            call flubioStopMsg('FLUBIO: unknown turbulence model '// val //'.')

        endif

        ! Skip these controls definition if DNS
        if(this%modelName/='dns') then

            ! Friction velocity method
            call this%json%get('FrictionVelocityFrom', val, found)
            if(lowercase(val)=='k') then
                this%frictionVelocityMethod = 1
            else
                this%frictionVelocityMethod = 0
            end if

            ! Turbulent production type
            call this%json%get('TurbulentProduction', val, found)
            if(lowercase(val)=='standard') then
                this%productionType = 0
            elseif(lowercase(val)=='incompressible') then
                    this%productionType = 1
            elseif(lowercase(val)=='vorticity') then
                this%productionType = 2
            else
                this%productionType = 0
            end if

            ! Wall function blending
            call this%json%get('Blending', val, found)
            if(lowercase(val)=='standard') then
                this%wallFunctionBlending = 0
            elseif(lowercase(val)=='stepwise') then
                this%wallFunctionBlending = 1
            elseif(lowercase(val)=='max') then
                this%wallFunctionBlending = 2
            elseif(lowercase(val)=='binomial') then
                this%wallFunctionBlending = 3
            elseif(lowercase(val)=='exponential') then
                this%wallFunctionBlending = 4
            elseif(lowercase(val)=='tanh') then
                this%wallFunctionBlending = 5
            else
                this%wallFunctionBlending = 0
            end if

            ! Curvature correction
            call this%json%get('curvatureCorrection', val, found)
            if(lowercase(val)=='yes') then
                this%curvatureCorr = 1
            else
                this%curvatureCorr = 0
            end if

            ! Output yplus
            call this%json%get('WriteYPlus', val, found)
            if(lowercase(val)=='yes') then
                this%outputYPlus = 1
            else
                this%outputYPlus = 0
            end if

            ! Ambient turbulence
            call this%json%get('ControlledDecay%active', val, found)
            if(found) then
                this%sustTerm = .true.
            else
                this%sustTerm = .false.
            end if

            ! Wall Distance corrections
            call this%json%get('WallDistCorr', this%wallDistCorr, found)
            if(.not. found) call raiseKeyErrorJson('WallDistCorr', found, dictName)

            ! Bounded quantitites
            call this%json%get('BoundFields%active', val, found)
            if(lowercase(val)=='yes' .or. lowercase(val)=='on') then
                this%bounded = 1
                call this%json%get('BoundFields%type', val2, found2)
                if(.not. found) call raiseKeyErrorJson('BoundFields%type', found2, dictName)
                this%boundType = trim(val2)
            else
                this%bounded = 0
            end if

        end if

    end subroutine setTurbModel

end module turbModel