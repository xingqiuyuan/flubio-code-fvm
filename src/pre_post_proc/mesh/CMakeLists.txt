# Library name
set(TARGET serialmesh)

# Collect files
file(GLOB src "*.f03")
set_source_files_properties(${src} PROPERTIES LANGUAGE Fortran)

# Add the library

add_library(${TARGET} ${src})
set_target_properties(${TARGET} PROPERTIES COMPILE_FLAGS ${CMAKE_Fortran_FLAGS})
target_include_directories(${TARGET} PRIVATE "${CMAKE_Fortran_MODULE_DIRECTORY}")
target_link_libraries(${TARGET} PRIVATE globalvar userdefinedtypes flubiompi commonutils orderpack strings math)
