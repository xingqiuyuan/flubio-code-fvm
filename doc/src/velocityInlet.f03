!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine velocityInletBoundary(iBFace, aC, bC, phib, mu, c)

	!==================================================================================================================
	! Description:
	!! velocityInletBoundary computes computes the diagonal coefficient and rhs correction when an inlet (specified velocity)
	!!	boundary condition is applied.
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &textbf{a}_C^b = \mu_b\frac{|S_b|}{d_{\perp}}(1-\textbf{n}_b^2) \\
	!!    &textbf{b}_C^b = -m_f\textbf{u}_b + \mu_b\frac{|S_b|}{d_{\perp}}\textbf{u}_b
	!! \end{eqnarray}
	!==================================================================================================================

		! modules
		use meshvar
		use massFlowVar

		! arguments
		implicit none

		integer :: iBFace
		!! target boundary face
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(3)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(3)
		!! right hand side corretion due to the velocity inlet boundary condition

		real :: phib(3)
		!! velocity value prescribed at the target boundary

		real :: nf(3)
		!! surface normal at a target boundary face

		real :: mu
		!! dinamic viscosity at a target boundary face

		real :: c

		real :: corr(3)
		!! auxliary variable used to hos the full correction (convective + diffusive term)
	!------------------------------------------------------------------------------------------------------------------

	! Get surface normal
		nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)

		corr = -c*mf(iBFace,1) + mu*mesh%gDiff(iBFace)*(1-nf**2)
		aC =  mu*mesh%gDiff(iBFace)*(1-nf**2)
		bC = corr*phib

!		corr=-mf(iBFace,1) + mu*mesh%gDiff(iBFace)
!		aC =  mu*mesh%gDiff(iBFace) + mf(iBFace,1)
!		bC = corr*phib

	end subroutine velocityInletBoundary

! *********************************************************************************************************************

	subroutine updateVelocityInletBoundaryField(phi, phi0, iBoundary, fComp)

	!==================================================================================================================
	! Description:
	!! updateVelocityInletBoundaryField loops over all the boundary faces of a target patch
	!! and updates the field face values with the inlet one:
	!! \begin{equation*}
	!!    \textbf{u}_b = \textbf{u}_{specified} \\
	!! \end{equation*}
	!==================================================================================================================

		! modules
		use meshvar

		! arguments
		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: iBFace
		!! target boundary face

		integer :: is
		!! start face of the target boundary

		integer :: ie
		!! end face of the target boundary

		integer :: iOwner
		!! owner of the target boundary face

		integer ::fComp
		!! number of field components

		integer :: patchFace
		!! auxiliary loop index
	!------------------------------------------------------------------------------------------------------------------
	
		real :: phi(numberOfElements+numberOfBFaces, fComp)
		!! field to be updated

		real :: phi0(fComp)
		!! value to be assigned at the target boundary
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace=is-numberOfIntFaces

		do iBFace=is,ie
			patchFace=patchFace+1
			phi(patchFace,:)=phi0
		enddo

	end subroutine updateVelocityInletBoundaryField

! *********************************************************************************************************************

	subroutine updateCustomInletBoundaryField(phi, iBoundary, expr, fComp)

	!==================================================================================================================
	! Description:
	!! updateVelocityInletBoundaryField is the same as updateDiricheletBoundaryField, but the boundary expression can be parsed
	!! from the boundary codition file, allowing for custom expressions.
	!==================================================================================================================

		use meshvar
		use globalTimeVar

		implicit none

		character(len = 10), dimension(4) :: variables
		!! variables in string

		character(*), dimension(fComp) :: expr
		!! expression passed to the interpreter

		character(len=500) :: func
		!! expression parsed from the boundary condition file
	!------------------------------------------------------------------------------------------------------------------

		integer :: iBoundary, is, ie, iOwner, patchFace

		integer :: i
		!! loop index

		integer :: iBFace
		!! boundary face loop index

		integer:: iComp
		!! taget components

		integer :: fComp
		!! number of field components
	!------------------------------------------------------------------------------------------------------------------

		real :: phi(numberOfElements+numberOfBFaces, fComp), pi
	!------------------------------------------------------------------------------------------------------------------

		real :: variablesValues(4)
		!! name of the variables to be interpreted

		real :: val
		!! auxiliary number

		real :: getBoundaryFieldValue
		!! function to get the interpreted fiedl value
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace=numberOfElements + (is-numberOfIntFaces-1)

		variables(1) = 'x'
		variables(2) = 'y'
		variables(3) = 'z'
		variables(4) = 't'

		do iComp=1,fComp

			do iBFace=is,ie

				patchFace=patchFace+1

				! variables
				variablesValues(1) = mesh%fcentroid(iBFace,1) ! x
				variablesValues(2) = mesh%fcentroid(iBFace,2) ! y
				variablesValues(3) = mesh%fcentroid(iBFace,3) ! z
				variablesValues(4) = time ! t

				! Set the your boundary field
				val = getBoundaryFieldValue(expr(iComp), variables, variablesValues)
				phi(patchFace, iComp) = val

			enddo

		enddo

	end subroutine updateCustomInletBoundaryField

! *********************************************************************************************************************

	subroutine velocityInletBoundaryDiffusionOnly(iBFace, aC, bC, phib, mu)

	!==================================================================================================================
	! Description:
	!! velocityInletBoundary computes computes the diagonal coefficient and rhs correction when an inlet (specified velocity)
	!!	boundary condition is applied.
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &textbf{a}_C^b = \mu_b\frac{|S_b|}{d_{\perp}}(1-\textbf{n}_b^2) \\
	!!    &textbf{b}_C^b = -m_f\textbf{u}_b + \mu_b\frac{|S_b|}{d_{\perp}}\textbf{u}_b
	!! \end{eqnarray}
	!==================================================================================================================

		! modules
		use meshvar
		use massFlowVar

		! arguments
		implicit none

		integer :: iBFace
		!! target boundary face
		!------------------------------------------------------------------------------------------------------------------

		real :: aC(3)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(3)
		!! right hand side corretion due to the velocity inlet boundary condition

		real :: phib(3)
		!! velocity value prescribed at the target boundary

		real :: nf(3)
		!! surface normal at a target boundary face

		real :: mu
		!! dinamic viscosity at a target boundary face

		real :: corr(3)
		!! auxliary variable used to hos the full correction (convective + diffusive term)
	!------------------------------------------------------------------------------------------------------------------

		! Get surface normal
		nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)

		corr = mu*mesh%gDiff(iBFace)*(1-nf**2)
		aC =  mu*mesh%gDiff(iBFace)*(1-nf**2)
		bC = corr*phib

	end subroutine velocityInletBoundaryDiffusionOnly

! *********************************************************************************************************************
