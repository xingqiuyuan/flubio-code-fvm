!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine updateOuletVelocityBoundaryField(phi, phiGrad, iBoundary)

	!==================================================================================================================
	! Description:
	!! updateOuletVelocityBoundaryField loops over all the boundary faces of a target patch
	!!	and updates the field face values by extrapolation from interior:
	!! \begin{equation*}
	!!    \textbf{u}_b = \textbf{u}_{C} + \nabla\textbf{u}_{b}d_{Cb} \\
	!! \end{equation*}
	!==================================================================================================================

		use meshvar

		implicit none

		integer iBoundary, iBFace, is, ie, iOwner, fComp, patchFace
	!------------------------------------------------------------------------------------------------------------------
	
		real dCb(3), dot, order

		real :: phi(numberOfElements+numberOfBFaces, 3)

		real :: phiGrad(numberOfElements+numberOfBFaces, 3, 3)
	!------------------------------------------------------------------------------------------------------------------

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1
		patchFace = numberOfElements+(is-numberOfIntFaces-1)

		order = 0.0

		do iBFace=is,ie

		   patchFace = patchFace+1
		   iOwner = mesh%owner(iBFace)
		   dCb = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)

		   ! u
		   dot=0.0
		   dot = dot_product(phiGrad(patchFace,:,1),dCb)
		   phi(patchFace,1) = phi(iOwner,1)+order*dot

		   ! v
		   dot = 0.0
		   dot = dot_product(phiGrad(patchFace,:,2),dCb)
		   phi(patchFace,2) = phi(iOwner,2)+order*dot

		   ! w
		   dot = 0.0
		   dot=dot_product(phiGrad(patchFace,:,3),dCb)
		   phi(patchFace,3) = phi(iOwner,3)+order*dot

		enddo

	end subroutine updateOuletVelocityBoundaryField

! *********************************************************************************************************************
