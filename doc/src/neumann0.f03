!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine neumann0Boundary(iBFace, aC, bC, c, fComp)

	!===================================================================================================================
	! Description:
	!! neumann0Boundary computes the diagonal coefficient and rhs correction when a zero gradient boundary condition is applied.
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &a_C^b = m_f \\
	!!    &b_C^b = 0
	!! \end{eqnarray}
	!===================================================================================================================

		! modules
		use meshvar
		use massFlowVar

		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: fComp
		!! number of field components

		integer :: iBFace
		!! Target face

		real :: c
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(3)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(3)
		!! right hand side corretion due to the velocity inlet boundary condition

	!------------------------------------------------------------------------------------------------------------------

		! Apply zero gradient at outlet, only convective term survives
		aC = c*mf(iBFace,1)
		bC = 0.0

	end subroutine neumann0Boundary

! *********************************************************************************************************************

	subroutine neumann0BoundaryFf(iBFace, aC, bC, rhof, c, fComp)

	!===================================================================================================================
	! Description:
	!! neumann0BoundaryFf cis the same as zeroGradietn but uses velocity face flux instead of the mass flow rate $m_f$.
	!===================================================================================================================

		! modules
		use meshvar
		use massFlowVar

		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: fComp
		!! number of field components

		integer :: iBFace
		!! Target face

		real :: rhof
		!! density at face

		real :: c
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(3)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(3)
		!! right hand side corretion due to the velocity inlet boundary condition

	!------------------------------------------------------------------------------------------------------------------

		! Apply zero gradient at outlet, only convective term survives
		aC = c*mf(iBFace,1)/rhof
		bC = 0.0

	end subroutine neumann0BoundaryFf

! *********************************************************************************************************************

	subroutine updateneumann0BoundaryField(phi, iBoundary, fComp)

	!==================================================================================================================
	! Description:
	!! updateneumann0BoundaryField loops over all the boundary faces of a target patch
	!! and updates the field face values with the cell center ones:.
	!! \begin{equation*}
	!!    \phi_b = \phi_C \\
	!! \end{equation*}
	!==================================================================================================================

		! modules
		use meshvar

		! arguments
		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: iBFace
		!! target boundary face

		integer :: is
		!! start face of the target boundary

		integer :: ie
		!! end face of the target boundary

		integer :: iOwner
		!! owner of the target boundary face

		integer ::fComp
		!! number of field components

		integer :: patchFace
		!! auxiliary loop index

!------------------------------------------------------------------------------------------------------------------

		real phi(numberOfElements+numberOfBFaces, fComp)
		!! field to be updated

!------------------------------------------------------------------------------------------------------------------

	is = mesh%boundaries%startFace(iBoundary)
	ie = is+mesh%boundaries%nFace(iBoundary)-1
	patchFace=numberOfElements + (is-numberOfIntFaces-1)

	do iBFace=is,ie
	   patchFace=patchFace+1
	   iOwner = mesh%owner(iBFace)
	   phi(patchFace,:)=phi(iOwner,:)
 	enddo

	end subroutine updateneumann0BoundaryField

! *********************************************************************************************************************

	subroutine neumann0BoundaryDiffusionOnly(iBFace, aC, bC, fComp)

	!===================================================================================================================
	! Description:
	!! neumann0Boundary computes the diagonal coefficient and rhs correction when a zero gradient boundary condition is applied.
	!! The correction reads:
	!! \begin{eqnarray}
	!!    &a_C^b = m_f \\
	!!    &b_C^b = 0
	!! \end{eqnarray}
	!===================================================================================================================

		! modules
		use meshvar
		use massFlowVar

		implicit none

		integer :: iBoundary
		!! target boundary

		integer :: fComp
		!! number of field components

		integer :: iBFace
		!! Target face
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(3)
		!! diagonal coefficient corretion due to the velocity inlet boundary condition at the target boundary face owner

		real :: bC(3)
		!! right hand side corretion due to the velocity inlet boundary condition
	!------------------------------------------------------------------------------------------------------------------

		! Apply zero gradient at outlet, only convective term survives
		aC = 0.0
		bC = 0.0

	end subroutine neumann0BoundaryDiffusionOnly

! *********************************************************************************************************************
