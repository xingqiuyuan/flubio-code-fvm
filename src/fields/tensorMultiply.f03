module tensorMultiply

    use flubioTensors

    implicit none

    interface operator (*)
        module procedure :: dot01
        module procedure :: dot10
        module procedure :: dot02
        module procedure :: dot02s
        module procedure :: dot20
        module procedure :: dot20s
        module procedure :: dot04
        module procedure :: dot04s
        module procedure :: dot40
        module procedure :: dot40s
        module procedure :: dot11
        module procedure :: dot12
        module procedure :: dot21
        module procedure :: dot2s1
        module procedure :: dot12s
        module procedure :: dot22
        module procedure :: dot2s2s
    end interface operator (*)

    interface operator (/)
        module procedure :: div01
        module procedure :: div10
        module procedure :: div02
        module procedure :: div02s
        module procedure :: div20
        module procedure :: div20s
        module procedure :: div04
        module procedure :: div04s
        module procedure :: div40
        module procedure :: div40s
        module procedure :: div11
        module procedure :: div12
        module procedure :: div21
        module procedure :: div2s1
        module procedure :: div12s
        module procedure :: div22
        module procedure :: div2s2s
    end interface operator (/)

    interface operator (**)
        module procedure ddot22
        module procedure ddot2s2s
        module procedure ddot24
        module procedure ddot2s4s
        module procedure ddot42
        module procedure ddot4s2s
        module procedure ddot44
        module procedure ddot4s4s
        module procedure pow2
        module procedure pow2s
    end interface operator (**)

contains 

!**********************************************************************************************************************

    function dot01(w, T) result(Tdot)

        type(ttbTensor1), intent(IN) :: T
        
        type(ttbTensor1) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot01

!**********************************************************************************************************************

    function dot10(T, w) result(Tdot)

        type(ttbTensor1), intent(IN) :: T
        
        type(ttbTensor1) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot10

!**********************************************************************************************************************

    function dot02(w, T) result(Tdot)

        type(ttbTensor2), intent(IN) :: T
        
        type(ttbTensor2) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot02

!**********************************************************************************************************************

    function dot02s(w, T) result(Tdot)

        type(ttbTensor2s), intent(IN) :: T
        
        type(ttbTensor2s) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot02s

!**********************************************************************************************************************

    function dot20(T, w) result(Tdot)

        type(ttbTensor2), intent(IN) :: T
        
        type(ttbTensor2) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot20

!**********************************************************************************************************************

    function dot20s(T, w) result(Tdot)

        type(ttbTensor2s), intent(IN) :: T
        
        type(ttbTensor2s) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot20s

!**********************************************************************************************************************

    function dot04(w, T) result(Tdot)

        type(ttbTensor4), intent(IN) :: T
        
        type(ttbTensor4) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot04

!**********************************************************************************************************************

    function dot04s(w, T) result(Tdot)

        type(ttbTensor4s), intent(IN) :: T
        
        type(ttbTensor4s) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot04s

!**********************************************************************************************************************

    function dot40(T, w) result(Tdot)

        type(ttbTensor4), intent(IN) :: T
        
        type(ttbTensor4) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot40

!**********************************************************************************************************************

    function dot40s(T, w) result(Tdot)

        type(ttbTensor4s), intent(IN) :: T
        
        type(ttbTensor4s) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T%dim)

        do iElement=1,T%dim 
            Tdot%T(iElement) = w * T%T(iElement) 
        enddo

    end function dot40s

!**********************************************************************************************************************

    function dot11(T1, T2) result(Tdot)

        type(ttbTensor1), intent(IN) :: T1, T2
        
        type(ttbTensor1) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T1%dim)

        do iElement=1,T1%dim 
            Tdot%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function dot11

!**********************************************************************************************************************

    function dot22(T1, T2) result(Tdot)

        type(ttbTensor2), intent(IN) :: T1, T2
        
        type(ttbTensor2) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T1%dim)

        do iElement=1,T1%dim 
            Tdot%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function dot22

!**********************************************************************************************************************

    function dot2s2s(T1, T2) result(Tdot)

        type(ttbTensor2s), intent(IN) :: T1, T2
        
        type(ttbTensor2s) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T1%dim)

        do iElement=1,T1%dim 
            Tdot%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function dot2s2s

!**********************************************************************************************************************

    function dot12(T1, T2) result(Tdot)

        type(ttbTensor1), intent(IN) :: T1

        type(ttbTensor2), intent(IN) ::  T2
        
        type(ttbTensor1) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T1%dim)

        do iElement=1,T1%dim 
            Tdot%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function dot12

!**********************************************************************************************************************

    function dot21(T1, T2) result(Tdot)

        type(ttbTensor2), intent(IN) :: T1

        type(ttbTensor1), intent(IN) ::T2
        
        type(ttbTensor1) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T1%dim)

        do iElement=1,T1%dim 
            Tdot%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function dot21

!**********************************************************************************************************************

    function dot12s(T1, T2) result(Tdot)

        type(ttbTensor1), intent(IN) :: T1

        type(ttbTensor2s), intent(IN) ::T2
        
        type(ttbTensor1) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T1%dim)

        do iElement=1,T1%dim 
            Tdot%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function dot12s

!**********************************************************************************************************************

    function dot2s1(T1, T2) result(Tdot)

        type(ttbTensor2s), intent(IN) :: T1

        type(ttbTensor1), intent(IN) ::T2
        
        type(ttbTensor1) :: Tdot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdot%create(T1%dim)

        do iElement=1,T1%dim 
            Tdot%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function dot2s1

!**********************************************************************************************************************

    function div01(w, T) result(Tdiv)

        type(ttbTensor1), intent(IN) :: T
        
        type(ttbTensor1) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div01

!**********************************************************************************************************************

    function div10(T, w) result(Tdiv)

        type(ttbTensor1), intent(IN) :: T
        
        type(ttbTensor1) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div10

!**********************************************************************************************************************

    function div02(w, T) result(Tdiv)

        type(ttbTensor2), intent(IN) :: T
        
        type(ttbTensor2) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div02

!**********************************************************************************************************************

    function div02s(w, T) result(Tdiv)

        type(ttbTensor2s), intent(IN) :: T
        
        type(ttbTensor2s) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div02s

!**********************************************************************************************************************

    function div20(T, w) result(Tdiv)

        type(ttbTensor2), intent(IN) :: T
        
        type(ttbTensor2) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div20

!**********************************************************************************************************************

    function div20s(T, w) result(Tdiv)

        type(ttbTensor2s), intent(IN) :: T
        
        type(ttbTensor2s) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div20s

!**********************************************************************************************************************

    function div04(w, T) result(Tdiv)

        type(ttbTensor4), intent(IN) :: T
        
        type(ttbTensor4) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div04

!**********************************************************************************************************************

    function div04s(w, T) result(Tdiv)

        type(ttbTensor4s), intent(IN) :: T
        
        type(ttbTensor4s) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div04s

!**********************************************************************************************************************

    function div40(T, w) result(Tdiv)

        type(ttbTensor4), intent(IN) :: T
        
        type(ttbTensor4) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div40

!**********************************************************************************************************************

    function div40s(T, w) result(Tdiv)

        type(ttbTensor4s), intent(IN) :: T
        
        type(ttbTensor4s) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        real, intent(IN) :: w

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T%dim)

        do iElement=1,T%dim 
            Tdiv%T(iElement) = w * T%T(iElement) 
        enddo

    end function div40s

!**********************************************************************************************************************

    function div11(T1, T2) result(Tdiv)

        type(ttbTensor1), intent(IN) :: T1, T2
        
        type(ttbTensor1) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T1%dim)

        do iElement=1,T1%dim 
            Tdiv%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function div11

!**********************************************************************************************************************

    function div22(T1, T2) result(Tdiv)

        type(ttbTensor2), intent(IN) :: T1, T2
        
        type(ttbTensor2) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T1%dim)

        do iElement=1,T1%dim 
            Tdiv%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function div22

!**********************************************************************************************************************

    function div2s2s(T1, T2) result(Tdiv)

        type(ttbTensor2s), intent(IN) :: T1, T2
        
        type(ttbTensor2s) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T1%dim)

        do iElement=1,T1%dim 
            Tdiv%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function div2s2s

!**********************************************************************************************************************

    function div12(T1, T2) result(Tdiv)

        type(ttbTensor1), intent(IN) :: T1

        type(ttbTensor2), intent(IN) ::  T2
        
        type(ttbTensor1) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T1%dim)

        do iElement=1,T1%dim 
            Tdiv%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function div12

!**********************************************************************************************************************

    function div21(T1, T2) result(Tdiv)

        type(ttbTensor2), intent(IN) :: T1

        type(ttbTensor1), intent(IN) ::T2
        
        type(ttbTensor1) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T1%dim)

        do iElement=1,T1%dim 
            Tdiv%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function div21

!**********************************************************************************************************************

    function div12s(T1, T2) result(Tdiv)

        type(ttbTensor1), intent(IN) :: T1

        type(ttbTensor2s), intent(IN) ::T2
        
        type(ttbTensor1) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T1%dim)

        do iElement=1,T1%dim 
            Tdiv%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function div12s

!**********************************************************************************************************************

    function div2s1(T1, T2) result(Tdiv)

        type(ttbTensor2s), intent(IN) :: T1

        type(ttbTensor1), intent(IN) ::T2
        
        type(ttbTensor1) :: Tdiv
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tdiv%create(T1%dim)

        do iElement=1,T1%dim 
            Tdiv%T(iElement) = T1%T(iElement) * T2%T(iElement)
        enddo

    end function div2s1

!**********************************************************************************************************************

    function ddot22(T1, T2) result(Tddot)

        type(ttbTensor2), intent(IN) :: T1

        type(ttbTensor2), intent(IN) ::T2
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: Tddot(T1%dim)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T1%dim 
            Tddot(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot22

!**********************************************************************************************************************

    function ddot2s2s(T1, T2) result(Tddot)

        type(ttbTensor2s), intent(IN) :: T1

        type(ttbTensor2s), intent(IN) ::T2
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: Tddot(T1%dim)
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T1%dim 
            Tddot(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot2s2s

!**********************************************************************************************************************

    function ddot24(T1, T2) result(Tddot)

        type(ttbTensor2), intent(IN) :: T1

        type(ttbTensor4), intent(IN) ::T2

        type(ttbTensor2) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tddot%create(T1%dim)

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot24

!**********************************************************************************************************************

    function ddot2s4s(T1, T2) result(Tddot)

        type(ttbTensor2s), intent(IN) :: T1

        type(ttbTensor4s), intent(IN) ::T2

        type(ttbTensor2s) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tddot%create(T1%dim)

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot2s4s

!**********************************************************************************************************************

    function ddot42(T1, T2) result(Tddot)

        type(ttbTensor4), intent(IN) :: T1

        type(ttbTensor2), intent(IN) ::T2

        type(ttbTensor2) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tddot%create(T1%dim)

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot42

!**********************************************************************************************************************

    function ddot4s2s(T1, T2) result(Tddot)

        type(ttbTensor4s), intent(IN) :: T1

        type(ttbTensor2s), intent(IN) ::T2

        type(ttbTensor2s) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        call Tddot%create(T1%dim)

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot4s2s

!**********************************************************************************************************************

    function ddot44(T1, T2) result(Tddot)

        type(ttbTensor4), intent(IN) :: T1

        type(ttbTensor4), intent(IN) ::T2

        type(ttbTensor4) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot44

!**********************************************************************************************************************

    function ddot4s4s(T1, T2) result(Tddot)

        type(ttbTensor4s), intent(IN) :: T1

        type(ttbTensor4s), intent(IN) ::T2

        type(ttbTensor4s) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** T2%T(iElement)
        enddo

    end function ddot4s4s

!**********************************************************************************************************************

    function pow2(T1, i) result(Tddot)

        type(ttbTensor2), intent(IN) :: T1

        type(ttbTensor2s) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer, intent(IN) :: i

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** i
        enddo

    end function pow2

!**********************************************************************************************************************

    function pow2s(T1, i) result(Tddot)

        type(ttbTensor2s), intent(IN) :: T1

        type(ttbTensor2s) :: Tddot
    !------------------------------------------------------------------------------------------------------------------

        integer, intent(IN) :: i
        
        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        do iElement=1,T1%dim 
            Tddot%T(iElement) = T1%T(iElement) ** i
        enddo

    end function pow2s

!**********************************************************************************************************************

end module tensorMultiply