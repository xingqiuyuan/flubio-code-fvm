!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	program flubio_fieldss2p

		use flubioMpi, only: flubioMsg
		use serialmeshvar, only: mesh
		
		implicit none

		character(len=100) :: fields
		!! field to distribute

		character(len=10), dimension(:), allocatable :: args
		!! command line arguments
	!------------------------------------------------------------------------------------------------------------------

		integer :: targetTime, nargs, nranks
	!------------------------------------------------------------------------------------------------------------------

		nargs = command_argument_count()

		if(nargs>0) then
			allocate(args(nargs))
			call fields2pParsing(args, nargs, nranks, targetTime, fields)
		else
			call flubioMsg('ERROR: no command line option has been provided, please run with -help for help.')
			stop
		endif

		!------------------!
		! read serial mesh !
		!------------------!

		call mesh%readMeshFromFiles()

		call fields2p(nranks, targetTime, fields)

	end program flubio_fieldss2p

! *********************************************************************************************************************

	subroutine fields2p(nranks, targetTime, fields)

		use serialmeshvar
		use serialVTKFormat, only: getFieldList, getNumberOfBoundaries, globalConnPre, getParallelMeshSizes

		implicit none

		character(len=*) :: fields
		!! fields to process coming as a comma separated string

		character(len=10) :: procID
		!! processor iRank

		character(len=10) :: timeStamp	
		!! time to process as string

		character(len=15) :: fieldName
		!! field name as string

		character(len=:), allocatable :: fileDir
		!! file directory
		
		character(len=:), allocatable :: postDir
		!! post processing directory

		character(len=10), dimension(:), allocatable :: fieldList
		!! list of fields to process

		character(len=:), allocatable :: file_name
		!! file name
	!------------------------------------------------------------------------------------------------------------------

		integer :: np(nranks)
		!! number of points in each processor's domain

		integer :: nbf(nranks) 
		!! number of boundary faces in each processor's domain

		integer :: ne(nranks)
		!! number of elements in each processor's domain

		integer :: nf(nranks)
		!! number of faces in each processor's domain

		integer :: nif(nranks)
		!! number of internal faces in each processor's domain

		integer :: nbe(nranks)
		!! number of boundary elements in each processor's domain

		integer :: nb(nranks)
		!! number of boundaries

		integer :: nrb(nranks)
		!! number of non-processor boundaries (real boudnaries)

		integer, dimension(:,:), allocatable :: startFace
		!! start face of each boundary for each processor's domain
		
		integer, dimension(:,:), allocatable :: nFaces
	    !! start face of each boundary for each processor's domain

		integer :: targetTime
		!! tagert time to process

		integer :: i, ii, j, isb, ieb, iRank, iElement, iBoundary, iBFace, iComp, iField, patchFaceB, patchFaceE
		!! loop index variables
		
		integer :: nranks
		!! number of processors used in the simulation 

		integer :: ierr
		!! error flag

		integer :: nComp
		!! number of components

		integer :: nFields 
		!! number of fields

		integer, dimension(:), allocatable :: local2global
		!! mapping between local and global index for mesh elements

		integer, dimension(:), allocatable :: local2globalF
		!! mapping between local and global index for mesh faces
	!-------------------------------------------------------------------------------------------------------------------

		real, dimension(:,:), allocatable :: field
		!! serial field at the current time step

		real, dimension(:,:), allocatable :: field0
		!! serial field at the previous time step

		real, dimension(:,:), allocatable :: field00
		!! serial field two time steps old

		real, dimension(:,:), allocatable :: phi
		!! distributed field at the current time step

		real, dimension(:,:), allocatable :: phi0
		!! distributed field at the previous time step

		real, dimension(:,:), allocatable :: phi00
		!! distributed field two time steps old
	!------------------------------------------------------------------------------------------------------------------

		!---------!
		! Prepare !
		!---------!

		write(timeStamp,'(i0)') targetTime

		call getNumberOfBoundaries(nb, nrb, nranks)

		allocate(startFace(maxval(nb),nranks))
		allocate(nFaces(maxval(nb),nranks))
		startFace=-1
		nFaces=-1

		!------------------------------!
		! Read mesh and boundary files !
		!------------------------------!

		do ii=1,nranks
			iRank = ii-1
			write(procID,'(i0)') iRank
			call getParallelMeshSizes(procID, nb(ii), &
						              np(ii), nf(ii), nif(ii), nbf(ii), ne(ii), nbe(ii), &
									  startFace(1:nb(ii),ii), nFaces(1:nb(ii),ii))
		enddo
		
		! Find out the field to read 
		fieldList = getFieldList(fields)

		! Get the global cell numbering
		allocate(local2global(numberOfElements))
		call globalConnPre(local2global, numberOfElements, nranks)

		! Loop over fields
		do iField=1, nFields

			call flubioMsg('FLUBIO: distributing field: '//trim(fieldList(iField)))

			! read serial field Field
			postDir='postProc/fields/'
			write(timeStamp,'(i0)') targetTime
			fileDir = postDir//trim(timeStamp)//'/'
			open(1,file = fileDir//trim(fieldName)//'_serial.bin', form='unformatted')

			    read(1) nComp
				read(1) fieldName

				! allocate field to read
			    allocate(field(numberOfElements + numberOfBFaces, nComp))
			    allocate(field0(numberOfElements + numberOfBFaces, nComp))
			    allocate(field00(numberOfElements + numberOfBFaces, nComp))

				field = 0.0
				field0 = 0.0
				field00 = 0.0

				do iComp = 1,nComp
					read(1) field(:,iComp)
					read(1) field0(:,iComp)
					read(1) field00(:,iComp)
				end do
			close(1)

			! Loop over partitions
			patchFaceE=0
			do ii=1,nranks

				! Initialize arrays
				iRank=ii-1
				write(procID,'(i0)') iRank

				allocate(local2globalF(nf(ii)))
				call globalFaceConn(local2globalF, nf(ii), nf(ii), iRank)

				! allocate field matrix
				allocate(phi(ne(ii)+nbf(ii), nComp))
				allocate(phi0(ne(ii)+nbf(ii), nComp))
				allocate(phi00(ne(ii)+nbf(ii), nComp))

				phi = 0.0
				phi0 = 0.0
				phi00 = 0.0

				! decompose field
				do i=1,ne(ii)
					patchFaceE = patchFaceE+1
					j = local2global(patchFaceE)
					phi(i,:) = field(j,:)
					phi0(i,:) = field0(j,:)
					phi00(i,:) = field00(j,:)
				enddo

				! Boundaries
				patchFaceE = ne(ii)
				do iBoundary=1,nrb(ii)

					isb = startFace(iBoundary,ii)
					ieb = isb+nFaces(iBoundary,ii)-1

					do iBFace=isb,ieb
						patchFaceE = patchFaceE +1
						j = numberOfElements + local2globalF(iBFace) - numberOfIntFaces
						phi(patchFaceE,iField) = field(j,iField)
					enddo

				enddo

				file_name = 'postProc/fields/'//trim(timeStamp)//'/'//trim(fieldList(iField))//'-d'//trim(procID)//'.bin'
				open(1,file=file_name,form='unformatted')

					! Read fields for the i-th partion
					write(1) nComp
					write(1) fieldName
		
					do iComp=1,nComp
						write(1) phi(:,iComp)
						write(1) phi0(:,iComp)
						write(1) phi00(:,iComp)
					enddo

				close(1)

				! Deallocate and reallocate with a new size on the partition
				deallocate(phi)
				deallocate(phi0)
				deallocate(phi00)

				deallocate(local2globalF)

			! End of partion
			end do

			! write Field
			postDir='postProc/fields/'
			fileDir = postDir//trim(timeStamp)//'/'
			open(1,file = fileDir//trim(fieldName)//'_serial.bin', form='unformatted')
				write(1) nComp
				write(1) fieldName

				do iComp = 1,nComp
					write(1) field(:,iComp)
					write(1) field0(:,iComp)
					write(1) field00(:,iComp)
				end do
			close(1)

			! deallocate current field
			deallocate(field)
			deallocate(field0)
			deallocate(field00)

		! End of fields
		end do

	end subroutine fields2p

! *********************************************************************************************************************

	subroutine fields2pParsing(args, nargs, nranks, targetTime, fields)

		use auxilaries

		implicit none

		character(len=*) args(nargs)

		character(len=100) :: fields
	!------------------------------------------------------------------------------------------------------------------

		integer p, nargs, nranks, targetTime, stat
	!------------------------------------------------------------------------------------------------------------------

		! Get the arguments
		do p=1,nargs
		call get_command_argument(number=p, value=args(p), status=stat)
		enddo

		! Process the options
		do p=1,nargs

			! Look for the number of processors
			if(args(p)=='-np') then
				read(args(p+1),*) nranks
			elseif(args(p)=='-time') then
				read(args(p+1),*) targetTime
			elseif(args(p)=='-fields') then
				fields = args(p+1)
			elseif(args(p)=='-help') then
				write(*,*) 'Command line options:'
				write(*,*) '-np: number of processors to be used'
				write(*,*) '-time: restart file to process'
				stop
			endif

		enddo

		! Check for the mandatory arguments
		call check_np(args, nargs)
		!call check_field(args, nargs)

	end subroutine fields2pParsing

! *********************************************************************************************************************
    	     
