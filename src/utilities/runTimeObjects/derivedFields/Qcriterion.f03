module QField

    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: velocity
    use flubioMpi
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: Q

    contains
        procedure :: run =>  computeQ
    end type Q

contains

! *********************************************************************************************************************

    subroutine computeQ(this)

    !==================================================================================================================
    ! Description:
    !! computeQcriterion computes Q criterion as:
    !! \begin{equation*}
    !!  Q = 0.5*(|\Omega|^2 + |S|^2)
    !! \end{equation*}
    !==================================================================================================================

        class(Q):: this
	!------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: trGradU, omega(3), Qcrit
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then

            do i=1,numberOfElements+numberOfBFaces

		        trGradU = velocity%phiGrad(i,1,1) + velocity%phiGrad(i,3,2) + velocity%phiGrad(i,3,3)

		        omega(1) = velocity%phiGrad(i,1,2)*velocity%phiGrad(i,2,1)
		        omega(2) = velocity%phiGrad(i,1,3)*velocity%phiGrad(i,3,1)
		        omega(3) = velocity%phiGrad(i,3,2)*velocity%phiGrad(i,2,3)

		        Qcrit = 0.5*(trGradU**2 -omega(1)-omega(2)-omega(3))

		        this%taskField%phi(i,1) = Qcrit

            end do

            call this%write()

        endif

    end subroutine computeQ

!**********************************************************************************************************************

end module QField
