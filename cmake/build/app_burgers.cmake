
set( app "flubio_burgers" )

set( src ${flubio_src}/solvers/BURGERS.f03 )

include( ${cmake_local}/build/app_0_post.cmake )
