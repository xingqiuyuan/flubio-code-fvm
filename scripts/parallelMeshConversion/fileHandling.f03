module fileHandling

    use strings
    use m_strings
    use userDefininedTypes

    implicit none

    contains

!**********************************************************************************************************************

    subroutine readListOfIntFromFile(fileName, returnList)

        character(len=*) :: fileName

        character(len=:), allocatable :: fileFormat 
    !------------------------------------------------------------------------------------------------------------------    

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------    

        call checkFormat(fileName, fileFormat)

        if (fileFormat == 'openfoam_ascii') then
            call readListOfIntOFAscii(fileName, returnList)
        elseif(fileFormat == 'openfoam_binary') then
            call readListOfIntOFBinary(fileName, returnList)
        else
            write(*,*) 'ERROR: cannot recognize file format for '// fileName
            stop
        endif   

    end subroutine readListOfIntFromFile

!**********************************************************************************************************************

    subroutine readListOfRealFromFile(fileName, returnList)

        character(len=*) :: fileName

        character(len=:), allocatable :: fileFormat 
    !------------------------------------------------------------------------------------------------------------------    

        real, dimension(:,:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------    

        call checkFormat(fileName, fileFormat)

        if (fileFormat == 'openfoam_ascii') then
            call readListOfRealOFAscii(fileName, returnList)
        elseif(fileFormat == 'openfoam_binary') then
            call readListOfRealOFBinary(fileName, returnList)
        else
            write(*,*) 'ERROR: cannot recognize file format for '// fileName
            stop
        endif   

    end subroutine readListOfRealFromFile    

!**********************************************************************************************************************

    subroutine readListOfCellStructFromFile(fileName, returnList)

        character(len=*) :: fileName

        character(len=:), allocatable :: fileFormat 
    !------------------------------------------------------------------------------------------------------------------    

        type(integerCellStruct), dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------    

        call checkFormat(fileName, fileFormat)

        if (fileFormat == 'openfoam_ascii') then
            call readListOfCellStructOFAscii(fileName, returnList)
        elseif(fileFormat == 'openfoam_binary') then
            call readListOfCellStructOFBinary(fileName, returnList)
        else
            write(*,*) 'ERROR: cannot recognize file format for '// fileName
            stop
        endif   

    end subroutine readListOfCellStructFromFile
    
!**********************************************************************************************************************

    subroutine readListOfIntOFAscii(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readMeshPointsOFAscii reads the nodes composing the mesh from the OF mesh files.
    !==================================================================================================================
    
        character(len=*) :: fileName
        !! file name

        character(len=80) :: line
        !! parsed line
    
        character(len=:), allocatable :: croppedLine, oneLine
        !! cropped line
    
        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: n
        !! loop index
    
        integer :: np
        !! number of mesh nodes
    
        integer :: ierr
        !! error flag

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------
    
        logical :: oneLineFlag
    !------------------------------------------------------------------------------------------------------------------

        open(1,file=fileName)
            
            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=np, oneLine=oneLine)
            
            allocate(returnList(np))

            if (.not. allocated(oneLine)) then 
                oneLineFlag = .false.
            else
                oneLineFlag = matchw(oneLine, '*(*')
            endif        

            if(oneLineFlag) then

                call substitute(oneLine,'(',' ')
                call substitute(oneLine,')',' ')
                call split_in_array(oneLine, carray, ' ')
            
                do n=2,size(carray)
                    call string_to_value(carray(n), returnList(n-1), ierr)
                enddo
                
            else
                do n =1,np
    
                    call readLine(1, line, ierr)
                    croppedLine = crop(line)
            
                    call substitute(croppedLine,'(','')
                    call substitute(croppedLine,')','')
                    call split_in_array(croppedLine, carray, ' ')
                        
                    call string_to_value(carray(1), returnList(n), ierr)
            
                enddo  
            endif        

    
        close(1)

    end subroutine readListOfIntOFAscii 
          
!**********************************************************************************************************************

    subroutine readListOfIntOFBinary(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readListOfInt reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================

        character(len=*) :: fileName
        !! file name

        character(len=:), allocatable :: oneLine
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
        !! loop index

        integer :: np
        !! number of mesh nodes

        integer, dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------

        open(1, file=fileName, FORM = 'UNFORMATTED', access='STREAM')

            call skipHeader(1, 'binary')
            call findStreamSize(1, fileFormat='binary', streamSize=np,  oneLine =  oneLine)

            allocate(returnList(np))

            ! Read points
            do n =1,np
                read(1) returnList(n)
            enddo  

        close(1)

    end subroutine readListOfIntOFBinary

!**********************************************************************************************************************

    subroutine readListOfRealOFAscii(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readMeshPointsOFAscii reads the nodes composing the mesh from the OF mesh files.
    !==================================================================================================================
        
        character(len=*) :: fileName
        !! file name
    
        character(len=80) :: line
        !! parsed line
        
        character(len=:), allocatable :: croppedLine, oneLine
        !! cropped line
        
        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: n
        !! loop index
        
        integer :: np
        !! number of mesh nodes
        
        integer :: ierr
            ! error flag
    
        real, dimension(:,:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------
        
        open(1,file=fileName)
                
            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=np, oneLine=oneLine)
                
            allocate(returnList(np,3))
                
            do n =1,np
        
                call readLine(1, line, ierr)
                croppedLine = crop(line)
            
                call substitute(croppedLine,'(','')
                call substitute(croppedLine,')','')
                call split_in_array(croppedLine, carray, ' ')
                        
                call string_to_value(carray(1), returnList(n,1), ierr)
                call string_to_value(carray(2), returnList(n,2), ierr)
                call string_to_value(carray(3), returnList(n,3), ierr)
            
            enddo  
        
        close(1)
    
    end subroutine readListOfRealOFAscii 
              
!**********************************************************************************************************************
    
    subroutine readListOfRealOFBinary(fileName, returnList)
    
    !==================================================================================================================
    ! Description:
    !! readListOfInt reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================
    
        character(len=*) :: fileName
        !! file name
    
        character(len=:), allocatable :: oneLine
    !------------------------------------------------------------------------------------------------------------------
    
        integer :: n
        !! loop index
    
        integer :: np
        !! number of mesh nodes
    
        real, dimension(:,:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------
    
        open(1, file=fileName, FORM = 'UNFORMATTED', access='STREAM')
    
            call skipHeader(1, 'binary')
            call findStreamSize(1, fileFormat='binary', streamSize=np,  oneLine=oneLine)
    
            allocate(returnList(np,3))
    
            ! Read points
            do n =1,np
                read(1) returnList(n,1:3)
            enddo  
    
        close(1)
    
    end subroutine readListOfRealOFBinary

!**********************************************************************************************************************

    subroutine readListOfCellStructOFAscii(fileName, returnList)

    !==================================================================================================================
    ! Description:
    !! readMeshPointsOFAscii reads the nodes composing the mesh from the OF mesh files.
    !==================================================================================================================
                
        type(integerCellStruct), dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: fileName
        !! file name
            
        character(len=80) :: line
        !! parsed line
                
        character(len=:), allocatable :: croppedLine, oneLine
        !! cropped line
                
        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: i
        !! loop index

        integer :: n
        !! loop index
                
        integer :: np
        !! number of mesh nodes
                
        integer :: ierr
        !! error flag

        integer :: numberOfFaceNodes
    !------------------------------------------------------------------------------------------------------------------
                
        open(1,file=fileName)
                        
            call skipHeader(aunit=1, fileFormat='ascii')
            call findStreamSize(aunit=1, fileFormat='ascii', streamSize=np, oneLine=oneLine)
                        
            allocate(returnList(np))
                        
            do n =1,np
                
                call readLine(1, line, ierr)
                croppedLine = crop(line)
    
                call substitute(croppedLine,'(',' ')
                call substitute(croppedLine,')',' ')
                call split_in_array(croppedLine, carray, ' ')
                
                call string_to_value(carray(1), numberOfFaceNodes, ierr)
                call returnList(n)%initIntColumn(numberOfFaceNodes, -1)
                returnList(n)%csize = numberOfFaceNodes

                do i =1,numberOfFaceNodes
                    call string_to_value(carray(i+1), returnList(n)%col(i), ierr)
                    returnList(n)%col(i) = returnList(n)%col(i)
                enddo    
                    
            enddo  
                
        close(1)
            
    end subroutine readListOfCellStructOFAscii 
                      
!**********************************************************************************************************************
            
    subroutine readListOfCellStructOFBinary(fileName, returnList)
            
    !==================================================================================================================
    ! Description:
    !! readListOfInt reads the nodes composing the mesh from the mesh files.
    !==================================================================================================================
            
        type(integerCellStruct), dimension(:), allocatable :: returnList
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: fileName
        !! file name
            
        character(len=:), allocatable :: oneLine
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: i 
        !! loop index

        integer :: n
        !! loop index
            
        integer :: np
        !! number of mesh nodes

        integer :: ni

        integer :: nf

        integer, dimension(:), allocatable :: numberOfFaceNodes

        integer, dimension(:), allocatable :: faceIndex
    !------------------------------------------------------------------------------------------------------------------
            
        open(1,file=fileName, form = 'unformatted', access='stream')
            
        call skipHeader(aunit=1, fileFormat='binary')
        call findStreamSize(aunit=1, fileFormat='binary', streamSize=ni, oneLine=oneLine)
        nf = ni -1

        ! Allocate arrays
        allocate(returnList(ni-1))
        allocate(faceIndex(ni))
        allocate(numberOfFaceNodes(ni-1))
       
        ! Read the number of vertex per face
        read(1) faceIndex(1)
        do n=2,ni
            read(1) faceIndex(n)
            numberOfFaceNodes(n-1) = faceIndex(n) - faceIndex(n-1)
            returnList(n-1)%csize = numberOfFaceNodes(n-1)
            call returnList(n-1)%initIntColumn(numberOfFaceNodes(n-1), -1)
        enddo 

        call findStreamSize(aunit=1, fileFormat='binary', streamSize=np, oneLine=oneLine)

        do n=1,nf
            do i =1,numberOfFaceNodes(n)
                read(1) returnList(n)%col(i)
                returnList(n)%col(i) = returnList(n)%col(i)
            enddo
        enddo  

    close(1)
            
    end subroutine readListOfCellStructOFBinary
        
!**********************************************************************************************************************

    subroutine skipHeader(aunit, fileFormat)

        implicit none
        
        character(len=*) :: fileFormat
        !! file format

        character(len=1) :: a, b, c
        !! dummy characters
    !------------------------------------------------------------------------------------------------------------------

        integer :: aunit
        !! unit to read

        integer :: counter
        !! line counter
    !------------------------------------------------------------------------------------------------------------------
 
        counter = 0
        do
            counter = counter + 1 
            
            if(counter>3) then
                c = b 
                b = a
            endif    

            if(fileFormat == 'ascii') then
                read(aunit, *) a
            else
                read(aunit) a    
            endif    

            if(a == '/' .and. b == '/' .and. c == ' ') then
                exit
            elseif(a == '}') then 
                exit   
            endif    
        enddo 
        
    end subroutine skipHeader

! *********************************************************************************************************************

    subroutine findStreamSize(aunit, fileFormat, streamSize, oneLine)

        implicit none 

        character(len=*) :: fileFormat

        character(len=:), allocatable :: anumber, oneLine

        character(len=1) :: dummy_binary
        !! dummy character

        character(len=100) :: dummy_ascii
        !! dummy character

        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: aunit
        !! unit to read

        integer :: counter
        !! line counter

        integer :: n
        !! character counter

        integer :: ierr
        !! error flag

        integer :: streamSize
        !! binary stream size
    !------------------------------------------------------------------------------------------------------------------

        logical :: is_num
    !------------------------------------------------------------------------------------------------------------------

        counter = 0
        n = 0
        is_num = .false.

        do
            counter = counter + 1

            if(fileFormat == 'ascii') then

                call readLine(aunit, dummy_ascii, ierr)
                
                call split_in_array(dummy_ascii, carray, '(')
                
                if(size(carray)>1) then
                    anumber = carray(1)
                    oneLine = dummy_ascii
                    is_num = isdigit(anumber)
                    if(.not. is_num) then
                        write(*,*) 'ERROR: file is corrupted as the line contains ( but the first character is not a number.'
                        stop
                        exit
                    endif                
                    exit

                endif    
                
                is_num = isdigit(dummy_ascii)
         
                if(is_num .and. n == 0) then
                    anumber = dummy_ascii
                elseif(trim(dummy_ascii)=='(') then
                    exit       
                endif   

            else

                read(aunit) dummy_binary

                is_num = isdigit(dummy_binary)
      
                if(is_num .and. n == 0) then
                    anumber = dummy_binary
                    n = n + 1
                elseif(is_num .and. n > 0) then
                    anumber = anumber // dummy_binary
                    n = n + 1
                elseif(dummy_binary=='(') then
                    exit       
                endif   

            endif    
              
        enddo

        call string_to_value(anumber, streamSize, ierr)

    end subroutine findStreamSize

! *********************************************************************************************************************    

    subroutine checkFormat(fileName, fileFormat)

        character(len=*) :: fileName 
        !! file name

        character(len=100) :: line
        !! parsed line

        character(len=:), allocatable :: fileFormat
        !! file format

        character(len=25), allocatable :: carray(:)
        !! splitted line
    !------------------------------------------------------------------------------------------------------------------

        integer :: n, ierr
    !------------------------------------------------------------------------------------------------------------------

        logical :: formatFound, isOpenFoam, isNum, isFoamFile, isCpp
    !------------------------------------------------------------------------------------------------------------------

        ! Check if it is an openFoam file
        open(1, file=fileName)
            call readLine(1, line, ierr)
            isCpp = matchw(line, '*C++*')
            isFoamFile = matchw(line, 'FoamFile')
            isOpenFoam = isCpp .or. isFoamFile
        close(1)

        if(isOpenFoam) then

            open(1, file=fileName)

                do
                    call readLine(1, line, ierr)
                    formatFound = matchw(line, '*format*')
                
                    if(formatFound) then 
                        call split_in_array(line, carray, ' ')
                        call substitute(carray(2), ';', '')
                        fileFormat = trim(carray(2))
                        exit    
                    endif    

                    if (ierr /= 0) then
                        exit
                    endif    
                enddo    

            close(1)

            fileFormat = 'openfoam_'//fileFormat
        
        else

            ! Check if it is a flubio format, ascii always starts with a number
            open(1, file=fileName)
                call readLine(1, line, ierr)
                isNum = isdigit(line)
             
                if(isNum) then
                    fileFormat = 'flubio_ascii'
                else
                    fileFormat = 'flubio_binary'
                endif        

            close(1)
            
        endif    

        !call flubioMsg('File format is '// fileFormat)

    end subroutine checkFormat    
     
! *********************************************************************************************************************


end module    
