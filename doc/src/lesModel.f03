!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module LES

!==================================================================================================================
! Description:
!! This module contains the base structure for LES model
!==================================================================================================================

    use flubioFields
    use meshvar
    use physicalConstants
    use velocityTensors

    implicit none

    type, public :: subgridModel

        character(len=:), allocatable :: modelName

        real, dimension(:), allocatable :: fwidth

        type(flubioField) :: nut

        contains
            procedure :: createLesModel
            procedure :: modelConstants => baseModelConstants
            procedure :: updateEddyViscosity => baseEddyViscosity

            procedure :: updateMeshFilter
            procedure :: nutWallFunction

    end type subgridModel

contains


    subroutine createLesModel(this)

    !==================================================================================================================
    ! Description:
    !! createLesModel sets up commmon qunatities in LES models, computing the filter width and initialising the eddy viscosity.
    !==================================================================================================================

        class(subgridModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call flubioTurbModel%json%get('ModelName', this%modelName, found)
        call raiseKeyErrorJSON('this%modelName', found)

        allocate(this%fwidth(numberOfElements))
        this%fwidth(1:numberOfElements)=mesh%volume(1:numberOfElements)**(1/3)

        call this%nut%createField(fieldName='nut', classType='scalar', nComp=1)
        this%nut%phi(:,1) = viscos

        call this%nut%setBcFlagsNeumann0()
        call this%nut%updateBoundaryField()
        call this%nut%updateGhosts()

    end subroutine createLesModel

! *********************************************************************************************************************

    subroutine baseEddyViscosity(this)

        class(subgridModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%nut%phi(:,1) = viscos

        ! Update nu and nut at boundaries
        call this%nut%updateBoundaryField()
        call this%nut%updateGhosts()

        call nu%updateBoundaryField()
        call nu%updateGhosts()

    end  subroutine baseEddyViscosity

! *********************************************************************************************************************

    subroutine baseModelConstants(this)

    !==================================================================================================================
    ! Description:
    !! baseModelConstants is a virtual function to set model constants, overridden by any other model
    !==================================================================================================================

        class(subgridModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
    !------------------------------------------------------------------------------------------------------------------

    end subroutine baseModelConstants

! *********************************************************************************************************************

    subroutine updateMeshFilter(this)

    !==================================================================================================================
    ! Description:
    !! updateMeshFilter computes the mesh filter width in each cell using the cubic root of the volume.
    !! In the future thi subroutine can be expanded with other rules.
    !==================================================================================================================

        class(subgridModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%fwidth(1:numberOfElements)=mesh%volume(1:numberOfElements)**(1/3)

    end subroutine updateMeshFilter

! *********************************************************************************************************************

    subroutine nutWallFunction(this)

    !==================================================================================================================
    ! Description:
    !! nutWallFunction computes the value of the eddy viscosity at solid walls for the LES turbulence models.
    !==================================================================================================================

        class(subgridModel) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, iWall, patchFace
    !------------------------------------------------------------------------------------------------------------------

        real :: SMALL
    !------------------------------------------------------------------------------------------------------------------

        SMALL=0.00000000001d0

        !--------------------------------------!
        ! Correct viscosity at wall boundaries !
        !--------------------------------------!

        patchFace=numberOfElements
        i=0

        do iBoundary=1,numberOfBoundaries

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
            iWall = 0

            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') i = i+1

            do iBFace=is,ie

                patchFace = patchFace+1
                iOwner = mesh%owner(iBFace)

                ! Get the wall boundaries
                if(trim(mesh%boundaries%bcType(iBoundary))=='wall') then

                    iWall=  iWall+1
                    nu%phi(patchFace,1) = viscos
                    this%nut%phi(patchFace,1) = 0.0

                elseif(trim(mesh%boundaries%bcType(iBoundary))/='wall') then

                    ! Zero gradient
                    this%nut%phi(patchFace,1) = this%nut%phi(iOwner,1)
                    nu%phi(patchFace,1) = viscos + this%nut%phi(iOwner,1)

                endif

            enddo

        enddo

    end subroutine nutWallFunction

! *********************************************************************************************************************

end module LES