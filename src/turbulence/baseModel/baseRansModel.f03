!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____ 	        !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|	        !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |     	        !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |     	        !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____ 	        !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|	        !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module baseRansModel

!==================================================================================================================
! Description:
!! baseRansModel is the base class for the turbulence models. EVery RANS model must inherit form this class.
!==================================================================================================================

      use flubioFields
      use math
      use meshvar
      use fieldvar
      use gradient
      use operators
      use physicalConstants
      use wallDistance
      use velocityTensors
      use ttbTensors

      implicit none

      type, public :: baseModelClass

        character(len=:), allocatable :: modelName
        !! Name for the RANS model

        integer :: ransModel
        !! Flag for the RANS model

        integer :: productionType
        !! Turbulent production method

        integer:: curvatureCorr
        !! curvature correction flag
    
        integer :: wallFunctionBlending
        !! Wall function blending between viscous and logarithmic layers

        integer :: outputYPlus
        !! Flag to output y+

        real, dimension(:), allocatable :: Pk
        !! Turbulent production field field (Moukalled et al, pp.764)

        real, dimension(:), allocatable :: yPlus
        !! Wall distance in wall units

        real, dimension(:,:), allocatable :: nuWall
        !! Effective wall viscosity

        real, dimension(:,:), allocatable :: tauWall
        !! Wall shear stress

        real :: Cmu
        !! Common costant found in turbulence models

        real :: kappa
        !! Universal constant

        real :: E
        !! Boundary layer E constant

        real :: B
        !! Boundary layer shift constant

        real :: roughnessConstant
        !! Roughness constant

        real :: roughnessHeight
        !! Roughness heigth

        real :: yplusLaminar
        !! Value of the laminar y+

        real :: tke_amb
        !! Free strem tke

        real ::  tdr_amb
        !! Free stream tdr

        type(flubioField) :: nut
        !! Eddy viscosity

        type(flubioWallDist) :: wd
        !! Wall distance

        type(flubioField) :: tkeField
        !! tke field to host model tke

        type(flubioField) :: tdrField
        !! tdr field to host model tdr

        type(transportEqn) :: tkeEqn
        !! tke equation

        type(transportEqn) :: tdrEqn
        !! tdr equation        

      contains

          procedure :: createModel => createBaseModel
          procedure :: setModelName
          procedure :: createCommonQuantities
          procedure :: modelCoeffs => baseModelCoeffs

          procedure :: updateEddyViscosity => updateBaseEddyViscosity
          procedure :: nutWallFunction => doNothing

          procedure :: tkeDiffusivity => muEffBase
          procedure :: tdrDiffusivity => muEffBase

          procedure :: computeFrictionVelocityU
          procedure :: computeRoughFrictionVelocityU
          procedure :: computeFrictionVelocityK => doNothing
          procedure :: computeFrictionVelocity

          procedure :: laminarYplus
          procedure :: computeYPlus

          procedure :: turbulentProduction => doNothing

          procedure :: tkeSourceTerm => doNothing
          procedure :: tdrSourceTerm => doNothing

          procedure :: turbulenceModel => doNothing

          procedure :: printresidual => doNothing
          procedure :: writeToFile => doNothing
          procedure :: verbose => doNothing

          procedure :: writeWallYPlus

      end type baseModelClass

contains

    subroutine createBaseModel(this)

    !==================================================================================================================
    ! Description:
    !! createBaseModel is the class constructor.
    !==================================================================================================================

        class(baseModelClass) :: this
     !------------------------------------------------------------------------------------------------------------------

        ! DNS Model does nothing

    end  subroutine createBaseModel

! ******************************************************************************************************************************

    subroutine setModelName(this)

    !==================================================================================================================
    ! Description:
    !! setModelName sets the name fo the turbulence model.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%modelName = flubioTurbModel%modelName
        this%ransModel = flubioTurbModel%ransModel
        this%productionType = flubioTurbModel%productionType
        this%curvatureCorr = flubioTurbModel%productionType
        this%wallFunctionBlending = flubioTurbModel%wallFunctionBlending
        this%outputYPlus = flubioTurbModel%outputYPlus

        ! Ambient turbulence if any
        if(flubioTurbModel%sustTerm) then
            this%tke_amb = flubioTurbModel%tke_amb
            this%tdr_amb = flubioTurbModel%tdr_amb
        else
            this%tke_amb = 0.0
            this%tdr_amb = 0.0
        end if

    end subroutine setModelName

! *********************************************************************************************************************

    subroutine createCommonQuantities(this)

    !==================================================================================================================
    ! Description:
    !! createCommonQuantities creates common fields used by all turbulence models.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%ransModel = flubioTurbModel%ransModel

        call this%setModelName()

        call this%nut%createField(fieldName='nut', classType='scalar', nComp=1)
        call this%nut%setBcFlagsNeumann0()
        call this%nut%updateBoundaryField()
        call this%nut%updateGhosts()

        ! Compute Wall Distance
        call this%wd%computeWallDistance()

        ! Common quantities
        allocate(this%yplus(numberOfElements+numberOfBFaces))
        allocate(this%nuWall(mesh%boundaries%maxWallFaces,numberOfWallBound))
        allocate(this%tauWall(mesh%boundaries%maxWallFaces,numberOfWallBound))
        allocate(this%Pk(numberOfElements))

        this%nuWall = viscos
        this%tauWall = 0.0
        this%yplus = 0.0 ! non-zero at the wall cells only

    end subroutine createCommonQuantities

! *********************************************************************************************************************

    subroutine baseModelCoeffs(this)

    !==================================================================================================================
    ! Description:
    !! baseModelCoeffs sets the values of base constants, common to all the turbulence models.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------
           
        this%Cmu = 0.09
        this%kappa = 0.41
        this%E = 9.80
        this%B = 5.25

        this%roughnessConstant = 5e-4
        this%roughnessHeight = 0.5

        call this%laminarYPlus()

    end subroutine baseModelCoeffs

! *********************************************************************************************************************

    subroutine updateBaseEddyViscosity(this)

    !==================================================================================================================
    ! Description:
    !! updateBaseEddyViscosity updates the eddy visocsity at boundaries and ghost cells,
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        this%nut%phi(:,1) = viscos

        ! Update nu and nut at boundaries
        call this%nut%updateBoundaryField()
        call this%nut%updateGhosts()

        call nu%updateBoundaryField()
        call nu%updateGhosts()

    end subroutine updateBaseEddyViscosity

! *********************************************************************************************************************

    subroutine computeFrictionVelocityU(this)

    !==================================================================================================================
    ! Description:
    !! computeFrictionVelocityU computes the friction velocity from the velocity wall function.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, pFace

        integer :: it, itmax, iWall
    !------------------------------------------------------------------------------------------------------------------

        real :: kappa, B, E, wallYplusLim, wallYplus

        real :: f, fprime, d, ypl, ut, res, tol, dot, SMALL

        real :: Up(3), nf(3), deltaU(3), magUp
    !------------------------------------------------------------------------------------------------------------------

        ! Prepare
        kappa = this%kappa
        E = this%E
        wallYplusLim = this%yplusLaminar

        itmax = 10
        tol = 1e-2
        SMALL = 1e-10

        ! Loop over wall boundaries
        do iBoundary=1,numberOfWallBound

            iWall = 0
            i = mesh%boundaries%wallBound(iBoundary)

            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            pFace = numberOfElements + (is - numberOfIntFaces)-1

            do iBFace=is,ie

                iWall = iWall+1
                iOwner = mesh%owner(iBFace)
                pFace = pFace + 1
                
                ! Velocity parallel to the wall
                nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)
                deltaU = velocity%phi(iOwner,:) - velocity%phi(pFace,:)
                dot = dot_product(deltaU,nf)

                ! Centroid velocity - wall velocity parallel to the wall
                Up = deltaU - dot*nf
                magUp = norm2(Up)

                !------------!
                ! Find yplus !
                !------------!

                it = 0
                res = 100000000.0
                d = mesh%area(iBFace)/mesh%wallDist(iBFace)

                do while(res>=tol)

                    it = it+1

                    if(it==1) then
                        ypl = wallYplusLim
                    else
                        ypl = wallYplus
                    endif

                    f = ypl + kappa*magUp*d/viscos
                    fprime = 1.0+log(E*ypl)

                    wallYplus = f/fprime
                    ut = viscos*(wallYplus/d)
                    res = abs((wallYplus - ypl)/wallYplusLim)
                   
                enddo
               
                !--------------------!
                ! Modified viscosity !
                !--------------------!

                ypl = wallYplus         
                this%yplus(iOwner) = ypl

                ! Total viscosity (nu+nut); nut=viscos*(nuWall-1)
                if(ypl > wallYplusLim) then
                  this%nuWall(iWall,iBoundary) = kappa*ypl*viscos/log(E*ypl)
                else
                  this%nuWall(iWall,iBoundary) = viscos
                end if

                this%tauWall(iWall,iBoundary) = this%nuWall(iWall,iBoundary)*magUp/d

            enddo

        enddo

    end subroutine computeFrictionVelocityU

! *********************************************************************************************************************

    subroutine computeRoughFrictionVelocityU(this)

    !==================================================================================================================
    ! Description:
    !! computeRoughFrictionVelocityU computes the friction velocity from the rough velocity wall function.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, iBoundary, iBFace, iElement, is, ie, iOwner, pFace

        integer :: it, itmax, iWall
    !------------------------------------------------------------------------------------------------------------------

        real :: kappa, B, E, Re, wallYplusLim, wallYplus

        real :: c1, c2, c3, c4, t1, t2, t3

        real :: KsPlus, dKsPlusdYPlus, G, yPlusGPrime 

        real :: f, fprime, d, ypl, ut, res, tol, dot, SMALL

        real :: Up(3), nf(3), deltaU(3), magUp
    !------------------------------------------------------------------------------------------------------------------

        ! Check 
        if (this%roughnessHeight<=0.0) then 
            call flubioStopMsg('FLUBIO ERROR: roughnessHeight is zero, please use smooth wall function instead!')
        endif

        ! Prepare
        kappa = this%kappa
        E = this%E
        wallYplusLim = this%yplusLaminar

        itmax = 10
        tol = 1e-2
        SMALL = 1e-10

        c1 = 1.0/(90.0 - 2.25) + this%roughnessConstant
        c2 = 2.25/(90.0 - 2.25)
        c3 = 2.0*atan(1.0)/log(90.0/2.25)
        c4 = c3*log(2.25)

        ! Loop over wall boundaries
        do iBoundary=1,numberOfWallBound

            iWall = 0
            i = mesh%boundaries%wallBound(iBoundary)

            is = mesh%boundaries%startFace(i)
            ie = is+mesh%boundaries%nFace(i)-1

            pFace = numberOfElements + (is - numberOfIntFaces)-1

            do iBFace=is,ie

                iWall = iWall+1
                iOwner = mesh%owner(iBFace)
                pFace = pFace + 1
                
                ! Velocity parallel to the wall
                nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)
                deltaU = velocity%phi(iOwner,:) - velocity%phi(pFace,:)
                dot = dot_product(deltaU,nf)

                ! Centroid velocity - wall velocity parallel to the wall
                Up = deltaU - dot*nf
                magUp = norm2(Up)

                ! Roughness parameters
                d = mesh%area(iBFace)/mesh%wallDist(iBFace)
                dKsPlusdYPlus = this%roughnessHeight/d
                Re = magUp*d/viscos

                !------------!
                ! Find yplus !
                !------------!

                it = 0
                res = 100000000.0

                do while(res>=tol)

                    it = it+1

                    if(it==1) then
                        ypl = wallYplusLim
                    else
                        ypl = wallYplus
                    endif
                    
                    KsPlus = ypl*dKsPlusdYPlus

                    if(KsPlus>=90.0) then 

                        t1 = c1*KsPlus - c2
                        t2 = c3*log(KsPlus) - c4
                        G = log(t1)*sin(t2)
                        yPlusGPrime =(c1*sin(t2)*KsPlus/t1) + (c3*log(t1)*cos(t2))

                        f = kappa*Re + ypl*(1 - yPlusGPrime)
                        fprime = this%roughnessConstant*KsPlus/t1

                    elseif(KsPlus>2.25 .and. KsPlus<90.0) then 

                        t1 = 1.0 + this%roughnessConstant*KsPlus
                        G = log(t1)
                        yPlusGPrime = this%roughnessConstant*KsPlus/t1

                        f = kappa*Re + ypl*(1 - yPlusGPrime)
                        fprime = 1.0 + log(E*ypl) -G -yPlusGPrime

                    else 
                        call flubioMsg('WARNING: KsPlus fell under 2.25, I am not sure how to handle this :/')
                        
                    endif

                        wallYplus = f/fprime
                        ut = viscos*(wallYplus/d)
                        res = abs((wallYplus - ypl)/wallYplusLim)
                   
                enddo
               
                !--------------------!
                ! Modified viscosity !
                !--------------------!

                ypl = wallYplus         
                this%yplus(iOwner) = ypl

                ! Total viscosity (nu+nut); nut=viscos*(nuWall-1)
                if(ypl > wallYplusLim) then
                  this%nuWall(iWall,iBoundary) = viscos*ypl**2/Re
                else
                  this%nuWall(iWall,iBoundary) = viscos
                end if

                this%tauWall(iWall,iBoundary) = this%nuWall(iWall,iBoundary)*magUp/d

            enddo

        enddo

    end subroutine computeRoughFrictionVelocityU

! *********************************************************************************************************************

    subroutine computeFrictionVelocity(this)

    !==================================================================================================================
    ! Description:
    !! computeFrictionVelocity computes the friction velocity using the selected method.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        if(flubioTurbModel%frictionVelocityMethod==0) then
            call this%computeFrictionVelocityU()
        elseif(flubioTurbModel%frictionVelocityMethod==1) then
            call this%computeFrictionVelocityK()
        elseif(flubioTurbModel%frictionVelocityMethod==3) then
            call this%computeRoughFrictionVelocityU()
        else
            call this%computeFrictionVelocityU()
        end if

    end subroutine computeFrictionVelocity

! *********************************************************************************************************************

    subroutine productionLimiter(P, limiter)

    !==================================================================================================================
    ! Description:
    !! productionLimiter limits the turbulent production. This procedure is used in some version of k-omega model.
    !==================================================================================================================

        implicit none

        real :: P, limiter
    !------------------------------------------------------------------------------------------------------------------

        if(P>limiter) P = limiter

    end subroutine productionLimiter

! *********************************************************************************************************************

    subroutine laminarYPlus(this)

    !==================================================================================================================
    ! Description:
    !! laminarYPlus computes the treshold for the laminar yPlus given the values of E and kappa.
    !==================================================================================================================

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: ypl
    !------------------------------------------------------------------------------------------------------------------

        ypl = 11.0

        do i=1,10
            ypl = log(max(this%E*ypl, 1.0))/this%kappa
        end do

        this%yplusLaminar = ypl

    end subroutine laminarYPlus

! *********************************************************************************************************************

    function computeYPlus(this) result(ypl)

    !==================================================================================================================
    ! Description:
    !! laminarYPlus computes the treshold for the laminar yPlus given the values of E and kappa.
    !==================================================================================================================
        
        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------
        
        integer :: iBoundary, iOwner, iBFace, is, ie, iWall, patchFace
    !------------------------------------------------------------------------------------------------------------------
        
        real :: den 
        !! fluid density
    
        real :: muEff
        !! effective viscosity
    
        real :: ut 
        !! friction velocity
    
        real :: d 
        !! wall distance
    
        real :: n(3) 
        !! boundary face normal
    
        real :: gradUw(3) 
        !! wall gradient
    
        real :: ypl(numberOfElements + numberOfBFaces)
        !! yplus at walls. Non-wall boundaries will return 0
    !------------------------------------------------------------------------------------------------------------------
    
        iWall = 0
    
        ypl = 0.0
    
        ! Loop over boundaries
        do iBoundary=1,numberOfRealBound
    
            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1
    
            patchFace = numberOfElements + (is-numberOfIntFaces-1)
    
            ! Wall boundaries only
            if(trim(mesh%boundaries%bcType(iBoundary))=='wall') then 
    
                iWall = iWall + 1
    
                do iBFace = is,ie
    
                    patchFace = patchFace + 1
                    iOwner = mesh%owner(iBFace)

                    ! Density and effective viscosity
                    den = rho%phi(patchFace,1)
                    muEff = nu%phi(patchFace,1) 
    
                    ! Wall distance 
                    d = this%wd%dperp(iOwner)
    
                    ! I might have called surfaceNormalGradient(), but this way is lighter and faster
                    n = mesh%Sf(iBFace,:)/mesh%area(iBFace)
                    gradUw(1) = dot_product(velocity%phiGrad(patchFace,:,1), n)
                    gradUw(2) = dot_product(velocity%phiGrad(patchFace,:,2), n)
                    gradUw(3) = dot_product(velocity%phiGrad(patchFace,:,3), n)
    
                    ! sqrt(tw/rho)
                    ut = norm2(gradUw)/sqrt(den)
    
                    ypl(patchFace) = muEff*ut*d/viscos
    
                enddo
    
            endif
    
        enddo
        
        ! Warning if there are not wall boundaries
        if(iWall < 1) call flubioMsg('FLUBIO WARNING: you asked for the yplus, but there are no wall boundaries.')
    
    end function computeYPlus
    
! *********************************************************************************************************************

    function blend(v, l, opt, yplus, threshold, n) result(blendedValue)

    !==================================================================================================================
    ! Description:
    !! blend uses a blending method to compute the a target value.
    !==================================================================================================================

        integer :: opt
    !------------------------------------------------------------------------------------------------------------------

        real v, l, blendedValue, gamma, yPlus

        real :: threshold, n
    !------------------------------------------------------------------------------------------------------------------

        if(opt == 0) then
            blendedValue = sqrt(v**2+l**2)
        elseif(opt == 1) then
            if(yPlus > threshold) then
                blendedValue = l
            else
                blendedValue = v
            end if
        elseif(opt == 2) then
            blendedValue = max(v, l)
        elseif(opt == 3) then
            blendedValue = (v**n + l**n)**(1/n)
        elseif(opt == 4) then
            gamma = 0.01*yPlus**4/(1.0+5*yPlus)
            blendedValue = v*exp(-gamma) + l*exp(-1.0/gamma)
        elseif(opt == 5) then
            gamma = tanh((yPlus/10.0)**2)
            blendedValue = (v+l)*gamma + (1.0-gamma)*(v**1.2 + l*1.2)**(1.0/1.2)
        else
            blendedValue = sqrt(v**2+l**2)
        end if

    end function blend

! *********************************************************************************************************************

    subroutine writeWallYPlus(this)

        class(baseModelClass) :: this

        type(flubioField) :: wallYPlus
    !------------------------------------------------------------------------------------------------------------------

        call wallYPlus%createWorkField(fieldName='wallYPlus', classType='scalar', ncomp=1)

        call wallYPlus%setBcFlagsNeumann0()

        wallYPlus%phi(:,1) = this%yplus

        call wallYPlus%updateBoundaryField()

        call wallYPlus%writeToFileW()

    end subroutine writeWallYPlus

! *********************************************************************************************************************

    subroutine doNothing(this)

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        call flubioStopMsg('FLUBIO: Something went wrong. The turbulence model has not been selected correctly!')

    end subroutine doNothing

! *********************************************************************************************************************

    function muEffBase(this) result(muEff)

        class(baseModelClass) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: muEff
    !------------------------------------------------------------------------------------------------------------------

        call flubioStopMsg('FLUBIO: Something went wrong. The turbulence model has not been selected correctly!')

    end function muEffBase

! *********************************************************************************************************************    

end module baseRansModel




