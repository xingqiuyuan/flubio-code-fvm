!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module faceLimited
	
!===================================================================================================================
! Description:
!! faceLimited implements the face limited version of the gradient.
!===================================================================================================================
	
	use flubioDictionaries
	use flubioFields
	use meshvar

	implicit none

contains

	subroutine faceLimitedMDGradient(field)

	!==================================================================================================================
	!  Description:
	!! faceLimitedMDGradient bounds the gradient using multidimensional clipping. The level of bounding is controllod by
	!! a coefficient between 0 and 1 (0: unbounded, 1: fully bounded). It is similar to the cellLimited version, but more diffusive.
	!! The gradient to be bounded can be calculated either by standard Gauss formula either by least squares method.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------

		integer :: i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer :: iElement, iBElements, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc
	!------------------------------------------------------------------------------------------------------------------

		real :: maxPhi(numberOfFaces, field%nComp)
		!! maximum cell center values among the neighbour cells

		real :: minPhi(numberOfFaces, field%nComp)
		!! minumum  cell center values among the neighbour cells

		real :: phiC
		!! Owner cell center value

		real :: phiN
		!! neighbour cell ceneter value

		real :: kappa
		!! clipping constant

		real :: km1k
		!! (k-1)/k

		real :: dot
		!! auxiliary variable to store a dot product

		real :: limiter
		!! face limiter

		real :: val
		!! auxiliary variable

		real :: rCfO(3)
		!! cell center to cell face distance, owner side

		real :: eCfO(3)
		!! normalised cell center to cell face distance, owner side

		real :: rCfN(3)
		!! cell center to cell face distance, neighbour side

		real :: eCfN(3)
		!! normalised cell center to cell face distance, neighbour side

		real :: dPhi(field%nComp)
		!! difference between dPhiMax and dPhiMin

		real :: phiMaxTilde
		!! difference between maxPhi and actual cell center value

		real :: phiMinTilde
		!! difference between minPhi and actual cell center value

		real :: dphiMax
		!! difference between maxPhi and actual cell center value

		real :: dphiMin
		!! difference between minPhi and actual cell center value
	!------------------------------------------------------------------------------------------------------------------

		fComp = field%nComp

		kappa = field%kLim
		km1k = (1.0-kappa)/kappa

		if(kappa < 1e-8) return

		!--------------------------!
		! Compute face max and min !
		!--------------------------!

		call findFaceMinMax(minPhi, maxPhi, field%phi, field%ghosts, fComp)

		!--------------------!
		! Limit the gradient !
		!--------------------!

		do iFace=1,numberOfIntFaces

		   iOwner = mesh%owner(iFace)
		   iNeighbour = mesh%neighbour(iFace)

		   rCfO = mesh%fcentroid(iFace,:) - mesh%centroid(iOwner,:)
		   eCfO = rCfO/(rCfO(1)**2+rCfO(2)**2+rCfO(3)**2)

		   rCfN = mesh%fcentroid(iFace,:) - mesh%centroid(iNeighbour,:)
		   eCfN = rCfN/(rCfN(1)**2+rCfN(2)**2+rCfN(3)**2)

		   do iComp=1,fComp

			   phiMaxTilde = maxPhi(iFace, iComp) + km1k*(maxPhi(iFace, iComp) - minPhi(iFace, iComp))
			   phiMinTilde = minPhi(iFace, iComp) - km1k*(maxPhi(iFace, iComp) - minPhi(iFace, iComp))

			   ! Owner
			   dphiMax = phiMaxTilde - field%phi(iOwner,iComp)
			   dphiMin = phiMinTilde - field%phi(iOwner,iComp)
			   dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
			   if(dot > dPhiMax) then
				   field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMax - dot)
			   elseif(dot < dPhiMin) then
				   field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMin - dot)
			   endif

			   ! Neighbour
			   dphiMax = phiMaxTilde - field%phi(iNeighbour,iComp)
			   dphiMin = phiMinTilde - field%phi(iNeighbour,iComp)
			   dot = dot_product(field%phigrad(iNeighbour,:,iComp), rCfN)
			   if(dot > dPhiMax) then
				   field%phigrad(iNeighbour,:,iComp) = field%phigrad(iNeighbour,:,iComp) + eCfN*(dPhiMax - dot)
			   elseif(dot < dPhiMin) then
				   field%phigrad(iNeighbour,:,iComp) = field%phigrad(iNeighbour,:,iComp) + eCfN*(dPhiMin - dot)
			   endif

		   enddo

		enddo

		!----------------------!
		! Processor Boundaries !
		!----------------------!

		do iBoundary=1,numberOfProcBound

		   iProc = mesh%boundaries%procBound(iBoundary)
		   is = mesh%boundaries%startFace(iProc)
		   ie = is+mesh%boundaries%nFace(iProc)-1

		   do iBFace=is,ie

			  iOwner = mesh%owner(iBFace)

			  rCfO = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)
			  eCfO = rCfO/(rCfO(1)**2+rCfO(2)**2+rCfO(3)**2)

			  do iComp=1,fComp

				  phiMaxTilde = maxPhi(iBFace, iComp) + km1k*(maxPhi(iBFace, iComp) - minPhi(iBFace, iComp))
				  phiMinTilde = minPhi(iBFace, iComp) - km1k*(maxPhi(iBFace, iComp) - minPhi(iBFace, iComp))

				  ! Owner
				  dphiMax = phiMaxTilde - field%phi(iOwner,iComp)
				  dphiMin = phiMinTilde - field%phi(iOwner,iComp)
				  dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
				  if(dot > dPhiMax) then
					  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMax - dot)
				  elseif(dot < dPhiMin) then
					  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMin - dot)
				  endif
			  enddo

		   enddo

		enddo

		!-----------------!
		! Real boundaries !
		!-----------------!

		do iBoundary=1,numberOfRealBound

		   is = mesh%boundaries%startFace(iBoundary)
		   ie = is+mesh%boundaries%nFace(iBoundary)-1

		   do iBFace=is,ie

			  iOwner = mesh%owner(iBFace)

			  rCfO = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)
			  eCfO = rCfO/(rCfO(1)**2+rCfO(2)**2+rCfO(3)**2)

			  do iComp=1,fComp
				  phiMaxTilde = maxPhi(iBFace, iComp) + km1k*(maxPhi(iBFace, iComp) - minPhi(iBFace, iComp))
				  phiMinTilde = minPhi(iBFace, iComp) - km1k*(maxPhi(iBFace, iComp) - minPhi(iBFace, iComp))

				  ! Owner
				  dphiMax = phiMaxTilde - field%phi(iOwner,iComp)
				  dphiMin = phiMinTilde - field%phi(iOwner,iComp)
				  dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
				  if(dot > dPhiMax) then
					  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMax - dot)
				  elseif(dot < dPhiMin) then
					  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMin - dot)
				  endif
			  enddo

		   enddo

		enddo

	end subroutine faceLimitedMDGradient

! *********************************************************************************************************************

	subroutine faceLimiter(grad, dPhiMax, dPhiMin, extrapolation, eCf)

	!==================================================================================================================!
	! Description:
	!! faceLimiter computes the face limter in order to clip oscillations.
	!==================================================================================================================!

		real :: grad(3)

		real :: dPhiMax
		!! difference between maxPhi and actual cell center value

		real :: dPhiMin
		!! difference between maxPhi and actual cell center value

		real :: extrapolation
		!! dot product between actual gradient ad cell to face distance

		real :: eCF
		!! face center to cell center versor
	!------------------------------------------------------------------------------------------------------------------

		if(extrapolation > dPhiMax) then
			grad = grad + eCf*(dPhiMax - extrapolation)
		elseif(extrapolation < dPhiMin) then
			grad = grad + eCf*(dPhiMin - extrapolation)
		endif

	end subroutine faceLimiter

! *********************************************************************************************************************

	subroutine findFaceMinMax(minPhi, maxPhi, phi, phiGhost, fComp)

	!==================================================================================================================
	!! Description:
	!! findFaceMinMax looks for the minimum and maximum value of a working variable between the owner and the corresponding
	!! neighbour, for each face composing the cell.
	!! This routine is used in the bounding procedure for the gradient.
	!==================================================================================================================

		integer :: i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer :: iElement, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc
	!------------------------------------------------------------------------------------------------------------------

		real :: maxPhi(numberOfFaces,fComp)
		!! maximum cell center values among the neighbour cells

		real :: minPhi(numberOfFaces,fComp)
		!! minumum cell center values among the neighbour cells

		real :: phiC
		!! Owner cell center value

		real :: phiN
		!! neighbour cell ceneter value

		real :: phi(numberOfElements+numberOfBFaces,fComp)
		!! field values

		real :: phiGhost(maxProcFaces,fComp,numberOfProcBound1)
		!! field values at processor boundaries
	!------------------------------------------------------------------------------------------------------------------

		maxPhi = 0.0
		minPhi = 0.0

		!---------------------------------------!
		! Find out the cell max and min for Phi !
		!---------------------------------------!

		! Internal Faces
		do iFace=1,numberOfIntFaces

			iOwner = mesh%owner(iFace)
			iNeighbour = mesh%neighbour(iFace)

			do iComp=1,fComp

				phiC = phi(iOwner,iComp)
				phiN = phi(iNeighbour,iComp)

				maxPhi(iFace,iComp) = max(phiC, phiN)
				minPhi(iFace,iComp) = min(phiC, phiN)

			enddo

		enddo

		! Processor Boundaries
		do iBoundary=1,numberOfProcBound

			iProc = mesh%boundaries%procBound(iBoundary)
			is = mesh%boundaries%startFace(iProc)
			ie = is+mesh%boundaries%nFace(iProc)-1
			pNeigh = 0

			do iBFace=is,ie

				pNeigh = pNeigh+1
				iOwner = mesh%owner(iBFace)

				 do iComp=1,fComp
					phiC = phi(iOwner,iComp)
					phiN = phiGhost(pNeigh,iComp,iBoundary)
					maxPhi(iBFace,iComp) = max(phiC, phiN)
					minPhi(iBFace,iComp) = min(phiC, phiN)
				  enddo

			 enddo

		enddo

		! Real Boundaries
		patchFace=numberOfElements

		do iBoundary=1,numberOfRealBound

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1

			do iBFace=is,ie

				iOwner = mesh%owner(iBFace)
				patchFace = patchFace+1

				 do iComp=1,fComp
					 phiC = phi(iOwner,iComp)
					 phiN = phi(patchFace,iComp)
					 maxPhi(iBFace,iComp) = max(phiC, phiN)
					 minPhi(iBFace,iComp) = min(phiC, phiN)
				  enddo

			 enddo

		enddo

	end subroutine findFaceMinMax

! *********************************************************************************************************************

end module faceLimited