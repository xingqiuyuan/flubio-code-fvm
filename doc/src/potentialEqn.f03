!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

subroutine potentialEqn

!==================================================================================================================
! Description:
!! potentialEqn implements the Laplace equation used in potential flows.
!==================================================================================================================

    use transportEquation
    use fieldvar
    use globalMeshVar
    use gradient
    use physicalConstants

    implicit none

    integer :: nCorr, reusePC

    integer iFace, iBface, is ,ie , iBoundary, i, iProc, pNeigh, iOwner ! delete me
    real phiC, phiN
!------------------------------------------------------------------------------------------------------------------

    ! reset coeffs
    call Teqn%resetCoeffs()
    reusePC = 0

!--------------------------!
! Update "Old" terms       !
!--------------------------!

    call T%updateOldFieldValues()

!--------------------------!
! Assemble diffusion term  !
!--------------------------!

    call Teqn%diffFluxes%assembleDiffusionFluxes(T, nu)

!--------------------------!
! Assemble linear system   !
!--------------------------!

    ! Boundary conditions
    call Teqn%applyBoundaryConditions(T, nu)

    ! Relax transport equation
    if(steadySim==1) call Teqn%relaxEq(T)

    ! Save the RHS with no diffusion term, reuse coeffs in non-orthogonal corrections!
     Teqn%bCDdtQ = Teqn%bC

    ! Diffusion
    call Teqn%addDiffusionTerm()

    ! All boundary conditions must ste to zero gradient. Thus I need a reference value
    ! call Teqn%setReferenceValue(T, 0.0)

!------------------------!
! Solve poisson equation !
!------------------------!

    do nCorr=1, flubioOptions%nCorr+1

        call Teqn%solve(T, setGuess=1, reusePC=reusePC, reuseMat=nCorr)

        ! Update boundary fields
        call T%updateBoundaryField()

        ! Compute gradient
        call computeGradient(T)

        ! reset RHS
         Teqn%bC = Teqn%bCDdtQ

        ! Update with the new cross diffusion
         call Teqn%updateRhsWithCrossDiffusion(T, nu, T%nComp)

        reusePC = 1

    end do

    ! Compute mass fluxes by constructing total diffusion fluxes at mesh faces.
    ! It is like computing the gradient of T and multiply by Sf
     call Teqn%diffFluxes%totalDiffusionFluxes(T, nu)
     call Teqn%diffFluxes%boundaryDiffusionFluxes_gdiff(T, nu)
     vf = -Teqn%diffFluxes%fluxTf

    ! Now, reconstruct the cell center velocity from the mass fluxes
     call interpolate(rho, 'harmonic', -1)
     call recontructFromFaceValues(velocity%phi(1:numberOfElements,:), rho%phif*vf)

end subroutine potentialEqn

! *********************************************************************************************************************
