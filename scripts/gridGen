#!/bin/bash

case_folder=${PWD}
econn=${PWD}/grid/econn.msh
econn_graph=${PWD}/grid/econn.graph
PROGNAME=decomposeMesh

# folders
case_dir=${PWD}
grid_dir=${PWD}/grid
OF=$HOME/FLUBIO/examples/ofMesh/
OF_etc=$HOME/OpenFOAM/OpenFOAM-v2012/etc/
format="openfoam"

usage()
{
  cat << EO
        Usage: $PROGNAME [options]

        Decompose the mesh in N partitions

        Options:
EO
  cat <<EO | column -s\& -t

        -h|--help & show usage
        -d|--directory & path to openFoam case directory
        -f|--format & format to save the mesh
EO
}

SHORTOPTS="d:f:hp"
LONGOPTS="help,subdivisions:,tool:,format:,np:,etype:,variant:,parallel"

ARGS=$(getopt -s bash --options $SHORTOPTS  \
  --longoptions $LONGOPTS --name $PROGNAME -- "$@" )

eval set -- "$ARGS"

while true; do
   case $1 in
      -h|--help)
         usage
         exit 0
         ;;
      -d| --directory)
         shift
         ofcase="$1"
         ;;
      -f| --format)
         shift
         format="$1"
         ;;         
      --)
         shift
         break
         ;;
      *)
         shift
         break
         ;;
   esac
   shift
done

# Clean
rm -r $grid_dir/processor* econn*

# Decompose mesh
source $OF_etc/bashrc

cd $ofcase

decomposePar -cellDist -force

# Copy files to working foldes
for d in  processor*; do
mkdir -p $grid_dir/"$d";
cp "$d"/constant/polyMesh/* $grid_dir/"$d"
done

if [ "$format" != "openfoam" ]; then
  # Convert Foam files
  for d in  processor*; do
  let "nProcs=nProcs+1"
  let "p=p+1"
  done

  #parallel
  cd $case_dir
  echo
  echo "Converting parallel mesh..."
  mpirun -np $nProcs foam2Flubio

  echo
  echo "Renumeber mesh..."

  # Global indexing
  cd $grid_dir
  echo ${PWD}
  renumber -np $nProcs

fi

echo "FLUBIO: Mesh converted successfully!"