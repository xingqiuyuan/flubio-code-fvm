module vorticityField

    use globalMeshVar
    use globalTimeVar
    use fieldvar, only: velocity
    use flubioMpi
    use runTimeObject

    implicit none

    type, public, extends(fieldObj) :: vorticity

    contains
        procedure :: run =>  computeVorticity
    
    end type vorticity

contains

! *********************************************************************************************************************

    subroutine computeVorticity(this)

    !==================================================================================================================
    ! Description:
    !! computeVorticity computes the vorticity as:
    !! \begin{equation*}
    !!  \omega = \nabla \times \textbf{u}
    !! \end{equation*}
    !==================================================================================================================

        class(vorticity):: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i
    !------------------------------------------------------------------------------------------------------------------

        real :: wx, wy, wz
    !------------------------------------------------------------------------------------------------------------------

        if(mod(itime, this%runAt)==0) then
            do i=1,numberOfElements+numberOfBFaces
                wx = velocity%phiGrad(i,3,2) - velocity%phiGrad(i,2,3)
                wy = velocity%phiGrad(i,1,3) - velocity%phiGrad(i,3,1)
                wz = velocity%phiGrad(i,2,1) - velocity%phiGrad(i,1,2)
                this%taskField%phi(i,1) = wx
                this%taskField%phi(i,2) = wy
                this%taskField%phi(i,3) = wz
            end do
            call this%write()
        endif

    end subroutine computevorticity

!**********************************************************************************************************************

end module vorticityField
