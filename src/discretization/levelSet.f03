module levelSet

    !==================================================================================================================
    ! Description:
    !! MFCT contains the data structure and the methods to assemble the multidimensional flux correct transport
    !! limiters
    !==================================================================================================================

    use convectiveFluxes
    use fieldvar, only: velocity
    use meshvar
    use math
    use interpolation
    use physicalConstants
    use globalTimeVar
    use operators

    implicit none

    type, public :: ls
        
        type(flubioField) :: psiLS
        !! level set field

        type(flubioField) :: H
        !! Heaviside function

        type(flubioField) :: d
        !! delta function

        integer :: nCorr
        !! number of level set function corrections
        
        real :: eps
        !!
        
        real :: gamma
        !!

        real :: deltaTau
        !! fake time step for the LS function updat

        real, dimension(:), allocatable :: psi0
        !! initial psi

        contains

            procedure :: createLS
            procedure :: reintialisePsi
            procedure :: updatePsiLS
            procedure :: heaviside
            procedure :: delta
        !    procedure :: interfaceNormals
        !    procedure :: intefaceCurvature
        
    end type ls

contains
    
    subroutine createLS(this)

    !==================================================================================================================
    ! Description:
    !! createLS create the LS object.
    !==================================================================================================================

        class(ls) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------
        
        call this%psiLS%setUpField('psiLS', classType='scalar', nComp=1)
        call this%H%setUpWorkField('H', classType='scalar', nComp=1)
        call this%d%setUpWorkField('delta', classType='scalar', nComp=1)

        allocate(this%psi0(numberOfElements+numberOfBFaces))

        this%psi0 = 0.0
        this%psiLS%phi = 0.0
        this%d%phi = 0.0
        this%H%phi = 0.0

        ! PARSE THESE PARAMETER FROM DICT. Hardcode values are for test only
        this%eps = 1.5*0.01
        this%gamma = 0.75*0.01
        this%deltaTau = 0.1*0.01
        this%nCorr = 15

    end subroutine createLS
    
! *********************************************************************************************************************    

    subroutine reintialisePsi(this, field)

    !==================================================================================================================
    ! Description:
    !! psi0 reinitialises the level set function phi.
    !==================================================================================================================

        class(ls) :: this
    !------------------------------------------------------------------------------------------------------------------
    
        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        this%psi0 = (2.0*field%phi(:,1)-1.0)*this%gamma

        this%psiLS%phi(:,1) = this%psi0

        call this%psiLS%updateBoundaryField()
        call computeGradient(this%psiLS)

    end subroutine reintialisePsi
    
! *********************************************************************************************************************    

    subroutine updatePsiLS(this, field)

    !==================================================================================================================
    ! Description:
    !! updatePsiLS updates the level set function
    !==================================================================================================================

        class(ls) :: this
    !------------------------------------------------------------------------------------------------------------------

        type(flubioField) :: field
        !! field to use to compute LS.
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iCorr
    !------------------------------------------------------------------------------------------------------------------

        real :: magGrad
    !------------------------------------------------------------------------------------------------------------------

        call this%reintialisePsi(field)

        do iCorr=1,this%nCorr

            do iElement=1,numberOfElements

                magGrad = sqrt(this%psiLS%phiGrad(iElement,1,1)**2 + &
                               this%psiLS%phiGrad(iElement,2,1)**2 + &
                                this%psiLS%phiGrad(iElement,3,1)**2)

                this%psiLS%phi(iElement,1) = this%psi0(iElement) +  &
                                             this%psi0(iElement)/sqrt(this%psi0(iElement)**2 + 1e-12)*(1.0 - magGrad)*this%deltaTau

            end do

            ! Update LS
            call this%psiLS%updateBoundaryField()

            ! Compute new gradient
            call computeGradient(this%psiLS)

            call this%psiLS%fieldVerbose()

        end do

    end subroutine updatePsiLS

! *********************************************************************************************************************    

    subroutine heaviside(this)

    !==================================================================================================================
    ! Description:
    !! heaviside computes \begin{equation} H(\phi) \end{equation}.
    !==================================================================================================================

        class(ls) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: pi, tmp
    !------------------------------------------------------------------------------------------------------------------

        pi = acos(-1.0)

        do iElement=1,numberOfElements + numberOfBFaces

            tmp = this%psiLS%phi(iElement,1)

            if(tmp < -this%eps) then
                this%H%phi(iElement,1) = 0.0
            elseif(tmp >= -this%eps .and. tmp <= this%eps) then
                this%H%phi(iElement,1) = 0.5*(1.0 + tmp/this%eps + sin(pi*tmp/this%eps)/pi)
            else
                this%H%phi(iElement,1) = 1.0
            end if

        end do

        call this%H%updateGhosts()

    end subroutine heaviside

! *********************************************************************************************************************

    subroutine delta(this)

    !==================================================================================================================
    ! Description:
    !! delta computes \begin{equation} \delta(\phi) \end{equation}
    !==================================================================================================================

        class(ls) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: pi, tmp
    !------------------------------------------------------------------------------------------------------------------

        pi = acos(-1.0)

        do iElement=1,numberOfElements + numberOfBFaces

            tmp = this%psiLS%phi(iElement,1)

            if(abs(tmp) > this%eps) then
                this%d%phi(iElement,1) = 0.0
            else
                this%d%phi(iElement,1) = (0.5/this%eps)*(1.0 + cos(pi*tmp/this%eps))
            end if

        end do

        call this%d%updateGhosts()

    end subroutine delta

! *********************************************************************************************************************

end module levelSet