
# setup the required thirdparty libs

include( ${cmake_local}/thirdparty/configure_mpi )
include( ${cmake_local}/thirdparty/configure_petsc )
include( ${cmake_local}/thirdparty/configure_boost )
include( ${cmake_local}/thirdparty/configure_json )
include( ${cmake_local}/thirdparty/configure_amgcl )
include( ${cmake_local}/thirdparty/configure_qcontainers )
include( ${cmake_local}/thirdparty/configure_penf )
include( ${cmake_local}/thirdparty/configure_fossil )
include( ${cmake_local}/thirdparty/configure_vtk )
include( ${cmake_local}/thirdparty/configure_ttb)

# set paths used in flubio build

set( thirdparty_lib_dirs 
  ${lib_dir_fossil}
  ${lib_dir_json}
  ${lib_dir_penf}
  ${lib_dir_qcontainers}
  ${lib_dir_vtk}
  ${lib_dir_ttb}
)

set( thirdparty_lib_names 
  ${lib_names_mpi}
  ${lib_names_petsc}
  ${lib_names_fossil}
  ${lib_names_json}
  ${lib_names_penf}
  ${lib_names_qcontainers}
  ${lib_names_vtk}
  ${lib_names_ttb}
)

set( thirdparty_inc_dirs
  ${inc_dir_fossil}
  ${inc_dir_json}
  ${inc_dir_penf}
  ${inc_dir_qcontainers}
  ${inc_dir_vtk}
  ${inc_dir_ttb}
)