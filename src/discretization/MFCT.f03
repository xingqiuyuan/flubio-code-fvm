module fluxCorrectedTransport

!==================================================================================================================
! Description:
!! MFCT contains the data structure and the methods to assemble the multidimensional flux correct transport
!! limiters
!==================================================================================================================

    use convectiveFluxes
    use fieldvar, only: velocity
    use meshvar
    use math
    use interpolation
    use physicalConstants
    use globalTimeVar
    use operators

    implicit none

    type, public :: fct

        real, dimension(:,:), allocatable :: phiL
        !! low order approximation

        real, dimension(:,:), allocatable :: Pp
        !! sum of inflows for anti-diffusion fluxes

        real, dimension(:,:), allocatable :: Pm
        !! sum of outflows for anti-diffusion fluxes

        real, dimension(:,:), allocatable :: Qp
        !! net fluxes in order to reach the local maximum

        real, dimension(:,:), allocatable :: Qm
        !! net fluxes in order to reach the local minimum

        real, dimension(:,:), allocatable :: minPhi
        !! net fluxes in order to reach the local maximum

        real, dimension(:,:), allocatable :: maxPhi
        !! net fluxes in order to reach the local minimum

        type(flubioField):: lambdap
        !! lambda plus

        type(flubiofield) :: lambdam
        !! lambda minus

        real, dimension(:,:), allocatable :: lambda
        !! cell limiter

        real, dimension(:,:), allocatable :: mfr
        !! compression mass fluxes

        real, dimension(:), allocatable :: phiLowerBound
        !! lower bound for phi

        real, dimension(:), allocatable :: phiUpperBound
        !! upper bound for phi

        contains
                
            procedure :: createFCT
            procedure :: computePhiL
            procedure :: computePhiLFromFf
            procedure :: computePhiFromA
            procedure :: computeQ
            procedure :: computeP
            procedure :: computeLambda
            procedure :: FCTbounding
            procedure :: computeInterfaceCompression
            procedure :: addInterfaceCompression

    end type fct

contains

    subroutine createFCT(this, nComp)

    !==================================================================================================================
    ! Description:
    !! createFCT is the class constructor.
    !==================================================================================================================

        class(fct) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: nComp
    !------------------------------------------------------------------------------------------------------------------

        allocate(this%phiL(numberOfElements,nComp))

        allocate(this%Pp(numberOfElements,nComp))
        allocate(this%Pm(numberOfElements,nComp))
        allocate(this%Qp(numberOfElements,nComp))
        allocate(this%Qm(numberOfElements,nComp))
        allocate(this%minPhi(numberOfElements,nComp))
        allocate(this%maxPhi(numberOfElements,nComp))

        call this%lambdap%setUpWorkField('lambdap', classType='scalar', nComp=nComp)
        call this%lambdam%setUpWorkField('lambdam', classType='scalar', nComp=nComp)
        allocate(this%lambda(numberOfFaces,nComp))
        allocate(this%mfr(numberOfFaces,nComp))

        allocate(this%phiLowerBound(nComp))
        allocate(this%phiUpperBound(nComp))

        this%phiL = 0.0
        this%Pp = 0.0
        this%Pm = 0.0
        this%Qp = 0.0
        this%Qm = 0.0
        this%mfr = 0.0
        this%minPhi = 0.0
        this%maxPhi = 0.0
        this%lambda = 1.0
        this%phiLowerBound = 0.0
        this%phiUpperBound = 1.0

    end subroutine createFCT

! *********************************************************************************************************************

    subroutine destroyFCT(this)

    !==================================================================================================================
    ! Description:
    !! destroyFCT is the class deconstructor.
    !==================================================================================================================

        class(fct) :: this
    !------------------------------------------------------------------------------------------------------------------

        deallocate(this%phiL)

        deallocate(this%Pp)
        deallocate(this%Pm)
        deallocate(this%Qp)
        deallocate(this%Qm)
        deallocate(this%minPhi)
        deallocate(this%maxPhi)
        deallocate(this%lambda)

        call this%lambdap%destroyWorkField()
        call this%lambdam%destroyWorkField()

    end subroutine destroyFCT

! *********************************************************************************************************************

    subroutine computePhiL(this, field)

    !==================================================================================================================
    ! Description:
    !! computePhiL computes the the updated value of phi using a low order scheme (upwind).
    !==================================================================================================================

        class(fct) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp, nComp
    !------------------------------------------------------------------------------------------------------------------

        real :: F(numberOfFaces,1)

        real :: tmp(numberOfElements, field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        F = faceFlux(field=velocity, opt=1, coeff=1.0)

        tmp = div(F, field, opt=0)

        do iComp=1,nComp
            this%phiL(1:numberOfElements,iComp) = field%phio(1:numberOfElements,iComp) - &
                                                  dtime*tmp(1:numberOfElements, iComp)/mesh%volume(1:numberOfElements)
        end do

    end subroutine computePhiL

! *********************************************************************************************************************

    subroutine computePhiLFromFf(this, F, field)

    !==================================================================================================================
    ! Description:
    !! computePhiLFromFf computes the the updated value of phi using a given flux.
    !==================================================================================================================

        class(fct) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: F(numberOfFaces,1)

        real :: tmp(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        tmp = div(F)

        do iComp=1,field%nComp
            this%phiL(1:numberOfElements,iComp) = field%phio(1:numberOfElements,iComp) - &
                    dtime*tmp(1:numberOfElements)/mesh%volume(1:numberOfElements)
        end do

    end subroutine computePhiLFromFf

! *********************************************************************************************************************

    subroutine computePhiFromA(this, A, field)

    !==================================================================================================================
    ! Description:
    !! computePhiFromA computes the the updated value of phi using a given flux.
    !==================================================================================================================

        class(fct) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: A(numberOfFaces,1)

        real :: tmp(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        tmp = div(A)

        do iComp=1,field%nComp
            field%phi(1:numberOfElements,iComp) = this%phiL(1:numberOfElements,iComp) - &
                    dtime*tmp(1:numberOfElements)/mesh%volume(1:numberOfElements)
        end do

    end subroutine computePhiFromA

! *********************************************************************************************************************

    subroutine computeInterfaceCompression(this, local_mf, field, cAlpha, rScheme)

    !==================================================================================================================
    ! Description:
    !! computeInterfaceCompression computes the the updated value of phi using a given flux.
    !==================================================================================================================

        class(fct) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp, iFace

        integer :: rScheme
    !------------------------------------------------------------------------------------------------------------------

        real :: cAlpha

        real :: arf

        real :: dot

        real :: small

        real :: gmag

        real :: maxFlux

        real :: c, theta

        real :: local_mf(numberOfFaces,1)

        real :: mfr(numberOfFaces,1)

        real :: phiGrad_f(numberOfFaces,3,field%nComp)
        !! field gradient interpolated at mesh faces

        real :: nf(numberOfFaces,3,field%nComp)

        real :: tmp(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        ! Compute nf
        call interpolateGradientCorrected(phiGrad_f, field, opt=1, fComp=field%nComp)

        do iComp=1,field%ncomp
            do iFace=1,numberOfFaces
                gmag = sqrt(phiGrad_f(iFace,1,iComp)**2 + phiGrad_f(iFace,2,iComp)**2 + phiGrad_f(iFace,3,iComp)**2)
                nf(iFace,1:3,iComp) = phiGrad_f(iFace,1:3,iComp)/(gmag + small)
            end do
        end do

        ! Interpolate the field at cell faces using a user defined 2nd order scheme
        if(rScheme==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(rScheme >= 2 .and. rScheme <= 4) then
            call interpolate(field, interpType='gradientbased', opt=rScheme)
        else
            call interpolate(field, interpType='tvd', opt=rScheme)
        end if

        ! Compute compression fluxes
        maxFlux = maxval(abs(local_mf(:,1)))
        do iComp=1,field%nComp
            do iFace=1,numberOfFaces
                dot = dot_product(nf(iFace,:, iComp), mesh%Sf(iFace,:)/mesh%area(iFace))
                arf = field%phif(iFace,iComp)*(1.0-field%phif(iFace,iComp))
                theta = acos(abs(dot))
                c = min(0.5*(1.0+cos(2.0*theta)), 1.0)
                !this%mfr(iFace,iComp) = min(c*abs(local_mf(iFace,1)), maxFlux)*dot*arf
                 this%mfr(iFace,iComp) = c*abs(local_mf(iFace,1))*dot*arf
            end do
        end do

    end subroutine computeInterfaceCompression

! *********************************************************************************************************************

    subroutine addInterfaceCompression(this, field)

    !==================================================================================================================
    ! Description:
    !! addInterfaceCompression computes the the updated value of phi using a given flux.
    !==================================================================================================================

        class(fct) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: tmp(numberOfElements)
    !------------------------------------------------------------------------------------------------------------------

        tmp = div(this%mfr)

        do iComp=1,field%nComp
            field%phi(1:numberOfElements,iComp) = field%phi(1:numberOfElements,iComp) - &
                    dtime*tmp(1:numberOfElements)/mesh%volume(1:numberOfElements)
        end do

    end subroutine addInterfaceCompression

! *********************************************************************************************************************

    subroutine computeQ(this, field)

    !==================================================================================================================
    ! Description:
    !! computeQ computes the Q+ and Q- as prescribed in the fct algorithm.
    !==================================================================================================================

        class(fct) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iComp
    !------------------------------------------------------------------------------------------------------------------

        real :: phia(field%nComp)

        real :: phib(field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        do iComp=1,field%nComp

            do iElement=1,numberOfElements

                phia(iComp) = min(this%phiUpperBound(iComp), this%maxPhi(iElement,iComp))
                phib(iComp) = max(this%phiLowerBound(iComp), this%minPhi(iElement,iComp))

                this%Qp(iElement,iComp) = (phia(iComp) - this%phiL(iElement,iComp))*mesh%volume(iElement)/dtime
                this%Qm(iElement,iComp) = (this%phiL(iElement,iComp) - phib(iComp))*mesh%volume(iElement)/dtime

            end do

        end do

    end subroutine computeQ

! *********************************************************************************************************************

    subroutine computeP(this, A, nComp)

    !==================================================================================================================
    ! Description:
    !! computeP computes the P+ and P- as prescribed in the fct algorithm.
     !==================================================================================================================

        class(fct) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer  :: iElement, iBoundary, iFace, iBFace, iComp, nComp

        integer :: iOwner, iNeighbour
    !------------------------------------------------------------------------------------------------------------------

        real :: A(numberOfFaces, nComp)
    !------------------------------------------------------------------------------------------------------------------

        ! Compute P+ and P-
        do iFace = 1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            do iComp = 1, nComp

                ! flux going away
                if(A(iFace,iComp)>0.0) then

                    this%Pm(iOwner,iComp) = this%Pm(iOwner,iComp) + A(iFace,iComp)
                    this%Pp(iNeighbour,iComp) = this%Pp(iNeighbour,iComp) + A(iFace,iComp)

                ! flux coming in
                else

                    this%Pp(iOwner,iComp) = this%Pp(iOwner,iComp) - A(iFace,iComp)
                    this%Pm(iNeighbour,iComp) = this%Pm(iNeighbour,iComp) - A(iFace,iComp)

                end if

            end do

        enddo

        ! Boundary faces
        do iBFace=numberofIntFaces+1,numberOfFaces

            iOwner = mesh%owner(iBFace)

            do iComp=1,nComp

                ! flux going away
                if(A(iBFace,iComp)>0.0) then

                    this%Pm(iOwner,iComp) = this%Pm(iOwner,iComp) + A(iBFace,iComp)

                    ! flux coming in
                else
                    this%Pp(iOwner,iComp) = this%Pp(iOwner,iComp) - A(iBFace,iComp)

                end if

            end do

        end do

    end subroutine computeP

! *********************************************************************************************************************

    subroutine computeLambda(this, A, nComp)

    !==================================================================================================================
    ! Description:
    !! computeLambda computes the lambda+, lambda- and lambda (face limiter) as prescribed in the fct algorithm.
    !==================================================================================================================

        class(fct) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer  :: iElement, iBoundary, iFace, iBFace, iProc, is, ie, iComp, nComp, pNeigh

        integer :: iOwner, iNeighbour
    !------------------------------------------------------------------------------------------------------------------

        real :: A(numberOfFaces, nComp)
    !------------------------------------------------------------------------------------------------------------------

        ! Compute cell center limiter
        do iComp=1,nComp

            do iElement=1,numberOfElements

                if(this%Pp(iElement,iComp) > 0.0) then
                    this%lambdap%phi(iElement,iComp) = min(1.0, this%Qp(iElement,iComp)/this%Pp(iElement,iComp) )
                else
                    this%lambdap%phi(iElement,iComp) = 0.0
                end if

                if(this%Pm(iElement,iComp) > 0.0) then
                    this%lambdam%phi(iElement,iComp) = min(1.0, this%Qm(iElement,iComp)/this%Pm(iElement,iComp))
                else
                    this%lambdam%phi(iElement,iComp) = 0.0
                end if

            end do

        end do

        ! Update ghost values for lambdas
        call this%lambdap%updateGhosts()
        call this%lambdam%updateGhosts()

        ! Compute the face limiters
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            do iComp = 1,nComp

                if(A(iFace,iComp)>=0.0) then
                    this%lambda(iFace,iComp) = min(this%lambdap%phi(iNeighbour,iComp),this%lambdam%phi(iOwner,iComp))
                else
                    this%lambda(iFace,iComp) = min(this%lambdam%phi(iNeighbour,iComp),this%lambdap%phi(iOwner,iComp))
                end if

            end do

        enddo

        ! Processor boundaries
        do iBoundary=1,numberOfProcBound

            iProc = mesh%boundaries%procBound(iBoundary)
            is = mesh%boundaries%startFace(iProc)
            ie = is+mesh%boundaries%nFace(iProc)-1
            pNeigh = 0

            do iBFace=is,ie

                pNeigh = pNeigh+1
                iOwner = mesh%owner(iBFace)

                do iComp = 1, nComp

                    if(A(iBFace,iComp)>=0.0) then
                        this%lambda(iBFace,iComp) = min(this%lambdap%ghosts(pNeigh,iComp,iBoundary), this%lambdam%phi(iOwner,iComp))
                    else
                        this%lambda(iBFace,iComp) = min(this%lambdap%phi(iOwner,iComp), this%lambdam%ghosts(pNeigh,iComp,iBoundary))
                    end if

                end do

            enddo

        enddo

    end subroutine computeLambda

! *********************************************************************************************************************

    subroutine FCTbounding(this, local_mf, field, interpType, interpOpt)

    !==================================================================================================================
    ! Description:
    !! FCTbounding computes the FCT limiters
    !==================================================================================================================

        class(fct) :: this

        type(flubioField) :: field
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) :: interpType
    !------------------------------------------------------------------------------------------------------------------

        integer  :: iElement, iBoundary, iFace, iBFace, iProc, is, ie, iComp, nComp, pNeigh

        integer :: iOwner, iNeighbour

        integer :: interpOpt
    !------------------------------------------------------------------------------------------------------------------
        
        real :: FL(numberOfFaces,1)

        real :: FH(numberOfFaces,1)

        real :: A(numberOfFaces,1)

        real :: local_mf(numberOfFaces,1)
    !------------------------------------------------------------------------------------------------------------------

        this%Pp = 0.0
        this%Pm = 0.0
        this%Qp = 0.0
        this%Qm = 0.0
        this%lambdap%phi = 1.0
        this%lambdam%phi =1.0
        this%lambda = 1.0

        nComp = field%nComp

        ! compute compression fluxes
        this%mfr = 0.0
        call this%computeInterfaceCompression(local_mf, field, 1.0, 0)
        !call this%computeInterfaceCompression(local_mf, field, 1.0, 6)

        ! Interpolate the field at cell faces using upwind
        call interpolate(field, interpType='tvd', opt=0)

        do iComp=1,nComp
            FL(:,iComp) = field%phif(:,iComp)*local_mf(:,1) + this%mfr(:,iComp)
        end do

        ! Interpolate the field at cell faces using a user defined 2nd order scheme
        if(interpOpt==1) then
            call interpolate(field, interpType='linear', opt=-1)
        elseif(interpOpt >= 2 .and. interpOpt <= 4) then

            this%mfr = 0.0
            call this%computeInterfaceCompression(local_mf, field, 1.0, interpOpt)

            call interpolate(field, interpType='gradientbased', opt=interpOpt)

        else

            this%mfr = 0.0
            call this%computeInterfaceCompression(local_mf, field, 1.0, interpOpt)

            call interpolate(field, interpType='tvd', opt=interpOpt)

        end if
        
        do iComp=1,nComp
            FH(:,iComp) = field%phif(:,iComp)*local_mf(:,1) + this%mfr(:,iComp)
        end do

        ! Compute the anti-diffusive fluxes A
        A = FH - FL

        ! Compute the low order approximation for phi
        call this%computePhiLFromFf(FL, field)

        ! Find min and max among neighbours
        call findCellMinMax(this%minPhi, this%maxPhi, field%phi, field%ghosts, field%nComp)

        ! Compute Q+ and Q-
        call this%computeQ(field)

        ! Compute P+ and P-
        call this%computeP(A, field%nComp)

        ! Compute FCT limiter
        call this%computeLambda(A, field%nComp)

        ! Compute the high order approximation for phi using the corrected fluxes
        A = A*this%lambda
        call this%computePhiFromA(A, field)

        !call this%addInterfaceCompression(field)

    end subroutine FCTbounding

! *********************************************************************************************************************

end module fluxCorrectedTransport