module probes

    use meshvar
    use flubioFields
    use m_strings
    use qvector_m

    implicit none

    ! Base class
    type, public :: sampleObj
        character(len=:), allocatable :: name
        character(len=:), allocatable :: type
        character(len=:), allocatable :: interpType
        character(len=:), allocatable :: timeTag
        character(len=15), dimension(:), allocatable :: fields
        type(flubioField) :: fieldToSample
        integer :: nn
    end type sampleObj

    ! Extend for probes
    type, extends(sampleObj), public :: probeObj
        integer :: nProbes
        type(qvector_t) :: ownedProbes
        real, dimension(:), allocatable :: x
        real, dimension(:), allocatable :: y
        real, dimension(:), allocatable :: z
        real, dimension(:,:), allocatable :: probeValues

    contains
        procedure :: execute => executeProbing
        procedure :: findOwnedProbes
        procedure :: writeProbesToFile
        procedure :: clear
    end type probeObj

    type, public :: probeObjList
        type(probeObj), dimension(:), allocatable :: task
    end type probeObjList

contains

! *********************************************************************************************************************

    subroutine executeProbing(this, iTime)

    !==================================================================================================================
    ! Description:
    !! executeProbing runs the post processing task for the target probing object.
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: timeTag
    !------------------------------------------------------------------------------------------------------------------

        integer :: iProbe, targetProbe, iTime, iField, iComp, nComp, ierr

        type(kdtree2_result), allocatable :: nearestNeighbours(:)
    !------------------------------------------------------------------------------------------------------------------

        real :: probe(3)

        real, dimension(:), allocatable :: probeValue

        real, dimension(:), allocatable :: sbuf, rbuf
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        write(timeTag,'(i0)') iTime
        this%timeTag = trim(timeTag)

        ! Loop over Fields
        do iField=1,size(this%fields)

            nComp = getFieldSize(this%fields(iField),iTime)

            allocate(sbuf(this%nProbes))
            allocate(rbuf(this%nProbes))
            allocate(probeValue(nComp))

            ! Load the field to sample
            call this%fieldToSample%createField(nComp)
            this%fieldToSample%fieldName(1) = this%fields(iField)
            call this%fieldToSample%readFromFile(iTime)
            call this%fieldToSample%linearInterpolation()

            allocate(this%probeValues(this%nProbes, nComp))
            this%probeValues = 0.0

            ! Start probing
            do iProbe=1,this%ownedProbes%size()

                ! Get the target probe
                call this%ownedProbes%getat(iProbe, targetProbe, found)
                call checkProbe(found)

                probe(1) = this%x(targetProbe)
                probe(2) = this%y(targetProbe)
                probe(3) = this%z(targetProbe)

                ! Interpolate
                if(this%interpType == 'nearestNeighbour') then
                    probeValue = nearestNeighbourCell(this%fieldToSample, probe)
                elseif(this%interpType == 'neighToProbe') then
                    !probeValue = neighToProbe(this%fieldToSample, probe)
                elseif(this%interpType == 'faceToProbe') then
                    probeValue = faceToProbe(this%fieldToSample, probe)
                else
                    call flubioStopMsg('ERROR: unknown interpolation type '//this%interpType)
                end if

                this%probeValues(targetProbe,1:nComp) = probeValue

            end do

            ! Gather the probe values from different processor
            do iComp=1,nComp
                sbuf(1:this%nProbes) = this%probeValues(1:this%nProbes,iComp)
                call mpi_Reduce(sbuf, rbuf, this%nProbes, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
                this%probeValues(1:this%nProbes, iComp) = rbuf
            end do

            ! writeToFile, master node ONLY!
            if(id == 0) call this%writeProbesToFile(this%fields(iField))

            ! Deallocate arrays and get ready for the next probe object
            deallocate(sbuf)
            deallocate(rbuf)
            deallocate(this%probeValues)
            call this%fieldToSample%destroyField()

        end do

    end subroutine executeProbing

! *********************************************************************************************************************

    subroutine findOwnedProbes(this)

    !==================================================================================================================
    ! Description:
    !! findOwnedProbes finds the probes hosted by a processor
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: p, index
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------

        do p=1,this%nProbes

            point(1) = this%x(p)
            point(2) = this%y(p)
            point(3) = this%z(p)

            index = mesh%findPointOwnership(point)

            if(index>-1) then
                call this%ownedProbes%addlast(p)
            end if

        end do

    end subroutine findOwnedProbes

! *********************************************************************************************************************

    subroutine writeProbesToFile(this, fieldName)

    !==================================================================================================================
    ! Description:
    !! writeProbesToFile write the probe values to a file.
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=*) fieldName
    !------------------------------------------------------------------------------------------------------------------

        integer :: n
    !------------------------------------------------------------------------------------------------------------------

        open(1, file='postProc/sampling/'//this%timeTag//'/'//trim(this%name)//'-'//trim(fieldName)//'.dat')
            write(1,*) '#   ', 'x   y   z   ', fieldName
            do n=1,this%nProbes
                write(1,*) this%x(n), this%y(n), this%z(n), this%probeValues(n,:)
            end do
        close(1)

    end subroutine writeProbesToFile

! *********************************************************************************************************************

    subroutine clear(this)

    !==================================================================================================================
    ! Description:
    !! clear deallocates some member data for the pbe class.
    !==================================================================================================================

        class(probeObj) :: this
    !------------------------------------------------------------------------------------------------------------------

        ! Deallocate for the next probe if any
        deallocate(this%z)
        deallocate(this%x)
        deallocate(this%y)
        deallocate(this%fields)
        call this%ownedProbes%clear()

    end subroutine clear

! *********************************************************************************************************************

    subroutine checkProbe(found)

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        if(.not. found) call flubioStopMsg('ERROR: probe not found!')

    end subroutine checkProbe

!**********************************************************************************************************************
!                                                Interpolation methods
!**********************************************************************************************************************

    function nearestNeighbourCell(field, probe) result(probeValue)

        type(flubioField) :: field

        type(kdtree2_result) :: nearestNeighbour(1)
    !------------------------------------------------------------------------------------------------------------------

        real :: probe(3), probeValue(field%nComp)
    !------------------------------------------------------------------------------------------------------------------

        mesh%cellCenters(:, numberOfElements+numberOfBFaces+1) = probe
        call kdtree2_n_nearest_around_point(mesh%meshTree, numberOfElements+numberOfBFaces+1, 1, 1, nearestNeighbour)

        probeValue = field%phi(nearestNeighbour(1)%idx,:)

    end function nearestNeighbourCell

! *********************************************************************************************************************

    function faceToProbe(field, probe) result(probeValue)

        type(flubioField) :: field

        type(kdtree2_result) :: nearestNeighbour(1)
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, nFaces, targetFace, iOwner, hostCell, boundaryProbe
    !------------------------------------------------------------------------------------------------------------------

        real :: probe(3), r(3), faceDistance, probeValue(field%nComp), w, wsum, small
    !------------------------------------------------------------------------------------------------------------------

        small = 1e-10

        boundaryProbe = 0

        mesh%cellCenters(:, numberOfElements+numberOfBFaces+1) = probe
        call kdtree2_n_nearest_around_point(mesh%meshTree, numberOfElements+numberOfBFaces+1, 1, 1, nearestNeighbour)
        hostCell = nearestNeighbour(1)%idx

        ! If the host cell is a boundary face, get its owner instead
        if(hostCell > numberOfElements) then
            iFace = numberOfIntFaces + (hostCell-numberOfElements)
            iOwner = mesh%owner(iFace)
            hostCell = iOwner
            boundaryProbe = 1
        end if

        ! Get list of face
        probeValue = 0.0
        wsum = 0.0
        nFaces = mesh%iFaces(hostCell)%csize

        do iFace=1,nFaces

            targetFace = mesh%iFaces(hostCell)%col(iFace)
            r = mesh%fcentroid(targetFace,:) - probe
            faceDistance = r(1)**2 + r(2)**2 + r(3)**2

            w = 1.0/(faceDistance + small)
            wsum = wsum + w
            probeValue = probeValue + w*field%phif(targetFace,:)

            ! If the weigh is bigger than 1e+3, return the face value
            if(boundaryProbe==1 .and. w>1000.0) then
                probeValue = field%phif(targetFace,:)
                return
            end if

        end do

        ! Add cell center value as well
        w = 1.0/(nearestNeighbour(1)%dis + small)
        probeValue = probeValue + w*field%phi(hostCell,:)
        wsum = wsum + w

        ! Divide with weights
        probeValue = probeValue/wsum

    end function faceToProbe

! *********************************************************************************************************************

end module probes