module meshPartitioning

    use serialmeshvar
    use userDefininedTypes
    use qvector_m
    use qhashtbl_m

    implicit none

    type, public :: interface
        type(qvector_t) :: faces
        integer, dimension(:,:), allocatable :: processorConnectivitiesTable
        type(qvector_t), dimension(:), allocatable :: procNeighs
    end type interface

    type, public :: nestedVector
        type(qvector_t), dimension(:), allocatable :: faces
        contains
            procedure :: initialiseInnerVector
    end type nestedVector

    type, public :: vecVec
        type(qvector_t) :: v
    end type vecVec

    type, public :: meshDecomp

        integer :: nid

        integer, dimension(:), allocatable :: epart

        type(qvector_t), dimension(:), allocatable :: cellDist
        type(qvector_t), dimension(:), allocatable :: faceDist
        type(qvector_t), dimension(:), allocatable :: ownerDist
        type(qvector_t), dimension(:), allocatable :: neighDist
        type(qvector_t), dimension(:), allocatable :: pointDist

        type(nestedVector), dimension(:), allocatable :: boundaryFaceDist
        type(nestedVector), dimension(:), allocatable :: procFaces
        type(interface) :: processorFacesList

        integer, dimension(:), allocatable :: globalToLocalCellMap
        integer, dimension(:), allocatable :: globalToLocalFaceMap
        integer, dimension(:,:), allocatable :: globalToLocalPointMap ! fast but expensive
        !type(qhashtbl_t), dimension(:), allocatable :: globalToLocalPointMap ! cheaper, but slower
        !type(qhashtbl_t), dimension(:), allocatable :: globalToLocalFaceMap

        integer :: numberOfBoundaries
        integer, dimension(:), allocatable :: numberOfPoints
        integer, dimension(:), allocatable :: numberOfIntFaces
        integer, dimension(:), allocatable :: numberOfProcessorFaces
        integer, dimension(:,:), allocatable :: numberOfBoundaryFaces
        integer, dimension(:), allocatable :: numberOfProcBoundaries

        integer, dimension(:,:), allocatable :: startFace
        integer, dimension(:,:), allocatable :: nFaces

        contains

            procedure :: readMetisMeshDecomposition
            procedure :: initialiseDecomposedMesh
            procedure :: facePartitions
            procedure :: pointPartitions
            procedure :: setProcessorBoundaries

            ! Output procedures
            procedure :: writeCellProcAddressing
            procedure :: writeFaceProcAddressing
            procedure :: writeBoundaryProcAddressing
            procedure :: writeFacePartitions
            procedure :: writeOwnerPartitions
            procedure :: writeNeighbourPartitions
            procedure :: writePointPartitions
            procedure :: writeBoundaryPartitions
            procedure :: saveCellDistToFile

    end type meshDecomp

contains

    subroutine initialiseDecomposedMesh(this)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: id, isize, nid
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(id)/8
        nid = this%nid

        ! Cell distribution
        allocate(this%numberOfIntFaces(this%nid))
        allocate(this%numberOfProcessorFaces(this%nid))
        allocate(this%numberOfBoundaryFaces(this%nid, numberOfBoundaries))
        this%numberOfIntFaces = 0
        this%numberOfProcessorFaces = 0
        this%numberOfBoundaryFaces = 0

        allocate(this%cellDist(nid))
        do id=1,this%nid
            call this%cellDist(id)%new(1, isize, QVECTOR_RESIZE_LINEAR)
        end do

        allocate(this%epart(numberOfElements))
        allocate(this%globalToLocalCellMap(numberOfElements))
        this%epart = -1
        this%globalToLocalCellMap = -1

        allocate(this%globalToLocalFaceMap(numberOfFaces))
        this%globalToLocalFaceMap = -1

        ! Face Distribution
        allocate(this%pointDist(this%nid))
        allocate(this%faceDist(this%nid))
        allocate(this%ownerDist(this%nid))
        allocate(this%neighDist(this%nid))
        allocate(this%numberOfProcBoundaries(this%nid))
        allocate(this%boundaryFaceDist(this%nid))
        allocate(this%procFaces(this%nid))
        !allocate(this%globalToLocalFaceMap(this%nid))
        allocate(this%startFace(this%nid, numberOfBoundaries))
        allocate(this%nFaces(this%nid, numberOfBoundaries))
        allocate(this%processorFacesList%processorConnectivitiesTable(this%nid, this%nid))
        allocate(this%processorFacesList%procNeighs(this%nid))

        do id=1,this%nid
            call this%pointDist(id)%new(1, isize, QVECTOR_RESIZE_LINEAR)
            call this%faceDist(id)%new(1, isize, QVECTOR_RESIZE_LINEAR)
            call this%ownerDist(id)%new(1, isize, QVECTOR_RESIZE_LINEAR)
            call this%neighDist(id)%new(1, isize, QVECTOR_RESIZE_LINEAR)
            !call this%globalToLocalFaceMap(id)%new(1, isize)
            call this%procFaces(id)%initialiseInnerVector(this%nid)
            call this%boundaryFaceDist(id)%initialiseInnerVector(numberOfBoundaries)
            call this%processorFacesList%procNeighs(id)%new(1, isize, QVECTOR_RESIZE_LINEAR)
        end do
        call this%processorFacesList%faces%new(1, isize, QVECTOR_RESIZE_LINEAR)

        ! Point distribuiton
        allocate(this%numberOfPoints(this%nid))

        allocate(this%globalToLocalPointMap(numberOfPoints, this%nid))

        !allocate(this%globalToLocalPointMap(this%nid))
        !do id=1,this%nid
        !    call this%globalToLocalPointMap(id)%new(1, isize)
        !end do

        this%processorFacesList%processorConnectivitiesTable = 0

    end subroutine initialiseDecomposedMesh

!**********************************************************************************************************************

    subroutine readMetisMeshDecomposition(this, nid)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: nProcs
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, iNode, id, iFill, nid, isize
    !------------------------------------------------------------------------------------------------------------------

        write(*,*)
        write(*,*) 'FLUBIO: initializing mesh decomposition'
        write(*,*) '========================================================================================='
        write(*,*)

        this%nid = nid
        write(nProcs,'(i0)') nid

        call this%initialiseDecomposedMesh()

        write(*,*) 'FLUBIO: distributing cells...'
        write(*,*)

        ! Read cell distribuiton (epart) from metis files
        open(1, file='grid/econn.msh.epart.'//nProcs)
            do iElement=1,numberOfElements
                read(1,*) this%epart(iElement)
            end do
        close(1)

        ! Fill cell distribution list
        do iElement=1,numberOfElements
            call this%cellDist(this%epart(iElement)+1)%addlast(iElement)
            this%globalToLocalCellMap(iElement) =  this%cellDist(this%epart(iElement)+1)%size()
        end do

    end subroutine readMetisMeshDecomposition

!**********************************************************************************************************************

    subroutine facePartitions(this)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: label
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iFace, targetFace, mappedFace, iFill, id, isize

        integer :: iOwner, iNeighbour, mappedOwner, mappedNeighbour, is, ie

        integer :: ownerProc, neighProc
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        write(*,*) 'FLUBIO: distributing owners, neghbours, faces...'
        write(*,*)

        ! Internal faces
        do iFace=1,numberOfIntFaces

            iOwner = mesh%owner(iFace)
            iNeighbour = mesh%neighbour(iFace)

            ownerProc = this%epart(iOwner)+1
            neighProc = this%epart(iNeighbour)+1

            ! Check if it is an internal face
            if(ownerProc == neighProc) then

                ! Get local owner
                iFill = this%globalToLocalCellMap(iOwner)
                mappedOwner = iFill

                ! Get local neighbour
                iFill = this%globalToLocalCellMap(iNeighbour)
                mappedNeighbour = iFill

                ! Add owner, neighbour and the shared faces to the host processor's list
                call this%ownerDist(ownerProc)%addlast(mappedOwner)
                call this%neighDist(ownerProc)%addlast(mappedNeighbour)
                call this%faceDist(ownerProc)%addlast(iFace)

                ! Fill the hash table with the face mapping, this part is a bit slow and has to be improved somehow
                !label = int2Char(iFace)
                !call this%globalToLocalFaceMap(ownerProc)%put(label, this%faceDist(ownerProc)%size())

                ! No need to use hash tabels, duplicated faces are never targeted
                this%globalToLocalFaceMap(iFace) = this%faceDist(ownerProc)%size()

                this%numberOfIntFaces(ownerProc) = this%numberOfIntFaces(ownerProc) + 1

            ! Processor face
            else

                ! Add processor faces to the list
                call this%processorFacesList%faces%addlast(iFace)

                ! Add the face to both procs
                call this%procFaces(ownerProc)%faces(neighProc)%addlast(iFace)
                call this%procFaces(neighProc)%faces(ownerProc)%addlast(-iFace)

                ! Update processor connectivities table
                this%processorFacesList%processorConnectivitiesTable(ownerProc, neighProc) = neighProc
                this%processorFacesList%processorConnectivitiesTable(neighProc, ownerProc) = ownerProc

            end if

        end do

        ! Set up processor connectivity list, I can deallocate processorConnectivitiesTable aftertwards
        do iOwner=1,this%nid
            do iNeighbour=1,this%nid
                iFill = this%processorFacesList%processorConnectivitiesTable(iOwner, iNeighbour)
                if( iFill > 0) then
                    call this%processorFacesList%procNeighs(iOwner)%addlast(iFill)
                end if
            end do
            this%numberOfProcBoundaries(iOwner) = this%processorFacesList%procNeighs(iOwner)%size()
        end do

        deallocate(this%processorFacesList%processorConnectivitiesTable)

        ! Boundary faces
        this%numberOfBoundaries = numberOfBoundaries
        do iBoundary=1,numberOfBoundaries

            is = mesh%boundaries%startFace(iBoundary)
            ie = is + mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie

                ! Get face local owner
                iOwner = mesh%owner(iBFace)
                ownerProc = this%epart(iOwner)+1
                iFill = this%globalToLocalCellMap(iOwner)
                mappedOwner = iFill

                ! Add owner and the boundary faces to the host processor's list
                call this%ownerDist(ownerProc)%addlast(mappedOwner)
                call this%faceDist(ownerProc)%addlast(iBFace)

                ! Update boundary faces lists
                call this%boundaryFaceDist(ownerProc)%faces(iBoundary)%addlast(iBFace)

                ! Fill the hash table with the face mapping
                !label = int2char(iBFace)
                !call this%globalToLocalFaceMap(ownerProc)%put(label, this%faceDist(ownerProc)%size()) ! slow

                ! TEST
                this%globalToLocalFaceMap(iBFace) = this%faceDist(ownerProc)%size()

                ! Update the number of face boundaries
                this%numberOfBoundaryFaces(ownerProc, iBoundary) = this%numberOfBoundaryFaces(ownerProc, iBoundary) + 1

            end do

            ! Set boundary local coordinates (startFace and nFaces)
            do id=1,this%nid

                call this%boundaryFaceDist(id)%faces(iBoundary)%getat(1, targetFace, found)

                !label = int2Char(targetFace)
                !call this%globalToLocalFaceMap(id)%get(label, mappedFace, found)

                if(found) then
                    mappedFace = this%globalToLocalFaceMap(targetFace)
                    this%startFace(id, iBoundary) = mappedFace
                else
                    this%startFace(id, iBoundary) = 42+1
                end if
                this%nFaces(id, iBoundary) = this%boundaryFaceDist(id)%faces(iBoundary)%size()
            end do

        end do

        ! Add processor boundaries
        call this%setProcessorBoundaries()

    end subroutine facePartitions

!**********************************************************************************************************************

    subroutine pointPartitions(this)

        use m_unista
        use m_refsor
        use qhashtbl_m

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: label
    !------------------------------------------------------------------------------------------------------------------

        type(qvector_t), dimension(:), allocatable :: points
    !------------------------------------------------------------------------------------------------------------------

        integer :: nn, iBoundary, iBFace, iFace, iNode, targetFace, iFill, id, p, isize, pSize

        integer :: numberOfLocalFaces

        integer, dimension(:), allocatable :: localFaceList, localPointList, faceNodes

        integer, dimension(:), allocatable :: mapper
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        write(*,*) 'FLUBIO: distributing points...'

        isize = storage_size(iFace)/8
        allocate(points(this%nid))
        do id=1,this%nid
            call points(id)%new(1, isize, QVECTOR_RESIZE_LINEAR)
        end do

        ! Loop over each face partition
        do id=1,this%nid

            numberOfLocalFaces = this%faceDist(id)%size()
            allocate(localFaceList(numberOfLocalFaces))

            call this%faceDist(id)%toarray(localFaceList)

            ! Loop over partition faces
            do iFace=1,numberOfLocalFaces

                targetFace = abs(localFaceList(iFace))
                nn = numberOfFaceNodes(targetFace)

                allocate(faceNodes(nn))
                faceNodes = mesh%fvertex(targetFace)%col

                ! Loop over face nodes and push them in a list
                do iNode=1,nn
                    p = faceNodes(iNode)
                    call points(id)%addlast(p)
                end do

                deallocate(faceNodes)

            end do

            ! Point list contains duplicated values, need to remove duplications
            psize = points(id)%size()
            allocate(localPointList(pSize))
            allocate(mapper(pSize))
            mapper = -1

            call points(id)%toarray(localPointList)

            psize = points(id)%size()
            call unista(localPointList, pSize, mapper)
            call refsor(localPointList(1:pSize))

            ! Add points
            this%numberOfPoints(id) = pSize
            do iNode=1,pSize
                call this%pointDist(id)%addlast(localPointList(iNode))
                !label = int2Char(localPointList(iNode))
                !call this%globalToLocalPointMap(id)%put(label, iNode)
                this%globalToLocalPointMap(localPointList(iNode), id) = iNode
            end do

            ! Deallocate arrays for the next processor
            deallocate(localFaceList)
            deallocate(localPointList)
            deallocate(mapper)

        end do

    end subroutine pointPartitions

!**********************************************************************************************************************

    subroutine setProcessorBoundaries(this)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID

        character(len=:), allocatable :: label
    !------------------------------------------------------------------------------------------------------------------

        integer :: id, iBFace, processorFace

        integer :: iOwner, iNeighbour, mappedOwner, mappedNeighbour, iFill

        integer :: ownerProc, neighProc
    !------------------------------------------------------------------------------------------------------------------

        logical :: success, found
    !------------------------------------------------------------------------------------------------------------------

        ! Loop over processor faces
        do iBFace=1,this%processorFacesList%faces%size()

            ! Get face global address
            call this%processorFacesList%faces%getat(iBFace, processorFace, success)
            call checkValue(success)

            iOwner = mesh%owner(processorFace)
            iNeighbour = mesh%neighbour(processorFace)

            ! Get owner and neighbour processor
            ownerProc = this%epart(iOwner)+1
            neighProc = this%epart(iNeighbour)+1

            ! First add the processor face to the faceDist for the sharing processors
            call this%faceDist(ownerProc)%addlast(processorFace)
            call this%faceDist(neighProc)%addlast(-processorFace)

        ! It looks like this is not need as I out proc faces at the end of the array
        !    label = int2Char(processorFace)
        !    call this%globalToLocalFaceMap(ownerProc)%put(label, this%faceDist(ownerProc)%size())
        !    call this%globalToLocalFaceMap(neighProc)%put(label, this%faceDist(neighProc)%size())

            ! Update number of processor faces from both sides
            this%numberOfProcessorFaces(ownerProc) = this%numberOfProcessorFaces(ownerProc) + 1
            this%numberOfProcessorFaces(neighProc) = this%numberOfProcessorFaces(neighProc) + 1

        end do

    end subroutine setProcessorBoundaries

!**********************************************************************************************************************

    subroutine writeCellProcAddressing(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID, ne

        character(len=:), allocatable :: fname

        character(len=*) :: fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement, localElement, id, numberOfLocalElements
    !------------------------------------------------------------------------------------------------------------------

        logical :: success
    !------------------------------------------------------------------------------------------------------------------

            write(procID,'(i0)') id-1
            fname = 'grid/processor'//trim(procID)//'/cellProcAddressing'
            call execute_command_line('mkdir -p '//'grid/processor'//trim(procID))

            numberOfLocalElements = this%cellDist(id)%size()
            write(ne,'(i0)') numberOfLocalElements
            write(*,*) ' * number of elements: '//trim(ne)

            if(fileFormat == 'ascii') then
                open(1, file = fname)      
                    write(1,'(i0)') numberOfLocalElements
                    do iElement=1,numberOfLocalElements
                        call this%cellDist(id)%getat(iElement, localElement, success)
                        call checkValue(success)
                        write(1,'(i0)') localElement-1 ! +1 will be added again while reading the mesh
                    end do
                close(1)
            else
                
                open(1, file = fname, form='unformatted')
                    write(1) numberOfLocalElements
                    do iElement=1,numberOfLocalElements
                        call this%cellDist(id)%getat(iElement, localElement, success)
                        call checkValue(success)
                        write(1) localElement-1 ! +1 will be added again while reading the mesh
                    end do
                close(1)

            endif

    end subroutine writeCellProcAddressing

!**********************************************************************************************************************

    subroutine writeFaceProcAddressing(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID

        character(len=:), allocatable :: fname

        character(len=*) :: fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iFace, iProc, targetFace, processorFace, globalFace, numberOfLocalFaces

        integer :: id, ownerProc, neighProc
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

            write(procID,'(i0)') id-1

            fname = 'grid/processor'//trim(procID)//'/faceProcAddressing'

            numberOfLocalFaces = this%faceDist(id)%size()
            if(fileFormat == 'ascii') then
                
                open(1, file = fname)
                     
                    write(1,'(i0)') numberOfLocalFaces
                    do iFace=1,this%ownerDist(id)%size()
                        call this%faceDist(id)%getat(iFace, globalFace, found)
                        call checkValue(found)
                        write(1,'(i0)') globalFace
                    end do

                    ! Processor Faces
                    do iProc=1,this%numberOfProcBoundaries(id)

                        ownerProc = id
                        neighProc = getcmpt(this%processorFacesList%procNeighs(ownerProc), iProc)

                        do iFace=1,this%procFaces(ownerProc)%faces(neighProc)%size()

                            call this%procFaces(ownerProc)%faces(neighProc)%getat(iFace, processorFace, found)
                            call checkValue(found)

                            write(1,'(i0)') processorFace

                        end do

                    end do

                close(1)

            else    

                open(1, file = fname, form='unformatted')

                    write(1) numberOfLocalFaces
                    do iFace=1,this%ownerDist(id)%size()
                        call this%faceDist(id)%getat(iFace, globalFace, found)
                        call checkValue(found)
                        write(1) globalFace
                    end do

                    ! Processor Faces
                    do iProc=1,this%numberOfProcBoundaries(id)

                        ownerProc = id
                        neighProc = getcmpt(this%processorFacesList%procNeighs(ownerProc), iProc)

                        do iFace=1,this%procFaces(ownerProc)%faces(neighProc)%size()

                            call this%procFaces(ownerProc)%faces(neighProc)%getat(iFace, processorFace, found)
                            call checkValue(found)

                            write(1) processorFace

                        end do

                    end do

                close(1)

        endif    

    end subroutine writeFaceProcAddressing

!**********************************************************************************************************************

    subroutine writeFacePartitions(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID, nft, nif

        character(len=:), allocatable :: label, flabel

        character(len=:), allocatable :: fname

        character(len=*) :: fileFormat

        character(len=6) fmt, asciiOrBinary 
    !------------------------------------------------------------------------------------------------------------------

        integer :: nn, iBoundary, iFace, iNode, iProc, targetFace, processorFace, id, p

        integer :: numberOfLocalFaces, faceSign, ownerProc, neighProc

        integer, dimension(:), allocatable :: localFaceList, faceNodes, mappedPoints, facePoints
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        ! Loop over each face partition
        write(procID,'(i0)') id-1
        fname = 'grid/processor'//trim(procID)//'/faces'

        numberOfLocalFaces = this%faceDist(id)%size()
        allocate(localFaceList(numberOfLocalFaces))
        call this%faceDist(id)%toarray(localFaceList)

        write(nft,'(i0)') numberOfLocalFaces
        write(*,*) ' * number of faces: '//trim(nft)

        write(nif,'(i0)') this%numberOfIntFaces(id)
        write(*,*) ' * number of internal faces: '//trim(nif)

        if(fileFormat == 'ascii') then

            open(1, file=fname)

                ! Loop over partition faces
                
                write(1,'(i0)') numberOfLocalFaces
                do iFace=1, this%ownerDist(id)%size()

                    faceSign = sign(1, localFaceList(iFace))
                    targetFace = abs(localFaceList(iFace))
                    nn = numberOfFaceNodes(targetFace)

                    allocate(faceNodes(nn))
                    faceNodes = mesh%fvertex(targetFace)%col

                    ! Loop over face nodes and push them in a list
                    allocate(mappedPoints(nn+1))
                    allocate(facePoints(nn))
                    mappedPoints(1) = nn

                    do iNode=1,nn
                        !label = int2Char(faceNodes(iNode))
                        !call this%globalToLocalPointMap(id)%get(label, p, found)
                        !call checkValue(found)
                        p = this%globalToLocalPointMap(faceNodes(iNode),id)
                        facePoints(iNode) = p-1
                    end do

                    ! Need to change the orientation at processor faces with a negative sign...
                    if(faceSign<0) then
                        call reverseArray(facePoints, nn)
                    end if

                    mappedPoints(2:nn+1) = facePoints(1:nn)
                    write(1,*) mappedPoints(1:nn+1)

                    deallocate(faceNodes)
                    deallocate(facePoints)
                    deallocate(mappedPoints)

                end do

                ! Processor Faces
                do iProc=1,this%numberOfProcBoundaries(id)

                    ownerProc = id
                    neighProc = getcmpt(this%processorFacesList%procNeighs(ownerProc), iProc)

                    do iFace=1,this%procFaces(ownerProc)%faces(neighProc)%size()

                    call this%procFaces(ownerProc)%faces(neighProc)%getat(iFace, processorFace, found)
                    call checkValue(found)

                    faceSign = sign(1, processorFace)
                    processorFace = abs(processorFace)
                    nn = numberOfFaceNodes(processorFace)

                    allocate(faceNodes(nn))
                    faceNodes = mesh%fvertex(processorFace)%col

                    ! Loop over face nodes and push them in a list
                    allocate(mappedPoints(nn+1))
                    allocate(facePoints(nn))
                    mappedPoints(1) = nn

                    do iNode=1,nn
                        !label = int2Char(faceNodes(iNode))
                        !call this%globalToLocalPointMap(id)%get(label, p, found)
                        !call checkValue(found)
                        p = this%globalToLocalPointMap(faceNodes(iNode),id)
                        facePoints(iNode) = p-1
                    end do

                    ! Need to change the orientation at processor faces with a negative sign...
                    if(faceSign<0) then
                        call reverseArray(facePoints, nn)
                    end if

                    mappedPoints(2:nn+1) = facePoints(1:nn)
                    write(1,*) mappedPoints(1:nn+1)

                    deallocate(faceNodes)
                    deallocate(facePoints)
                    deallocate(mappedPoints)

                    end do

                end do

            close(1)
        
        else
     
            open(1, file=fname, form='unformatted')

                ! Loop over partition faces
                
                write(1) numberOfLocalFaces
                do iFace=1, this%ownerDist(id)%size()

                    faceSign = sign(1, localFaceList(iFace))
                    targetFace = abs(localFaceList(iFace))
                    nn = numberOfFaceNodes(targetFace)

                    allocate(faceNodes(nn))
                    faceNodes = mesh%fvertex(targetFace)%col

                    ! Loop over face nodes and push them in a list
                    allocate(mappedPoints(nn+1))
                    allocate(facePoints(nn))
                    mappedPoints(1) = nn

                    do iNode=1,nn
                        !label = int2Char(faceNodes(iNode))
                        !call this%globalToLocalPointMap(id)%get(label, p, found)
                        !call checkValue(found)
                        p = this%globalToLocalPointMap(faceNodes(iNode),id)
                        facePoints(iNode) = p-1
                    end do

                    ! Need to change the orientation at processor faces with a negative sign...
                    if(faceSign<0) then
                        call reverseArray(facePoints, nn)
                    end if

                    mappedPoints(2:nn+1) = facePoints(1:nn)
                    write(1) mappedPoints(1:nn+1)

                    deallocate(faceNodes)
                    deallocate(facePoints)
                    deallocate(mappedPoints)

                end do

                ! Processor Faces
                do iProc=1,this%numberOfProcBoundaries(id)

                    ownerProc = id
                    neighProc = getcmpt(this%processorFacesList%procNeighs(ownerProc), iProc)

                    do iFace=1,this%procFaces(ownerProc)%faces(neighProc)%size()

                    call this%procFaces(ownerProc)%faces(neighProc)%getat(iFace, processorFace, found)
                    call checkValue(found)

                    faceSign = sign(1, processorFace)
                    processorFace = abs(processorFace)
                    nn = numberOfFaceNodes(processorFace)

                    allocate(faceNodes(nn))
                    faceNodes = mesh%fvertex(processorFace)%col

                    ! Loop over face nodes and push them in a list
                    allocate(mappedPoints(nn+1))
                    allocate(facePoints(nn))
                    mappedPoints(1) = nn

                    do iNode=1,nn
                        !label = int2Char(faceNodes(iNode))
                        !call this%globalToLocalPointMap(id)%get(label, p, found)
                        !call checkValue(found)
                        p = this%globalToLocalPointMap(faceNodes(iNode),id)
                        facePoints(iNode) = p-1
                    end do

                    ! Need to change the orientation at processor faces with a negative sign...
                    if(faceSign<0) then
                        call reverseArray(facePoints, nn)
                    end if

                    mappedPoints(2:nn+1) = facePoints(1:nn)
                    write(1) mappedPoints(1:nn+1)

                    deallocate(faceNodes)
                    deallocate(facePoints)
                    deallocate(mappedPoints)

                    end do

                end do

            close(1)

        endif            

        deallocate(localFaceList)

    end subroutine writeFacePartitions

!**********************************************************************************************************************

    subroutine writeOwnerPartitions(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID

        character(len=:), allocatable :: fname

        character(len=*) :: fileFormat


    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, id, iProc, numberOfLocalOwners, numberOfLocalFaces

        integer :: iOwner, iNeighbour, mappedOwner, mappedNeighbour, processorFace, ownerProc, neighProc
    !------------------------------------------------------------------------------------------------------------------

        logical :: success
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id-1
        fname = 'grid/processor'//trim(procID)//'/owner'

        numberOfLocalOwners = this%OwnerDist(id)%size()
        numberOfLocalFaces = this%faceDist(id)%size()

        if(fileFormat == 'ascii') then
            open(1, file = fname)
                 
                write(1,'(i0)') numberOfLocalFaces
                do iFace=1,numberOfLocalOwners
                    call this%ownerDist(id)%getat(iFace, iOwner, success)
                    call checkValue(success)
                    write(1,'(i0)') iOwner-1
                end do

                ! Add processor face owners
                do iProc=1,this%numberOfProcBoundaries(id)

                    ownerProc = id
                    neighProc = getcmpt(this%processorFacesList%procNeighs(id), iProc)

                    do iFace=1,this%procFaces(id)%faces(neighProc)%size()

                        call this%procFaces(id)%faces(neighProc)%getat(iFace, processorFace, success)
                        call checkValue(success)

                        processorFace = abs(processorFace)

                        iOwner = mesh%owner(processorFace)
                        mappedOwner = this%globalToLocalCellMap(iOwner)

                        iNeighbour = mesh%neighbour(processorFace)
                        mappedNeighbour = this%globalToLocalCellMap(iNeighbour)

                        if(ownerProc == this%epart(iOwner)+1) then
                            write(1,'(i0)') mappedOwner-1
                        elseif(ownerProc == this%epart(iNeighbour)+1) then
                            write(1,'(i0)') mappedNeighbour-1
                        else
                            write(*,*) 'Something odd is happening, target face is not hosted by ownerProc or neighProc'
                        stop
                        end if

                    end do

                end do

            close(1)

        else

            open(1, file = fname, form='unformatted')

                write(1) numberOfLocalFaces

                do iFace=1,numberOfLocalOwners
                    call this%ownerDist(id)%getat(iFace, iOwner, success)
                    call checkValue(success)
                    write(1) iOwner-1
                end do

                ! Add processor face owners
                do iProc=1,this%numberOfProcBoundaries(id)

                    ownerProc = id
                    neighProc = getcmpt(this%processorFacesList%procNeighs(id), iProc)

                    do iFace=1,this%procFaces(id)%faces(neighProc)%size()

                        call this%procFaces(id)%faces(neighProc)%getat(iFace, processorFace, success)
                        call checkValue(success)

                        processorFace = abs(processorFace)

                        iOwner = mesh%owner(processorFace)
                        mappedOwner = this%globalToLocalCellMap(iOwner)

                        iNeighbour = mesh%neighbour(processorFace)
                        mappedNeighbour = this%globalToLocalCellMap(iNeighbour)

                        if(ownerProc == this%epart(iOwner)+1) then
                            write(1) mappedOwner-1
                        elseif(ownerProc == this%epart(iNeighbour)+1) then
                            write(1) mappedNeighbour-1
                        else
                            write(*,*) 'Something odd is happening, target face is not hosted by ownerProc or neighProc'
                        stop
                        end if

                    end do

                end do

            close(1)

        endif

    end subroutine writeOwnerPartitions

!**********************************************************************************************************************

    subroutine writeNeighbourPartitions(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID

        character(len=:), allocatable :: fname

        character(len=*) :: fileFormat

       
    !------------------------------------------------------------------------------------------------------------------

        integer :: iFace, iNeighbour, id, numberOfIntLocalFaces
    !------------------------------------------------------------------------------------------------------------------

        logical :: success
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id-1
        fname = 'grid/processor'//trim(procID)//'/neighbour'

        numberOfIntLocalFaces = this%numberOfIntFaces(id)
        if(fileFormat == 'ascii') then

            open(1, file = fname)
                
                write(1,'(i0)') numberOfIntLocalFaces
                do iFace=1,numberOfIntLocalFaces
                    call this%neighDist(id)%getat(iFace, iNeighbour, success)
                    call checkValue(success)
                    write(1,'(i0)') iNeighbour-1
                end do
            close(1)
        
        else 

            
            

            open(1, file = fname, form='unformatted')
                
                write(1) numberOfIntLocalFaces
                do iFace=1,numberOfIntLocalFaces
                    call this%neighDist(id)%getat(iFace, iNeighbour, success)
                    call checkValue(success)
                    write(1) iNeighbour-1
                end do
            close(1)

        endif    

    end subroutine writeNeighbourPartitions

!**********************************************************************************************************************

    subroutine writePointPartitions(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID, npt

        character(len=:), allocatable :: fname

        character(len=*) :: fileFormat

       
    !------------------------------------------------------------------------------------------------------------------

        integer :: iPoint, iNode, id, numberOfLocalPoints
    !------------------------------------------------------------------------------------------------------------------

        real :: point(3)
    !------------------------------------------------------------------------------------------------------------------

        logical :: success
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id-1
        fname = 'grid/processor'//trim(procID)//'/points'
        numberOfLocalPoints = this%numberOfPoints(id)

        write(npt,'(i0)') numberOfLocalPoints
        write(*,*) ' * number of points: '//trim(npt)

        if(fileFormat == 'ascii') then

            open(1, file = fname)
                write(1,'(i0)') numberOfLocalPoints
                do iPoint=1, numberOfLocalPoints

                    call this%pointDist(id)%getat(iPoint, iNode, success)
                    call checkValue(success)

                    point = mesh%vertex(iNode, :)
                    write(1,*) point

                end do
            close(1)

        else 

            open(1, file = fname, form='unformatted')
                
                write(1) numberOfLocalPoints
                do iPoint=1, numberOfLocalPoints

                    call this%pointDist(id)%getat(iPoint, iNode, success)
                    call checkValue(success)

                    point = mesh%vertex(iNode, :)
                    write(1) point

                end do
            close(1)

        endif    

    end subroutine writePointPartitions

!**********************************************************************************************************************

    subroutine writeBoundaryPartitions(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID, nb, nsf, npf, nbf

        character(Len=:), allocatable :: proc1, proc2

        character(len=:), allocatable :: fname, procBoundName

        character(len=*) :: fileFormat
    !------------------------------------------------------------------------------------------------------------------

        integer :: nn, iBoundary, iBFace, iOwner, iNeighbour, iProc, id, is, ie

        integer :: numberOfBounds, startFace, nFaces, procFace, mappedProcFace, ownerProc, neighProc
    !------------------------------------------------------------------------------------------------------------------

        logical :: success
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id-1
        fname = 'grid/processor'//trim(procID)//'/boundary'

        numberOfBounds = this%numberOfBoundaries + this%numberOfProcBoundaries(id)
        open(1, file = fname)

            write(1,'(i0)') numberOfBounds

            ! Loop over real boundaries
            do iBoundary=1,numberOfBoundaries

                startFace = this%startFace(id, iBoundary)
                nFaces = this%nFaces(id, iBoundary)

                
                write(1,'(a)') mesh%boundaries%userName(iBoundary)
                write(1,'(a)') 'type'
                write(1,'(a)') mesh%boundaries%bcType(iBoundary)
                write(1,'(a)') 'nFaces'
                write(1,'(i0)') this%nFaces(id, iBoundary)
                write(1,'(a)') 'startFace'
                write(1,'(i0)') this%startFace(id, iBoundary)-1 ! 1 will be added back later

            end do

            ! Processor boundaries
            is = this%numberOfIntFaces(id) + sum(this%numberOfBoundaryFaces(id, :)) + 1
            do iProc=1,this%numberOfProcBoundaries(id)

                neighProc = getcmpt(this%processorFacesList%procNeighs(id), iProc)
                proc1 = int2Char(id-1)
                proc2 = int2Char(neighProc-1)
                procBoundName = 'procBoundary'//proc1//'to'//proc2

                write(1,'(a)') procBoundName
                write(1,'(a)') 'type'
                write(1,'(a)') 'processor'
                write(1,'(a)') 'nFaces'
                write(1,'(i0)') this%procFaces(id)%faces(neighProc)%size()
                write(1,'(a)') 'startFace'
                write(1, '(i0)') is-1
                write(1,'(a)') 'myProcNo'
                write(1,'(i0)') id-1
                write(1,'(a)') 'neighProcNo'
                write(1,'(i0)') neighProc-1

                is = is + this%procFaces(id)%faces(neighProc)%size() !-1

                ! Print some info
                write(nsf,'(i0)') this%procFaces(id)%faces(neighProc)%size()
                write(*,*) ' * number of faces shared with processor '//proc2//': '//trim(nsf)

            end do

            write(nb,'(i0)') this%numberOfProcBoundaries(id)
            write(*,*) ' * number of processor boundaries: '//trim(nb)

            write(nbf,'(i0)') sum(this%numberOfBoundaryFaces(id, :))
            write(*,*) ' * number of boundary faces: '//trim(nbf)

            write(npf,'(i0)') this%numberOfProcessorFaces(id)
            write(*,*) ' * number of processor faces: '//trim(npf)

        close(1)

    end subroutine writeBoundaryPartitions

!**********************************************************************************************************************

    subroutine writeBoundaryProcAddressing(this, id, fileFormat)

        class(meshDecomp) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=10) :: procID

        character(len=:), allocatable :: fname

        character(len=*) :: fileFormat

       
    !------------------------------------------------------------------------------------------------------------------

        integer :: numberOfBounds, iBoundary, iProc, id, is
    !------------------------------------------------------------------------------------------------------------------

        write(procID,'(i0)') id-1
        fname = 'grid/processor'//trim(procID)//'/boundaryProcAddressing'

        numberOfBounds = this%numberOfBoundaries + this%numberOfProcBoundaries(id)
        if(fileFormat == 'ascii') then
            open(1, file = fname)
                
                write(1,'(i0)') numberOfBounds
                ! Loop over real boundaries
                do iBoundary=1,numberOfBoundaries
                    write(1,'(i0)') iBoundary-1
                end do

                ! Processor boundaries
                do iProc=1,this%numberOfProcBoundaries(id)
                    is = -1
                    write(1,'(i0)') is
                end do
            close(1)

        else 

            
            
            

            open(1, file = fname, form='unformatted')
                
                write(1) numberOfBounds
                ! Loop over real boundaries
                do iBoundary=1,numberOfBoundaries
                    write(1) iBoundary-1
                end do

                ! Processor boundaries
                do iProc=1,this%numberOfProcBoundaries(id)
                    is = -1
                    write(1) is
                end do
            close(1)

        endif

    end subroutine writeBoundaryProcAddressing

!**********************************************************************************************************************

    subroutine checkValue(success)

        logical :: success
    !------------------------------------------------------------------------------------------------------------------

        if(.not. success) then
            write(*,*) 'ERROR: something went wrong while getting the value from qvector'
            stop
        end if

    end subroutine checkValue

!**********************************************************************************************************************

    function int2Char(my_int) result(my_str)

        character(len=:), allocatable :: my_str

        character(len=10) :: dummychar
    !------------------------------------------------------------------------------------------------------------------

        integer :: my_int
    !------------------------------------------------------------------------------------------------------------------

        write(dummyChar,'(i0)') my_int
        my_str = trim(dummychar)

    end function int2Char

!**********************************************************************************************************************

    function char2Int(my_str) result(my_int)

        character(len=*), allocatable :: my_str
    !------------------------------------------------------------------------------------------------------------------

        integer :: my_int
    !------------------------------------------------------------------------------------------------------------------

        write(my_int,'(i0)') my_str

    end function char2Int

!**********************************************************************************************************************

    subroutine initialiseInnerVector(this, nid)

        class(nestedVector) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, j, isize, nid
    !------------------------------------------------------------------------------------------------------------------

        isize = storage_size(i)/8
        allocate(this%faces(nid))

        do i=1,nid
            call this%faces(i)%new(1, isize, QVECTOR_RESIZE_LINEAR)
        end do

    end subroutine initialiseInnerVector

!**********************************************************************************************************************

    function getcmpt(vec, pos) result(val)

        type(qvector_t) :: vec
    !------------------------------------------------------------------------------------------------------------------

        integer :: pos, val
    !------------------------------------------------------------------------------------------------------------------

        logical success
    !------------------------------------------------------------------------------------------------------------------

        call vec%getat(pos, val,success)
        call checkValue(success)

    end function getcmpt

!**********************************************************************************************************************

    subroutine saveCellDistToFile(this)

        class(meshDecomp) :: this
   !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: postDir
        !! post processing directory

        character(len=:), allocatable :: fieldDir
        !! field directory

        character(len=15) :: fieldName
        !! name of the field

        character(len=:), allocatable :: timeStamp
        !! name of the field
   !------------------------------------------------------------------------------------------------------------------

        logical :: dirExists
        !! boolean to check if the directory exists
   !------------------------------------------------------------------------------------------------------------------

        integer :: iBoundary, iBFace, iElement, is, ie, iFill, iOwner, nComp
   !------------------------------------------------------------------------------------------------------------------

        real :: epart(numberOfElements+numberOfBFaces, 1)
   !------------------------------------------------------------------------------------------------------------------

        epart = 0.0
        do iElement=1,numberOfElements
            epart(iElement,1) = real(this%epart(iElement))
        end do

        iFill = numberOfElements
        do iBoundary=1,numberOfBoundaries

            is = mesh%boundaries%startFace(iBoundary)
            ie = is+mesh%boundaries%nFace(iBoundary)-1

            do iBFace=is,ie
                iFill = iFill+1
                iOwner = mesh%owner(iBFace)
                epart(iFill,1) = epart(iOwner,1)
            end do

        end do

        postDir='postProc/fields/'
        timeStamp = '0'
        fieldDir = postDir//trim(timeStamp)//'/'

        nComp = 1
        fieldName = 'cellDist'

        ! Create a directory for the field. If it does not exists create it.
        inquire(file=trim(fieldDir)//'/.', exist=dirExists)

        if(dirExists) then

            open(1,file='postProc/fields/fieldList.txt')
                write(1,*) trim(fieldName)
            close(1)

            ! write Field
            open(1,file=fieldDir//trim(fieldName)//'-d'//trim(timeStamp)//'.bin', form='unformatted')

                write(1) nComp
                write(1) fieldName

                write(1) epart(:,1)
                write(1) epart(:,1)
                write(1) epart(:,1)

            close(1)

        elseif(.not. dirExists) then

            open(1,file='postProc/fields/fieldList.txt')
                write(1,*) fieldName
            close(1)

            ! create the folder
            call execute_command_line ('mkdir -p ' // adjustl(trim(fieldDir) ) )

            ! write Field
            open(1,file=fieldDir//trim(fieldName)//'-d'//trim(timeStamp)//'.bin', form='unformatted')

                write(1) nComp
                write(1) fieldName

                write(1) epart(:,1)
                write(1) epart(:,1)
                write(1) epart(:,1)

            close(1)

        end if

    end subroutine saveCellDistToFile

!**********************************************************************************************************************

    subroutine reverseArray(a, isize)

        integer :: i, head, tail, isize

        integer :: a(isize), temp
    !------------------------------------------------------------------------------------------------------------------

        head = 1
        tail = isize
        do
            if (head >= tail)  exit
            temp    = a(Head)
            a(Head) = a(Tail)
            a(Tail) = temp
            head    = head + 1
            tail    = tail - 1
        end do

    end subroutine reverseArray

end module meshPartitioning
