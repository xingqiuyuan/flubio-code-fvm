
# usage:
# include ${cmake_local}/debug.cmake
# debug_PRINT( a short message )
# set( xx "a value" )
# debug_DUMP( xx )

macro( debug_PRINT msg )
  message( STATUS "debug_PRINT: ${msg}" )
endmacro()

macro( debug_DUMP var )
  message( STATUS "debug_DUMP: ${var}=${${var}}" )
endmacro()
