!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module cellLimited

!======================================================================================================================
! Description:
!! cellLimited is the module containing the methods for the cell limited gradient.
!======================================================================================================================

	use flubioDictionaries
	use flubioFields
	use meshvar
	use TVDInterpolations, only: getTVDLimiterValue

	implicit none

contains

	subroutine cellLimitedMDGradient(field)

	!==================================================================================================================
	! Description:
	!! cellLimitedMDGradient bounds the gradient using multidimensional clipping. The level of bounding is
	!! controlled by the coefficient k, between 0 and 1 (0: unbounded, 1: fully bounded).
	!! The gradient to be bounded can be calculated either with standard Gauss formula, either by the LSQ method.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------
		
		integer :: i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer :: iElement, iBElements, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc
	!------------------------------------------------------------------------------------------------------------------
	
		real :: maxPhi(numberOfElements, field%nComp)
		!! maximum cell center values among the neighbour cells

		real :: minPhi(numberOfElements, field%nComp)
 		!! minumum  cell center values among the neighbour cells

		real :: phiC
		!! Owner cell center value

		real :: phiN
		!! neighbour cell ceneter value

		real :: kappa
		!! clipping constant

		real :: km1k
		!! (k-1)/k

		real :: dot
		!! auxiliary variable to store a dot product

		real :: limiter
		!! face limiter

		real :: val
		!! auxiliary variable

		real :: rCfO(3)
		!! cell center to cell face distance, owner side

		real :: eCfO(3)
		!! normalised cell center to cell face distance, owner side

		real :: rCfN(3)
		!! cell center to cell face distance, neighbour side

		real :: eCfN(3)
		!! normalised cell center to cell face distance, neighbour side

		real :: dPhiMax(field%nComp)
		!! difference between maxPhi and actual cell center value

		real :: dPhiMin(field%nComp)
		!! difference between maxPhi and actual cell center value

		real :: dPhiMaxTilde(numberOfElements, field%nComp)
		!! difference between maxPhi and actual cell center value

		real :: dPhiMinTilde(numberOfElements, field%nComp)
		!! difference between minPhi and actual cell center value
	!------------------------------------------------------------------------------------------------------------------

		fComp = field%nComp

		kappa = field%kLim
		km1k = (1.0-kappa)/kappa

		if(kappa < 1e-8) return

		!------------------------------!
		! Find out max and min for Phi !
		!------------------------------!
	
		call findCellMinMax(minPhi, maxPhi, field%phi, field%ghosts, fComp)

		!-------------------!
		! Compute dPhiTilde !
		!-------------------!

		do iElement=1,numberOfElements
		   dPhiMax = maxPhi(iElement,:) - field%phi(iElement,:)
		   dPhiMin = minPhi(iElement,:) - field%phi(iElement,:)

		   dPhiMaxTilde(iElement,:) = dPhiMax + km1k*(dPhiMax-dPhiMin)
		   dPhiMinTilde(iElement,:) = dPhiMin - km1k*(dPhiMax-dPhiMin)
		enddo

		!--------------------!
		! Limit the gradient !
		!--------------------!
	
		do iFace=1,numberOfIntFaces 
	
		   iOwner = mesh%owner(iFace)
		   iNeighbour = mesh%neighbour(iFace)
	
		   rCfO = mesh%fcentroid(iFace,:) - mesh%centroid(iOwner,:)
		   eCfO = rCfO/(rCfO(1)**2+rCfO(2)**2+rCfO(3)**2)
	
		   rCfN = mesh%fcentroid(iFace,:) - mesh%centroid(iNeighbour,:)
		   eCfN = rCfN/(rCfN(1)**2+rCfN(2)**2+rCfN(3)**2)

		   do iComp=1,fComp

			  ! Owner
			  dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
			  if(dot > dPhiMaxTilde(iOwner,iComp)) then
				  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMaxTilde(iOwner,iComp) - dot)
			  elseif(dot < dPhiMinTilde(iOwner,iComp)) then
				  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMinTilde(iOwner,iComp) - dot)
			  endif

			  ! Neighbour
			  dot = dot_product(field%phigrad(iNeighbour,:,iComp), rCfN)
			  if(dot > dPhiMaxTilde(iNeighbour,iComp)) then
				  field%phigrad(iNeighbour,:,iComp) = field%phigrad(iNeighbour,:,iComp) + eCfN*(dPhiMaxTilde(iNeighbour,iComp) - dot)
			  elseif(dot < dPhiMinTilde(iNeighbour,iComp)) then
				  field%phigrad(iNeighbour,:,iComp) = field%phigrad(iNeighbour,:,iComp) + eCfN*(dPhiMinTilde(iNeighbour,iComp) - dot)
			  endif

		   enddo

		enddo

		!----------------------!
		! Processor Boundaries !
		!----------------------!
	
		do iBoundary=1,numberOfProcBound
	
		   iProc = mesh%boundaries%procBound(iBoundary)
		   is = mesh%boundaries%startFace(iProc)
		   ie = is+mesh%boundaries%nFace(iProc)-1
			 
		   do iBFace=is,ie
	
			  iOwner = mesh%owner(iBFace)
	
			  rCfO = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)
			  eCfO = rCfO/(rCfO(1)**2+rCfO(2)**2+rCfO(3)**2)
	
			  do iComp=1,fComp
				 dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
				 if(dot > dPhiMaxTilde(iOwner,iComp)) then
					 field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMaxTilde(iOwner,iComp) - dot)
				 elseif(dot < dPhiMinTilde(iOwner,iComp)) then
					 field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMinTilde(iOwner,iComp) - dot)
				 endif
			  enddo
	
		   enddo
	
		enddo

		!-----------------!
		! Real boundaries !
		!-----------------!

		do iBoundary=1,numberOfRealBound
	
		   is = mesh%boundaries%startFace(iBoundary)
		   ie = is+mesh%boundaries%nFace(iBoundary)-1

		   do iBFace=is,ie
	
			  iOwner = mesh%owner(iBFace)
	
			  rCfO = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)
			  eCfO = rCfO/(rCfO(1)**2+rCfO(2)**2+rCfO(3)**2)

			  do iComp=1,fComp
				  dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
				  if(dot > dPhiMaxTilde(iOwner,iComp)) then
					  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMaxTilde(iOwner,iComp) - dot)
				  elseif(dot < dPhiMinTilde(iOwner,iComp)) then
					  field%phigrad(iOwner,:,iComp) = field%phigrad(iOwner,:,iComp) + eCfO*(dPhiMinTilde(iOwner,iComp) - dot)
				  endif
			  enddo
	
		   enddo

		enddo

	end subroutine cellLimitedMDGradient

!**********************************************************************************************************************

	subroutine cellLimitedGradient(field)

	!==================================================================================================================
	! Description:
	!! cellLimitedGradient bounds the gradient using equal clipping in all directions. The level of bounding is
	!! controlled by the coefficient k, between 0 and 1 (0: unbounded, 1: fully bounded).
	!! The gradient to be bounded can be calculated either with standard Gauss formula, either by the LSQ method.
	!==================================================================================================================

		type(flubioField) :: field
		!! field used to compute the gradient
	!------------------------------------------------------------------------------------------------------------------
		
		integer :: i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer :: iElement, iBElements, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc
	!------------------------------------------------------------------------------------------------------------------
	
		real :: maxPhi(numberOfElements, field%nComp)
		!! maximum cell center values among the neighbour cells

		real :: minPhi(numberOfElements, field%nComp)
 		!! minumum  cell center values among the neighbour cells

		real :: phiC
		!! Owner cell center value

		real :: phiN
		!! neighbour cell ceneter value

		real :: kappa
		!! clipping constant

		real :: km1k
		!! (k-1)/k

		real :: dot
		!! auxiliary variable to store a dot product

		real :: limiter
		!! face limiter

		real :: val
		!! auxiliary variable

		real :: rCfO(3)
		!! cell center to cell face distance, owner side

		real :: rCfN(3)
		!! cell center to cell face distance, neighbour side

		real :: dPhiMax(field%nComp)
		!! difference between maxPhi and actual cell center value

		real :: dPhiMin(field%nComp)
		!! difference between maxPhi and actual cell center value

		real :: dPhiMaxTilde(numberOfElements, field%nComp)
		!! difference between maxPhi and actual cell center value

		real :: dPhiMinTilde(numberOfElements, field%nComp)
		!! difference between minPhi and actual cell center value

		real:: psi(numberOfElements, field%nComp)
		!! limiter to be used in the clipping process
	!------------------------------------------------------------------------------------------------------------------

		fComp = field%nComp

		kappa = field%kLim
		km1k = (1.0-kappa)/kappa

		if(kappa < 1e-8) return

		!------------------------------!
		! Find out max and min for Phi !
		!------------------------------!
	
		call findCellMinMax(minPhi, maxPhi, field%phi, field%ghosts, fComp)

		!-------------------!
		! Compute dPhiTilde !
		!-------------------!

		do iElement=1,numberOfElements
		   dPhiMax = maxPhi(iElement,:) - field%phi(iElement,:)
		   dPhiMin = minPhi(iElement,:) - field%phi(iElement,:)

		   dPhiMaxTilde(iElement,:) = dPhiMax + km1k*(dPhiMax-dPhiMin)
		   dPhiMinTilde(iElement,:) = dPhiMin - km1k*(dPhiMax-dPhiMin)
		enddo

		!--------------------!
		! Limit the gradient !
		!--------------------!

		psi = 1.0

		do iFace=1,numberOfIntFaces 
	
		   iOwner = mesh%owner(iFace)
		   iNeighbour = mesh%neighbour(iFace)
	
		   rCfO = mesh%fcentroid(iFace,:) - mesh%centroid(iOwner,:)	
		   rCfN = mesh%fcentroid(iFace,:) - mesh%centroid(iNeighbour,:)

		   do iComp=1,fComp

			    ! Owner
				dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
				call faceLimiter(psi(iOwner,iComp), dPhiMaxTilde(iOwner,iComp), dPhiMinTilde(iOwner,iComp), dot, opt=11)

			  	! Neighbour
			  	dot = dot_product(field%phigrad(iNeighbour,:,iComp), rCfN)
				call faceLimiter(psi(iNeighbour, iComp), dPhiMaxTilde(iNeighbour,iComp), dPhiMinTilde(iNeighbour,iComp), dot, opt=11)

		   enddo

		enddo

		!----------------------!
		! Processor Boundaries !
		!----------------------!
	
		do iBoundary=1,numberOfProcBound
	
		   iProc = mesh%boundaries%procBound(iBoundary)
		   is = mesh%boundaries%startFace(iProc)
		   ie = is+mesh%boundaries%nFace(iProc)-1
			 
		   do iBFace=is,ie
	
			  iOwner = mesh%owner(iBFace)
	
			  rCfO = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)
			
			  do iComp=1,fComp
				dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
				call faceLimiter(psi(iOwner,iComp), dPhiMaxTilde(iOwner,iComp), dPhiMinTilde(iOwner,iComp), dot, opt=11)
			  enddo
	
		   enddo
	
		enddo

		!-----------------!
		! Real boundaries !
		!-----------------!

		do iBoundary=1,numberOfRealBound
	
		   is = mesh%boundaries%startFace(iBoundary)
		   ie = is+mesh%boundaries%nFace(iBoundary)-1

		   do iBFace=is,ie
	
			  iOwner = mesh%owner(iBFace)
	
			  rCfO = mesh%fcentroid(iBFace,:) - mesh%centroid(iOwner,:)

			  do iComp=1,fComp
				  dot = dot_product(field%phigrad(iOwner,:,iComp), rCfO)
				  call faceLimiter(psi(iOwner,iComp), dPhiMaxTilde(iOwner,iComp), dPhiMinTilde(iOwner,iComp), dot, opt=11)
			  enddo
	
		   enddo

		enddo

		! apply limiter
		call applyLimiter(psi, field) 

	end subroutine cellLimitedGradient

! *********************************************************************************************************************

	subroutine findCellMinMax(minPhi, maxPhi, phi, phiGhost, fComp)

	!==================================================================================================================
	! Description:
	!! findCellMinMax loops over neighbour cells to find the maximum and the minimum value of a working variable.
	!! This routine is used in the bounding procedure for the gradient.
	!==================================================================================================================
		
		integer i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer iElement, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc
	!------------------------------------------------------------------------------------------------------------------

		real :: maxPhi(numberOfElements,fComp)
		!! maximum cell center values among the neighbour cells

		real :: minPhi(numberOfElements,fComp)
		!! minumum cell center values among the neighbour cells

		real :: phiC
		!! Owner cell center value

		real :: phiN
		!! neighbour cell ceneter value

		real :: phi(numberOfElements+numberOfBFaces,fComp)
		!! field values  
		
		real :: phiGhost(maxProcFaces,fComp,numberOfProcBound1)
		!! field values at processor boundaries
	!------------------------------------------------------------------------------------------------------------------
	
	!	maxPhi=-10000000.0
	!	minPhi=100000000.0

		maxPhi = phi(1:numberOfElements,:)
		minPhi = phi(1:numberOfElements,:)

		!---------------------------------------!
		! Find out the cell max and min for Phi !
		!---------------------------------------!

		! Internal Faces
		do iFace=1,numberOfIntFaces
	
			iOwner = mesh%owner(iFace)
			iNeighbour = mesh%neighbour(iFace)
	
			do iComp=1,fComp
				phiC = phi(iOwner,iComp)
				phiN = phi(iNeighbour,iComp)
	
				maxPhi(iOwner,iComp) = max(maxPhi(iOwner,iComp),phiN)
				minPhi(iOwner,iComp) = min(minPhi(iOwner,iComp),phiN)
	
				maxPhi(iNeighbour,iComp) = max(maxPhi(iNeighbour,iComp),phiC)
				minPhi(iNeighbour,iComp) = min(minPhi(iNeighbour,iComp),phiC)
			enddo   
	
		enddo

		! Processor Boundaries
		do iBoundary=1,numberOfProcBound
	
			iProc = mesh%boundaries%procBound(iBoundary)
			is = mesh%boundaries%startFace(iProc)
			ie = is+mesh%boundaries%nFace(iProc)-1
			pNeigh = 0
	
			do iBFace=is,ie
	
				pNeigh = pNeigh+1
				iOwner = mesh%owner(iBFace)
				
				 do iComp=1,fComp
					phiN = phiGhost(pNeigh,iComp,iBoundary)
					maxPhi(iOwner,iComp) = max(maxPhi(iOwner,iComp),phiN)
					minPhi(iOwner,iComp) = min(minPhi(iOwner,iComp),phiN)
				  enddo   
	
			 enddo
	
		enddo

		! Real Boundaries
		patchFace = numberOfElements
		do iBoundary=1,numberOfRealBound
	
			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1
	
			do iBFace=is,ie
	
				iOwner = mesh%owner(iBFace)
				patchFace = patchFace+1
	
				 do iComp=1,fComp
					 phiN = phi(patchFace,iComp)
					 maxPhi(iOwner,iComp) = max(maxPhi(iOwner,iComp),phiN)
					 minPhi(iOwner,iComp) = min(minPhi(iOwner,iComp),phiN)
				 enddo
			 enddo
	
		enddo

	end subroutine findCellMinMax

! *********************************************************************************************************************

	function findCellMin(phi, phiGhost, fComp) result(minPhi)

	!==================================================================================================================
	! Description:
	!! findCellMin loops over neighbour cells to find the minimum value of a working variable.
	!==================================================================================================================

		integer i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer iElement, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc
	!------------------------------------------------------------------------------------------------------------------

		real :: minPhi(numberOfElements,fComp)
		!! minumum cell center values among the neighbour cells

		real :: phiC
		!! Owner cell center value

		real :: phiN
		!! neighbour cell ceneter value

		real :: phi(numberOfElements+numberOfBFaces,fComp)
		!! field values

		real :: phiGhost(maxProcFaces,fComp,numberOfProcBound1)
		!! field values at processor boundaries
	!------------------------------------------------------------------------------------------------------------------

		minPhi = phi(1:numberOfElements,:)

		!---------------------------------------!
		! Find out the cell max and min for Phi !
		!---------------------------------------!

		! Internal Faces
		do iFace=1,numberOfIntFaces

			iOwner = mesh%owner(iFace)
			iNeighbour = mesh%neighbour(iFace)

			do iComp=1,fComp
				phiC = phi(iOwner,iComp)
				phiN = phi(iNeighbour,iComp)

				minPhi(iOwner,iComp) = min(minPhi(iOwner,iComp),phiN)
				minPhi(iNeighbour,iComp) = min(minPhi(iNeighbour,iComp),phiC)
			enddo

		enddo

		! Processor Boundaries
		do iBoundary=1,numberOfProcBound

			iProc = mesh%boundaries%procBound(iBoundary)
			is = mesh%boundaries%startFace(iProc)
			ie = is+mesh%boundaries%nFace(iProc)-1
			pNeigh = 0

			do iBFace=is,ie

				pNeigh = pNeigh+1
				iOwner = mesh%owner(iBFace)

				do iComp=1,fComp
					phiN = phiGhost(pNeigh,iComp,iBoundary)
					minPhi(iOwner,iComp) = min(minPhi(iOwner,iComp),phiN)
				enddo

			enddo

		enddo

		! Real Boundaries
		patchFace = numberOfElements
		do iBoundary=1,numberOfRealBound

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1

			do iBFace=is,ie

				iOwner = mesh%owner(iBFace)
				patchFace = patchFace+1

				do iComp=1,fComp
					phiN = phi(patchFace,iComp)
					minPhi(iOwner,iComp) = min(minPhi(iOwner,iComp),phiN)
				enddo
			enddo

		enddo

	end function findCellMin

! *********************************************************************************************************************

	function findCellMax(phi, phiGhost, fComp) result(maxPhi)

	!==================================================================================================================
	! Description:
	!! findCellMax loops over neighbour cells to find the maximum value of a working variable.
	!==================================================================================================================

		integer i, is, ie, pNeigh, patchFace, iBoundary
	!------------------------------------------------------------------------------------------------------------------

		integer iElement, iFace, iBFace, iComp, fComp, iOwner, iNeighbour, iProc
	!------------------------------------------------------------------------------------------------------------------

		real :: maxPhi(numberOfElements,fComp)
		!! maximum cell center values among the neighbour cells

		real :: phiC
		!! Owner cell center value

		real :: phiN
		!! neighbour cell ceneter value

		real :: phi(numberOfElements+numberOfBFaces,fComp)
		!! field values

		real :: phiGhost(maxProcFaces,fComp,numberOfProcBound1)
		!! field values at processor boundaries
	!------------------------------------------------------------------------------------------------------------------

		maxPhi = phi(1:numberOfElements,:)

		!---------------------------------------!
		! Find out the cell max and min for Phi !
		!---------------------------------------!

		! Internal Faces
		do iFace=1,numberOfIntFaces

			iOwner = mesh%owner(iFace)
			iNeighbour = mesh%neighbour(iFace)

			do iComp=1,fComp
				phiC = phi(iOwner,iComp)
				phiN = phi(iNeighbour,iComp)

				maxPhi(iOwner,iComp) = max(maxPhi(iOwner,iComp),phiN)
				maxPhi(iNeighbour,iComp) = max(maxPhi(iNeighbour,iComp),phiC)
			enddo

		enddo

		! Processor Boundaries
		do iBoundary=1,numberOfProcBound

			iProc = mesh%boundaries%procBound(iBoundary)
			is = mesh%boundaries%startFace(iProc)
			ie = is+mesh%boundaries%nFace(iProc)-1
			pNeigh = 0

			do iBFace=is,ie

				pNeigh = pNeigh+1
				iOwner = mesh%owner(iBFace)

				do iComp=1,fComp
					phiN = phiGhost(pNeigh,iComp,iBoundary)
					maxPhi(iOwner,iComp) = max(maxPhi(iOwner,iComp),phiN)
				enddo

			enddo

		enddo

		! Real Boundaries
		patchFace = numberOfElements
		do iBoundary=1,numberOfRealBound

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1

			do iBFace=is,ie

				iOwner = mesh%owner(iBFace)
				patchFace = patchFace+1

				do iComp=1,fComp
					phiN = phi(patchFace,iComp)
					maxPhi(iOwner,iComp) = max(maxPhi(iOwner,iComp),phiN)
				enddo
			enddo

		enddo

	end function findCellMax

! *********************************************************************************************************************

	subroutine faceLimiter(psi, dPhiMax, dPhiMin, extrapolation, opt)

	!==================================================================================================================
	! Description:
	!! faceLimiter computes the face limter in order to clip oscillations.
	!==================================================================================================================

		integer :: opt 
		!! limiter option, default is usually Minmod
	!------------------------------------------------------------------------------------------------------------------

		real :: dPhiMax
		!! difference between maxPhi and actual cell center value

		real :: dPhiMin
		!! difference between maxPhi and actual cell center value

		real :: extrapolation
		!! dot product between actual gradient ad cell to face distance

		real :: psi
		!! computed limiter

		real :: rf
		!! gradient ratio
	!------------------------------------------------------------------------------------------------------------------

    	rf = 1.0

		if (extrapolation > 1e-12) then 
			rf = dPhiMax/extrapolation
		elseif (extrapolation < -1e-12) then
			rf = dPhiMin/extrapolation
		else
			return
		endif

		psi = min(psi, getTVDLimiterValue(rf, opt))

	end subroutine faceLimiter

!**********************************************************************************************************************

	subroutine applyLimiter(psi, field)

	!==================================================================================================================
	! Description:
	!! applyLimiter applies the celllimiter to the gradient component.
	!==================================================================================================================

		type(flubioField) :: field
	!------------------------------------------------------------------------------------------------------------------

		integer iElement, iComp
	!------------------------------------------------------------------------------------------------------------------

		real :: psi(numberOfElements, field%nComp)
	!------------------------------------------------------------------------------------------------------------------

		do iComp=1,field%nComp
			do iElement=1,numberOfElements
				field%phiGrad(iElement,:,iComp) = field%phiGrad(iElement,:,iComp)*psi(iElement, iComp)
			enddo
		enddo

	end subroutine applyLimiter

!**********************************************************************************************************************

end module cellLimited	