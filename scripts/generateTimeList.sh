#! /bin/bash 

files=$(ls postProc/fields/)
re="^[0-9]+$"

rm postProc/fields/timeList.txt

for f in $files
do
   if [[ $f =~ $re ]]; then
      echo $f >> postProc/fields/timeList.txt
   fi
done
