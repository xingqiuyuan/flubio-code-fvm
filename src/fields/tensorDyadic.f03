module tensorDyadic

    use flubioTensors

    implicit none

    interface operator (.dya.)
        ! dyadic product of rank 2 and rank 4 tensor combinations
        module procedure dyadic11
        module procedure dyadic22
        module procedure dyadic2s2s
    end interface
    
    interface operator (.cdya.)
        ! symmetric crossed dyadic product of rank 2 tensor combinations
        ! (i,j,k,l) = 1/2 * (i,k,j,l)*(i,l,j,k)
        module procedure crossdyadic22
        module procedure crossdyadic2s2s
    end interface
    
    interface operator (.icdya.)
        ! inner crossed dyadic product of rank 2 tensor combinations
        ! (i,j,k,l) = (i,k,j,l)
        module procedure icrossdyadic22
        module procedure icrossdyadic2s2s
    end interface

contains 

!**********************************************************************************************************************

function dyadic11(T1, T2) result(Tdydic)

    type(ttbTensor1), intent(IN) :: T1, T2
    
    type(ttbTensor2) :: Tdydic
!------------------------------------------------------------------------------------------------------------------

    integer :: iElement
!------------------------------------------------------------------------------------------------------------------

    call Tdydic%create(T1%dim)

    do iElement=1,T1%dim 
        Tdydic%T(iElement) = T1%T(iElement) .dya. T2%T(iElement)
    enddo

end function dyadic11

!**********************************************************************************************************************

function dyadic22(T1, T2) result(Tdydic)

    type(ttbTensor2), intent(IN) :: T1, T2
    
    type(ttbTensor4) :: Tdydic
!------------------------------------------------------------------------------------------------------------------

    integer :: iElement
!------------------------------------------------------------------------------------------------------------------

    call Tdydic%create(T1%dim)

    do iElement=1,T1%dim 
        Tdydic%T(iElement) = T1%T(iElement) .dya. T2%T(iElement)
    enddo

end function dyadic22

!**********************************************************************************************************************

function dyadic2s2s(T1, T2) result(Tdydic)

    type(ttbTensor2s), intent(IN) :: T1, T2
    
    type(ttbTensor4s) :: Tdydic
!------------------------------------------------------------------------------------------------------------------

    integer :: iElement
!------------------------------------------------------------------------------------------------------------------

    call Tdydic%create(T1%dim)

    do iElement=1,T1%dim 
        Tdydic%T(iElement) = T1%T(iElement) .dya. T2%T(iElement)
    enddo

end function dyadic2s2s

!**********************************************************************************************************************

function crossdyadic22(T1, T2) result(Tdydic)

    type(ttbTensor2), intent(IN) :: T1, T2
    
    type(ttbTensor4) :: Tdydic
!------------------------------------------------------------------------------------------------------------------

    integer :: iElement
!------------------------------------------------------------------------------------------------------------------

    call Tdydic%create(T1%dim)

    do iElement=1,T1%dim 
        Tdydic%T(iElement) = T1%T(iElement) .cdya. T2%T(iElement)
    enddo

end function crossdyadic22

!**********************************************************************************************************************

function crossdyadic2s2s(T1, T2) result(Tdydic)

    type(ttbTensor2s), intent(IN) :: T1, T2
    
    type(ttbTensor4s) :: Tdydic
!------------------------------------------------------------------------------------------------------------------

    integer :: iElement
!------------------------------------------------------------------------------------------------------------------

    call Tdydic%create(T1%dim)

    do iElement=1,T1%dim 
        Tdydic%T(iElement) = T1%T(iElement) .cdya. T2%T(iElement)
    enddo

end function crossdyadic2s2s

!**********************************************************************************************************************

function icrossdyadic22(T1, T2) result(Tdydic)

    type(ttbTensor2), intent(IN) :: T1, T2
    
    type(ttbTensor4) :: Tdydic
!------------------------------------------------------------------------------------------------------------------

    integer :: iElement
!------------------------------------------------------------------------------------------------------------------

    call Tdydic%create(T1%dim)

    do iElement=1,T1%dim 
        Tdydic%T(iElement) = T1%T(iElement) .icdya. T2%T(iElement)
    enddo

end function icrossdyadic22

!**********************************************************************************************************************

function icrossdyadic2s2s(T1, T2) result(Tdydic)

    type(ttbTensor2s), intent(IN) :: T1, T2
    
    type(ttbTensor4s) :: Tdydic
!------------------------------------------------------------------------------------------------------------------

    integer :: iElement
!------------------------------------------------------------------------------------------------------------------

    call Tdydic%create(T1%dim)

    do iElement=1,T1%dim 
        Tdydic%T(iElement) = T1%T(iElement) .icdya. T2%T(iElement)
    enddo

end function icrossdyadic2s2s

!**********************************************************************************************************************

end module tensorDyadic