#include <iostream>
#include <iterator>
#include <iomanip>
#include <fstream>
#include <vector>
#include <numeric>
#include <cmath>
#include <string>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/foreach.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/scope_exit.hpp>

#include <amgcl/backend/builtin.hpp>
#include <amgcl/value_type/static_matrix.hpp>
#include <amgcl/adapter/crs_tuple.hpp>
#include <amgcl/adapter/block_matrix.hpp>

#if defined(SOLVER_BACKEND_VEXCL)
#  include <amgcl/backend/vexcl.hpp>
#  include <amgcl/backend/vexcl_static_matrix.hpp>
#elif defined(SOLVER_BACKEND_CUDA)
#  include <amgcl/backend/cuda.hpp>
#  include <amgcl/relaxation/cusparse_ilu0.hpp>
#else
#  ifndef SOLVER_BACKEND_BUILTIN
#    define SOLVER_BACKEND_BUILTIN
#  endif
#endif

#include <amgcl/mpi/util.hpp>
#include <amgcl/mpi/make_solver.hpp>
#include <amgcl/mpi/preconditioner.hpp>
#include <amgcl/mpi/solver/runtime.hpp>
#include <amgcl/relaxation/as_preconditioner.hpp>

#include <amgcl/mpi/distributed_matrix.hpp>
#include <amgcl/mpi/schur_pressure_correction.hpp>
#include <amgcl/mpi/block_preconditioner.hpp>
#include <amgcl/mpi/amg.hpp>
#include <amgcl/mpi/coarsening/runtime.hpp>
#include <amgcl/mpi/relaxation/runtime.hpp>
#include <amgcl/mpi/partition/runtime.hpp>
#include <amgcl/mpi/direct_solver/runtime.hpp>
#include <amgcl/mpi/solver/runtime.hpp>
#include <amgcl/mpi/cpr.hpp>

#include <amgcl/io/mm.hpp>
#include <amgcl/io/binary.hpp>
#include <amgcl/profiler.hpp>

namespace amgcl {
    profiler<> prof;
}

namespace math = amgcl::math;

//*******************************************************************************************************************//

extern "C"{

    void solverAMGCL(int* C, int* R, double* V, double* F, double* sol, 
                     int* n,  double* tols, int* solverOptions, int* niters, double* xbar);   
 
    void solverAMGCLSchur(int* C, int* R, double* V, double* F, double* sol, 
                     int* n,  double* tols, int* solverOptions,  int* solverOptionsU, 
                                            int* solverOptionsP, int* niters, double* xbar, double* res); 
    void solverAMGCLCPR(int* C, int* R, double* V, double* F, double* sol, 
                     int* n,  double* tols, int* solverOptions,  int* solverOptionsU, 
                                            int* solverOptionsP, int* niters, double* xbar, double* res); 

    void c_set_omp_num_threads();
}

//*******************************************************************************************************************//

void c_set_omp_num_threads(){
    int ierr = setenv("OMP_NUM_THREADS","1",1);
    if(ierr==-1){
        std::cout << "setting OMP_NUM_THREADS through C++ in c_set_omp_num_threads did not work!" << std::endl;
    }
}

std::string getSolverOptions(int opt){

    std::vector<std::string> options; 
    options.reserve(9);
    options.push_back("cg");
    options.push_back("bicgstab");
    options.push_back("bicgstabl");
    options.push_back("gmres");
    options.push_back("lgmres");
    options.push_back("fgmres");
    options.push_back("idrs");
    options.push_back("richardson");
    options.push_back("preonly");

    std::string option = options.at(opt);

    return option;
}

std::string getCoarseningOptions(int opt){

    std::vector<std::string> options;
    options.reserve(2);
    options.push_back("aggregation");
    options.push_back("smoothed_aggregation");

    std::string option = options.at(opt);

    return option;
}

std::string getRelaxationOptions(int opt){

    std::vector<std::string> options;
    options.reserve(9);

    options.push_back("damped_jacobi");
    options.push_back("gauss_seidel");
    options.push_back("ilu0");
    options.push_back("iluk");
    options.push_back("ilut");
    options.push_back("ilup");
    options.push_back("spai0");
    options.push_back("spai1");
    options.push_back("chebyshev");
    
    std::string option = options.at(opt);

    return option;
}

double norm_1(std::vector<double>& x){

    // Define a local communicator using the global one 
    amgcl::mpi::communicator comm(MPI_COMM_WORLD);

    // Compute norm_1
    double lnorm1 = 0.0;
    double gnorm1 = 0.0;
    
    for(int i=0;i<x.size();i++){
        lnorm1 += fabs(x.at(i));
    }

    // Sum up processor's contribution
    gnorm1 = comm.reduce(MPI_SUM, lnorm1);

    return gnorm1;

}


template<typename T>
void computeResidual(std::shared_ptr<T> solver, std::vector<double>& xsol, std::vector<double>& rhs, 
                     double& xbar, double& initResidual, double& scalingFactor){

    // Local working vectors
    std::vector<double> ax(xsol.size(), 0.0); 
    std::vector<double> axbar(xsol.size(), 0.0); 
    std::vector<double> Ax_Axbar;
    std::vector<double> b_Ax; 

    // Compute working vectors
    amgcl::backend::spmv(1, solver->system_matrix(), xsol, 0, ax);
    amgcl::backend::spmv(1, solver->system_matrix(), ax, 0, axbar);

    // Add vectors
    std::transform(ax.begin(), ax.end(), axbar.begin(), 
                   std::back_inserter(Ax_Axbar), std::minus<double>());

    std::transform(ax.begin(), ax.end(), rhs.begin(), 
                   std::back_inserter(b_Ax), std::minus<double>());

    // Compute residual and its scaling factor
    double small = 1.0e-12;
    scalingFactor = norm_1(Ax_Axbar) + norm_1(b_Ax) + small;
    initResidual = norm_1(b_Ax);
}

void solverAMGCL(int* C, int* R, double* V, double* F, double* sol, 
                 int* n, double* tols, int* solverOptions, int* niters, double* xbar){

    int ne = n[0];
    int nnz = n[1];
   
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

#if defined(SOLVER_BACKEND_BUILTIN)
    typedef amgcl::backend::builtin<double> Backend;
#elif defined(SOLVER_BACKEND_VEXCL)
    typedef amgcl::backend::vexcl<double> Backend;
#elif defined(SOLVER_BACKEND_CUDA)
    typedef amgcl::backend::cuda<double> Backend;
#endif

    // Create amgcl solver
    typedef
        amgcl::mpi::make_solver<
            amgcl::runtime::mpi::preconditioner<Backend>,
            amgcl::runtime::mpi::solver::wrapper<Backend>
            >
        Solver;

    // Define the profiler 
    using amgcl::prof;

    // Cackend parameters
    typename Backend::params bprm;

    // Convert CSR arrays in vectors
    std::vector<double> f(F, F + ne);
    std::vector<int> ptr(R, R + ne+1);
    std::vector<int> col(C, C + nnz);
    std::vector<double> val(V, V + nnz);
    std::vector<double> xsol(sol, sol + ne);

// Copy the rhs to GPU if required
#if defined(SOLVER_BACKEND_BUILTIN)
    amgcl::backend::numa_vector<double> rhs(f);
#elif defined(SOLVER_BACKEND_VEXCL)
    vex::Context ctx(vex::Filter::Env);
    bprm.q = ctx;

    if (rank == 0) std::cout << ctx << std::endl;

    vex::vector<double> rhs(ctx, f);
#elif defined(SOLVER_BACKEND_CUDA)
    cusparseCreate(&bprm.cusparse_handle);
    thrust::device_vector<double> rhs(f);
#endif

    // Fill options for AMGCL
    int opt = solverOptions[0];
    std::string solverOpt = getSolverOptions(opt);

    opt = solverOptions[1];
    std::string coarseningOpt = getCoarseningOptions(opt);

    opt = solverOptions[2];
    std::string relaxationOpt = getRelaxationOptions(opt);

    boost::property_tree::ptree prm;
    prm.put("solver.type", solverOpt);

    prm.put("solver.abstol", tols[0]);
    prm.put("solver.tol", tols[1]);

    prm.put("precond.npre", solverOptions[3]);
    prm.put("precond.npost", solverOptions[4]); 
    prm.put("precond.ncycle", solverOptions[5]); 
    prm.put("solver.maxiter", solverOptions[6]); 
    prm.put("precond.pre_cycles", solverOptions[7]);

    if(solverOptions[8]>-1){
        prm.put("precond.coarse_enough", solverOptions[8]);
    }
    
    prm.put("precond.coarsening.type", coarseningOpt);
    prm.put("precond.relax.type", relaxationOpt);
    prm.put("precond.coarsening.aggr.eps_strong", tols[2]);    
    
    //prm.put("precond.direct_coarse", true);
    //bool do_trunc = (solverOptions[14] != 0);
    //prm.put("precond.coarsening.aggr.do_trunc", do_trunc);   
    //prm.put("precond.coarsening.eps_trunc", tols[3]);
    //bool allow_rebuild = (solverOptions[11] != 0);
    //prm.put("precond.allow_rebuild", allow_rebuild);    

    // Setup solver
    std::shared_ptr<Solver> solve;
    amgcl::backend::crs<double> A(std::tie(ne, ptr, col, val));
    solve = std::make_shared<Solver>(MPI_COMM_WORLD, A, prm, bprm);

// Create the solution vector 
#if defined(SOLVER_BACKEND_BUILTIN)
    amgcl::backend::numa_vector<double> x(sol, sol + ne);
#elif defined(SOLVER_BACKEND_VEXCL)
    vex::vector<double> x(ctx, sol, sol + ne);
#elif defined(SOLVER_BACKEND_CUDA)
    thrust::device_vector<double> x(sol, sol + ne);
#endif

    // Solve the linear system
    int    iters;
    double error;

    // Inital residual
    //double initRes = 0;
    //double scalingFactor = 0;
    //computeResidual(solve, xsol, f, xbar[0], initRes, scalingFactor);
    //res[0] = initResidual;
    //res[1] = initResidual/scalingFactor;

    // Solve the linear system
    std::tie(iters, error) = (*solve)(rhs, x);
  
    niters[0] = iters;

// Copy the solution back to fortran
#if defined(SOLVER_BACKEND_BUILTIN)
    // copy out values
    for(int i=0;i<ne;i++){
        sol[i] = x[i];
    }
#elif defined(SOLVER_BACKEND_VEXCL)
    int error;
    if(rank==0){
        std::cout << "FLUBIO: a way to copy back the solution is still to be found. This does not mean it is difficult, simply I had no time to look at it yet." << std::endl;
    }
    MPI_Abort(MPI_COMM_WORLD, error)
#elif defined(SOLVER_BACKEND_CUDA)
    int error;
    if(rank==0){
        std::cout << "FLUBIO: a way to copy back the solution is still to be found. This does not mean it is difficult, simply I had no time to look at it yet." << std:endl;
    }
    MPI_Abort(MPI_COMM_WORLD, error)
#endif

/*
    //Final Residual
    std::vector<double> xfinal(sol, sol + ne); 
    computeResidual(solve, xfinal, f, xbar[0], initRes, scalingFactor);
    res[2] = finalResidual;
    res[3] = finalResidual/scalingFactor;
*/ 

}

void solverAMGCLSchur(int* C, int* R, double* V, double* F, double* sol, 
                 int* n, double* tols, int* solverOptions, int* solverOptionsU, int* solverOptionsP, int* niters, double* xbar, double* res){

    // blocksize*ne
    int ne = n[0];

    // blocksize*blocksize*nnz
    int nnz = n[1];

    // block size (3 in 2D, 4 in 3D) 
    int blocksize = n[2];
   
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

#if defined(SOLVER_BACKEND_BUILTIN)
    typedef amgcl::backend::builtin<double> Backend;
#elif defined(SOLVER_BACKEND_VEXCL)
    typedef amgcl::backend::vexcl<double> Backend;
#elif defined(SOLVER_BACKEND_CUDA)
    typedef amgcl::backend::cuda<double> Backend;
#endif

    // Define the profiler 
    using amgcl::prof;

    // Cackend parameters
    typename Backend::params bprm;

    // Convert CSR arrays in vectors
    std::vector<double> f(F, F + ne);
    std::vector<int> ptr(R, R + ne+1);
    std::vector<int> col(C, C + nnz);
    std::vector<double> val(V, V + nnz);
    std::vector<double> xsol(sol, sol + ne);

// Copy the rhs to GPU if required
#if defined(SOLVER_BACKEND_BUILTIN)
    amgcl::backend::numa_vector<double> rhs(f);
#elif defined(SOLVER_BACKEND_VEXCL)
    vex::Context ctx(vex::Filter::Env);
    bprm.q = ctx;

    if (rank == 0) std::cout << ctx << std::endl;

    vex::vector<double> rhs(ctx, f);
#elif defined(SOLVER_BACKEND_CUDA)
    cusparseCreate(&bprm.cusparse_handle);
    thrust::device_vector<double> rhs(f);
#endif

    boost::property_tree::ptree prm;

    // Mark pressure variables
    std::vector<char> pm(ne, 0);
    for(ptrdiff_t i = 0; i < ne; i += blocksize) pm[i] = 1;
    prm.put("precond.pmask", static_cast<void*>(&pm[0]));
    prm.put("precond.pmask_size", ne);

    // Setup solver
    std::string coarseningOpt;
    std::string relaxationOpt;
    std::string solverOpt; 

    // Outer solver 
    int opt = solverOptions[0];
    solverOpt = getSolverOptions(opt); 

    prm.put("solver.type", solverOpt);
    prm.put("solver.abstol", tols[0]);
    prm.put("solver.tol", tols[1]);
    prm.put("solver.maxiter", solverOptions[1]);
    prm.put("solver.M", solverOptions[2]);  

    // U solver
    opt = solverOptionsU[0];
    solverOpt = getSolverOptions(opt); 
    prm.put("precond.usolver.solver.type", solverOpt);
    prm.put("precond.usolver.solver.abstol", tols[6]);
    prm.put("precond.usolver.solver.tol", tols[7]);
    prm.put("precond.usolver.solver.maxiter", solverOptionsU[6]); 

    // GMRES restart
    if (solverOpt.find("gmres") != std::string::npos) {
        prm.put("precond.usolver.solver.M", solverOptionsU[10]);
    }

    // P solver
    opt = solverOptionsP[0];
    solverOpt = getSolverOptions(opt); 
    opt = solverOptionsP[1];
    coarseningOpt = getCoarseningOptions(opt);
    opt = solverOptionsP[2];
    relaxationOpt = getRelaxationOptions(opt);

    prm.put("precond.psolver.solver.type", solverOpt);
    prm.put("precond.psolver.solver.abstol", tols[8]);
    prm.put("precond.psolver.solver.tol", tols[9]);
 
    prm.put("precond.psolver.precond.coarsening.type", coarseningOpt);
    prm.put("precond.psolver.precond.relax.type", relaxationOpt);
    prm.put("precond.psolver.precond.npre", solverOptionsP[3]);
    prm.put("precond.psolver.precond.npost", solverOptionsP[4]); 
    prm.put("precond.psolver.precond.ncycle", solverOptionsP[5]);
    prm.put("precond.psolver.solver.maxiter", solverOptionsP[6]);  
    prm.put("precond.psolver.precond.pre_cycles", solverOptionsP[7]);
    prm.put("precond.psolver.precond.coarsening.aggr.eps_strong", tols[4]);
   

    // HARDCODED PARAMTERS!
    /*
    prm.put("solver.type", "gmres");
    prm.put("solver.M", 50);
    prm.put("solver.tol", 1e-6);

    prm.put("precond.usolver.solver.type", "preonly");
    prm.put("precond.usolver.solver.tol", 1e-2);

    prm.put("precond.psolver.solver.type", "preonly");
    prm.put("precond.psolver.solver.tol", 1e-3);
    */

    // Schur pressure correction solver
    typedef
        amgcl::mpi::make_solver<
            amgcl::mpi::schur_pressure_correction<
                amgcl::mpi::make_solver<
                    amgcl::mpi::block_preconditioner<
                        amgcl::relaxation::as_preconditioner<Backend, amgcl::runtime::relaxation::wrapper>
                        >,
                    amgcl::runtime::mpi::solver::wrapper<Backend>
                    >,
                amgcl::mpi::make_solver<
                    amgcl::mpi::amg<
                        Backend,
                        amgcl::runtime::mpi::coarsening::wrapper<Backend>,
                        amgcl::runtime::mpi::relaxation::wrapper<Backend>,
                        amgcl::runtime::mpi::direct::solver<double>,
                        amgcl::runtime::mpi::partition::wrapper<Backend>
                        >,
                        amgcl::runtime::mpi::solver::wrapper<Backend>
                    >
                >,
                amgcl::runtime::mpi::solver::wrapper<Backend>
            > Solver;

    //Solver solve(MPI_COMM_WORLD, std::tie(ne, ptr, col, val), prm, bprm);

    // Setup solver as a pointer
    std::shared_ptr<Solver> solve;
    amgcl::backend::crs<double> A(std::tie(ne, ptr, col, val));
    solve = std::make_shared<Solver>(MPI_COMM_WORLD, A, prm, bprm);

// Create the solution vector 
#if defined(SOLVER_BACKEND_BUILTIN)
    amgcl::backend::numa_vector<double> x(sol, sol + ne);
#elif defined(SOLVER_BACKEND_VEXCL)
    vex::vector<double> x(ctx, sol, sol + ne);
#elif defined(SOLVER_BACKEND_CUDA)
    thrust::device_vector<double> x(sol, sol + ne);
#endif

    // Inital residual
    double initRes = 0.0;
    double finalRes = 0.0;
    double scalingFactor = 0;
    computeResidual(solve, xsol, f, xbar[0], initRes, scalingFactor);
    res[0] = initRes;
    res[1] = scalingFactor;

    // Solve the linear system
    int    iters;
    double error;
    std::tie(iters, error) = (*solve)(rhs, x);
    //std::tie(iters, error) = solve(rhs, x);

    niters[0] = iters;

// Copy the solution back to fortran
#if defined(SOLVER_BACKEND_BUILTIN)
    // copy out values
    for(int i=0;i<ne;i++){
        sol[i] = x[i];
    }
#elif defined(SOLVER_BACKEND_VEXCL)
    int error;
    if(rank==0){
        std::cout << "FLUBIO: a way to copy back the solution is still to be found. This does not mean it is difficult, simply I had no time to look at it yet." << std::endl;
    }
    MPI_Abort(MPI_COMM_WORLD, error)
#elif defined(SOLVER_BACKEND_CUDA)
    int error;
    if(rank==0){
        std::cout << "FLUBIO: a way to copy back the solution is still to be found. This does not mean it is difficult, simply I had no time to look at it yet." << std:endl;
    }
    MPI_Abort(MPI_COMM_WORLD, error)
#endif

    //Final Residual
    std::vector<double> xfinal(sol, sol + ne); 
    computeResidual(solve, xfinal, f, xbar[0], finalRes, scalingFactor);
    res[2] = finalRes;
    res[3] = scalingFactor;
}

void solverAMGCLCPR(int* C, int* R, double* V, double* F, double* sol, 
                 int* n, double* tols, int* solverOptions, int* solverOptionsU, int* solverOptionsP, int* niters, double* xbar, double* res){

    // blocksize*ne
    int ne = n[0];

    // blocksize*blocksize*nnz
    int nnz = n[1];

    // block size (3 in 2D, 4 in 3D) 
    int blocksize = n[2];
   
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

#if defined(SOLVER_BACKEND_BUILTIN)
    typedef amgcl::backend::builtin<double> Backend;
#elif defined(SOLVER_BACKEND_VEXCL)
    typedef amgcl::backend::vexcl<double> Backend;
#elif defined(SOLVER_BACKEND_CUDA)
    typedef amgcl::backend::cuda<double> Backend;
#endif

    // Define the profiler 
    using amgcl::prof;

    // Cackend parameters
    typename Backend::params bprm;

    // Convert CSR arrays in vectors
    std::vector<double> f(F, F + ne);
    std::vector<int> ptr(R, R + ne+1);
    std::vector<int> col(C, C + nnz);
    std::vector<double> val(V, V + nnz);
    std::vector<double> xsol(sol, sol + ne);

// Copy the rhs to GPU if required
#if defined(SOLVER_BACKEND_BUILTIN)
    amgcl::backend::numa_vector<double> rhs(f);
#elif defined(SOLVER_BACKEND_VEXCL)
    vex::Context ctx(vex::Filter::Env);
    bprm.q = ctx;

    if (rank == 0) std::cout << ctx << std::endl;

    vex::vector<double> rhs(ctx, f);
#elif defined(SOLVER_BACKEND_CUDA)
    cusparseCreate(&bprm.cusparse_handle);
    thrust::device_vector<double> rhs(f);
#endif

    boost::property_tree::ptree prm;

    // Mark pressure variables
    std::vector<char> pm(ne, 0);
    for(ptrdiff_t i = 0; i < ne; i += blocksize) pm[i] = 1;

    prm.put("precond.block_size", blocksize);

    // Setup solver
    std::string coarseningOpt;
    std::string relaxationOpt;
    std::string solverOpt; 

/*
    // Outer solver 
    int opt = solverOptions[0];
    solverOpt = getSolverOptions(opt); 

    prm.put("solver.type", solverOpt);
    prm.put("solver.abstol", tols[0]);
    prm.put("solver.tol", tols[1]);
    prm.put("solver.maxiter", solverOptions[1]);
    prm.put("solver.M", solverOptions[2]);  

    // U solver
    opt = solverOptionsU[0];
    solverOpt = getSolverOptions(opt); 
    prm.put("precond.usolver.solver.type", solverOpt);
    prm.put("precond.usolver.solver.abstol", tols[6]);
    prm.put("precond.usolver.solver.tol", tols[7]);
    prm.put("precond.usolver.solver.maxiter", solverOptionsU[6]); 

    // GMRES restart
    if (solverOpt.find("gmres") != std::string::npos) {
        prm.put("precond.usolver.solver.M", solverOptionsU[10]);
    }

    // P solver
    opt = solverOptionsP[0];
    solverOpt = getSolverOptions(opt); 
    opt = solverOptionsP[1];
    coarseningOpt = getCoarseningOptions(opt);
    opt = solverOptionsP[2];
    relaxationOpt = getRelaxationOptions(opt);

    prm.put("precond.psolver.solver.type", solverOpt);
    prm.put("precond.psolver.solver.abstol", tols[8]);
    prm.put("precond.psolver.solver.tol", tols[9]);
 
    prm.put("precond.psolver.precond.coarsening.type", coarseningOpt);
    prm.put("precond.psolver.precond.relax.type", relaxationOpt);
    prm.put("precond.psolver.precond.npre", solverOptionsP[3]);
    prm.put("precond.psolver.precond.npost", solverOptionsP[4]); 
    prm.put("precond.psolver.precond.ncycle", solverOptionsP[5]);
    prm.put("precond.psolver.solver.maxiter", solverOptionsP[6]);  
    prm.put("precond.psolver.precond.pre_cycles", solverOptionsP[7]);
    prm.put("precond.psolver.precond.coarsening.aggr.eps_strong", tols[4]); 
*/

    // HARDCODED PARAMTERS!
    /*
    prm.put("solver.type", "gmres");
    prm.put("solver.M", 50);
    prm.put("solver.tol", 1e-6);

    prm.put("precond.usolver.solver.type", "preonly");
    prm.put("precond.usolver.solver.tol", 1e-2);

    prm.put("precond.psolver.solver.type", "preonly");
    prm.put("precond.psolver.solver.tol", 1e-3);
    */

    //CPR solver
    typedef
        amgcl::mpi::make_solver<
            amgcl::mpi::cpr<
                amgcl::mpi::amg<
                    Backend,
                    amgcl::runtime::mpi::coarsening::wrapper<Backend>,
                    amgcl::runtime::mpi::relaxation::wrapper<Backend>,
                    amgcl::runtime::mpi::direct::solver<double>,
                    amgcl::runtime::mpi::partition::wrapper<Backend>
                    >,
                amgcl::mpi::relaxation::as_preconditioner<
                    amgcl::runtime::mpi::relaxation::wrapper<Backend>
                    >
                >,
            amgcl::runtime::mpi::solver::wrapper<Backend>
            >
        Solver;

    // Setup solver as a pointer
    std::shared_ptr<Solver> solve;
    amgcl::backend::crs<double> A(std::tie(ne, ptr, col, val));
    solve = std::make_shared<Solver>(MPI_COMM_WORLD, A, prm, bprm);

// Create the solution vector 
#if defined(SOLVER_BACKEND_BUILTIN)
    amgcl::backend::numa_vector<double> x(sol, sol + ne);
#elif defined(SOLVER_BACKEND_VEXCL)
    vex::vector<double> x(ctx, sol, sol + ne);
#elif defined(SOLVER_BACKEND_CUDA)
    thrust::device_vector<double> x(sol, sol + ne);
#endif

    // Inital residual
    double initRes = 0.0;
    double finalRes = 0.0;
    double scalingFactor = 0;
    computeResidual(solve, xsol, f, xbar[0], initRes, scalingFactor);
    res[0] = initRes;
    res[1] = scalingFactor;

    // Solve the linear system
    int    iters;
    double error;
    std::tie(iters, error) = (*solve)(rhs, x);
    
    //std::tie(iters, error) = solve(rhs, x);

    niters[0] = iters;

// Copy the solution back to fortran
#if defined(SOLVER_BACKEND_BUILTIN)
    // copy out values
    for(int i=0;i<ne;i++){
        sol[i] = x[i];
    }
#elif defined(SOLVER_BACKEND_VEXCL)
    int error;
    if(rank==0){
        std::cout << "FLUBIO: a way to copy back the solution is still to be found. This does not mean it is difficult, simply I had no time to look at it yet." << std::endl;
    }
    MPI_Abort(MPI_COMM_WORLD, error)
#elif defined(SOLVER_BACKEND_CUDA)
    int error;
    if(rank==0){
        std::cout << "FLUBIO: a way to copy back the solution is still to be found. This does not mean it is difficult, simply I had no time to look at it yet." << std:endl;
    }
    MPI_Abort(MPI_COMM_WORLD, error)
#endif

    //Final Residual
    std::vector<double> xfinal(sol, sol + ne); 
    computeResidual(solve, xfinal, f, xbar[0], finalRes, scalingFactor);
    res[2] = finalRes;
    res[3] = scalingFactor;
}