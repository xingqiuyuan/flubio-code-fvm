	program writeProcAddressing

		implicit none

		character(10) procID, proc
		character*10, dimension(:), allocatable :: args
	!------------------------------------------------------------------------------------------------------------------

		integer :: iElement, iFace, numberOfElements, numberOfFaces

		integer :: n, no, nn, max_owner, max_neighbour

		integer, dimension(:), allocatable :: owner, neighbour
	!------------------------------------------------------------------------------------------------------------------

		!--------------------------!
		! Read Owner and Neighbour !
		!--------------------------!

		open(1,file='processor0/FLUBIO_owner')
			read(1,*) no
			allocate(owner(no))

			do n=1,no
				read(1,*) owner(n)
			enddo
		close(1)

		open(1,file='processor0/FLUBIO_neighbour')
			read(1,*) nn
			allocate(neighbour(nn))

			do n=1,nn
				read(1,*) neighbour(n)
			enddo
		close(1)

		! Set sizes, very important
		owner = owner+1
		neighbour = neighbour+1
		max_owner = maxval(owner)
		max_neighbour = maxval(neighbour)
		numberOfElements = max0(max_owner, max_neighbour)

		open(1, file='processor0/FLUBIO_faces')
			read(1,*) numberOfFaces
		close(1)

		! Write cellProcAddr
		open(1, file='processor0/FLUBIO_cellProcAddressing_r')
			 write(1,'(i0)') numberOfElements
			 do iElement=1,numberOfElements
				write(1,'(i0)') iElement
			 enddo
		close(1)

		! Write faceProcAddressing
		open(1, file='processor0/FLUBIO_faceProcAddressing_r')
			write(1,'(i0)') numberOfFaces
			do iFace=1,numberOfFaces
				write(1,'(i0)') iFace
			enddo
		close(1)

	end program writeProcAddressing

!********************************************************************************************************************************


