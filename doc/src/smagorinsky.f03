!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module SmagorinskyModel

    use LES

    implicit none

    type, public, extends(subgridModel) :: smagorinsky

        real :: Cs

    contains
        procedure :: modelConstants =>  setupSmagorinsky
        procedure :: updateEddyViscosity => smagorinskyEddyViscosity

    end type smagorinsky


contains

! *********************************************************************************************************************

    subroutine setupSmagorinsky(this)

    !==================================================================================================================
    ! Description:
    !! setupSmagorinsky sets up Smagorinsky model
    !==================================================================================================================

        class(smagorinsky) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: optName
    !------------------------------------------------------------------------------------------------------------------

        logical :: found
    !------------------------------------------------------------------------------------------------------------------

        call flubioTurbModel%json%get('Cs', this%Cs, found)
        call raiseKeyErrorJSON('Cs', found)

        call this%createLesModel()

    end subroutine setupSmagorinsky

! *********************************************************************************************************************

    subroutine smagorinskyEddyViscosity(this)

    !==================================================================================================================
    ! Description:
    !! subgridSmag implementes the Smagorinsky LES model.
    !==================================================================================================================

        class(smagorinsky) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer :: iElement
    !------------------------------------------------------------------------------------------------------------------

        real :: S(numberOfElements,6)
        !! Strain rate tensor

        real :: Ssum(numberOfElements)
        !! Strain rate tensor product with itself

        real :: modS(numberOfElements)
        !! Strain rate module

        real :: coeff
        !! Smagorinsky constant
    !------------------------------------------------------------------------------------------------------------------

        !-----------------------!
        ! Compute strain tensor !
        !-----------------------!

        modS = strainTensorModule()

        !------------------------!
        ! Compute eddy viscosity !
        !------------------------!

        do iElement=1,numberOfElements
            coeff=(this%Cs*this%fwidth(iElement))**2
            this%nut%phi(iElement,1) = rho%phi(iElement,1)*coeff*modS(iElement)
            nu%phi(iElement,1) = viscos+this%nut%phi(iElement,1)
        enddo

        ! Update nu and nut at boundaries
        call this%nutWallFunction()

        call this%nut%updateGhosts()
        call nu%updateGhosts()

    end subroutine smagorinskyEddyViscosity

! *********************************************************************************************************************

end module SmagorinskyModel