!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module runTimeTasks

    use CoField
    use continuityErrors
    use sumFields
    use multiplyFields
    use minMaxFields
    use massFlowRate
    use surfaceFlux
    use derivedFields
    use forceMonitors
    use probes
    use lineProbes
    use sampledSurfaces
    use statistics
    use recipeHandling

    implicit none

    type, public :: runTimeObjs
        class(runTimeObj), allocatable :: task
    end type runTimeObjs

    ! Global array for runtime objects
    type(runTimeObjs), dimension(:), allocatable, target :: runTimeObjects

contains

    subroutine setMonitorsList()

    !==================================================================================================================
    ! Description:
    !! setRunTimeProc fills the the run time processing dictionary.
    !==================================================================================================================

    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName, typeValue, monitorName, path
        !! First array element lenght

        character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------

        integer :: iMonitor, taskIndex
    !------------------------------------------------------------------------------------------------------------------

        logical :: keyFound, typeFound, monitorFound, monitorsFound, nameFound
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer
    !------------------------------------------------------------------------------------------------------------------

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)

        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)

        ! Allocate the monitors array
        allocate(runTimeObjects(flubioMonitors%numberOfMonitors))
        
        taskIndex = 0
        do iMonitor=1,flubioMonitors%numberOfMonitors

            ! Get monitors pointer
            call jcore%get_child(monitorsPointer, iMonitor, monitorPointer, monitorFound)

            ! Get monitor name
            call jcore%get_path(monitorPointer, path, nameFound)
            if(nameFound) then
                call split_in_array(path, splitPath, '.')
                monitorName = splitPath(2)
            else
                call flubioStopMsg('ERROR: cannot find the monitor name, please check if the dictionary is correct.')
            end if

            ! Get monitor type
            call jcore%get(monitorPointer, 'type', typeValue, typeFound)

            if(typeFound)then

                taskIndex = taskIndex + 1

                if(lowercase(trim(typeValue))=='forcecoeffs') then
                    allocate(forceCoeffs::runTimeObjects(iMonitor)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = typeValue
                elseif(lowercase(trim(typeValue))=='pressurecoeff') then
                    allocate(Cp::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = typeValue
                elseif(lowercase(trim(typeValue))=='frictioncoeff') then
                    allocate(Cf::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = typeValue
                elseif(lowercase(trim(typeValue))=='flowstatistics') then
                    allocate(flowStats::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = typeValue

                ! Derived Field
                elseif(lowercase(trim(typeValue))=='cp0') then
                    allocate(CP0::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='vorticity') then
                    allocate(vorticity::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='qcriterion') then
                    allocate(Q::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='stainrate') then
                    allocate(E::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='yplus') then
                    allocate(yplus::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='co') then
                    allocate(Co::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='continuityerrors') then
                    allocate(contErr::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'

                ! Utilites
                elseif(lowercase(trim(typeValue))=='addfields') then
                    allocate(addObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='multiplyfields') then
                    allocate(multiplyObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'derivedfield'
                elseif(lowercase(trim(typeValue))=='minmax') then
                    allocate(minMaxObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'minmaxfield'
                elseif(lowercase(trim(typeValue))=='surfaceflux') then
                    allocate(surfaceFluxObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'surfaceflux'
                elseif(lowercase(trim(typeValue))=='massflowrate') then
                    allocate(massFlowRateObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'massflowrate'
                elseif(lowercase(trim(typeValue))=='recipehandling') then
                    allocate(recipeHandlingObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'recipehandling'                                                                                           

                ! Sampling
                elseif(lowercase(trim(typeValue))=='probes') then
                    allocate(probeObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'probes'
                elseif(lowercase(trim(typeValue))=='lineprobes') then
                    allocate(lineProbeObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'lineprobes'
                elseif(lowercase(trim(typeValue))=='sampledsurfaces') then
                    allocate(sampledSurfaceObj::runTimeObjects(taskIndex)%task)
                    runTimeObjects(taskIndex)%task%objName = trim(monitorName)
                    runTimeObjects(taskIndex)%task%objType = 'sampledsurfaces'
                else
                    call flubioStopMsg('ERROR: unknown monitor type: '//trim(typeValue))
                end if
            else
                call flubioStopMsg("ERROR: cannot find the monitor type. Please either specify it or check the spelling.")
            end if
        enddo

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()

    end subroutine setMonitorsList

!**********************************************************************************************************************

    subroutine initialiseTasks

    !==================================================================================================================
    ! Description:
    !! initialiseTasks initialises the tasks sequencially as they are stored in the polymorphic vector.
    !==================================================================================================================

        integer :: iTask
    !-----------------------------------------------------------------------------------------------------------------

        do iTask =1,flubioMonitors%numberOfMonitors
            call runTimeObjects(iTask)%task%initialise()
        enddo

    end subroutine initialiseTasks

!**********************************************************************************************************************

    subroutine runTasks

    !==================================================================================================================
    ! Description:
    !! runTasks runs the tasks sequencially as they are stored in the polymorphic vector.
    !==================================================================================================================

        integer :: iTask
    !-----------------------------------------------------------------------------------------------------------------

        do iTask =1,flubioMonitors%numberOfMonitors
            call runTimeObjects(iTask)%task%run()
        enddo

    end subroutine runTasks

!**********************************************************************************************************************

    function getNumberOfFieldTasks() result(nFieldTasks)

        !==================================================================================================================
        ! Description:
        !! runTasks runs the tasks sequencially as they are stored in the polymorphic vector.
        !==================================================================================================================
    
            integer :: iTask, nFieldTasks
        !-----------------------------------------------------------------------------------------------------------------
    
            nFieldTasks = 0
            do iTask =1,flubioMonitors%numberOfMonitors
                if(runTimeObjects(iTask)%task%objType=='derivedfield') nFieldTasks = nFieldTasks + 1
            enddo
    
    end function getNumberOfFieldTasks
    
!**********************************************************************************************************************

    function getFieldTasksPosition() result(pos)

    !==================================================================================================================
    ! Description:
    !! runTasks runs the tasks sequencially as they are stored in the polymorphic vector.
    !==================================================================================================================
    
            integer :: iTask, posIndex, nFieldTasks

            integer, dimension(:), allocatable :: pos
        !-----------------------------------------------------------------------------------------------------------------
    
            nFieldTasks = getNumberOfFieldTasks()

            allocate(pos(nFieldTasks))

            posIndex = 0
            do iTask =1,flubioMonitors%numberOfMonitors
                if(runTimeObjects(iTask)%task%objType=='derivedfield') then 
                    posIndex = posIndex + 1
                    pos(posIndex) = iTask
                endif    
            enddo
    
    end function getFieldTasksPosition
    
!**********************************************************************************************************************    
    
    subroutine getNumberOfFieldTasksFromDict(numberOfDerivedFields, derivedFieldPosition)

    !==================================================================================================================
    ! Description:
    !! getNumberOfFieldTasksFromDict finds out the number of dervied field tasks defined in the dictionary.
    !==================================================================================================================

            character(len=:), allocatable :: typeValue, monitorName, path
            !! First array element lenght
    
            character(len=50), dimension(:), allocatable  :: splitPath
    !------------------------------------------------------------------------------------------------------------------
    
            integer :: iMonitor, isize

            integer :: numberOfDerivedFields

            integer, dimension(:), allocatable :: derivedFieldPosition
    !------------------------------------------------------------------------------------------------------------------
    
            logical :: keyFound, typeFound, monitorFound, monitorsFound, nameFound
    !------------------------------------------------------------------------------------------------------------------
    
            type(json_core) :: jcore
    
            type(json_value), pointer :: dictPointer, monitorsPointer, monitorPointer

            type(qvector_t) :: tmpIndex
    !------------------------------------------------------------------------------------------------------------------
    
        isize = storage_size(iMonitor)/8
        numberOfDerivedFields = 0
        call tmpIndex%new(1, isize, QVECTOR_RESIZE_LINEAR)

        ! Get the pointer to the dictionary
        call flubioMonitors%json%get(dictPointer)
    
        ! Initialize json factory
        call jcore%initialize()
        call jcore%get(dictPointer, 'Monitors', monitorsPointer, monitorsFound)
    
        do iMonitor=1,flubioMonitors%numberOfMonitors
    
            ! Get monitors pointer
            call jcore%get_child(monitorsPointer, iMonitor, monitorPointer, monitorFound)
    
            ! Get monitor name
            call jcore%get_path(monitorPointer, path, nameFound)
            if(nameFound) then
                call split_in_array(path, splitPath, '.')
                monitorName = splitPath(2)
            else
                call flubioStopMsg('ERROR: cannot find the monitor name, please check if the dictionary is correct.')
            end if
    
            ! Get monitor type
            call jcore%get(monitorPointer, 'type', typeValue, typeFound)
            write(*,*) typeValue
            if(typeFound .and. lowercase(typeValue)=='derivedfield') then
                numberOfDerivedFields = numberOfDerivedFields + 1
                call tmpIndex%addlast(iMonitor)
            end if

        enddo
    
        call tmpIndex%toarray(derivedFieldPosition)

        ! Clean up
        nullify(dictPointer)
        if(monitorsFound) nullify(monitorsPointer)
        if(monitorFound) nullify(monitorPointer)
        call jcore%destroy()
    
    end subroutine getNumberOfFieldTasksFromDict
    
!**********************************************************************************************************************

end module runTimeTasks
