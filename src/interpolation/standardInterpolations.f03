!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module standardInterpolations

!==================================================================================================================
! Description:
!! standardInterpolations implements the methods to perform standard interpolations like linear or harmonic.
!==================================================================================================================

	use flubioDictionaries
	use flubioFields
	use meshvar

	implicit none 
	
contains

	subroutine harmonicInterpolation(field)

	!==================================================================================================================
	! Description: harmonicInterpolation performs the harmonic interpolation between two adjacent cells
	!! in order to find the face value.
	!! The harmonic interpolation is often preferred in the interpolation of physical quantities
	!! such as fluid density or viscosity.
	!==================================================================================================================

		type(flubioField) :: field
		!! field to interpolate
	!------------------------------------------------------------------------------------------------------------------

		integer :: iBoundary, iFace, iBFace, iOwner, iNeighbour

		integer :: i, is, ie, iComp, patchFace, pNeigh
	!------------------------------------------------------------------------------------------------------------------

		real :: aux(field%nComp)
		!! auxiliary value

		real :: small
	!------------------------------------------------------------------------------------------------------------------

		small = 1.0e-12

		!----------------!
		! Interior faces !
		!----------------!

		do iFace=1,numberOfIntFaces
	
		   iOwner = mesh%owner(iFace)
		   iNeighbour = mesh%neighbour(iFace)

		   aux = (1.0-mesh%gf(iFace))/(field%phi(iNeighbour,:)+small) + mesh%gf(iFace)/(field%phi(iOwner,:)+small)
		   field%phif(iFace,:) = 1.0/aux

		enddo

		!---------------!
		! BoundaryFaces !
		!---------------!

		patchFace = 0
		i = 0
		do iBoundary=1,numberOfBoundaries
	
		   is = mesh%boundaries%startFace(iBoundary)
		   ie = is+mesh%boundaries%nFace(iBoundary)-1
		   pNeigh = 0
	
		   if(mesh%boundaries%realBound(iBoundary)==-1) i=i+1
	
		   do iBFace=is,ie
	
			  patchFace = patchFace+1
	
			  iOwner = mesh%owner(iBFace)
	
			  if(mesh%boundaries%realBound(iBoundary)/=-1) then
				  field%phif(iBFace,:) = field%phi(numberOfElements+patchFace,:)
			  else
				  
				  pNeigh = pNeigh+1

				  aux = (1.0-mesh%gf(iBFace))/(field%ghosts(pNeigh,:,i)+small) + mesh%gf(iBFace)/(field%phi(iOwner,:)+small)
				  field%phif(iBFace,:) = 1.0/aux
			   
			  endif
	
		   enddo
	
		enddo
		
	end subroutine harmonicInterpolation	

!**********************************************************************************************************************

	subroutine interpolateElementToFaceStd(field, opt)

	!==================================================================================================================
	! Description:
	!! interpolateElementToFaceStd performs a standard linear interpolation to find the face values of a variable.
	!! 0: simply use gf calculated from the mesh, putting f' on the face
	!! else: put f' at the minimum distance from f
	!! fp is the face centred value, sorry for the wrong notation
	!==================================================================================================================

		type(flubioField) :: field
		!! field to interpolate
	!------------------------------------------------------------------------------------------------------------------

		integer :: iBoundary, iBFace,  iFace, iOwner, iNeighbour

		integer :: i, pNeigh, is, ie, opt, patchFace
	!------------------------------------------------------------------------------------------------------------------

		real :: corr
		!! skewness correction

		real :: gC(numberOfFaces)
		!! linear interpolation weight

		real :: magSf
		!! face area
	!------------------------------------------------------------------------------------------------------------------

		!---------!
		! Prepare !
		!---------!

		field%phif = 0.0

		if(opt==0) then
			gC = mesh%gf
		else
			if(flubioOptions%skwOpt==0) gC = 1.0-mesh%skw_gf
			if(flubioOptions%skwOpt==1) gC = 0.5
		endif

		call field%updateGhosts()

		!-------------------------------------------!
		! Interpolation with no skewness correction !
		!					    					!
		! ATTENTION HERE: The skewness correction   !
		! weights are defined differently from the  !
		! standard one. In the latter case they are !
		! (owner- face) while in the other they are !
		! neigh - face. This is why in the   	    !
		! interpolation there is (1-gf) on owner.   !
		!-------------------------------------------!

		do iFace=1,numberOfIntFaces

		   iOwner = mesh%owner(iFace)
		   iNeighbour = mesh%neighbour(iFace)

		   field%phif(iFace,:) = gC(iFace)*field%phi(iNeighbour,:) + (1.0-gC(iFace))*field%phi(iOwner,:)

		enddo

		!----------------!
		! Boundary Faces !
		!----------------!

		patchFace = 0
		i = 0
		do iBoundary=1,numberOfBoundaries

		   is = mesh%boundaries%startFace(iBoundary)
		   ie = is+mesh%boundaries%nFace(iBoundary)-1
		   pNeigh = 0

		   if(mesh%boundaries%realBound(iBoundary)==-1) i = i+1

		   do iBFace=is, ie

			  patchFace = patchFace+1
			  iOwner = mesh%owner(iBFace)

			  ! Real boundaries
			  if(mesh%boundaries%realBound(iBoundary)/=-1) then
				 field%phif(iBFace,:) = field%phi(numberOfElements+patchFace,:)

			  ! Processor Boundaries
			  else

				 pNeigh = pNeigh+1
				 field%phif(iBFace,:) = gC(iBFace)*field%ghosts(pNeigh,:,i) + (1.0-gC(iBFace))*field%phi(iOwner,:)

			  endif

		   enddo

		enddo

	end subroutine interpolateElementToFaceStd

! *********************************************************************************************************************

	subroutine interpolateElementToFace(phi_f, phi, phiGhost, nComp, opt)

	!==================================================================================================================
	! Description:
	!! interpolateElementToFace performs a standard linear interpolation to find the face values of a field
	!! This routine is the same as interpolateElementToFace but operates on fields instead of arrays
	!! 0: simply use gf calculated from the mesh, putting f' on the face
	!! else: put f' at the minimum distance from f
	!! fp is the face centred value, sorry for the wrong notation
	!==================================================================================================================

		integer :: iBoundary, iBFace,  iFace, Owner, Neighbour

		integer :: i, pNeigh, is, ie, opt, nComp, patchFace
	!------------------------------------------------------------------------------------------------------------------

		real :: phi(numberOfElements+numberOfBFaces, nComp), phi_f(numberOfFaces, nComp)
		!! field values
	!------------------------------------------------------------------------------------------------------------------

		real :: phiGhost(mesh%boundaries%maxProcFaces, nComp,numberOfProcBound1)
		!! ghost values
	!------------------------------------------------------------------------------------------------------------------

		real :: corr
		!! skewness correction

		real :: gC
		!! linear interpolation weight

		real :: magSf
		!! face area
	!------------------------------------------------------------------------------------------------------------------

		!---------!
		! Prepare !
		!---------!

		phi_f = 0.0

		!-------------------------------------------!
		! Interpolation with no skewness correction !
		!					    					!
		! ATTENTION HERE: The skewness correction   !
		! weights are defined differently from the  !
		! standard one. In the latter case they are !
		! (owner- face) while in the other they are !
		! neigh - face. This is why in the   	    !
		! interpolation there is (1-gf) on owner.   !
		!-------------------------------------------!

		do iFace=1,numberOfIntFaces

			Owner = mesh%owner(iFace)
			Neighbour = mesh%neighbour(iFace)

			if(opt==0) then
				gC=mesh%gf(iFace)
			else
				if(flubioOptions%skwOpt==0) gC = 1.0-mesh%skw_gf(iFace)
				if(flubioOptions%skwOpt==1) gC = 0.5d0
			endif

			phi_f(iFace,:) = gC*phi(Neighbour,:) + (1-gC)*phi(Owner,:)

		enddo

		!----------------!
		! Boundary Faces !
		!----------------!

		patchFace = 0
		i = 0

		do iBoundary=1,numberOfBoundaries

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1
			pNeigh = 0

			if(mesh%boundaries%realBound(iBoundary)==-1) i=i+1

			do iBFace=is, ie

				patchFace = patchFace+1
				Owner = mesh%owner(iBFace)

				if(mesh%boundaries%realBound(iBoundary)/=-1) then

					! Real boundaries
					phi_f(iBFace,:) = phi(numberOfElements+patchFace,:)

				else

					! Processor Boundaries
					pNeigh = pNeigh+1

					if(opt==0) then
						gC = mesh%gf(iBFace)
					else
						gC = 1.0-mesh%skw_gf(iBFace)
					endif

					phi_f(iBFace,:) = gC*phiGhost(pNeigh,:,i)+(1.0-gC)*phi(Owner,:)

				endif

			enddo

		enddo

	end subroutine interpolateElementToFace

! *********************************************************************************************************************

	subroutine recontructFromFaceValues(phi, vf)

	!==================================================================================================================
	! Description:
	!! recontructFromFaceValues reconstructs the field a cell center from face values (e.g. u from uf).
	!==================================================================================================================

		integer :: iBoundary, iFace, iBFace, iElement, iOwner, iNeighbour, is, ie, iProc
	!------------------------------------------------------------------------------------------------------------------

		real :: Sf(3)
		!! face area vector

		real :: nf(3)
		!! face normal

		real :: D(3,3)
		!! dyadic product

		real :: vf(numberOfFaces,1)
		!! face flux

		real :: phi(numberOfElements,3)
		!! reconstructed value

		real :: Sf_nf(numberOfElements, 3, 3)
		!! dyadic product between Sf and nf

		real :: nf_vf(numberOfElements, 3)
		!! Vector obtained the multiplication between the face flux and the face normal

		real :: invD(3,3)
		!! dyadic product inverse at a target cell

		real :: u(3)
		!! cell velocity
	!------------------------------------------------------------------------------------------------------------------

		Sf_nf = 0.0
		nf_vf = 0.0

		!-------------------------!
		! Assemble Interior Faces !
		!-------------------------!

		do iFace=1,numberOfIntFaces

			iOwner = mesh%owner(iFace)
			iNeighbour = mesh%neighbour(iFace)

			nf = mesh%Sf(iFace,:)/mesh%area(iFace)
			Sf = mesh%Sf(iFace,:)

			call dyadic_product(Sf, nf, D)

			Sf_nf(iOwner,:,:) = Sf_nf(iOwner,:,:) + D
			Sf_nf(iNeighbour,:,:) = Sf_nf(iNeighbour,:,:) + D

			nf_vf(iOwner,:) = nf_vf(iOwner,:) + vf(iFace,1)*nf
			nf_vf(iNeighbour,:) = nf_vf(iNeighbour,:) + vf(iFace,1)*nf

		enddo

		!----------------------!
		! Processor Boundaries !
		!----------------------!

		do iBoundary=1,numberOfProcBound

			iProc = mesh%boundaries%procBound(iBoundary)
			is = mesh%boundaries%startFace(iProc)
			ie = is+mesh%boundaries%nFace(iProc)-1

			do iBFace=is,ie

				iOwner = mesh%owner(iBFace)

				nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)
				Sf = mesh%Sf(iBFace,:)

				call dyadic_product(Sf, nf, D)
				Sf_nf(iOwner,:,:) = Sf_nf(iOwner,:,:) + D
				nf_vf(iOwner,:) = nf_vf(iOwner,:) + vf(iBFace,1)*nf

			enddo

		enddo

		!-----------------!
		! real Boundaries !
		!-----------------!

		do iBoundary=1,numberOfRealBound

			is = mesh%boundaries%startFace(iBoundary)
			ie = is+mesh%boundaries%nFace(iBoundary)-1

			do iBFace = is,ie

				iOwner = mesh%owner(iBFace)

				nf = mesh%Sf(iBFace,:)/mesh%area(iBFace)
				Sf = mesh%Sf(iBFace,:)

				call dyadic_product(Sf, nf, D)
				Sf_nf(iOwner,:,:) = Sf_nf(iOwner,:,:) + D
				nf_vf(iOwner,:) = nf_vf(iOwner,:) + vf(iBFace,1)*nf

			end do

		end do

		!-------------------------------!
		! Compute the cell center value !
		!-------------------------------!

		do iElement=1,numberOfElements
			invD = matInv3(Sf_nf(iElement,:,:))
			phi(iElement,:) = matMul(invD, nf_vf(iElement,:))
		end do

	end subroutine recontructFromFaceValues

!**********************************************************************************************************************

end module standardInterpolations 
