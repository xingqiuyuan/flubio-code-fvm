module immersedBoundary

    use generalDictionaries
    use fossil
    use vecfor, only: ex_R8P, ey_R8P, ez_R8P
    use qvector_m
    use meshvar

    implicit none 

    type, extends(generalDict) :: ibDictionary

        character(len=100), dimension(:), allocatable :: ibSurfaces 
        integer :: numberOfSurfaces
        logical :: found

        contains

            procedure :: set => setIBDict

    end type

    type stlFilesWrapper
        type(file_stl_object)    :: STLFile
        type(surface_stl_object) :: STLSurface
    end type stlFilesWrapper

    type :: flubioIBM

        character(len=100), dimension(:), allocatable :: ibNames

        integer :: numberOfBodies

        type(ibDictionary) :: ibDict

        type(stlFilesWrapper), dimension(:), allocatable :: ibSurfaces

        contains

            !procedure :: create
            !procedure :: destroy


            ! Immersed boundary surface handling

            !procedure :: loadIBSurfaces
            !procedure :: surfaceOwnership
            !procedure :: findIBcells
            !procedure :: findIBFaces
            !procedure :: IBnormals

            ! Geometry updates (translation and rotation)
            !procedure :: updateIBSurfaces
         

    end type flubioIBM

contains

    subroutine create(this)

    !===================================================================================================================
    ! Description:
    !! create is the class constructor.
    !==================================================================================================================

        class(flubioIBM) :: this
    !------------------------------------------------------------------------------------------------------------------

    end subroutine create
    
 !**********************************************************************************************************************

    subroutine destroy(this)
    !===================================================================================================================
    ! Description:
    !! destroy is the class distructor.
    !==================================================================================================================

        class(flubioIBM) :: this
    !------------------------------------------------------------------------------------------------------------------

    end subroutine destroy

 !**********************************************************************************************************************
    
    subroutine setIBDict(this)

    !===================================================================================================================
    ! Description:
    !! setIBDict parses and stores the regions from settings/IBM.
    !==================================================================================================================

        class(ibDictionary) :: this
    !------------------------------------------------------------------------------------------------------------------

        character(len=:), allocatable :: dictName
        !! Dictionary name

        character(len=:), allocatable :: optName
        !! Option name

        character(len=:), allocatable :: val
        !! Value

        character(len=:), allocatable :: path
        !! Path within the dict

        character(len=50), dimension(:), allocatable  :: splitPath
        !! Path splitted by target separator

    !------------------------------------------------------------------------------------------------------------------

        logical :: found, surfacesFound, surfaceFound
    !------------------------------------------------------------------------------------------------------------------

        integer :: i, error_cnt
    !------------------------------------------------------------------------------------------------------------------

        type(json_core) :: jcore

        type(json_value), pointer :: dictPointer, surfacesPointer, surfacePointer
    !------------------------------------------------------------------------------------------------------------------

        dictName = 'settings/IBM'

       ! Initialise the JSON dictionary
        inquire(file=trim(dictName), exist=found)

        if (.not. found) then
            this%found = .false.
            this%numberOfSurfaces = 0
        else
            this%found = .true.
            call this%initJSON(dictName)
            call this%json%info('Surfaces', n_children=this%numberOfSurfaces)
        end if

        ! Find out surface names
        call this%json%get(dictPointer)

        call jcore%initialize()
        call jcore%get(dictPointer, 'Surfaces', surfacesPointer, surfacesFound)

        ! Allocate the monitors array
        allocate(this%ibSurfaces(this%numberOfSurfaces))

        do i=1,this%numberOfSurfaces

            ! Get surfaces pointer
            call jcore%get_child(surfacesPointer, i, surfacePointer, surfaceFound)

            ! Get monitor name
            call jcore%get_path(surfacePointer, path, found)
            if(nameFound) then
                call split_in_array(path, splitPath, '.')
                this%ibSurfaces(i) = splitPath(2)
            else
                call flubioStopMsg('ERROR: cannot find the surface name, please check if the dictionary is correct.')
            end if

        enddo

    end subroutine setIBDict

end module immersedBoundary
