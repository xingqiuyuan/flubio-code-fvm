!*******************************************************************************************!
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

module serialElements

    use globalMeshVar
    use serialFaces
    use flubioContainters

    implicit none

    type, public, extends(grid_faces) :: grid_elements

        integer :: numberOfElements

        type(integerCellStruct), dimension(:), allocatable :: iFaces

        type(integerCellStruct), dimension(:), allocatable :: elmFaceSign

        type(integerCellStruct), dimension(:), allocatable :: iNeighbours

        integer, dimension(:), allocatable :: numberOfNeighbours

        ! Geometric varaibles
        real, dimension(:), allocatable :: volume

        real, dimension(:), allocatable :: oldVolume

        real, dimension(:,:), allocatable :: centroid

        !-------------!
        ! ghost cells !
        !-------------!

        real, dimension(:,:,:), allocatable :: ghost

        real, dimension(:,:,:), allocatable :: periodicFaces

        real, dimension(:,:,:), allocatable :: poffset

        integer, dimension(:,:), allocatable :: neigh

        contains
            procedure :: buildElements

    end type grid_elements

contains

    subroutine buildElements(this)

        class(grid_elements) :: this
    !------------------------------------------------------------------------------------------------------------------

        integer i, iElement, iFace, iBFace, nf, iOwner, iNeighbour, max_faces
    !------------------------------------------------------------------------------------------------------------------

        integer, dimension(:), allocatable :: cind, iFaces, nn, numberOfNeighs
    !------------------------------------------------------------------------------------------------------------------

        this%numberOfElements=numberOfElements

        allocate(this%iFaces(numberOfElements))
        allocate(this%elmFaceSign(numberOfElements))
        allocate(this%iNeighbours(numberOfElements))

        allocate(numberOfElementFaces(numberOfElements))
        allocate(cind(numberOfElements))
        allocate(nn(numberOfElements))
        allocate(numberOfNeighs(numberOfElements))
        allocate(this%numberOfNeighbours(numberOfElements))

        numberOfElementFaces = 0
        nn = 0
        numberOfNeighs = 0
        cind = 0

        ! Find out number of faces composing each elements, internal faces
        do iFace=1,numberOfIntFaces

            iOwner = this%Owner(iFace)
            nn(iOwner) = nn(iOwner)+1
            numberOfNeighs(iOwner) = numberOfNeighs(iOwner)+1

            iNeighbour = this%neighbour(iFace)
            nn(iNeighbour) = nn(iNeighbour)+1
            numberOfNeighs(iNeighbour) = numberOfNeighs(iNeighbour)+1

        end do

        ! Add boundary faces
        do iBFace=numberOfIntFaces+1,numberOfFaces
            iOwner = this%Owner(iBFace)
            nn(iOwner) = nn(iOwner)+1
        enddo

        ! Allocate structures
        do iElement=1,numberOfElements

            this%iFaces(iElement)%csize = nn(iElement)
            this%elmFaceSign(iElement)%csize = nn(iElement)
            this%iNeighbours(iElement)%csize = numberOfNeighs(iElement)

            call this%iFaces(iElement)%initIntColumn(nn(iElement), -1)
            call this%elmFaceSign(iElement)%initIntColumn(nn(iElement), 0)
            call this%iNeighbours(iElement)%initIntColumn(numberOfNeighs(iElement), 0)

        end do

        ! Assign internal faces to the owner element
        do iFace=1,numberOfIntFaces

            iOwner = this%Owner(iFace)
            iNeighbour = this%neighbour(iFace)

            cind(iOwner) = cind(iOwner)+1

            this%iFaces(iOwner)%col(cind(iOwner)) = iFace
            this%elmFaceSign(iOwner)%col(cind(iOwner)) = 1
            this%iNeighbours(iOwner)%col(cind(iOwner)) = iNeighbour

            cind(iNeighbour) = cind(iNeighbour)+1

            this%iFaces(iNeighbour)%col(cind(iNeighbour)) = iFace
            this%elmFaceSign(iNeighbour)%col(cind(iNeighbour)) = -1
            this%iNeighbours(iNeighbour)%col(cind(iNeighbour)) = iOwner

        enddo

        numberOfFaces=this%numberOfFaces

        ! Assign boundary faces (physical boundaries + processor boundaries) to the owner element
        do iBFace=numberOfIntFaces+1,numberOfFaces

            iOwner = this%Owner(iBFace)

            cind(iOwner) = cind(iOwner)+1

            this%iFaces(iOwner)%col(cind(iOwner)) = iBFace
            this%elmFaceSign(iOwner)%col(cind(iOwner)) = 1

        enddo

        this%numberOfNeighbours=0

        ! Count the number of neighbours of the elements. Processor boundaries are taken into account later
        do iElement=1,numberOfElements
            do i=1, this%iNeighbours(iElement)%csize
                if (this%iNeighbours(iElement)%col(i)/=0) then
                    this%numberOfNeighbours(iElement) = this%numberOfNeighbours(iElement)+1
                endif
            enddo
        enddo

        numberOfBElements = numberOfFaces - numberOfIntFaces
        numberOfBFaces = numberOfFaces - numberOfIntFaces

    ! Ending message
        write(*,*)
        write(*,*) 'FLUBIO: mesh elements built!'
        write(*,*)

    end subroutine buildElements

! *********************************************************************************************************************

    subroutine getNumberOfFaces(faces,fsize,ie)

        integer iFace, ie, fsize, faces(ie), counter
    !------------------------------------------------------------------------------------------------------------------

        fsize=0

        do iFace=1,ie
            if(faces(iFace)/=0) then
                fsize=fsize+1
            endif
        enddo

    end subroutine getNumberOfFaces

! *********************************************************************************************************************

end module serialElements