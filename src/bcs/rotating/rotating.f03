!*******************************************************************************************! 
!  ______ _     _    _ ____ _____ ____         _____  ______ _______ _____  _____           !
! |  ____| |   | |  | |  _ \_   _/ __ \       |  __ \|  ____|__   __/ ____|/ ____|          !
! | |__  | |   | |  | | |_) || || |  | |______| |__) | |__     | | | (___ | |               !
! |  __| | |   | |  | |  _ < | || |  | |______|  ___/|  __|    | |  \___ \| |               !
! | |    | |___| |__| | |_) || || |__| |      | |    | |____   | |  ____) | |____           !
! |_|    |______\____/|____/_____\____/       |_|    |______|  |_| |_____/ \_____|          !
!*******************************************************************************************!

!*******************************************************************************************!
!                                                                                           !
! License:                                                                                  !
! Creative commons CC BY-NC 2.0                                                             !
!                                                                                           !
! How to cite:                                                                              !
! E. Alinovi, J. Guerrero, "FLUBIO – An unstructured, parallel, finite-volume               !
! based Navier-Stokes and convection-diffusion like equations solver for teaching           !
! and research purposes," SoftwareX, Volume 13, January 2021, 100655.                       !
! https://doi.org/10.1016/j.softx.2020.100655                                               !
!                                                                                           !
! Code repository:                                                                          !
! https://gitlab.com/alie89/flubio-code-fvm                                                 !
!                                                                                           !
!*******************************************************************************************!

	subroutine rotatingBoundary(iBFace, iBoundary, aC, bC, mu, bcDict, c)

	!==================================================================================================================
	! Description:
	!! dirichletBoundary computes the diagonal coefficient and rhs correction when a dirichlet (fixed value)
	!!	boundary condition is applied. The correction reads:
	!! \begin{eqnarray}
	!!    &a_C^b = \mu_b\frac{S_b}{d_{Cb}} \\
	!!    &b_C^b = -m_f\phi_b + \mu_b\frac{S_b}{d_{Cb}}\phi_b
	!! \end{eqnarray}
	!==================================================================================================================

		use meshvar
		use massFlowVar
		use math, only: cross_product
		use json_module

		implicit none

		type(json_file) :: bcDict
        !! boundary condtion dictionary in json format
	!------------------------------------------------------------------------------------------------------------------

		integer :: iBFace
		!! target boundary face

		integer :: iBoundary
		!! target boundary
	!------------------------------------------------------------------------------------------------------------------

		real :: aC(3)
		!! diagonal coefficent correction

		real :: bC(3)
		!! rhs correction

		real :: mu
		!! diffusion coefficient

		real :: phib(3)
		!! boundary velocity

		real :: r(3)
		!! distance between center of rotation and boudary face center

		real, dimension(:),allocatable :: axis
		!! axis of rotation

		real, dimension(:),allocatable :: center_of_rotation
		!! center of rotation
		
		real :: omega
		!! angular velocity

		real :: c 
		!! switch to turn off the mass flow at boundary
	!------------------------------------------------------------------------------------------------------------------

		call bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%axis', axis)
		call bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%center_of_rotation', center_of_rotation)
		call bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%omega', omega)

		r =  mesh%fcentroid(iBFace,:) - center_of_rotation
		phib = cross_product(omega*axis, r)

		aC = mu*mesh%gDiff(iBFace)
		bC = -mf(iBFace,1)*phib + mu*mesh%gDiff(iBFace)*phib
		
	end subroutine rotatingBoundary

! *********************************************************************************************************************

	subroutine updateRotatingBoundaryField(phi, iBoundary, bcDict)

	!==================================================================================================================
	! Description:
	!! updateRotatingBoundaryField loops over all the boundary faces of a target patch 
	!! and updates the field face values with the rotating one:
	!! \begin{equation*}
	!!    \phi_b = \omega \cross r \\
	!! \end{equation*}
	!==================================================================================================================

		use meshvar
		use json_module

		implicit none

		type(json_file) :: bcDict
        !! boundary condtion dictionary in json format
	!------------------------------------------------------------------------------------------------------------------

		integer :: iBoundary, iBFace, is, ie, iOwner, patchFace
	!------------------------------------------------------------------------------------------------------------------
	
		real :: phi(numberOfElements+numberOfBFaces, 3)
		!! field values

		real :: phi0(3)
		!! value to be assigned at the boundary

		real, dimension(:),allocatable :: axis
		!! axis of rotation

		real, dimension(:),allocatable :: center_of_rotation
		!! center of rotation

		real :: r(3)
		!! distance between center of rotation and boudary face center
		
		real :: omega
		!! angular velocity
	!------------------------------------------------------------------------------------------------------------------

		call bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%axis', axis)
		call bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%center_of_rotation', center_of_rotation)
		call bcDict%get(trim(mesh%boundaries%userName(iBoundary))//'%omega', omega)

		is = mesh%boundaries%startFace(iBoundary)
		ie = is+mesh%boundaries%nFace(iBoundary)-1

		patchFace = numberOfElements + (is-numberOfIntFaces-1)

		do iBFace=is,ie
			patchFace = patchFace+1
			r = mesh%fcentroid(iBFace,:) - center_of_rotation
			phi0 = cross_product(omega*axis, r)
			phi(patchFace,:) = phi0
		enddo

	end subroutine updateRotatingBoundaryField

! *********************************************************************************************************************
